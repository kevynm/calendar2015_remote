package com.outblazeventures.calendar2012;
//------------Warning!!------------------
// It is not used now!!!
// Tapjoy, chartboost and Mopub are implemented in the main Activity



import com.chartboost.sdk.ChartBoost;
import com.flurry.android.FlurryAgent;
import com.mopub.mobileads.MoPubInterstitial.MoPubInterstitialListener;
import com.mopub.mobileads.MoPubView;
import com.mopub.mobileads.MoPubView.OnAdLoadedListener;
import com.outblazeventures.thirdparty.mopub.MopubHandler;
import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyFeaturedAppNotifier;
import com.tapjoy.TapjoyFeaturedAppObject;
import com.tapjoy.TapjoyLog;
import com.tapjoy.TapjoyNotifier;
import com.tapjoy.TapjoySpendPointsNotifier;
import com.tapjoy.TapjoyVideoNotifier;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class Mopub implements MopubHandler, MoPubInterstitialListener, TapjoyNotifier, TapjoyFeaturedAppNotifier, TapjoySpendPointsNotifier, TapjoyVideoNotifier
{
	Context mContext;
	
	boolean adLoaded = false;
	
	protected MoPubView adView;
//	protected MoPubInterstitial interstitial;
	
//	protected ChartBoost _cb;

	MyHandler handler = new MyHandler();
	class MyHandler extends Handler{
		int status = View.INVISIBLE;
		
		@Override
		public void handleMessage(Message msg){
//			Log.d("Debug","Debug: handlerMessage status:" + status);
			adView.setVisibility(status);
		}
		
		public void changeStatus(boolean show){
			if(show)
				status = View.VISIBLE;
			else
				status = View.GONE;
			
			sendEmptyMessage(status);
		}
	};
	
	
	
	public Mopub(Context context){
		this.mContext = context;
		loadAd();	
	}
	
	private void loadAd(){

        adView = new MoPubView(mContext);
        adView.setLayoutParams(new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        adView.setAdUnitId("agltb3B1Yi1pbmNyDQsSBFNpdGUYqP7REgw");
        adView.loadAd();
        
        adView.setOnAdLoadedListener(new OnAdLoadedListener() {
            public void OnAdLoaded(MoPubView mpv) {
            	System.out.println("finish to load the mopub");
            	adLoaded = true;
            }
        });
        
//        tapjoy
        TapjoyConnect.requestTapjoyConnect(mContext,
                "0b357ed9-0160-4c1d-b18d-461a5e779a21", "bW2qwJKYmc7LCpdFk5sC");
        
//		flurry
        FlurryAgent.onStartSession(mContext, "169VSKJXLX8FRSJAR3F8");
        
		
        // Load Interstitial
//        interstitial = new MoPubInterstitial(activity, "agltb3B1Yi1pbmNyDQsSBFNpdGUYyc-GDww");
//        interstitial.setListener(this);
//        interstitial.showAd();
        
        
//        Tapjoy offerwall
//		TapjoyLog.enableLogging(true);
		//TapjoyConnect.requestTapjoyConnect(mContext, "8caadbbc-4187-4803-a814-c191bed54d76", "5JjA5qURErVuikt3Qnwu");
		TapjoyConnect.getTapjoyConnectInstance().initVideoAd(this);
	}
	
	public void add(RelativeLayout layout){
        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
        		RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        layout.addView(adView, adParams);
        this.showAds(false);
	}

	@Override
	public void showAds(boolean show) {
		// TODO Auto-generated method stub
//		Load banner
		Log.e("Debug","Debug: show Ads function show: " + show);
		handler.changeStatus(show);
	}

	@Override
	public void showAdsByTime(final int timer) {
		// TODO Auto-generated method stub
		Thread t = new Thread(){
			@Override
			public void run(){
				while(adLoaded){
					try {
						handler.changeStatus(true);
						Thread.sleep(timer);
						handler.changeStatus(false);
						break;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		t.start();
	}
	
	@Override
	public void showChartBoost(){
		Log.e("debug","show chart boost!");
//      ChartBoost
//		ChartBoost _cb = ChartBoost.getSharedChartBoost();
//		_cb.setContext(activity);
//		_cb.setAppId("4ee03d0fcb60152950000030");
//		_cb.setAppSignature("961c16a1e5b6ee0c2a6182fb515c23bb37075e9e");
//		_cb.install();
//		_cb.loadInterstitial();
	}
	
	@Override
	public void flurryEvent(String args){
    	FlurryAgent.logEvent(args);
	}
	
	@Override
	public void tapjoyEvent(String args){
        TapjoyConnect.getTapjoyConnectInstance().actionComplete(args);
	}
	
	public void OnInterstitialLoaded() {
		// TODO Auto-generated method stub
		
	}

	public void OnInterstitialFailed() {
		// TODO Auto-generated method stub
		Log.d("MopubNativeTestActivity", "failed to receive OnInterstitialFailed");
	}
	
	public void destroy(){
    	adView.destroy();
        FlurryAgent.onEndSession(mContext);
//    	interstitial.destroy();
	}

	@Override
	public void showTayjoyOfferwall() {
		// TODO Auto-generated method stub
		Log.i("mopub","showOffWall");
		TapjoyConnect.getTapjoyConnectInstance().showOffers();
	}
	
	public void refreshPoint(){
		TapjoyConnect.getTapjoyConnectInstance().getTapPoints(this);
	}

	@Override
	public void getFeaturedAppResponse(TapjoyFeaturedAppObject featuredApObject)
	{
		TapjoyConnect.getTapjoyConnectInstance().showFeaturedAppFullScreenAd();
	}

	@Override
	public void getFeaturedAppResponseFailed(String error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getUpdatePoints(String currencyName, int pointTotal) {
		// TODO Auto-generated method stub0
		Log.i("mopub","onre on update point: " + pointTotal);
		//GameManager.addCurrentPoint(pointTotal);
		TapjoyConnect.getTapjoyConnectInstance().spendTapPoints(pointTotal, this);
	}

	@Override
	public void getUpdatePointsFailed(String error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getSpendPointsResponse(String currencyName, int pointTotal) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getSpendPointsResponseFailed(String error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void videoReady() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void videoError(int statusCode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void videoComplete() {
		// TODO Auto-generated method stub
		
	}
	
}