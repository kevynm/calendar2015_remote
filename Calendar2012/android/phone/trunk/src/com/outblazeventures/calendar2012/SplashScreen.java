package com.outblazeventures.calendar2012;

import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyLog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class SplashScreen extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Call Full Screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Delete Title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
     // Enables logging to the console.
      		TapjoyLog.enableLogging(true);
      		
      		// Connect with the Tapjoy server.  Call this when the application first starts.
      		// REPLACE THE APP ID WITH YOUR TAPJOY APP ID.
      		// REPLACE THE SECRET KEY WITH YOUR SECRET KEY.
      		TapjoyConnect.requestTapjoyConnect(getApplicationContext(), thirdpartyconstant.TAPJOY_ID, thirdpartyconstant.TAPJOY_SECRET);
        gotoGame();
    }
    
	private void gotoGame() {
		new Thread(new Runnable() {
			public void run() {
				android.os.SystemClock.sleep(3000);
				startActivity(new Intent(SplashScreen.this, com.outblazeventures.calendar2012.CalendarActivity.class));
				close();
			}
		}).start();
	}

	private void close(){this.finish();}
}