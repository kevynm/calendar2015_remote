package com.outblazeventures.calendar2012;
//Alan Chu@OB    2012-1-17

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.widget.LinearLayout;

public class canvasview extends View implements OnGestureListener{
    
    private static final float MINP = 0.25f;
    private static final float MAXP = 0.75f;
    
    private GestureDetector gestureDetector;
    private Bitmap  mBitmap, blank;
    private Canvas  mCanvas;
    private Path    mPath;
    private Paint   mBitmapPaint;
    private Paint   mPaint, textPaint;
    private Bitmap  icon, icon2, circle;
    private int choice = 0; 
    private boolean drawpath = true, showtext = false, confirm = false, fling = true;
    private int iconh, iconw;
    private String temptext;
    private int texttop = 0, textleft = 0;
    private Context mcontext;
    private int screenwidth, screenheight;
    
    
    public canvasview(Context c) {
        super(c);
        mcontext =c;
    }
    
    public canvasview(Context context, AttributeSet attrs) {
        super(context, attrs);
        mcontext = context;
    }
    
    public canvasview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mcontext = context;
    }
    
    public void createblankbitmap(int screenwidth, int screenheight, Paint mPaint){
    	blank = Bitmap.createBitmap(screenwidth, screenheight, Bitmap.Config.ARGB_8888);
    }


    public void init(int screenwidth, int screenheight, Paint mPaint){

    	this.screenwidth = screenwidth;
    	this.screenheight = screenheight;
    	
    	//gestureDetector = new GestureDetector(this);

        this.mPaint = mPaint;
       
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //textPaint.setAntiAlias(true);
       // textPaint.setDither(true);
        textPaint.setColor(Color.RED);
        if (screenheight > 600){
        	 textPaint.setTextSize(30);
        } else if (screenheight < 400){
        	textPaint.setTextSize(15);
        }
        else {
        	textPaint.setTextSize(20);
        }
       

        mBitmap = Bitmap.createBitmap(screenwidth, screenheight, Bitmap.Config.ARGB_8888);;
        mCanvas = new Canvas(mBitmap);
        
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        circle = BitmapFactory.decodeResource(getResources(), R.drawable.date_highlighted);
        //icon = BitmapFactory.decodeResource(getResources(), R.drawable.icon);
        
    	
    }
    
    

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        //canvas.drawColor(Color.TRANSPARENT);
        canvas.drawColor(Color.TRANSPARENT); 
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
      

        if (showtext == true){
	        //Log.e("ShowText", "true");
	        //canvas.drawText(temptext, mX, mY, textPaint);
        }
        //mCanvas.drawText(temptext, 70, 70, textPaint);
        if (confirm == true){
        	
        	showtext = false;
        	confirm = false;
        }
        
        if (choice == 1){ 
        	mCanvas.drawPath(mPath, mPaint);}
      
        else {
        	canvas.drawPath(mPath, mPaint);
        }
    }
    
    private float mX = 0, mY = 0;
    private static final float TOUCH_TOLERANCE = 4;
    
    private void touch_start(float x, float y) {

    	if (drawpath){
	        mPath.reset();
	        mPath.moveTo(x, y);
	        mX = x;
	        mY = y;
    	}
    	if (icon2!= null && choice != 1 && choice!= 2 && choice !=3){
    	mCanvas.drawBitmap(icon2, x - (iconw/2), y - (iconh/2), mBitmapPaint);
    	}
    }
    private void touch_move(float x, float y) {
    	if (drawpath){
	        float dx = Math.abs(x - mX);
	        float dy = Math.abs(y - mY);
	        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
	            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
	            mX = x;
	            mY = y;
	        }
    	}
    }
    private void touch_up() {
    	if (drawpath){
	        mPath.lineTo(mX, mY);
	        // commit the path to our offscreen
	        mCanvas.drawPath(mPath, mPaint);
	        // kill this so we don't double draw
	        mPath.reset();
    	}
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	
    	if (fling == true){
    		return false;
    		//return gestureDetector.onTouchEvent(event);
    		//return CalendarActivity.getHoriView().onTouchEvent(event);
    	} else{
        float x = event.getX();
        float y = event.getY();
        
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                break;
        }
        return true;
    	}
    }
    
    
    
    
    public void pencil(Paint mPaint, int choice){
    	this.mPaint = mPaint;
    	this.choice = choice;
    }
    
    public void eraser(Paint mPaint, int choice){
    	this.mPaint = mPaint;
    	this.choice = choice;
 	
    }
    
    public void setChoice(Paint mPaint, int choice){
    	this.mPaint = mPaint;
    	this.choice = choice;
 	
    }
    
    public void setChoice(int choice){
    	this.choice = choice;
    }
    

    
    public Bitmap getBitmap(){
    	return this.mBitmap;
    }
    
    public void setBitmap(Bitmap mBitmap){
    	this.icon = mBitmap; 	
    	mCanvas.drawBitmap(icon, 0, 0, mBitmapPaint);
    }
    
    public void setIcon(int id, Paint mPaint){
    	this.mPaint = mPaint;
    	choice = 0;
    	icon2 = BitmapFactory.decodeResource(getResources(), id);
    	iconh = icon2.getHeight();
    	iconw = icon2.getWidth();
    }
    
    public void setFling(boolean canfling){
    	this.fling  = canfling;
    }
    
    
    public void setText(String text){
    	this.temptext = text;
    	showtext = true;
    	
    }
    
    public void confirm(int left, int top){
    	this.texttop = top;
    	this.textleft = left;
    	mCanvas.drawText(temptext, textleft, texttop, textPaint);
    	confirm = true;
    }
    
    public void clear(int screenwidth, int  screenheight){
    	
    	//mBitmap = Bitmap.createBitmap(screenwidth, screenheight, Bitmap.Config.ARGB_8888);
        //mCanvas = new Canvas(mBitmap);
        mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        //mCanvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        //mCanvas.drawText("testing", 100, 100, textPaint);
        
    }

	public void drawdatecircle(int circleleft, int circletop) {
		// TODO Auto-generated method stub
		mCanvas.drawBitmap(circle, circleleft, circletop, mBitmapPaint);
		mCanvas.drawText("testing", 100, 100, textPaint);
	}

	@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		 Log.d("canvas", "...onFling...");
		return false;
	}

	@Override
	public void onLongPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
    public void SaveImage(int current, canvasview canvas){
    	String[] FILENAME = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November" ,"December"};
        Bitmap tempBitmap = canvas.getBitmap();
        try {
    	FileOutputStream fos = mcontext.openFileOutput(FILENAME[current], Context.MODE_PRIVATE);
    	ByteArrayOutputStream stream = new ByteArrayOutputStream();
    	tempBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
    	byte[] byteArray = stream.toByteArray();
    	
    	
			fos.write(byteArray);
			
			Log.e("Calendar", "size:" + byteArray.length);
    	fos.close();
    	Log.e("Calendar", "Save!!!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("Calendar", "Save failed!!!");
		}
    }

    public canvasview LoadImage(int current, canvasview canvas){
    	String[] FILENAME = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November" ,"December"};
    	
    	
    	try{

    	FileInputStream fis = mcontext.openFileInput(FILENAME[current]);
    	byte[] input = new byte[fis.available()];
    	fis.read(input, 0, input.length);
    	Bitmap bitmap = BitmapFactory.decodeByteArray(input, 0, input.length);
    	
    	Log.e("Calendar", Integer.toString(bitmap.getHeight()));
    	Log.e("Calendar", Integer.toString(bitmap.getWidth()));
    	Log.e("Calendar", Integer.toString(input.length));
    	//Bitmap bitmap = BitmapFactory.decodeByteArray(inputbuffer, 0, inputbuffer.length);
    	//canview.init(screenwidth, screenheight, mPaint);
    	fis.close();
		canvas.init(screenwidth, screenheight, mPaint);
    	canvas.setBitmap(bitmap);
    	
    	

    	}catch (FileNotFoundException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace(); 
    		Log.e("Calendar", "Load Not found!!!");
    		canvas.init(screenwidth, screenheight, mPaint);
    	}
    	
    	catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("Calendar", "Load failed!!!");
			canvas.init(screenwidth, screenheight, mPaint);
		}
		return canvas;
    } 
    
    public void garbagecollect(){
    	if (this.mBitmap != null){
    		this.mBitmap.recycle();
    	}
    	if (this.icon != null){
    		this.icon.recycle();
    	}
    	
 
    }
    
}