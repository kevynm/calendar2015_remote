package com.outblazeventures.calendar2012;
//Alan Chu@OB    2012-1-17


import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import com.chartboost.sdk.ChartBoost;

import com.flurry.android.FlurryAgent;
import com.mopub.mobileads.MoPubView;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.widget.FrameLayout.LayoutParams;

public class CalendarActivity extends Activity implements OnGestureListener , OnDoubleTapListener, OnTouchListener{   


	
		
	 public static int edit_choice = 0, eraser =1, pencil =2, text =3;

	 
	 private RelativeLayout rlayout;
     private static ViewFlipper mViewFlipper;   
	 //private GestureDetector gestureDetector;

	 private TextView tv2;
	 private static ImageView imgtop1;
	 private static ImageView imgbottom1;
	 private ImageButton month_select;
	 private ImageButton info;
	 private static HorizontalScrollView horiView;

	 private ImageButton tv, tv3;
	 private ImageButton drawbtn;
	 private ImageButton erasebtn, musicbtn, clearbtn;
	 public static int currentframe;
	 public static int opencount;

	 private LinearLayout layout;
	 private LinearLayout lLayout1;
	 private LinearLayout editlayout, controllayout;
	 private canvasview canview, canview2;
	 private static int screenwidth;

	 private ImageView[] img_array = new ImageView[24];
	 private int screenheight;
	 private int textwidth = 50 , textheight= 50;
	 private int choice;
	 private int mYear;
	 private AlertDialog.Builder alert;
	 private RelativeLayout.LayoutParams params, params1, params2, params3, rparams;
	 private static boolean mpplaying = false;
	 private MediaPlayer mPlayer; 
	 private ImageButton[] image_icon = new ImageButton[21];
	 private ImageButton[] img_btn = new ImageButton[12];
	// private canvasview[] canvasv  = new canvasview[12];
	 final float tolerance  = 40;
	 private ImageView datecircle; 
	 private boolean exceed = false, editmode= false;
	 private int month, DoM, DoW,WoM;
	 private RelativeLayout[] rlayouts = new RelativeLayout[12];
	 private boolean[] loaded = new boolean[12];
     private Calendar c;
     private Handler mHandler = new Handler();
	 
	 
	 private Paint mPaint;	 
	 View.OnTouchListener gestureListener;   
    private final String tag = "gesture";
    
 // Declare an instance variable for your MoPubView.
    private MoPubView mAdView;
    
    
    
    
    public void CalendarActivity(){
    }
    
    @Override  
    public void onCreate(Bundle savedInstanceState) {   
        super.onCreate(savedInstanceState);  
        //Call Full Screen
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				//WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Delete Title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.main);   
       
		
		 
        //gestureDetector = new GestureDetector(this);
      //GestureDetector.OnDoubleTapListener gestureListener = new OnDoubleTapListener(this);
        
        //set current frame
        currentframe = 0;
        //opencount++;
        Log.i("open", Integer.toString(opencount));
       // if (opencount==4){
        	opencount = 0;
       // }
        

        	
   
	   

        
        //get screen size
        Display display = getWindowManager().getDefaultDisplay(); 
        screenwidth = display.getWidth();
        screenheight = display.getHeight();

           
       // Button buttonNext1 = (Button) findViewById(R.id.Button_next1);   
        
        
        
        rlayout = (RelativeLayout) findViewById(R.id.relativeLayout13); 
        month_select = (ImageButton) findViewById(R.id.monthButton);
        info = (ImageButton) findViewById(R.id.infoButton);      
        horiView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView1);
        horiView.setOnTouchListener(this);
        lLayout1 = (LinearLayout) findViewById(R.id.linearLayout1);
        
        
        int childnum = lLayout1.getChildCount();

        for (int i=0; i< childnum; i++){
        	img_array[i] = (ImageView) findViewById(imgviews[i]);
        	img_array[i].setMinimumWidth(screenwidth);
        	img_array[i].setMaxWidth(screenwidth);
        }
        
        for (int i=12; i< childnum+12; i++){
        	img_array[i] = (ImageView) findViewById(imgviews[i]);
        	img_array[i].setMinimumWidth(screenwidth);
        	img_array[i].setMaxWidth(screenwidth);
        	img_array[i].setScaleType(ScaleType.CENTER_CROP);
        }
        
		//mScroller = new Scroller(getApplicationContext());
		Log.i("Activity start", "start");

		//final ViewConfiguration configuration = ViewConfiguration.get(getApplicationContext());
		//mTouchSlop = configuration.getScaledTouchSlop();
		//mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
		
		horiView.setOnTouchListener(this);
        
        
        
        
        
       // imgbottom1.setDrawingCacheEnabled(true);

		layout = (LinearLayout) findViewById(R.id.selectLayout1);
		editlayout = (LinearLayout) findViewById(R.id.selectLayout2);
		controllayout = (LinearLayout) findViewById(R.id.selectLayout3);
		canview = (canvasview) findViewById(R.id.canvasview12);
		canview2 = new canvasview(this);
		for (int i=0; i<12; i++){
			//canvas[i] = (canvasview) findViewById(canviews[i]);
			rlayouts[i] = (RelativeLayout) findViewById(relayout[i]);
			loaded[i] = false;
		}
		loaded[0] = true;
		
		
		rparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);
		
		//initialize the paint
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.TRANSPARENT);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(4);
        
        mPlayer = MediaPlayer.create(CalendarActivity.this, R.raw.calendar_bgm_03);
		
		
		

        

        
        //canview  = LoadImage(currentframe, canview);
        //canview.setVisibility(View.VISIBLE);
        controllayout.setVisibility(View.INVISIBLE);
        
        //set up inner purchase
        //Mopub
        
        info.setOnClickListener(new OnClickListener(){
        	public void onClick(View v) {
        		
        		
        		Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("https://market.android.com/search?q=outblaze"));
				startActivity(i);

        	}
        });
        
        
        month_select.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
             if (layout.getVisibility() == View.VISIBLE){
            	 //Enter Edit Mode
            	layout.setVisibility(View.GONE);
            	layout.setFocusable(false);
              	 layout.setClickable(false);
            	 layout.setEnabled(false);
            	editlayout.setFocusable(true);
           	   editlayout.setClickable(true);
            	 editlayout.setEnabled(true);
            	 canview.setFocusable(true);
            	 canview.setFocusableInTouchMode(true);
            	 editmode = true;
            	editlayout.setVisibility(View.VISIBLE);
            	controllayout.setVisibility(View.VISIBLE);
            	canview.setFling(false);
            	LoadImage(currentframe, canview);
            	canview2.setVisibility(View.INVISIBLE);
            	canview.setVisibility(View.VISIBLE);
            	//canview.setVisibility(View.VISIBLE);
            	
            	month_select.setBackgroundResource(R.drawable.month_mode);}
             else if (layout.getVisibility() == View.GONE){
            	 //Leave Edit Mode
            	 layout.setVisibility(View.VISIBLE);
            	 layout.setFocusable(true);
            	 layout.setClickable(true);
            	 layout.setEnabled(true);
            	 editlayout.setFocusable(false);
            	 editlayout.setClickable(false);
            	 editlayout.setEnabled(false);
            	 editlayout.setVisibility(View.GONE);
            	 controllayout.setVisibility(View.INVISIBLE);
            	 try{
	            		rlayout.removeView(tv);
						rlayout.removeView(tv2);
						rlayout.removeView(tv3);
	            	}catch(Exception e){
	            		
	            	}
            	 
            	 editmode = false;
            	 //canview.setVisibility(View.GONE);
            	 canview.setFocusable(false);
            	 canview.setFocusableInTouchMode(false);
            	 
             	
             	canview.setVisibility(View.GONE);
 
            	 SaveImage(currentframe, canview);
            	 LoadImage(currentframe, canview2);
            	 canview2.setVisibility(View.VISIBLE);
            	 
            	 canview.setFling(true);
            	 month_select.setBackgroundResource(R.drawable.edit_mode);
             
             }
             
            }
        });
        
        
     // The month choose layout
        choosemonthlayout();
        
	 // The edit layout
	    
	    seteditlayout();	   
	    
	    

	    
	   /* List<ImageButton> list = new ArrayList<ImageButton>();*/

	   
        
        
        clearbtn = new ImageButton(this);
        clearbtn.setBackgroundResource(R.drawable.arrow_left);
        clearbtn.layout(5, 0, 5, 0);
        clearbtn.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		canview.clear(screenwidth, screenheight);
        	}
        });
        
       // editlayout.addView(clearbtn, p );

        c = Calendar.getInstance();

        mYear = c.get(Calendar.YEAR);
		if (mYear == 2012){
		circledate();
	} else {
		
	}



    }
    

    
    private void circledate() {
		// TODO Auto-generated method stub

    	//int fd = c.getFirstDayOfWeek();
        DoW = c.get(Calendar.DAY_OF_WEEK);
        WoM = c.get(Calendar.WEEK_OF_MONTH);
        month = c.get(Calendar.MONTH);
        DoM = c.get(Calendar.DAY_OF_MONTH);

        datecircle = new ImageView(this);
        datecircle.setImageResource(R.drawable.date_highlighted); 
        
     // Get the Week of Month
        c.set(Calendar.DAY_OF_MONTH, 1);
        int offset = c.get(Calendar.DAY_OF_WEEK) - 2;
        WoM = (DoM + offset)/7;
        if (WoM == 5){
        	WoM = 0;
        }
        

        c.set(Calendar.DAY_OF_MONTH, DoM);
        
        
        //imgtop1.setImageResource(dogs[month]);
        //imgbottom1.setImageResource(months[month]); 
        currentframe = month;

	     
        
        //canvas[currentframe].setVisibility(View.VISIBLE);
        LoadImage(currentframe, canview2);
  
        
        int circletop;
        // int abc = c.getFirstDayOfWeek();
        //imgbottom1.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        //float ratio = (float)(imgbottom1.getMeasuredHeight()/imgbottom1.getMeasuredWidth());
        int bottomw = screenwidth;
        int bottomh = screenheight;
        
        //int testw = imgtop1.getLayoutParams().width;

        datecircle.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int circleh = datecircle.getMeasuredHeight();
        int circlew = datecircle.getMeasuredWidth();
        float screenratio = screenheight/screenwidth;
        int circleleft, offset2;
        
        if (screenratio <= 1.5){
        	offset2 = (int)(screenwidth - ((screenheight - 10)/ 1.5));
        	circleleft = offset2/2 + (bottomw-offset2)/7*DoW - circlew;
        }else {
        	circleleft = bottomw/7*DoW - circlew;
        }
        //screen height 800
        /*if (bottomh == 800){
        	circletop = bottomh/2 + 58*WoM - circleh + 27;
        }else if (bottomh > 1000){
        	circletop = bottomh/2 + bottomh*59/800*WoM - circleh+33;
        }else if (bottomh < 400){
        	circletop = bottomh/2 + bottomh*60/800*WoM - circleh+12;        	
        }else {
        	circletop = bottomh/2 + bottomh*59/800*WoM - circleh+21;
        }*/
        if (screenratio <= 1.5){
        	circletop = (int)bottomh/2 + bottomh/13*WoM - circleh+10;
        }else {
        	offset2 = (int)(screenheight - (screenwidth* 1.5));
        	circletop = offset2/2 + bottomh/2 +(bottomh - offset2)/13*DoW - circleh + 10;
        }
        
        //Log.e("First Day of week", Integer.toString(fd));
        Log.i("ScrollTo", Integer.toString(currentframe * screenwidth));
        Log.e("Year", Integer.toString(mYear));
        Log.e("Day of week", Integer.toString(DoW));
        Log.e("Week of Month", Integer.toString(WoM));
        
        Log.e("height", Integer.toString(bottomh));
        Log.e("width", Integer.toString(bottomw));
        Log.e("left", Integer.toString(circleleft));
        Log.e("top", Integer.toString(circletop));

        
        RelativeLayout.LayoutParams paramscircle = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        paramscircle.setMargins(circleleft, circletop, 0, 0);
        

        //datecircle.setTag(0);
		//tv.setMinimumHeight(50);
		//tv.setMinimumWidth(100);
        rlayouts[month].addView(canview2, rparams);
        rlayouts[month].addView(datecircle, paramscircle);
        
        
    	 if(loaded[currentframe] == false){
    		img_array[currentframe+12].setImageResource(dogs[currentframe]);
    	 }

	if (currentframe != 0){
		if (loaded[currentframe-1] == false){
          img_array[currentframe+11].setImageResource(dogs[currentframe-1]);
          loaded[currentframe-1] = true;
		}
	}
	if (currentframe != 11){
		if(loaded[currentframe+1] == false){
          img_array[currentframe+13].setImageResource(dogs[currentframe+1]);
         loaded[currentframe+1] = true;
		}
	}
        
        horiView.post(new Runnable() {
            @Override
            public void run() {
            	horiView.scrollTo(month * screenwidth, 0);
            } 
        });
        


        
        
	}
    
	@Override
    protected void onStart (){
    	super.onStart();
    
    	//Flurry
        FlurryAgent.onStartSession(this, thirdpartyconstant.FLURRY_ID);
        
    	//Mopub
    	setupmopub();
    	
    	loadChartBoost();
        // your code
 	// Show the Tapjoy banner ad.
 		//TapjoyConnect.getTapjoyConnectInstance().setBannerAdSize(TapjoyDisplayAdSize.TJC_AD_BANNERSIZE_640X100);
 		//TapjoyConnect.getTapjoyConnectInstance().getDisplayAd(this);
        
    	//Mopub ads = new Mopub(this);
    	//ads.add(rlayout);

	}

    
	@Override
    protected void onResume(){
    	super.onResume();
    	if (mpplaying == true){
    		mPlayer.start();
    	}

    }
    protected void onPause(){
    	super.onPause();
    	if (mpplaying == true){
    		mPlayer.pause();
    	}
    }
	

	@Override
    protected void onStop (){
    	super.onStop();
    	if (editmode == true){
    	SaveImage(currentframe, canview);
    	FlurryAgent.onEndSession(this);
    	}

    }
	
	
	@Override
    protected void onDestroy (){
    	super.onDestroy();
    	mPlayer.release();
    	mAdView.destroy();
    	canview.garbagecollect();
    	canview2.garbagecollect();
    	System.gc();
    	

    }

	@Override
    public boolean onTouchEvent(MotionEvent me) {
		return horiView.onTouchEvent(me);
       // return gestureDetector.onTouchEvent(me);
    }
	
    
	@Override
	public boolean onDoubleTap(MotionEvent e) {   
	       Log.d(tag, "...onDoubleTap...");   
	       /*if(getmViewFlipper().isFlipping()) {   
	           getmViewFlipper().stopFlipping();   
	       }else {   
	           getmViewFlipper().startFlipping();   
	       }   */
	       return true;   
	    }  
	@Override
	public boolean onDoubleTapEvent(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	

	@Override
	public boolean onSingleTapConfirmed(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,   
	           float velocityY) {   
	       Log.d(tag, "...onFling...");   
 
	      
	       return false;   
	    }  
	@Override
	public void onLongPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
    public void SaveImage(int current, canvasview canvas){
    	String[] FILENAME = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November" ,"December"};
        Bitmap tempBitmap = canview.getBitmap();
        try {
    	FileOutputStream fos = openFileOutput(FILENAME[current], Context.MODE_PRIVATE);
    	ByteArrayOutputStream stream = new ByteArrayOutputStream();
    	tempBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
    	byte[] byteArray = stream.toByteArray();
    	
    	
			fos.write(byteArray);
			
			Log.e("Calendar", "size:" + byteArray.length);
    	fos.close();
    	Log.e("Calendar", "Save!!!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("Calendar", "Save failed!!!");
		}catch (NullPointerException e){
			Log.e("Bitmap", "Not Found");
		}
    }
	
    public void LoadImage(int current, canvasview canvas){
    	String[] FILENAME = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November" ,"December"};
    	
    	
    	try{

    	FileInputStream fis = openFileInput(FILENAME[current]);
    	byte[] input = new byte[fis.available()];
    	fis.read(input, 0, input.length);
    	Bitmap bitmap = BitmapFactory.decodeByteArray(input, 0, input.length);
    	
    	Log.e("Calendar", Integer.toString(bitmap.getHeight()));
    	Log.e("Calendar", Integer.toString(bitmap.getWidth()));
    	Log.e("Calendar", Integer.toString(input.length));
    	//Bitmap bitmap = BitmapFactory.decodeByteArray(inputbuffer, 0, inputbuffer.length);
    	//canview.init(screenwidth, screenheight, mPaint);
    	fis.close();
		canvas.init(screenwidth, screenheight, mPaint);
    	canvas.setBitmap(bitmap);
    	bitmap.recycle();
    	//System.gc();

    	
    	

    	}catch (FileNotFoundException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace(); 
    		Log.e("Calendar", "Load Not found!!!");
    		canvas.init(screenwidth, screenheight, mPaint);
    	}
    	
    	catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("Calendar", "Load failed!!!");
			canvas.init(screenwidth, screenheight, mPaint);
		}catch (NullPointerException e){
			Log.e("Bitmap", "Not Found");
		}
		//return canvas;
    } 
    
   

    
    
    private void seteditlayout() {
    	
    	controllayout.setBackgroundColor(Color.argb(75, 255, 255, 255));

    	
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
    	
    	// -------------------------------------------
        // The Test Edit Button!!
  	  ImageButton  imgbtn21 = new ImageButton(this);
	        imgbtn21.setBackgroundResource(edit[21]);

	        imgbtn21.layout(5, 0, 5, 0);
	        imgbtn21.setMinimumHeight(40);
	        imgbtn21.setMinimumWidth(40);
	        imgbtn21.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	
	            	try{
	            		rlayout.removeView(tv);
						rlayout.removeView(tv2);
						rlayout.removeView(tv3);
	            	}catch(Exception e){
	            		
	            	}
	            	choice =text;
	            	canview.setChoice(choice);
	            	removeselected();
	            	mPaint.setXfermode(null);
	                alert = new AlertDialog.Builder(CalendarActivity.this);
	                final EditText input = new EditText(CalendarActivity.this);
	                alert.setView(input);
	                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int whichButton) {
	                String value = input.getText().toString().trim();
	                Toast.makeText(CalendarActivity.this.getApplicationContext(), R.string.press_done,
	                Toast.LENGTH_SHORT).show();
	                
	                textwidth = 50;
	                textheight = 80;
	                canview.setText(value);
	        		params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
	        				RelativeLayout.LayoutParams.WRAP_CONTENT);
	        		params1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	        		params2 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	        		params3 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	        		params1.setMargins(50, 100, 0, 0);
	        		params2.setMargins(50, 50, 0, 0);        		
	        		params3.setMargins(120, 100, 0, 0);
	        		
	               // final TextView tv = new TextView(CalendarActivity.this);
	                //tv.setText(value);
	                //tv.setTextColor(Color.RED);
	                //rlayout.addView(tv);
	        		
	        		//Done Button
	        		tv = new ImageButton(CalendarActivity.this);
	        		//rlayout.removeView(tv);
	        		tv.setBackgroundResource(R.drawable.done_btn);
	        		tv.setTag(0);
	        		tv.setMinimumHeight(30);
	        		tv.setMinimumWidth(70);
	                
	               
	                //tv.setFocusableInTouchMode(true);
	                tv.setFocusable(true);
	                
	                //Move Button
	        		tv3 = new ImageButton(CalendarActivity.this);
	        		//rlayout.removeView(tv);
	        		tv3.setBackgroundResource(R.drawable.move_iphone);
	        		tv3.setTag(2);
	        		tv3.setMinimumHeight(20);
	        		tv3.setMinimumWidth(40);
	               
	               
	                tv3.setFocusableInTouchMode(true);
	                tv3.setFocusable(true);
	                
	                //Text
	                tv2 = new TextView(CalendarActivity.this);
	        		//rlayout.removeView(tv);
	        		tv2.setBackgroundResource(Color.TRANSPARENT);
	        		tv2.setPadding(0, 0, 0, 0);
	        		tv2.setText(value);
	        		tv2.setTextColor(Color.RED);
	        		tv2.setTextSize(20);
	        		tv2.setTag(1);
	        		//tv.setBackgroundColor(Color.TRANSPARENT);
	                rlayout.addView(tv2, params2);
	                rlayout.addView(tv3, params3);
	                rlayout.addView(tv, params1);
	               // tv.setTextSize(20);
	                tv2.setFocusableInTouchMode(true);
	                tv2.setFocusable(true);
	                
	                tv.setOnTouchListener(new OnTouchListener(){
	                	public boolean onTouch(View view,MotionEvent me){
							// TODO Auto-generated method stub
	                		if (me.getAction() == MotionEvent.ACTION_DOWN) {
	                			canview.confirm(textwidth, textheight);
								rlayout.removeView(view);
								rlayout.removeView(tv2);
								rlayout.removeView(tv3);
	                		}
							return false;
							
						}
	                });
	        
	                tv3.setOnTouchListener(new OnTouchListener(){
	                	public boolean onTouch(View view,MotionEvent me){
	                		Log.i("ME", Integer.toString(me.getAction()));
	                		
	                		if (me.getAction() == MotionEvent.ACTION_DOWN) {
	                			Log.i("Drag", "StartDragging");
	                		}
	                		if (me.getAction() == MotionEvent.ACTION_UP) {
	                			
	                			Log.i("Drag", "Stopped Dragging");
	                			if ((int)me.getRawX() > screenwidth-(view.getMeasuredWidth()) && exceed == false){
	                				params3.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2-180, (int)me.getRawY()-view.getMeasuredHeight(), 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
	                				exceed = true;
	                              } else if ((int)me.getRawX() < view.getMeasuredWidth() && exceed == true){
		                				params3.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2, (int)me.getRawY()-view.getMeasuredHeight(), 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
		                				exceed = false;
		                              }
	                		} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
	                			
	                				System.out.println("Dragging");
	                				Log.i("Rawx", Integer.toString((int)me.getRawX()-view.getMeasuredWidth()/2-100));
	                				Log.i("Rawy", Integer.toString((int)me.getRawY()-100-view.getMeasuredHeight()-view.getMeasuredHeight()/2));
	                				//params1.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2-100, (int)me.getRawY()-view.getMeasuredHeight()-view.getMeasuredHeight()/4, 0, 0);
	                				//params2.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2-100, (int)me.getRawY()-50-view.getMeasuredHeight()-view.getMeasuredHeight()/4, 0, 0);
	                				//params3.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2, (int)me.getRawY()-view.getMeasuredHeight()-view.getMeasuredHeight()/4, 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
	                				
	                				if (exceed == false){
	                					params1.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2-70, (int)me.getRawY()-view.getMeasuredHeight(), 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
		                				params2.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2-70, (int)me.getRawY()-50-view.getMeasuredHeight(), 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
		                				params3.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2, (int)me.getRawY()-view.getMeasuredHeight(), 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
		             					textwidth = (int)me.getRawX()-view.getMeasuredWidth()/2-70;
	                					textheight = (int)me.getRawY()-50-view.getMeasuredHeight()/2-view.getMeasuredHeight()/5;
	                				} else{
	                					params1.setMargins((int)me.getRawX()+view.getMeasuredWidth()/2, (int)me.getRawY()-view.getMeasuredHeight(), 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
		                				params2.setMargins((int)me.getRawX()+view.getMeasuredWidth()/2, (int)me.getRawY()-50-view.getMeasuredHeight(), 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
	                					params3.setMargins((int)me.getRawX()-view.getMeasuredWidth()/2, (int)me.getRawY()-view.getMeasuredHeight(), 0-view.getMeasuredWidth(), 0-view.getMeasuredHeight());
		             					textwidth = (int)me.getRawX()+view.getMeasuredWidth()/2;
	                					textheight = (int)me.getRawY()-50-view.getMeasuredHeight()/2-view.getMeasuredHeight()/5;
	                				}
	                				tv.setLayoutParams(params1);
	                				tv2.setLayoutParams(params2);
	                				view.setLayoutParams(params3);
	                				
	                				
	              				
	                				
	                				//view.setPadding((int) me.getRawX(), (int) me.getRawY(), 0, 0);
	                				
	                				view.invalidate();
	                			}
	                		
	                		return false;
	                		
	                	}
	                });
	               
	              
	                }
	                });

	                alert.setNegativeButton("Cancel",
	                new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int whichButton) {
	                dialog.cancel();
	                }
	                });
	            	alert.show();
	            	
	            	//canview.setIcon(edit[21]);
	            	//flipperframe = mViewFlipper.getDisplayedChild();
	            	
	            }
	        });
	        controllayout.addView(imgbtn21, p );
    	

    	
    	
    	 drawbtn = new ImageButton(this);
         drawbtn.setBackgroundResource(edit[edit.length-2]);
         drawbtn.layout(5, 0, 5, 0);
         drawbtn.setMinimumHeight(40);
         drawbtn.setMinimumWidth(40);
         drawbtn.setOnClickListener(new OnClickListener() {
         	public void onClick(View v) {
         		choice = pencil; 
         		//canview.setIcon(null);
         		removeselected();
         		drawbtn.setBackgroundResource(R.drawable.bt_freehand2);
         		mPaint.setStrokeWidth(4);
         		mPaint.setColor(Color.BLUE);
         		mPaint.setXfermode(null);
         		
         		canview.pencil(mPaint, choice);
         		
         }
         });
         
         
         controllayout.addView(drawbtn, p );
         // -------------------------------------
         
         erasebtn = new ImageButton(this);
         erasebtn.setBackgroundResource(edit[edit.length-1]);
         erasebtn.layout(5, 0, 5, 0);
         erasebtn.setMinimumHeight(40);
         erasebtn.setMinimumWidth(40);
         erasebtn.setOnClickListener(new OnClickListener() {
         	public void onClick(View v) {
         		choice = eraser; 
         		removeselected();
         		erasebtn.setBackgroundResource(R.drawable.bt_eraser2);
         		mPaint.setColor(Color.TRANSPARENT);
         		mPaint.setStrokeWidth(18);
                 mPaint.setXfermode(new PorterDuffXfermode(
                         PorterDuff.Mode.CLEAR));
         		canview.eraser(mPaint, choice);
         		
         }
         });
         
         
         controllayout.addView(erasebtn, p );
         
      // -------------------------------------

         musicbtn = new ImageButton(this);
         if (mpplaying == true){ 
      		musicbtn.setBackgroundResource(R.drawable.music_on);
      		mPlayer.setLooping(true);
      		mPlayer.start();
      		} else {
      			musicbtn.setBackgroundResource(R.drawable.music_off);
      			mpplaying = false;
      		}
       
         musicbtn.layout(5, 0, 5, 0);
         musicbtn.setMinimumHeight(40);
         musicbtn.setMinimumWidth(40);
         musicbtn.setOnClickListener(new OnClickListener() {
         	public void onClick(View v) {
         		//choice = eraser; 
         		if (mpplaying == false){ 
         		musicbtn.setBackgroundResource(R.drawable.music_on);
         		mPlayer.setLooping(true);
         		mPlayer.start();
         		mpplaying = true;
         		} else {
         			mPlayer.pause();
         			musicbtn.setBackgroundResource(R.drawable.music_off);
         			mpplaying = false;
         		}
     
         }
         });
         
         
         
         controllayout.addView(musicbtn, p );
         
         
         //------------------------------------
    	
    	
    	
		// TODO Auto-generated method stub
        LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
  // -------------------------------------------
        for (int i = 0; i < 20; i++) {
        	//Log.e("i", Integer.toString(i));
        image_icon[i] = new ImageButton(this);
        image_icon[i].setBackgroundColor(Color.TRANSPARENT);
        image_icon[i].setImageResource(edit[i]);
        image_icon[i].setTag(i);
        image_icon[i].layout(0, 0, 0, 0);
        image_icon[i].setScaleType(ScaleType.CENTER_CROP);
        image_icon[i].setMinimumHeight(40);
        image_icon[i].setMinimumWidth(40);
        image_icon[i].setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	removeselected();
               int btnid;
            	btnid = (Integer) v.getTag();
            	mPaint.setXfermode(null);
            	mPaint.setColor(Color.TRANSPARENT);
            	canview.setIcon(edit[btnid], mPaint);
            	v.setBackgroundResource(R.drawable.icon_highlighted);
	
            }
        });
        
        editlayout.addView(image_icon[i], p1 );
        
        }

	        
	        // -------------------------------------------
	    	/*  ImageButton  imgbtn1 = new ImageButton(this);
		        imgbtn1.setBackgroundColor(Color.TRANSPARENT);
                imgbtn1.setImageResource(edit[1]);
		        imgbtn1.layout(5, 0, 5, 0);
		        imgbtn1.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            	mPaint.setXfermode(null);
		            	removeselected();
		            	canview.setIcon(edit[1]);
		            	v.setBackgroundResource(R.drawable.icon_highlighted);
				        v.setBackgroundColor(Color.TRANSPARENT);
		            	//flipperframe = mViewFlipper.getDisplayedChild();
		            	
		            }


		        });
    editlayout.addView(imgbtn1, p1 );*/
    
  
					        
					        
						        



		        
	        
	}
    
	public void removeselected() {
		// TODO Auto-generated method stub
		
		 for (int i = 0; i < 20; i++) {
		     image_icon[i].setBackgroundColor(Color.TRANSPARENT);
		 }
		 drawbtn.setBackgroundResource(R.drawable.bt_freehand);
		 erasebtn.setBackgroundResource(R.drawable.bt_eraser);
		
	}
	
	

	
	public static ViewFlipper getmViewFlipper() {
		return mViewFlipper;
	}


	public static ImageView getImageTop() {
		return imgtop1;
	}

	public static ImageView getImageBottom() {
		return imgbottom1;
	}
	
	public static void setcurrentframe(int frame){
		currentframe = frame;
			
	     horiView.smoothScrollTo(currentframe * screenwidth, 0);

			
	}
	public int getcurrentframe(){
		return currentframe;
	}
	
	public static HorizontalScrollView getHoriView(){
		return horiView;
	}
	
	public canvasview getcanview(){
		return canview;
	}




	private ImageButton[] imagebtn;
	
    private int[] dogs = {R.drawable.dog01, R.drawable.dog02, R.drawable.dog03, R.drawable.dog04, R.drawable.dog05, R.drawable.dog06, R.drawable.dog07 , R.drawable.dog08, R.drawable.dog09, R.drawable.dog10, R.drawable.dog11, R.drawable.dog12};
    private int[] months = {R.drawable.january2011,R.drawable.february2011, R.drawable.march2011, R.drawable.april2011, R.drawable.may2011, R.drawable.june2011, R.drawable.july2011, R.drawable.august2011, R.drawable.september2011, R.drawable.october2011, R.drawable.november2011, R.drawable.december2011};
    private int[] select = {R.drawable.jan, R.drawable.feb, R.drawable.mar, R.drawable.apr, R.drawable.may, R.drawable.jun, R.drawable.jul, R.drawable.aug, R.drawable.sep, R.drawable.oct, R.drawable.nov, R.drawable.dec};
    private int[] edit = {R.drawable.calendar_icon_aeroplane, R.drawable.calendar_icon_basketball, R.drawable.calendar_icon_bear, R.drawable.calendar_icon_birthday, R.drawable.calendar_icon_books, R.drawable.calendar_icon_camera, R.drawable.calendar_icon_car, R.drawable.calendar_icon_christmas, R.drawable.calendar_icon_coffee, R.drawable.calendar_icon_dress, R.drawable.calendar_icon_easter, R.drawable.calendar_icon_fishing, R.drawable.calendar_icon_flower, R.drawable.calendar_icon_halloween, R.drawable.calendar_icon_hearts, R.drawable.calendar_icon_karaoke, R.drawable.calendar_icon_party, R.drawable.calendar_icon_present, R.drawable.calendar_icon_roastedchicken, R.drawable.calendar_icon_sunny, R.drawable.calendar_icon_tie, R.drawable.bt_text, R.drawable.bt_freehand, R.drawable.bt_eraser};
    private int[] imgviews = {R.id.imageView1, R.id.imageView2, R.id.imageView3, R.id.imageView4, R.id.imageView5, R.id.imageView6, R.id.imageView7, R.id.imageView8, R.id.imageView9, R.id.imageView10, R.id.imageView11, R.id.imageView12, R.id.imageView13, R.id.imageView14, R.id.imageView15, R.id.imageView16, R.id.imageView17, R.id.imageView18, R.id.imageView19, R.id.imageView20, R.id.imageView21, R.id.imageView22, R.id.imageView23, R.id.imageView24};
    private int[] relayout = {R.id.relativeLayout1, R.id.relativeLayout2, R.id.relativeLayout3, R.id.relativeLayout4, R.id.relativeLayout5, R.id.relativeLayout6, R.id.relativeLayout7, R.id.relativeLayout8, R.id.relativeLayout9, R.id.relativeLayout10, R.id.relativeLayout11, R.id.relativeLayout12};
   // private int[] canviews = {R.id.canvasview, R.id.canvasview01, R.id.canvasview02, R.id.canvasview03, R.id.canvasview04, R.id.canvasview05, R.id.canvasview06, R.id.canvasview07, R.id.canvasview08, R.id.canvasview09, R.id.canvasview10, R.id.canvasview11, R.id.canvasview12};

	@Override
	public boolean onTouch(View arg0, MotionEvent ev) {
		// TODO Auto-generated method stub
		
		     if (editmode == true){
		    	 return true;
		     }else {
			
				final int action = ev.getAction();
				final float x = ev.getX();

		        
		        
		        switch (action) {
				case MotionEvent.ACTION_DOWN:


					break;

				case MotionEvent.ACTION_MOVE:
	

					break;

				case MotionEvent.ACTION_UP:
					Log.i("Calendar","Action up");

						
			            int lScrollX = horiView.getScrollX();
			            int lPageWidth = screenwidth;

			            // Move to the current page.
			            int originalPage = currentframe;
			            currentframe = (lScrollX + (lPageWidth / 2)) / lPageWidth;
			            horiView.smoothScrollTo(currentframe * lPageWidth, 0);
			           
			            if (originalPage > currentframe){
			            	 canview2.garbagecollect();
			            	 rlayouts[originalPage].removeView(canview2);
			            	 LoadImage(currentframe, canview2);
			            	 rlayouts[currentframe].addView(canview2, rparams);
			            	 Log.e("currentframe", Integer.toString(currentframe));

			     	            	if (currentframe != 0){
			     	            		if(loaded[currentframe-1] == false){
			     		
			     		            img_array[currentframe+11].setImageResource(dogs[currentframe-1]);
			     		            loaded[currentframe-1] = true;
			     	            		}
			     		           
			     	            }
			            }else if (originalPage < currentframe){
			            	canview2.garbagecollect();
			            	rlayouts[originalPage].removeView(canview2);
			            	LoadImage(currentframe, canview2);
			            	rlayouts[currentframe].addView(canview2, rparams);
			            	Log.e("currentframe", Integer.toString(currentframe));
							if (currentframe != 11){
								if(loaded[currentframe+1] == false){
		     	
		     		            img_array[currentframe+13].setImageResource(dogs[currentframe+1]);
		     		           loaded[currentframe+1] = true;
								}
		     		           
		     	            }
			            
			            /*new Thread(new Runnable() {
			                public void run() {
			                	  LoadImage(currentframe, canview);
			         	           
			         	            	if (currentframe != 11){
			         		            img_array[currentframe+1].setImageResource(imgviews[currentframe+1]);
			         		            img_array[currentframe+13].setImageResource(imgviews[currentframe+13]);
			         	            	}
			         	            	if (currentframe != 0){
			         		            img_array[currentframe-1].setImageResource(imgviews[currentframe-1]);
			         		            img_array[currentframe+11].setImageResource(imgviews[currentframe+11]);
			         	            	}
			         	            
			         	            }
			            }).start();*/
			            }
	
			              
			            
			            return true;
				

				case MotionEvent.ACTION_CANCEL:
					
				}
				
				return false;
		     }
		     


	}
	
	private void choosemonthlayout() {
		LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        

       // ImageButton[] img_btn = new ImageButtonClass[12];
        
    	// Set The select Month layout

	    for (int i = 0; i < months.length; i++) {
	       
	        
	        

	        /*Button buttonView = new Button(this);
	        buttonView.setText("Button " + i);
	        layout.addView(buttonView, p);*/

            
       
            
	        img_btn[i] = new ImageButton(this);
	        img_btn[i].setBackgroundResource(select[i]);
	        img_btn[i].setTag(i);
	        img_btn[i].layout(5, 0, 5, 0);
	       
	        
	        
	        img_btn[i].setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	             
			   //getmViewFlipper().setInAnimation(getApplicationContext(), R.anim.push_left_in);   
			   //getmViewFlipper().setOutAnimation(getApplicationContext(), R.anim.push_left_out);   
			   //getmViewFlipper().showNext();
	           
	           int originalPage = currentframe; 	
			   int id = (Integer) v.getTag();
			   Log.e("id",Integer.toString(id));
			   

	           
			   //getImageTop().setImageResource(dogs[id]);
			   //getImageBottom().setImageResource(months[id]);
			   setcurrentframe(id);
			   
			   canview2.garbagecollect();
			   rlayouts[originalPage].removeView(canview2);
		      	 LoadImage(currentframe, canview2);
		      	 rlayouts[currentframe].addView(canview2, rparams);
		      	 
		      	 if(loaded[currentframe] == false){
		      		img_array[currentframe+12].setImageResource(dogs[currentframe]);
		      	 }

          	if (currentframe != 0){
          		if (loaded[currentframe-1] == false){
		            img_array[currentframe+11].setImageResource(dogs[currentframe-1]);
		            loaded[currentframe-1] = true;
          		}
          	}
          	if (currentframe != 11){
          		if(loaded[currentframe+1] == false){
		            img_array[currentframe+13].setImageResource(dogs[currentframe+1]);
		           loaded[currentframe+1] = true;
          		}
          	}
		      
	           
	        }
	        });
	        layout.addView(img_btn[i], p );
	    }
	}

	private ImageView setImageResources(int i) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void setupmopub(){
		/*params = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, 50);
		  mAdView = new MoPubView(this);
		  mAdView.setAdUnitId(thirdpartyconstant.PUB_ID_320x50); // Enter your Ad Unit ID from www.mopub.com
		  mAdView.loadAd();
		  rlayout.addView(mAdView, params);*/
		  
		  
		  mAdView = (MoPubView) findViewById(R.id.adview);
		  mAdView.setAdUnitId(thirdpartyconstant.PUB_ID_320x50); // Enter your Ad Unit ID from www.mopub.com
		  mAdView.loadAd();
	}
	
	private void loadChartBoost(){
		//if (opencount == 0){
		     // Configure ChartBoost
	        ChartBoost _cb = ChartBoost.getSharedChartBoost();
	        _cb.setContext(CalendarActivity.this);
	        _cb.setAppId(thirdpartyconstant.CHARTB_ID);
	        _cb.setAppSignature(thirdpartyconstant.CHARTB_SIGN);
	        
	        _cb.install();
		       Log.e("Chart Boost","Chart Boost");

		        _cb.loadInterstitial();
		        //_cb.loadMoreApps();
		   // }
	}
	
    private static final int SETTING_MENU_ID = Menu.FIRST;
    private static final int CLEAR_MENU_ID = Menu.FIRST + 1;
    private static final int MODE_MENU_ID = Menu.FIRST + 2;
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        
        //menu.add(0, SETTING_MENU_ID, 0, "Settings").setShortcut('3', 'c');
        //menu.add(0, CLEAR_MENU_ID, 0, "Clear All").setShortcut('4', 's');
        //menu.add(0, MODE_MENU_ID, 0, "Edit Mode").setShortcut('5', 'z');


        /****   Is this the mechanism to extend with filter effects?
        Intent intent = new Intent(null, getIntent().getData());
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        menu.addIntentOptions(
                              Menu.ALTERNATIVE, 0,
                              new ComponentName(this, NotesList.class),
                              null, intent, 0, null);
        *****/
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();

        if(editmode) {

        menu.add(0, MODE_MENU_ID, 0, "Leave Edit Mode");

        } else {

        menu.add(0, MODE_MENU_ID, 0, "Edit Mode");

        }


        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       // mPaint.setXfermode(null);
       // mPaint.setAlpha(0xFF);

        switch (item.getItemId()) {
            case SETTING_MENU_ID:
                
                return true;
            case CLEAR_MENU_ID:
                return true;
            case MODE_MENU_ID:
            	 if (layout.getVisibility() == View.VISIBLE){
                	 //Enter Edit Mode
                	layout.setVisibility(View.GONE);
                	layout.setFocusable(false);
                  	layout.setClickable(false);
                	layout.setEnabled(false);
                	
                	editlayout.setFocusable(true);
               	    editlayout.setClickable(true);
                	editlayout.setEnabled(true);
                	 canview.setFocusable(true);
                	 canview.setFocusableInTouchMode(true);
                	 editmode = true;
                	editlayout.setVisibility(View.VISIBLE);
                	controllayout.setVisibility(View.VISIBLE);
                	canview.setFling(false);
                	LoadImage(currentframe, canview);
                	canview2.setVisibility(View.INVISIBLE);
                	canview.setVisibility(View.VISIBLE);
               
                	
                	month_select.setBackgroundResource(R.drawable.month_mode);}
                 else if (layout.getVisibility() == View.GONE){
                	 //Leave Edit Mode
                	 layout.setVisibility(View.VISIBLE);
                	 layout.setFocusable(true);
                	 layout.setClickable(true);
                	 layout.setEnabled(true);
                	 editlayout.setFocusable(false);
                	 editlayout.setClickable(false);
                	 editlayout.setEnabled(false);
                	 editlayout.setVisibility(View.GONE);
                	 controllayout.setVisibility(View.INVISIBLE);
                	 try{
    	            		rlayout.removeView(tv);
    						rlayout.removeView(tv2);
    						rlayout.removeView(tv3);
    	            	}catch(Exception e){
    	            		
    	            	}
                	 
                	 editmode = false;
              
                	 canview.setFocusable(false);
                	 canview.setFocusableInTouchMode(false);
                	 
                 	
                 	 canview.setVisibility(View.GONE);
     
                	 SaveImage(currentframe, canview);
                	 LoadImage(currentframe, canview2);
                	 canview2.setVisibility(View.VISIBLE);
                	 
                	 canview.setFling(true);
                	 month_select.setBackgroundResource(R.drawable.edit_mode);
                 
                 }

                return true;

        }
        return super.onOptionsItemSelected(item);
    }
	
	

	

}


