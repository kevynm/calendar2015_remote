package com.outblazeventures.calendar2012;

import java.util.Calendar;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.FrameLayout.LayoutParams;

public class ShowCalendar extends RelativeLayout{
	
	final static int b = Color.BLACK;
	final static int r = Color.RED;
	final static int t = Color.TRANSPARENT;
	private Context mcontext;
    private TextView tv;
    private int screenwidth;
    private int screenheight;
    private Calendar cal;
    private LayoutParams params;
    
 
	
	int[][] calnum = new int[][] {
			//Jan
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Feb
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//March
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Apr
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//May
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Jun
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Jul
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Aug
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Sep
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Oct
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Nov
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			//Dec
			{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
			
	};
	
    public ShowCalendar(Context c) {
        super(c);
        mcontext =c;
    }
    
    public ShowCalendar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mcontext = context;
    }
    
    public ShowCalendar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mcontext = context;
    }


    public void init(int screenwidth, int screenheight, Context c){
    	mcontext = c;
    	this.screenheight = screenheight;
    	this.screenwidth = screenwidth;
    	cal = Calendar.getInstance();
    	params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    	params.setMargins(0,0,0,0);
    }
    
    
    
    public void drawview(int top, int left, int currentmonth){
    	for (int i=0; i<31; i++){
	    	tv = new TextView(mcontext);
	    	tv.setText(Integer.toString(i));
	    	tv.setTextColor(calnum[currentmonth][i]);
	    	addView(tv, params);
    	}
    }
    
    public void drawdate(int currentmonth){
    	int DoW = 0, WoM = 0, DoM = 0;
    	 cal.set(Calendar.MONTH, currentmonth);

       
        	for (int d=0; d<31; d++){
                // Get the Week of Month and Day of Week
               
                cal.set(Calendar.DAY_OF_MONTH, d);
                DoW = cal.get(Calendar.DAY_OF_WEEK);
                DoM = cal.get(Calendar.DAY_OF_MONTH);
                int offset = DoW - 2;
                WoM = (DoM + offset)/7;
                if (WoM == 5){
                	WoM = 0;
                }
                computePosition(DoW, WoM, currentmonth);
        	}
        	
        
        


        
    }
    
    public void computePosition(int DoW, int WoM, int month){
    	int datetop;
        int dateleft = screenwidth/7*DoW - 27;
        //screen height 800
        if (screenheight == 800){
        	datetop = screenheight/2 + 58*WoM - 27 + 27;
        }else if (screenheight > 800){
        	datetop = screenheight/2 + screenheight*59/800*WoM - 27+33;
        }else {
        	datetop = screenheight/2 + screenheight*59/800*WoM - 27+21;
        }
        drawview(datetop, dateleft, month);
    }
    
}
