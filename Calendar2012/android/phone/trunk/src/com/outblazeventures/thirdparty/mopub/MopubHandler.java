package com.outblazeventures.thirdparty.mopub;

public interface MopubHandler {
	public void showAds(boolean show);
	public void showAdsByTime(int timer);
	
	public void showChartBoost();
	
	public void flurryEvent(String args);
	
	public void tapjoyEvent(String args);
	
	public void showTayjoyOfferwall();
}
