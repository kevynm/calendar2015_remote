//
//  TagetedTouchDelegate.m
//  PaintingController
//
//  Created by outblaze on 02/08/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "TargetedTouchController.h"


@implementation TargetedTouchController

@synthesize targetTouches, targetedTouchControllerDelegate;

#pragma mark Lifecycle

- (id)init {
	if ((self = [super init]) != nil) {
		targetTouches = [[NSMutableArray alloc] init];
	}
	
	return self;
}

- (void)dealloc {
	[targetTouches release], targetTouches = nil;
	[super dealloc];
}

#pragma mark Touch event reciver

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	for (UITouch *touch in touches) {
		if ([self targetedTouchBegan:touch withEvent:event]) {
			[targetTouches addObject:touch];
			if (targetedTouchControllerDelegate != nil) [targetedTouchControllerDelegate ttController:self targetedTouchBegan:touch withEvent:event];
			break;
		}
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	for (UITouch *touch in touches) {
		if ([targetTouches containsObject:touch]) {
			[self targetedTouchMoved:touch withEvent:event];
			if (targetedTouchControllerDelegate != nil) [targetedTouchControllerDelegate ttController:self targetedTouchMoved:touch withEvent:event];
			break;
		}
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	for (UITouch *touch in touches) {
		if ([targetTouches containsObject:touch]) {
			[self targetedTouchEnded:touch withEvent:event];
			//autorelease for callback
			[[touch retain] autorelease];
			[targetTouches removeObject:touch];
			if (targetedTouchControllerDelegate != nil) [targetedTouchControllerDelegate ttController:self targetedTouchEnded:touch withEvent:event];
			break;
		}
	}
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	for (UITouch *touch in touches) {
		if ([targetTouches containsObject:touch]) {
			[self targetedTouchCancelled:touch withEvent:event];
			//autorelease for callback
			[[touch retain] autorelease];
			[targetTouches removeObject:touch];
			if (targetedTouchControllerDelegate != nil) [targetedTouchControllerDelegate ttController:self targetedTouchCancelled:touch withEvent:event];
			break;
		}
	}
}

#pragma mark Targeted touch event

- (BOOL)targetedTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	//Default ignore
	return NO;
}

- (void)targetedTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
	//Abstract method
}

- (void)targetedTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	//Abstract method
}

- (void)targetedTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
	//Abstract method
}

#pragma mark Interface

- (void)inactivateTargetTouch:(UITouch *)touch {
	[targetTouches removeObject:touch];
}


@end
