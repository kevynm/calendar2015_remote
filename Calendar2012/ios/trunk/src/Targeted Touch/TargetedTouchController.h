//
//  TagetedTouchDelegate.h
//  PaintingController
//
//  Created by outblaze on 02/08/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TargetedTouchControllerDelegate;



@interface TargetedTouchController : NSObject {
	
	id<TargetedTouchControllerDelegate> targetedTouchControllerDelegate;
	NSMutableArray *targetTouches;

}

@property(nonatomic, assign) id<TargetedTouchControllerDelegate> targetedTouchControllerDelegate;
@property(nonatomic, readonly) NSMutableArray *targetTouches;

//Touch event reciver
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;

//Targeted touch event
- (BOOL)targetedTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)targetedTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)targetedTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)targetedTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event;

//Interface
- (void)inactivateTargetTouch:(UITouch *)touch;

@end




@protocol TargetedTouchControllerDelegate

@optional
- (void)ttController:(TargetedTouchController *)controller targetedTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)ttController:(TargetedTouchController *)controller targetedTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)ttController:(TargetedTouchController *)controller targetedTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)ttController:(TargetedTouchController *)controller targetedTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event;

@end
