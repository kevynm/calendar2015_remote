//
//  DataManager.h
//  
//
//  Created by Ming on 29/05/2009.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
//  Version 1.0

#define PI			(2.0 * asin (1.0))
#define RADIANS(x)	(x * PI / 180.0)

#define IsiPad		[Utilities isiPad]
#define IsiPhone	!IsiPad

@interface Utilities : NSObject {}

+ (BOOL)isiPad;
+ (float)getOSVersion;

+ (NSString*)getPathForResource:(NSString*)sFilename;
+ (NSString*)getPathForSavedData:(NSString*)sFilename;

+ (BOOL)isFileExist:(NSString*)sPath;
+ (void)deleteFile:(NSString*)sPath;

//+ (float)getDistanceBetweenPoints:(CGPoint)point1 :(CGPoint)point2;


//#ifdef SCNetworkReachabilityRef
+ (BOOL)reachable:(NSString*)sURL;
+ (BOOL)haveInternetConnection;
//#endif

+ (NSString*)getMD5:(NSString*)sInputString;

+ (NSDate*)convertDateToLocalTimeZone:(NSDate*)pDate fromTimeZone:(NSString*)sTimeZone;

+ (NSString*)formatNumber:(int)nValue;

+ (float)inBetweenFromValue:(float)fromValue toValue:(float)toValue percentage:(float)percentage;

+ (void)logCurrentTime;
+ (void)logError:(NSError*)error;

+ (void)setAnimationForImageView:(UIImageView*)imageView filenameFormat:(NSString*)filenameFormat startIndex:(int)startIndex numOfFrame:(int)numOfFrame fps:(float)fps repeatCount:(int)repeatCount;
+ (void)rotateUIView:(UIView*)view angle:(float)fAngle speed:(float)fSpeed;

+ (void)fadeInUIView:(UIView*)view speed:(float)fSpeed;
+ (void)fadeOutUIView:(UIView*)view speed:(float)fSpeed;
+ (void)flashUIView:(UIView*)view speed:(float)fSpeed;

+ (void)launchAppStore:(NSString*)sAppID;
+ (void)launchAppStoreReview:(NSString*)sAppID;

@end