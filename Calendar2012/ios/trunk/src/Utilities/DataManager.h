//
//  DataManager.h
//  
//
//  Created by Ming on 29/05/2009.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
//  Version 1.0

@interface DataManager : NSObject {}

+ (NSString *) getStringForKey:(NSString *)key;
+ (NSString *) getStringForKey:(NSString *)key setDefault:(NSString *)defaultString;

+ (int) getIntForKey:(NSString *)key;
+ (int) getIntForKey:(NSString *)key setDefault:(int)defaultValue;

+ (float) getFloatForKey:(NSString *)key;
+ (float) getFloatForKey:(NSString *)key setDefault:(float)defaultValue;

+ (BOOL) getBoolForKey:(NSString *)key;
+ (BOOL) getBoolForKey:(NSString *)key setDefault:(BOOL)defaultValue;

+ (id) getObjectForKey:(NSString *)key;
+ (id) getObjectForKey:(NSString *)key setDefault:(id)defaultObject;

+ (void) setString:(NSString *)string forkey:(NSString *)key;
+ (void) setInt:(int)value forkey:(NSString *)key;
+ (void) setFloat:(float)value forkey:(NSString *)key;
+ (void) setBool:(BOOL)value forkey:(NSString *)key;
+ (void) setObject:(id)object forkey:(NSString *)key;

+ (BOOL) checkHaveKey:(NSString*)keyWord;
+ (void) removeAllKey:(NSString*)keyWord;

@end
