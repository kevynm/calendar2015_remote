//
//  DataManager.m
//  
//
//  Created by Ming on 29/05/2009.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
//  Version 1.0

#import "Utilities.h"

// For Network
#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>

// For MD5
#import <CommonCrypto/CommonDigest.h>

@implementation Utilities

+ (BOOL)isiPad {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
	// OS Version >= 3.2
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
		// This device is an iPad running iPhone 3.2 or later.
		return YES;
	}
	else
	{
		// This device is an iPhone or iPod touch.
		return NO;
	}
#else
	// OS Version < 3.2
	// This device is an iPhone or iPod touch.
	return NO;
#endif
}

+ (float)getOSVersion {
	NSString *sVersion = [UIDevice currentDevice].systemVersion;
	return [sVersion floatValue];
}

+ (NSString*) getPathForResource:(NSString*)sFilename
{
	return [[NSBundle mainBundle] pathForResource:[sFilename stringByDeletingPathExtension] ofType:[sFilename pathExtension]];
}

+ (NSString*) getPathForSavedData:(NSString*)sFilename
{
	NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
	return [documentsDirectory stringByAppendingPathComponent:sFilename];
}

+ (BOOL)isFileExist:(NSString*)sPath;
{
	NSData *data = [NSData dataWithContentsOfFile:sPath];
	if( data != nil ) {
		return YES;
	}
	
	return NO;
}

+ (void)deleteFile:(NSString*)sPath {
	if ([self isFileExist:sPath]) {
		[[NSFileManager defaultManager] removeItemAtPath:sPath error:nil];
	}
}
/*
+ (float)getDistanceBetweenPoints:(CGPoint)point1 :(CGPoint)point2
{
	float x = point1.x - point2.x;
	float y = point1.y - point2.y;
	return sqrt(x * x + y * y);
}
*/

//#ifdef SCNetworkReachabilityRef

+ (BOOL)reachable:(NSString*)sURL {
	NSURL *pURL = [NSURL URLWithString: [sURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
	NSString *sHost = [pURL host];
	SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL, [sHost UTF8String]);
	SCNetworkReachabilityFlags flags;
	BOOL success = SCNetworkReachabilityGetFlags(reachability, &flags);
	BOOL bCanReach = success && (flags & kSCNetworkReachabilityFlagsReachable) && !(flags & kSCNetworkReachabilityFlagsConnectionRequired);
	if (reachability != nil) {
		CFRelease(reachability);
	}
	
	[self haveInternetConnection];
	
	return bCanReach;
}

+ (BOOL)haveInternetConnection {
	// Check for both WIFI and 3G
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	
	SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
	
	SCNetworkReachabilityFlags flags;
	BOOL success = SCNetworkReachabilityGetFlags(reachability, &flags);
	BOOL bCanReach = success && (flags & kSCNetworkReachabilityFlagsReachable) && !(flags & kSCNetworkReachabilityFlagsConnectionRequired);
	if (reachability != nil) {
		CFRelease(reachability);
	}
	
	return bCanReach;
}

//#endif

+ (NSString*)getMD5:(NSString*)sInputString {
	const char *cStr = [sInputString UTF8String];
	
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	
	CC_MD5( cStr, strlen(cStr), result );
	
	return [NSString 
			
			stringWithFormat: @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			
			result[0], result[1],
			
			result[2], result[3],
			
			result[4], result[5],
			
			result[6], result[7],
			
			result[8], result[9],
			
			result[10], result[11],
			
			result[12], result[13],
			
			result[14], result[15]
			
			];
}

+ (NSDate*)convertDateToLocalTimeZone:(NSDate*)pDate fromTimeZone:(NSString*)sTimeZone {
	// Convert to GMT+0
	NSTimeZone *oldTimeZone = [NSTimeZone timeZoneWithName:sTimeZone]; 
	NSTimeInterval oldTimeOffset = [oldTimeZone secondsFromGMT];
	NSDate *newDate = [pDate addTimeInterval:-oldTimeOffset];
	
	// Convert to local GMT
	NSTimeZone *localTimeZone = [NSTimeZone localTimeZone]; 
	NSTimeInterval localTimeOffset = [localTimeZone secondsFromGMT];
	newDate = [newDate addTimeInterval:localTimeOffset];
	
	return newDate;
}

+ (NSString*)formatNumber:(int)nValue {
	NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
	[numFormatter setNumberStyle:kCFNumberFormatterDecimalStyle];
	[numFormatter setRoundingMode:kCFNumberFormatterRoundDown];
	[numFormatter setMaximum:nil];
	
	NSString* sText = [numFormatter stringFromNumber:[NSNumber numberWithInt:nValue]];
	
	[numFormatter release];
	
	return sText;
}

+ (float)inBetweenFromValue:(float)fromValue toValue:(float)toValue percentage:(float)percentage {
	float result = toValue;
	
	if (toValue > fromValue) {
		result = fromValue + (toValue - fromValue) * percentage;
	}
	else if (toValue < fromValue) {
		result = fromValue - (fromValue - toValue) * percentage;
	}
	
	return result;
}

+ (void)logCurrentTime {
	NSLog(@"%@", [[NSDate date] description]);
}

+ (void)logError:(NSError*)error {
	if (error != NULL) {
		NSLog(@"%@\n%@\n%@", [error localizedDescription], [error localizedFailureReason], [error localizedRecoverySuggestion]);
	}
	else {
		NSLog(@"Error log is null!");
	}
}

+ (void)setAnimationForImageView:(UIImageView*)imageView filenameFormat:(NSString*)filenameFormat startIndex:(int)startIndex numOfFrame:(int)numOfFrame fps:(float)fps repeatCount:(int)repeatCount {
	NSMutableArray *array = [NSMutableArray array];
	
	for (int i=startIndex; i<startIndex+numOfFrame; i++) {
		[array addObject:[UIImage imageNamed:[NSString stringWithFormat:filenameFormat, i]]];
	}
	
	imageView.animationImages = array;
	
	imageView.animationDuration = (float)numOfFrame/fps;
	imageView.animationRepeatCount = repeatCount;
}

+ (void)rotateUIView:(UIView*)view angle:(float)fAngle speed:(float)fSpeed {
	float fDegree = ((long long)([NSDate timeIntervalSinceReferenceDate] * 1000) % (int)(fAngle*100.0f)) * (0.01f*fSpeed);
	view.transform = CGAffineTransformMakeRotation(RADIANS(fDegree));
}

+ (void)fadeInUIView:(UIView*)view speed:(float)fSpeed {
	if (view.alpha < 1.0f) {
		view.alpha += 0.1f*fSpeed;
	}
}

+ (void)fadeOutUIView:(UIView*)view speed:(float)fSpeed {
	if (view.alpha > 0) {
		view.alpha -= 0.1f*fSpeed;
	}
}

+ (void)flashUIView:(UIView*)view speed:(float)fSpeed {
	float alpha = ((long long)([NSDate timeIntervalSinceReferenceDate] * (1000.0f * fSpeed)) % 2000) * 0.001f;
	if (alpha > 1.0f) {
		alpha = 2.0f - alpha;
	}
	
	if (view.alpha < alpha - 0.1f*fSpeed) {
		view.alpha += 0.1f*fSpeed;
	}
	else if (view.alpha > alpha + 0.1f*fSpeed) {
		view.alpha -= 0.1f*fSpeed;
	}
	else {
		view.alpha = alpha;
	}
}

+ (void)launchAppStore:(NSString*)sAppID {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%@&mt=8", sAppID]]];
}

+ (void)launchAppStoreReview:(NSString*)sAppID {
	// Supposed user are going to open iPad review page if this app is iPad app
	if ([self isiPad]) {
		// iPad app don't have review page, go to app page
		[self launchAppStore:sAppID];
	}
	else {
		// this is iPhone app, go to review page
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", sAppID]]];
	}
}

@end
