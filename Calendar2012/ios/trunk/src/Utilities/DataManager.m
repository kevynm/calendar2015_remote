//
//  DataManager.m
//  
//
//  Created by Ming on 29/05/2009.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
//  Version 1.0

#import "DataManager.h"

@implementation DataManager

+ (NSString *) getStringForKey:(NSString *)key
{
	return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+ (NSString *) getStringForKey:(NSString *)key setDefault:(NSString *)defaultString;
{
	NSString *string = [self getStringForKey:key];
	
	if( string )
	{
		// return value when found
		return string;
	}
	else
	{
		// Set default value when key not found
		//[self setString:defaultString forkey:key];
		return defaultString;
	}
}

+ (int) getIntForKey:(NSString *)key
{
	NSString *string = [self getStringForKey:key];
	return [string intValue];
}

+ (int) getIntForKey:(NSString *)key setDefault:(int)defaultValue
{
	NSString *string = [self getStringForKey:key];
	
	if( string )
	{
		// return value when found
		return [string intValue];
	}
	else
	{
		// Set default value when key not found
		//[self setInt:defaultValue forkey:key];
		return defaultValue;
	}
}

+ (float) getFloatForKey:(NSString *)key
{
	NSString *string = [self getStringForKey:key];
	return [string floatValue];
}

+ (float) getFloatForKey:(NSString *)key setDefault:(float)defaultValue
{
	NSString *string = [self getStringForKey:key];
	
	if( string )
	{
		// return value when found
		return [string floatValue];
	}
	else
	{
		// Set default value when key not found
		//[self setFloat:defaultValue forkey:key];
		return defaultValue;
	}
}

+ (BOOL) getBoolForKey:(NSString *)key
{
	NSString *string = [self getStringForKey:key];
	
	if( [string intValue] == 0 )
		return NO;
	else
		return YES;
}

+ (BOOL) getBoolForKey:(NSString *)key setDefault:(BOOL)defaultValue
{
	NSString *string = [self getStringForKey:key];
	
	if( string )
	{
		// return value when found
		if( [string intValue] == 0 )
			return NO;
		else
			return YES;
	}
	else
	{
		// Set default value when key not found
		//[self setBool:defaultValue forkey:key];
		return defaultValue;
	}
}

+ (id) getObjectForKey:(NSString *)key
{
	return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+ (id) getObjectForKey:(NSString *)key setDefault:(id)defaultObject
{
	id object = [self getObjectForKey:key];
	
	if( object )
	{
		// return value when found
		return object;
	}
	else
	{
		// Set default value when key not found
		//[self setBool:defaultValue forkey:key];
		return defaultObject;
	}
}

+ (void) setString:(NSString *)string forkey:(NSString *)key
{
	[[NSUserDefaults standardUserDefaults] setObject:string forKey:key];
}

+ (void) setInt:(int)value forkey:(NSString *)key
{
	NSString *string = [NSString stringWithFormat:@"%d", value];
	[self setString:string forkey:key];
}

+ (void) setFloat:(float)value forkey:(NSString *)key
{
	NSString *string = [NSString stringWithFormat:@"%f", value];
	[self setString:string forkey:key];
}

+ (void) setBool:(BOOL)value forkey:(NSString *)key
{
	if( value )
	{
		[self setString:@"1" forkey:key];
	}
	else
	{
		[self setString:@"0" forkey:key];
	}
}

+ (void) setObject:(id)object forkey:(NSString *)key
{
	[[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
}

+ (BOOL) checkHaveKey:(NSString*)keyWord
{
	NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
	for( id key in dict )
	{
		NSString *keyName = key;
		
		if( [keyName rangeOfString:keyWord].length > 0 )
		{
			return YES;
		}
	}
	
	return NO;
}

+ (void) removeAllKey:(NSString*)keyWord
{
	NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
	for( id key in dict )
	{
		NSString *keyName = key;
		
		if( [keyName rangeOfString:keyWord].length > 0 )
		{
			[[NSUserDefaults standardUserDefaults] removeObjectForKey:keyName];
		}
	}
}

@end
