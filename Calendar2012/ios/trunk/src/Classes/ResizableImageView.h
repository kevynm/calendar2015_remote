//
//  ResizableImageView.h
//  TouchExplorer
//
//  Created by Yen Liew on 28/06/2009.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
// Extending UIImageView, so that it can be resized, and rotated.

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
//
// Define some constants for movement , rotation , and scaling
//

#define pi 3.1415926535897932384
#define GLOW_WIDTH 0 //6


@interface ResizableImageView : UIView 
{
	
	NSInteger vID;
	NSInteger uid;
	NSString *key;
	
	UIImage* 	image;
	
	UILabel*	iLabel;
	CGColorRef	highlightColor;

	// Indicate if the image is highlited
	BOOL highlight;
	BOOL isBeingTouched; // one of the finger is on me
	BOOL verbose;

	// Scaling ratiol
	CGSize	maxAllowableSize;
	CGSize	minAllowableSize;
	
	CGFloat vAngle;
	CGFloat vRatio;
	
	NSInteger vType;
	
	UIImageView* iFrameTL;
	UIImageView* iFrameTR;
	UIImageView* iFrameBL;
	UIImageView* iFrameBR;
	UIImageView* iFrameBG;
}

@property (nonatomic, readonly) UIImageView* iFrameTL;
@property (nonatomic, readonly) UIImageView* iFrameTR;
@property (nonatomic, readonly) UIImageView* iFrameBL;
@property (nonatomic, readonly) UIImageView* iFrameBR;
@property (nonatomic, readonly) UIImageView* iFrameBG;

@property (nonatomic, readonly) BOOL isBeingTouched ; 
@property (nonatomic, readonly) BOOL highlight ; 
@property (nonatomic, readonly) UIImage* 	image;
@property (nonatomic, readonly) UILabel* 	iLabel;

@property (nonatomic, retain) NSString* key;

@property (nonatomic) NSInteger vID;
@property (nonatomic) NSInteger uid;
@property (nonatomic) NSInteger vType;
@property (nonatomic) CGFloat vAngle;
@property (nonatomic) CGFloat vRatio;

-(void) setHightlightColor:(CGColorRef)color;
-(void) setNeedHighlight:(BOOL)enable;
-(id)initWithImage:(UIImage*) myImage;
//Resize the image by a preset step size
- (void)moveImage:(CGPoint)origLoc newLoc:(CGPoint)newLoc;
- (void)placeImage:(CGPoint)tarLoc;

// Resize the image by a given ratio
- (void)resizeImage:(CGFloat)byRatio;
- (void) mustResize: (CGFloat)byRatio;

// Resize the image by a given ratio
- (void)rotateImage:(CGFloat)byRadian;

-(void) fnSetFrame;
-(void) fnUpdateFrame;

// Resize the image by a given ratio
- (void)rotateImage:(CGPoint)prevFirstPoint prevSecondPoint:(CGPoint)prevSecondPoint
					firstPoint:(CGPoint)firstPoint secondPoint:(CGPoint)secondPoint;
@end

