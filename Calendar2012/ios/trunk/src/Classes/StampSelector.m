//
//  StampSelector.m
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "StampSelector.h"


@implementation StampSelector


#pragma mark -
#pragma mark Initialization


- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization.
		stampPaths = [[self getStampList] retain];
    }
    return self;
}

- (NSArray*)getStampList {
	NSArray *raw = [[NSBundle mainBundle] pathsForResourcesOfType:@"png" inDirectory:nil];
	return [raw filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lowercaseString CONTAINS %@",@"calendar_icon", nil]];
	
}

- (NSString*)getPathByIndex:(NSUInteger)idx {
	return [stampPaths objectAtIndex:idx];
}



#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	self.tableView.backgroundColor = [UIColor clearColor];
	self.tableView.pagingEnabled = NO;
	self.tableView.allowsSelection = YES;
	self.tableView.showsVerticalScrollIndicator = NO;
	
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.tableView.rowHeight = 70;
    }else{
        self.tableView.rowHeight = 40;
    }

	
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	[self scrollTop];
}

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [stampPaths count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
	if (stampPaths != nil) {
		NSString* imgSrc = [stampPaths objectAtIndex:[indexPath row]];
		UIImage* img = [UIImage imageWithContentsOfFile:imgSrc];
		cell.imageView.image = img;
		cell.imageView.transform = CGAffineTransformMakeRotation(M_PI/2);
		cell.selectedBackgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_highlighted.png"]] autorelease];
	}
    
    return cell;
	
}

- (void)scrollTop {
	NSUInteger ipath[2];
	ipath[0] = 0;
	ipath[1] = [stampPaths count] - 1;
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathWithIndexes:ipath length:2] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void)scrollRight {
	NSArray *ipaths = [self.tableView indexPathsForVisibleRows];
	NSInteger row = [(NSIndexPath*)[ipaths objectAtIndex:0] row];
	row += 4;
	if (row >= [stampPaths count]) row = [stampPaths count] - 1;
	NSUInteger ipath[2];
	ipath[0] = 0;
	ipath[1] = row;
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathWithIndexes:ipath length:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)scrollLeft {
	NSArray *ipaths = [self.tableView indexPathsForVisibleRows];
	NSInteger row = [(NSIndexPath*)[ipaths objectAtIndex:0] row];
	row -= 4;
	if (row < 0) row = 0;
	NSUInteger ipath[2];
	ipath[0] = 0;
	ipath[1] = row;
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathWithIndexes:ipath length:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
	
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
	 */
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[stampPaths release];
    [super dealloc];
}


@end

