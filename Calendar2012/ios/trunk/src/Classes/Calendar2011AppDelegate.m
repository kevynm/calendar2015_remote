//
//  Calendar2011AppDelegate.m
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Calendar2011AppDelegate.h"
#import "Calendar2011ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "DataManager.h"

#define FLURRY_EVENT_USE_STAMP      @"Use stamp"
#define FLURRY_EVENT_USE_PAN        @"Use pen"
#define FLURRY_EVENT_USE_ERASER     @"Use eraser"
#define FLURRY_EVENT_USE_TEXT       @"Use text"
#define FLURRY_EVENT_Tap_INFO       @"Tap Info"
#define FLURRY_EVENT_USE_CAL       @"Start Use Calendar"



@implementation Calendar2011AppDelegate

@synthesize window;
@synthesize viewController;


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after app launch. 
    [self.window addSubview:viewController.view];
    [self.window makeKeyAndVisible];

    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Calendar BGM 03" ofType:@"mp3"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    bgPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    bgPlayer.numberOfLoops = -1; //infinite
    
    // To minimize lag time before start of output, preload buffers      
    [bgPlayer prepareToPlay];
    
    [self loadBGM];
    
    
    [[MunerisManager sharedManager]logStartEvent:FLURRY_EVENT_USE_CAL withParameters:nil];
    
    
	return YES;
}
-(void)loadBGM{
    BOOL shouldPlayMusic = [DataManager getBoolForKey:BGM_ONOFF setDefault:YES];
    
    if(shouldPlayMusic){
        [bgPlayer play];
    }
}


-(void)playBG{
    [DataManager setBool:YES forkey:BGM_ONOFF];
    if(![bgPlayer isPlaying]){
        [bgPlayer play];
    }
}

-(void)stopBG{

    [DataManager setBool:NO forkey:BGM_ONOFF];
    if([bgPlayer isPlaying]){
        [bgPlayer pause];
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    
//    [bgPlayer pause];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    [self loadBGM];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
    [[MunerisManager sharedManager]logEndEvent:FLURRY_EVENT_USE_CAL withParameters:nil];
    
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application{
    [[MunerisManager sharedManager] updateEnvars];
    
    [self loadBGM];
}

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
