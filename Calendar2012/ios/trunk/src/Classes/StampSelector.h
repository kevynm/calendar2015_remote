//
//  StampSelector.h
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface StampSelector : UITableViewController {
	NSArray *stampPaths;
}

-(NSArray*) getStampList;
-(void) scrollTop;
-(void) scrollLeft;
-(void) scrollRight;
-(NSString*) getPathByIndex:(NSUInteger)idx;

@end
