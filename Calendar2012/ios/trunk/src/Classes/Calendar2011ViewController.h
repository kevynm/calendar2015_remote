//
//  Calendar2011ViewController.h
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarView.h"

#import "Muneris.h"
//#import "Facebook.h"
#import "MunerisAdsView.h"
//#import "MunerisFacebookDelegate.h"
#import "IAPManager.h"



@interface Calendar2011ViewController : UIViewController
<UIAlertViewDelegate,MunerisBannerAdsDelegate, MunerisInboxDelegate, 
MunerisTakeoverDelegate, MunerisOffersDelegate,IAPManagerDelegate> 
{
	NSUInteger activeMonth; //Month to display
    NSUInteger lastMonth; // last Month to display
	CalendarView *activeCalendarView;
	
	//month select mode
	NSMutableArray *monthSelectionBus, *monthSelectionUpImages, *monthSelectionDownImages;
	UIView *monthSelectionBar;
	UIButton *hideToolBarBu;
	
	//touch control
	UITouch *firstTouch;
	BOOL isTouchRecognized;
	//animation control
	BOOL isAnimating;
    
    CGFloat flipThreshold;
    CGSize hideToolBarBuSize;
    CGSize monthSelectionBarSize;
    
    IBOutlet UIButton *preMonBtn, *nextMonBtn;
    
    UIScrollView *monthSelectionSV;
    CGRect monthSelectionBarHideFrame;
    CGRect monthSelectionBarShowFrame;
    
    // iphone
    CGRect monthSelectionInnerBarFrame;
    
    MunerisAdsView* _munerisAdsView;
    UIButton *noAdv;
    UIAlertView *loadingAlert;
    
}
@property (nonatomic,retain) IBOutlet UIButton *preMonBtn, *nextMonBtn;



- (void)goMonth:(int)pMonth;

- (void)refreshAds;
- (void)checkMessages;
//- (void)facebookConnect;
- (void)checkVersion;
- (void)getEnvars;
- (void)showTakeOver;
- (void)showMoreApps;
- (void)removeAds;
- (void)loadAds;

-(BOOL)shouldShowAdv;
-(void)setShouldShowAdv:(BOOL)shouldShow;
-(void) showLoading;
-(void)hideLoading;
    
-(IBAction) onMonthLeftClick;
-(IBAction) onMonthRightClick;

@end

