
#import <CoreGraphics/CoreGraphics.h>

CGFloat distanceBetweenPoints(CGPoint first, CGPoint second);
CGFloat angleBetweenPoints(CGPoint first, CGPoint second) ;
CGFloat angleBetweenLines(CGPoint line1Start, CGPoint line1End, 
						  CGPoint line2Start, CGPoint line2End) ;
CGPoint vectorBetweenPoints(CGPoint firstPoint, CGPoint secondPoint); 


// The code probably should not go here
// it's clearer to move it to other files 

#define VERBOSE(verbose, ...) if (verbose) NSLog(__VA_ARGS__) 

