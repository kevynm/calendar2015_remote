//
//  TextEntryView.h
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResizableImageView.h"


@interface TextEntryView : UIView <UITextViewDelegate>{
	id textEntryViewDelegate;
	
	IBOutlet UIView *mainView;
	IBOutlet UITextView *textView;
	IBOutlet UIView *moveIndicator;
	
	ResizableImageView *resultView;
}

@property (readwrite, assign) id textEntryViewDelegate;

- (IBAction)doneButtonOnPressed;

@end
