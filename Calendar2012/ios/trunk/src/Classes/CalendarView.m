//
//  CalendarView.m
//  Calendar2011
//
//	A single calendar page
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
#define BGM_ONOFF @"bgMusic"
#import "CalendarView.h"
#import "DataManager.h"
#import "Calendar2011AppDelegate.h"
#import "Muneris.h"

#define FLURRY_EVENT_USE_PAN        @"Use pen"
#define FLURRY_EVENT_USE_ERASER     @"Use eraser"
#define FLURRY_EVENT_USE_TEXT       @"Use text"
#define FLURRY_EVENT_Tap_INFO       @"Tap Info"
#define FLURRY_EVENT_USE_CAL       @"Start Use Calendar"

@interface CalendarView (hidden)

-(void) initViews;
- (void)selectMonth:(int)pMonth;
- (void)deselectStampSelector;
- (void)selectTool:(UIButton *)toolBu;
-(void) addStampWithPath:(NSString*)path;
-(void) commitStamp;
- (void)saveDraw;
-(void)setupDimension;

@end



@implementation CalendarView


static inline CGFloat distanceBetweenPoints(CGPoint a, CGPoint b) {
	return sqrt((b.x-a.x)*(b.x-a.x) + (b.y-a.y)*(b.y-a.y));
}

-(void)setupDimension{
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        ctrlBarSize = CGSizeMake(768.0f, 80.0f);
        textEntryViewPos = CGPointMake(0.0f, 0.0f);
        
        
    }else{
        ctrlBarSize = CGSizeMake(320.0f, 50.0f);
        textEntryViewPos = CGPointMake(0.0f, 0.0f);
    }    
}


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
		activeMonth = [[NSCalendar currentCalendar] ordinalityOfUnit:NSMonthCalendarUnit inUnit:NSYearCalendarUnit forDate:[NSDate date]];
        [self setupDimension];
		[self initViews];
		
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame forMonth:(NSUInteger)month {
	
	self = [super initWithFrame:frame];
	if (self) {
		activeMonth = month;
        [self setupDimension];
		[self initViews];
		
	}
	return self;
}


- (void)initViews {
	//touch control
	touchCount = 0;
	
	//tool size
	penSizeLevel = 0;
	erasorSizeLevel = 0;
	
	//Background images
	calendarBG = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	[self addSubview:calendarBG];
	[calendarBG release];
	
	//Foreground
	calendarFG = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	[self addSubview:calendarFG];
	[calendarFG release];
	
	//Freehand drawing (also used as the commit layer)
    
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
         paintLayer = [[PaintingViewController alloc] initWithNibName:@"PaintingViewController" bundle:[NSBundle mainBundle]];
     }else{
         paintLayer = [[PaintingViewController alloc] initWithNibName:@"PaintingViewController_iphone" bundle:[NSBundle mainBundle]];
     }
    
	
//    [paintLayer.view setBounds:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	[self addSubview:paintLayer.view];
	
	//Overlay, used for placement of stamps and text views
	overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-ctrlBarSize.height)];
	overlay.userInteractionEnabled = NO;
	[self addSubview:overlay];
	[overlay release];
	
	//select today
	NSDate *today = [NSDate date];
	//!!!!testing
	today = [NSDate dateWithTimeInterval:60*60*24*(TEST_DAY_OFFSET) sinceDate:today];
	
	NSUInteger thisMonth = [[NSCalendar currentCalendar] ordinalityOfUnit:NSMonthCalendarUnit inUnit:NSYearCalendarUnit forDate:today];
	if (thisMonth == activeMonth) {
		//calculate the column
		//Note: column and weekday are start from 1
		
		//NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
		NSCalendar *calendar = [NSCalendar currentCalendar];
		
		/*
		int firstWeekDays[] = {7, 3, 3, 6, 1, 4, 6, 2, 5, 7, 3, 5};
		int firstWeekDay = firstWeekDays[activeMonth-1];
		int thisDay = [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:today];
		
		int column = ceilf(thisDay/7.0);
		int weekday = (thisDay+firstWeekDay-2)%7 +1;
		if (weekday < firstWeekDay) ++column;
		*/
		
		
		//int column = [[[NSCalendar currentCalendar] components:NSWeekdayOrdinalCalendarUnit fromDate:today] week];
		int column = [calendar ordinalityOfUnit:NSWeekdayOrdinalCalendarUnit inUnit:NSMonthCalendarUnit forDate:today];
		
		//if the first day week day is < this week day
		//column should +1 for the space
		//int weekday = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:today];
		//int weekday = [calendar ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSYearCalendarUnit forDate:today];
		int weekday = [[[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:today] weekday];
		int thisDay = [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:today];
		int firstWeekDay = ((weekday - thisDay) % 7) + 1;
		if (firstWeekDay <= 0) firstWeekDay += 7;
		//int column = ceilf(thisDay/7.0);
		if (weekday < firstWeekDay) ++column;
		
		//[calendar release];
		
		//after calculation, convert column and weekday back to start from 0
		column = (column-1)%5;
		--weekday;
		
		//add the date highlight
        int initX = 76;
		int initY = 521;
		int diffX = 105;
		int diffY = 89;
        
        if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            initX = 27;
            initY = 242;
            diffX = 105/2.4;
            diffY = 89/2.133;
        }

		UIImageView *dayHighlight = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"date_highlighted.png"]];
		dayHighlight.frame = CGRectMake(initX + diffX*weekday, initY + diffY*column, dayHighlight.frame.size.width, dayHighlight.frame.size.height);
		[self addSubview:dayHighlight];
		[dayHighlight release];
	}
	
	//Finally, the control bar
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [[NSBundle mainBundle] loadNibNamed:@"ControlBar" owner:self options:nil];          
    }else{
        [[NSBundle mainBundle] loadNibNamed:@"ControlBar-iphone" owner:self options:nil];
        [[NSBundle mainBundle] loadNibNamed:@"PenTools" owner:self options:nil];
    }

    penTools.frame = CGRectMake(320-penTools.frame.size.width-5, 120, penTools.frame.size.width, penTools.frame.size.height);
    [self addSubview:penTools];
    [penTools release];
    
	ctrlBar.frame = CGRectMake(0, self.frame.size.height-ctrlBarSize.height+2, ctrlBarSize.width, ctrlBarSize.height);
	[self addSubview:ctrlBar];
	[ctrlBar release];
	//don't have to retain button
	[penBu release];
	[erasorBu release];
	[textBu release];
	
	//and the selector view
	stampSelector = [[StampSelector alloc] initWithStyle:UITableViewStylePlain];
    
	
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        stampSelector.tableView.frame = CGRectMake(-45, -175, 70, 350);
        stampSelector.tableView.transform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-M_PI/2), -40, 292);
    }else{
        CGSize stampSelectorSize = CGSizeMake(40, 220);
//        stampSelector.tableView.frame = CGRectMake(-90, -175, stampSelectorSize.width, stampSelectorSize.height);
                stampSelector.tableView.frame = CGRectMake(-152, -125, stampSelectorSize.width, stampSelectorSize.height);
        
        stampSelector.tableView.transform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-M_PI/2), -40, 292);
    }
    
	
    
	stampSelector.tableView.delegate = self;
	[ctrlBar addSubview:stampSelector.tableView];
	
	//listen on terminate event
	//[[UIApplication sharedApplication] addObserver:self forKeyPath:UIApplicationDidEnterBackgroundNotification options:NSKeyValueObservingOptionPrior context:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];
	
	[self selectMonth:activeMonth];
    
    [self updateSoundBtn];
}

- (void)dealloc {
	[self saveDraw];
	
	[drawFileURL release];
	[stampSelector release];
	[paintLayer release];
    [super dealloc];
}

#pragma mark -
#pragma mark Other Event

- (void)receiveNotification:(NSNotification *)pN {
	if ([[pN name] isEqualToString:UIApplicationDidEnterBackgroundNotification]) {
		[self saveDraw];
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (paintLayer.usingTool) {
		[self deselectStampSelector];
		return;
	}
	
	//deselect tool button
	[self selectTool:nil];
	
	NSString *path = [stampSelector getPathByIndex:[indexPath row]];
	activeStampPath = path;
	//[self addStampWithPath:path];
	//[tableView deselectRowAtIndexPath:indexPath animated:NO];
	paintLayer.view.userInteractionEnabled = NO;
	overlay.userInteractionEnabled = YES;
}

- (void)onTextEntryViewCommit:(UIView *)resultView {
	[self drawView:resultView];
	[textEntryView removeFromSuperview];
	textEntryView = nil;
	[self selectTool:nil];
}

#pragma mark -
#pragma mark Procedure

- (void)selectMonth:(int)pMonth {
	if (pMonth < 1 || pMonth > 12) return;
	activeMonth = pMonth;
	
	//Background images
	NSArray *backgroundNames = [NSArray arrayWithObjects:
								@"JanBanner", @"FebBanner", @"MarBanner", @"AprBanner", @"MayBanner", @"JuneBanner",
								@"JulyBanner", @"AugBanner", @"SeptBanner", @"OctBanner", @"NovBanner", @"DecBanner", nil];
	NSString *bgName = [backgroundNames objectAtIndex:activeMonth-1];
	NSString *bgPath = [[NSBundle mainBundle] pathForResource:bgName ofType:@"png"];
	UIImage *bgimg = [UIImage imageWithContentsOfFile:bgPath];
	calendarBG.image = bgimg;
	
	//user drawing
	NSString *drawFilePath = [Utilities getPathForSavedData:[bgName stringByAppendingString:@"_draw.png"]];
	drawFileURL = [[NSURL alloc] initFileURLWithPath:drawFilePath];
	if (drawFileURL != nil && [Utilities isFileExist:[drawFileURL path]]) {
		NSData *imageData = [NSData dataWithContentsOfURL:drawFileURL];
		[paintLayer.paintingView setRootImage:[UIImage imageWithData:imageData]];
	}
	
	//Foreground
	NSArray *monthList = [NSArray arrayWithObjects:@"january", @"february", @"march", @"april", @"may", @"june", @"july", @"august", @"september", @"october", @"november", @"december", nil];
	NSString *fgName = [(NSString*) [monthList objectAtIndex:(activeMonth-1)] stringByAppendingString:@"2015"];
	NSString *fgPath = [[NSBundle mainBundle] pathForResource:fgName ofType:@"png"];
	
	UIImage *fgimg = [UIImage imageWithContentsOfFile:fgPath];
	calendarFG.image = fgimg;
    
    //adjust the pen tool frame
    CGRect tempToolRect = penTools.frame;
    if (activeMonth == 1)
    {
        tempToolRect.origin.y = 110;
    }
    else if (activeMonth == 2)
    {
        tempToolRect.origin.y = 140;
    }
    else
    {
        tempToolRect.origin.y = 120;
    }
    penTools.frame = tempToolRect;
}

- (void)deselectStampSelector {
	//[self commitStamp];
	activeStampPath = nil;
	[stampSelector.tableView deselectRowAtIndexPath:[stampSelector.tableView indexPathForSelectedRow] animated:NO];
}

- (void)selectTool:(UIButton *)toolBu {
 
    
	currentTool = toolBu;
	
	[penBu setBackgroundImage:nil forState:UIControlStateNormal];
	[erasorBu setBackgroundImage:nil forState:UIControlStateNormal];
	[textBu setBackgroundImage:nil forState:UIControlStateNormal];
	
	if (textEntryView != nil) {
		[textEntryView removeFromSuperview];
		textEntryView = nil;
	}
	
	if (toolBu != nil) {
		[self deselectStampSelector];
		[toolBu setBackgroundImage:[UIImage imageNamed:@"icon_highlighted.png"] forState:UIControlStateNormal];
	}
}

- (void)addStampWithPath:(NSString*)path {
	if (activeStamp != nil) [self commitStamp];
	UIImage *img = [UIImage imageWithContentsOfFile:path];
	ResizableImageView *stamp = [[[ResizableImageView alloc] initWithImage:img] autorelease];
	stamp.center = self.center;
	[overlay addSubview:stamp];
	//stamp.alpha = 0.6;
	stamp.frame = CGRectMake(-stamp.frame.size.width, -stamp.frame.size.height, stamp.frame.size.width, stamp.frame.size.height);
	activeStamp = stamp;
}

- (void)commitStamp {
	if (activeStamp == nil) return;
	
	[self drawView:activeStamp];
	[activeStamp removeFromSuperview];
	activeStamp = nil;
	
}

#pragma mark -
#pragma mark Interface

- (void)drawView:(UIView *)pView {
	UIImage *bgImg = [paintLayer.paintingView.imageStack lastObject];
	UIImage *resultImg;
	
	UIGraphicsBeginImageContext(CGSizeMake(self.frame.size.width,self.frame.size.height-ctrlBarSize.height));
	CGContextRef cref = UIGraphicsGetCurrentContext();
	[bgImg drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-ctrlBarSize.height)];
	
	CGContextTranslateCTM(cref, pView.center.x, pView.center.y);
	CGContextConcatCTM(cref, pView.transform);
	CGContextTranslateCTM(cref, -pView.bounds.size.width/2, -pView.bounds.size.height/2);
	[pView.layer renderInContext:UIGraphicsGetCurrentContext()];
	
	resultImg = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	[paintLayer.paintingView insertImage:resultImg];
}

- (BOOL)usingTool {
	return paintLayer.usingTool;
}

- (void)hideToolBar {
    [self selectTool:nil];
	ctrlBar.hidden = YES;
}

- (void)showToolBar {
	ctrlBar.hidden = NO;
}

- (void)saveDraw {
    NSLog(@"save draw");
	UIImage *drawImage = [[paintLayer.paintingView imageStack] lastObject];
	if (drawImage != nil) {
		NSData *imgData = UIImagePNGRepresentation(drawImage);
		if (imgData != nil && ![imgData writeToURL:drawFileURL atomically:YES]) {
			[[NSException exceptionWithName:@"Fail to save drawing" reason:nil userInfo:nil] raise];
		}
	}
}

#pragma mark -
#pragma mark Button Events

- (IBAction)onPenButtonClick {
	if (paintLayer.usingTool) return;
	if(ctrlBar.hidden)return;
    
	ZapTool *tool;
	
    [[MunerisManager sharedManager]logStartEvent:FLURRY_EVENT_USE_PAN withParameters:nil];
    
	if (currentTool != penBu) {
		[self selectTool:penBu];
		paintLayer.view.userInteractionEnabled = YES;
		overlay.userInteractionEnabled = NO;
		
		tool = [[ZapTool alloc] init];
		paintLayer.currentPaintingTool = tool;
		[tool release];
		tool.strokeColor = [UIColor blackColor];
	} else {
		tool = (ZapTool *) paintLayer.currentPaintingTool;
		penSizeLevel = (penSizeLevel+1)%2;
	}
	
	float toolSizes[2] = {1, 5};
	tool.lineWidth = toolSizes[penSizeLevel];
	NSString *imageNames[] = {@"bt_freehand.png", @"bt_freehand2.png"};
	[penBu setImage:[UIImage imageNamed:imageNames[penSizeLevel]] forState:UIControlStateNormal];

}

- (IBAction)onEraserButtonClick {
	if (paintLayer.usingTool) return;
    if(ctrlBar.hidden)return;
	
	ZapEraseTool *tool = (ZapEraseTool *) paintLayer.currentPaintingTool;
	    [[MunerisManager sharedManager]logStartEvent:FLURRY_EVENT_USE_ERASER withParameters:nil];
    
	if (currentTool != erasorBu) {
		[self selectTool:erasorBu];
		paintLayer.view.userInteractionEnabled = YES;
		overlay.userInteractionEnabled = NO;
		
		tool = [[ZapEraseTool alloc] init];
		paintLayer.currentPaintingTool = tool;
		[tool release];
	} else {
		erasorSizeLevel = (erasorSizeLevel+1)%2;
	}
	
	float toolSizes[] = {20, 60};
	tool.lineWidth = toolSizes[erasorSizeLevel];
	NSString *imageNames[] = {@"bt_eraser.png", @"bt_eraser2.png"};
	[erasorBu setImage:[UIImage imageNamed:imageNames[erasorSizeLevel]] forState:UIControlStateNormal];
}

- (IBAction)onTextButtonClick {
	if (paintLayer.usingTool) return;
	if(ctrlBar.hidden)return;
	if (currentTool == textBu) return;
	    [[MunerisManager sharedManager]logStartEvent:FLURRY_EVENT_USE_TEXT withParameters:nil];
    
	[self selectTool:textBu];
	paintLayer.view.userInteractionEnabled = NO;
	overlay.userInteractionEnabled = YES;
	
	textEntryView = [[TextEntryView alloc] initWithFrame:CGRectMake(textEntryViewPos.x, textEntryViewPos.y, self.frame.size.width, self.frame.size.height)];
	textEntryView.textEntryViewDelegate = self;
	[self addSubview:textEntryView];
	[textEntryView release];
}

- (IBAction)onStampLeftClick {
	[stampSelector scrollLeft];
}

- (IBAction)onStampRightClick {
	[stampSelector scrollRight];
}

- (void)goPreviousMonth {
	[self selectMonth:activeMonth-1];
}

- (void)goNextMonth {
	[self selectMonth:activeMonth+1];
}

#pragma mark -
#pragma mark Touches Events

#define STAMP_CAN_MOVE NO

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	//count touches
	++touchCount;
	
	NSArray *touchList = [touches allObjects];
	for (int i = 0; i < [touchList count]; i++) {
		UITouch *touch = [touchList objectAtIndex:i];
		if ( [touch.view isKindOfClass:[ResizableImageView class]] ) {
			ResizableImageView *stamp = (ResizableImageView*)touch.view;
			if (activeStamp == stamp) {
				[self.nextResponder touchesBegan:touches withEvent:event];
				return;
			}
			break;
		}
	}
	
	/*
	if (activeStamp != nil) {
		[self commitStamp];
	}
	 */
	if (activeStampPath != nil && activeStamp == nil) {
		UITouch *touch = [touchList objectAtIndex:0];
		[self addStampWithPath:activeStampPath];
        
//        CGPoint touchPop = [touch locationInView:overlay];
        
//        NSLog(@"touchPop %f , %f ",touchPop.x, touchPop.y);
//        NSLog(@"overlay %f , %f ",overlay.frame.size.width, overlay.frame.size.height);
        
		[activeStamp placeImage:[touch locationInView:overlay]];
		
		//commit immediately
		if (!STAMP_CAN_MOVE) {
			[self commitStamp];
		}
	}
	
	[self.nextResponder touchesBegan:touches withEvent:event];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	//count touches
	--touchCount;

	//_isZooming = NO;
	[self.nextResponder touchesCancelled:touches withEvent:event];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	/*
	if ([touches count] >= 2) {
		_isZooming = YES;
	}
	 */
	
	if (activeStamp != nil) {
		if ([touches count] == 1) {
			UITouch *touch = [touches anyObject];
			[activeStamp placeImage:[touch locationInView:overlay]];
		/*
		} else if ([touches count] == 2) {
			NSArray *touchlist = [touches allObjects];
			if ([touchlist count] != 2) return;
			
			UITouch *touch1 = [touchlist objectAtIndex:0];
			UITouch *touch2 = [touchlist objectAtIndex:1];
			
			CGFloat d1 = distanceBetweenPoints([touch1 previousLocationInView:overlay], [touch2 previousLocationInView:overlay]);
			CGFloat d2 = distanceBetweenPoints([touch1 locationInView:overlay], [touch2 locationInView:overlay]);
			[activeStamp rotateImage:[touch1 previousLocationInView:overlay] prevSecondPoint:[touch2 previousLocationInView:overlay] firstPoint:[touch1 locationInView:overlay] secondPoint:[touch2 locationInView:overlay]];
			[activeStamp resizeImage:(d2/d1)];
		 */
		}
	}
	[self.nextResponder touchesMoved:touches withEvent:event];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	//count touches
	--touchCount;
	
	//_isZooming = NO;
	
	/*
	NSUInteger numTaps = [[touches anyObject] tapCount];
	if (numTaps >= 2) {
		[self commitStamp];
	}
	 */
	
	if (touchCount == 0 && activeStamp != nil) {
		[self commitStamp];
	}
	
	[self.nextResponder touchesEnded:touches withEvent:event];
}

-(IBAction) onSoundButtonClick{
    // toggle
    
    BOOL BGM = [DataManager getBoolForKey:BGM_ONOFF setDefault:YES];
    Calendar2011AppDelegate *appDelegate = (Calendar2011AppDelegate*)[[UIApplication sharedApplication]delegate];

    if(BGM){
        [appDelegate stopBG];
    }else{
        [appDelegate playBG];
    }
    
    [self updateSoundBtn];
}

-(void)updateSoundBtn{
    
    BOOL BGM = [DataManager getBoolForKey:BGM_ONOFF setDefault:YES];
    NSString *iconName = nil;
    if(BGM){
        iconName = @"music_on.png";
    }else{
        iconName = @"music_off.png";
    }
    [soundBtn setImage:[UIImage imageNamed:iconName] forState:UIControlStateNormal];

}
@end
