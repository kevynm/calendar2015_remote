//
//  Calendar2011AppDelegate.h
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "MunerisDelegates.h"
#import "MunerisBaseAppDelegate.h"

#define BGM_ONOFF @"bgMusic"


@class Calendar2011ViewController;
@class AVAudioPlayer;

@interface Calendar2011AppDelegate : MunerisBaseAppDelegate <UIApplicationDelegate,MunerisInboxDelegate> {
    UIWindow *window;
    Calendar2011ViewController *viewController;
    
    AVAudioPlayer *bgPlayer;
    
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Calendar2011ViewController *viewController;


-(void)stopBG;
-(void)playBG;
-(void)loadBGM;
@end

