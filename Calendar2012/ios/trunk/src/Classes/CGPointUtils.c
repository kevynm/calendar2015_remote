#include "CGPointUtils.h"
#include <math.h>

CGFloat distanceBetweenPoints(CGPoint first, CGPoint second)
{
	CGFloat deltaX = second.x - first.x;
	CGFloat deltaY = second.y - first.y;

	return sqrt(deltaX*deltaX + deltaY*deltaY);
}

CGFloat angleBetweenPoints(CGPoint first, CGPoint second) 
{
 	CGFloat height = fabs(second.y - first.y);
 	CGFloat width = fabs(second.x - first.x);
 	CGFloat rads = atan2f(height, width);
	return rads;

 //	return radiansToDegrees(rads);
 //degs = degrees(atan((top - bottom)/(right - left)))
}

CGFloat angleBetweenLines(CGPoint line1Start, CGPoint line1End, CGPoint line2Start, CGPoint line2End) 
{
 	CGFloat a = line1End.x - line1Start.x;
 	CGFloat b = line1End.y - line1Start.y;
 	CGFloat c = line2End.x - line2Start.x;
 	CGFloat d = line2End.y - line2Start.y;
 
 	CGFloat rads = acos(((a*c) + (b*d)) / ((sqrt(a*a + b*b)) * (sqrt(c*c + d*d))));
 
	return rads;
 //return radiansToDegrees(rads);
}

CGPoint vectorBetweenPoints(CGPoint firstPoint, CGPoint secondPoint) 
{
	// NSLog(@"Point One: %f, %f", firstPoint.x, firstPoint.y);
	// NSLog(@"Point Two: %f, %f", secondPoint.x, secondPoint.y);

	CGFloat xDifference = firstPoint.x - secondPoint.x;
	CGFloat yDifference = firstPoint.y - secondPoint.y;

	CGPoint result = CGPointMake(xDifference, yDifference);

	return result;
}
