//
//  Calendar2011ViewController.m
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Calendar2011ViewController.h"
#import "Calendar2011AppDelegate.h"
#import "Utilities.h"
#import "CommonConfig.h"
#import "DataManager.h"

#define FLURRY_EVENT_USE_STAMP      @"Use stamp"
#define FLURRY_EVENT_USE_PAN        @"Use pen"
#define FLURRY_EVENT_USE_ERASER     @"Use eraser"
#define FLURRY_EVENT_USE_TEXT       @"Use text"
#define FLURRY_EVENT_Tap_INFO       @"Tap Info"
#define FLURRY_EVENT_USE_CAL       @"Start Use Calendar"


@interface Calendar2011ViewController (hidden)

- (void)hideToolBar;
- (void)showToolBar;
-(void)resetDimension;
-(void)updateMonthBarWithAnimated:(BOOL) animated;
-(void) purchaseNoAdd;
@end





@implementation Calendar2011ViewController
@synthesize  preMonBtn, nextMonBtn;

/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization
 }
 return self;
 }
 */


// Implement loadView to create a view hierarchy programmatically, without using a nib.
/*
 - (void)loadView {
 //get current date
 self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
 systemDate = [NSDate date];
 NSUInteger month = (NSUInteger) [[NSCalendar currentCalendar] ordinalityOfUnit:NSMonthCalendarUnit inUnit:NSYearCalendarUnit forDate:systemDate];
 activeCalendarView = [[CalendarView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024) forMonth:month];
 [self.view addSubview:activeCalendarView];
 [activeCalendarView release];
 }
 */



-(void)resetDimension{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        hideToolBarBuSize = CGSizeMake(70.0f, 70.0f);
        monthSelectionBarSize =  CGSizeMake(768.0f, 80.0f);
        flipThreshold = 6;
        
        
        monthSelectionBarHideFrame = CGRectMake(0, self.view.frame.size.height, monthSelectionBarSize.width, monthSelectionBarSize.height);                                    
        monthSelectionBarShowFrame = CGRectMake(0, self.view.frame.size.height-monthSelectionBarSize.height, self.view.frame.size.width, monthSelectionBarSize.height);
                                              
                                              
    }else{
        hideToolBarBuSize = CGSizeMake(40.0f, 40.0f);
        monthSelectionBarSize =  CGSizeMake(280, 50.0f);
        flipThreshold = 3;
        
        CGFloat offsetX = 50.0f;
//        monthSelectionBarShowFrame = CGRectMake(offsetX, self.view.frame.size.height, 220, monthSelectionBarSize.height);
//        monthSelectionBarHideFrame = CGRectMake(offsetX, self.view.frame.size.height-monthSelectionBarSize.height, 220, monthSelectionBarSize.height);
        
        monthSelectionInnerBarFrame = CGRectMake(offsetX, 0, 220, monthSelectionBarSize.height);
        monthSelectionBarHideFrame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, monthSelectionBarSize.height);
        monthSelectionBarShowFrame = CGRectMake(0, self.view.frame.size.height-monthSelectionBarSize.height, self.view.frame.size.width, monthSelectionBarSize.height);
        
    }    
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {    
    [self resetDimension];
	//constant setting
	NSString *names[] = {@"bt_m_Jan", @"bt_m_Feb", @"bt_m_Mar", @"bt_m_Apr", @"bt_m_May", @"bt_m_Jun", @"bt_m_Jul", @"bt_m_Aug", @"bt_m_Sep", @"bt_m_Oct", @"bt_m_Nov", @"bt_m_Dec"};
	monthSelectionUpImages = [[NSMutableArray alloc] initWithCapacity:12];
	for (int i=0; i<12; i++) {
		[monthSelectionUpImages addObject:[UIImage imageNamed:[names[i] stringByAppendingString:@".png"]]];
	}
	monthSelectionDownImages = [[NSMutableArray alloc] initWithCapacity:12];
	for (int i=0; i<12; i++) {
		[monthSelectionDownImages addObject:[UIImage imageNamed:[names[i] stringByAppendingString:@"_selected.png"]]];
	}
	
    

    
    [super viewDidLoad];
    
 
	
	//background
	NSDate *systemDate = [NSDate date];
	systemDate = [NSDate dateWithTimeInterval:60*60*24*(TEST_DAY_OFFSET) sinceDate:systemDate];
	activeMonth = (NSUInteger) [[NSCalendar currentCalendar] ordinalityOfUnit:NSMonthCalendarUnit inUnit:NSYearCalendarUnit forDate:systemDate];
	activeCalendarView = [[CalendarView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) forMonth:activeMonth];
	[self.view addSubview:activeCalendarView];
	[activeCalendarView release];
	
	//month selection bar

    
      if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
          monthSelectionBar = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, monthSelectionBarSize.height)];
          monthSelectionBar.hidden = YES;
          [self.view addSubview:monthSelectionBar];
          [monthSelectionBar release];
          
          
          float widthInit = 85;
          float widthStep = 50;
          monthSelectionBus = [[NSMutableArray alloc] initWithCapacity:12];
          for (int i=0; i<12; i++) {
              UIButton *monBu = [[UIButton alloc] initWithFrame:CGRectMake(widthInit+i*widthStep, 15, widthStep, 50)];
              [monthSelectionBus addObject:monBu];
              if (i+1==activeMonth) {
                  [monBu setBackgroundImage:[monthSelectionDownImages objectAtIndex:i] forState:UIControlStateNormal];
              } else {
                  [monBu setBackgroundImage:[monthSelectionUpImages objectAtIndex:i] forState:UIControlStateNormal];
              }
              [monBu addTarget:self action:@selector(monthButtonOnPressed:) forControlEvents:UIControlEventTouchUpInside];
              [monthSelectionBar addSubview:monBu];
              [monBu release];
          }
          
          
          
      }else{
   
          monthSelectionBar = [[UIView alloc] initWithFrame:monthSelectionBarShowFrame];
 
          monthSelectionSV = [[UIScrollView alloc]initWithFrame:monthSelectionInnerBarFrame];
          [monthSelectionSV setContentSize:CGSizeMake(40*12, monthSelectionBarSize.height)];
          [monthSelectionSV setShowsHorizontalScrollIndicator:NO];


          UIView *controlBarMonthView = [[[[NSBundle mainBundle] loadNibNamed:@"ControlBarMonth-iphone" owner:self options:nil]objectAtIndex:0]retain];  

          [controlBarMonthView addSubview:monthSelectionSV];      
          [monthSelectionBar addSubview:controlBarMonthView];
          
          [controlBarMonthView release];
          [monthSelectionSV release];
          
          
          [self.view addSubview:monthSelectionBar];
          monthSelectionBar.hidden = YES;

          
          float widthInit = 0;
          float widthStep = 40;
          monthSelectionBus = [[NSMutableArray alloc] initWithCapacity:12];
          for (int i=0; i<12; i++) {
              UIButton *monBu = [[UIButton alloc] initWithFrame:CGRectMake(widthInit+i*widthStep, 5, widthStep, widthStep)];
              [monthSelectionBus addObject:monBu];
              if (i+1==activeMonth) {
                  [monBu setBackgroundImage:[monthSelectionDownImages objectAtIndex:i] forState:UIControlStateNormal];
              } else {
                  [monBu setBackgroundImage:[monthSelectionUpImages objectAtIndex:i] forState:UIControlStateNormal];
              }
              [monBu addTarget:self action:@selector(monthButtonOnPressed:) forControlEvents:UIControlEventTouchUpInside];
              [monthSelectionSV addSubview:monBu];
              [monBu release];
          }
      }
    
    
    NSString *noAdvImg = nil;
    CGRect advRect = CGRectZero;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        noAdvImg = @"NoAdv_ipad.png";
        advRect = CGRectMake(30, 125, 233,122);
        
    }else{
        noAdvImg = @"NoAdv_iphone.png";
        advRect = CGRectMake(15, 60, 95,76);
    }

    
    noAdv = [[UIButton alloc]initWithFrame:advRect];

    [noAdv setBackgroundImage:[UIImage imageNamed:noAdvImg] forState:UIControlStateNormal];
	[noAdv addTarget:self action:@selector(purchaseNoAdd) forControlEvents:UIControlEventTouchUpInside];
    
    if([self shouldShowAdv]){
    [self.view addSubview:noAdv];    
    }
	


	//hide tool bar
	hideToolBarBu = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-hideToolBarBuSize.width, self.view.frame.size.height-hideToolBarBuSize.height, hideToolBarBuSize.width, hideToolBarBuSize.height)];
	[hideToolBarBu setBackgroundImage:[UIImage imageNamed:@"month_mode.png"] forState:UIControlStateNormal];
	[hideToolBarBu addTarget:self action:@selector(modeButtonOnPressed) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:hideToolBarBu];
	[hideToolBarBu release];
	
	//info page
	UIButton *infoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-hideToolBarBuSize.height, hideToolBarBuSize.width, hideToolBarBuSize.height)];
	[infoButton setBackgroundImage:[UIImage imageNamed:@"info_mode.png"] forState:UIControlStateNormal];
	[infoButton addTarget:self action:@selector(infoButtonOnPressed) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:infoButton];
	[infoButton release];
	
    if(firstTouch){
        [firstTouch release];
        firstTouch = nil;
    }
    
	isAnimating = NO;
    
    [self loadAds];
    [self showTakeOver];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark IAPManager

-(void) purchaseNoAdd{
    if ([Utilities haveInternetConnection]) {
        [[IAPManager sharedManager]addDelegate:self];
        
        if([[IAPManager sharedManager]canMakePurchases]){
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                [[IAPManager sharedManager]purchaseWithProduct:NOADV_INAPPKEY_IPAD];
            }else{
                [[IAPManager sharedManager]purchaseWithProduct:NOADV_INAPPKEY_IPHONE];
            }
        }else{
            // itunes store issues
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                                message:@"Cannot connect to iTunes Store"
                                                               delegate:nil 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
            [alertView show];
            [alertView release];
        
        }
        
    }else{
        // popup no internet
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No internet" 
                                                            message:@"Please connect to internet"
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        
    }
    
}

-(void) showLoading {
    
    loadingAlert = [[[UIAlertView alloc] initWithTitle:@"Connecting to iTunes Store\nPlease Wait..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil] autorelease];
    [loadingAlert show];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    // Adjust the indicator so it is up a few pixels from the bottom of the alert
    indicator.center = CGPointMake(loadingAlert.bounds.size.width / 2, loadingAlert.bounds.size.height - 50);
    [indicator startAnimating];
    [loadingAlert addSubview:indicator];
    [indicator release];

}

-(void)hideLoading{
    [loadingAlert dismissWithClickedButtonIndex:0 animated:YES];
}
-(void) IAPManagerStartTransaction{
    NSLog(@"start transaction");
    [noAdv setEnabled:NO];
    [self showLoading];
    
}
-(void) IAPManager:(id)_sender transactionSucceed:(SKPaymentTransaction*)transaction{
    
    NSString* identifier = transaction.payment.productIdentifier;
    
    NSString* compareString = nil;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        compareString = NOADV_INAPPKEY_IPAD;
    }else{
        compareString = NOADV_INAPPKEY_IPHONE;
    }
    
    
    {   NSLog(@"success: %@", identifier);
        
        if([identifier isEqualToString:compareString]){
            [self setShouldShowAdv:NO];
            [self removeAds];
            [noAdv removeFromSuperview];
            noAdv = nil;
            
            [[MunerisManager sharedManager]logEvent:@"In-App: Bought Remove Ads" withParameters:nil];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Transaction Succeed" 
                                                                message:@"Ads have been Removed"
                                                               delegate:nil 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
            [alertView show];
            [alertView release];

        }
        
    }
    
  [self hideLoading];
}


-(void) IAPManager:(id)_sender transactionFailed:(SKPaymentTransaction*)transaction{
    [noAdv setEnabled:YES];
    
    [self hideLoading];
    
}
-(void) IAPManager:(id)_sender transactionError:(SKPaymentTransaction*)transaction{
    
    [noAdv setEnabled:YES];
    [self hideLoading];
    
}



- (void)dealloc {
    [nextMonBtn release];
    [preMonBtn release];
	[monthSelectionBus release];
	[monthSelectionUpImages release];
	[monthSelectionDownImages release];
    
    [super dealloc];
}

#pragma mark Touch Event

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	if (!monthSelectionBar.hidden) {
		if (firstTouch != nil) {
            [firstTouch release];
            firstTouch = nil;
		}

        isTouchRecognized = NO;
        firstTouch = [[touches anyObject]retain];
	}
}

float CGPointLength(CGPoint p1, CGPoint p2) {
	float dxSq = p1.x - p2.x;
	dxSq *= dxSq;
	float dySq = p1.y - p2.y;
	dySq *= dySq;
	return sqrtf(dxSq + dySq);
}


- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if (isTouchRecognized || ![touches containsObject:firstTouch]) return;
	
	CGPoint pos = [firstTouch locationInView:[firstTouch view]];
	CGPoint prePos = [firstTouch previousLocationInView:[firstTouch view]];
	float touchSpeed = CGPointLength(pos, prePos);
	NSLog(@"touchSpeed %f", touchSpeed);
	if (touchSpeed > flipThreshold) {
		isTouchRecognized = YES;
		if (pos.x - prePos.x < 0) {
			[self goMonth:activeMonth+1];
		} else {
			[self goMonth:activeMonth-1];
		}
	}
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if (![touches containsObject:firstTouch]) return;
	
	if (!isTouchRecognized) {
		//!!!! disabled
		//[self showToolBar];
	}
	if(firstTouch){
        [firstTouch release];
        firstTouch = nil;
    }
	
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	if (![touches containsObject:firstTouch]) return;
    
    if(firstTouch){
        [firstTouch release];
        firstTouch = nil;
    }
}

#pragma mark Button Event and Procedure

- (void)monthButtonOnPressed:(UIButton *)sender {
	//flip page
	int currentValue = [monthSelectionBus indexOfObject:sender];
	[self goMonth:currentValue+1];
}


-(IBAction) onMonthLeftClick{
    [self goMonth:activeMonth-1];
}
-(IBAction) onMonthRightClick{
    [self goMonth:activeMonth+1];
}


- (void)goMonth:(int)pMonth {
	if (pMonth == activeMonth || pMonth < 1 || pMonth > 12 || isAnimating) return;
	
	//update month selection button
	UIButton *monthSelectionBu;
	monthSelectionBu = [monthSelectionBus objectAtIndex:activeMonth-1];
	[monthSelectionBu setBackgroundImage:[monthSelectionUpImages objectAtIndex:activeMonth-1] forState:UIControlStateNormal];
	monthSelectionBu = [monthSelectionBus objectAtIndex:pMonth-1];
	[monthSelectionBu setBackgroundImage:[monthSelectionDownImages objectAtIndex:pMonth-1] forState:UIControlStateNormal];

	
	
	int goDirection = pMonth - activeMonth;
    lastMonth = activeMonth;
	activeMonth = pMonth;
	
    [self updateMonthBarWithAnimated:YES];
    
    
	//CGRect initFrame = goDirection < 0 ? CGRectMake(-768, 0, 768, 1024) : CGRectMake(1536, 0, 768, 1024);
	CGRect leftFrame = CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
	CGRect rightFrame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
	CGRect centerFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height); 
	float animationDuration = 1;
	
	isAnimating = YES;
	
	//slide out old one
	[UIView beginAnimations:@"slideOutOld" context:activeCalendarView];
	activeCalendarView.frame = goDirection < 0 ? rightFrame : leftFrame;
	[UIView setAnimationDuration:animationDuration];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView commitAnimations];
	
	//create new one
	CalendarView *newCalendarView = [[CalendarView alloc] initWithFrame:goDirection < 0 ? leftFrame : rightFrame forMonth:activeMonth];
	[newCalendarView hideToolBar];
	activeCalendarView.userInteractionEnabled = NO;
	//[self.view addSubview:activeCalendarView];
	[self.view insertSubview:newCalendarView aboveSubview:activeCalendarView];
	activeCalendarView = newCalendarView;
	[activeCalendarView release];
	
	//slide in new one
	[UIView beginAnimations:nil context:nil];
	activeCalendarView.frame = centerFrame;
	[UIView setAnimationDuration:animationDuration];
	[UIView commitAnimations];
    
}

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
	if ([animationID isEqualToString:@"slideOutOld"]) {
		UIView *oldView = (UIView *)context;
		[oldView removeFromSuperview];
		isAnimating = NO;
	}
}

- (void)modeButtonOnPressed {
	if (activeCalendarView.usingTool) {
		return;
	}
	
	if (monthSelectionBar.hidden) {
		[self hideToolBar];
	} else {
		[self showToolBar];
	}
}

- (void)hideToolBar {
	[activeCalendarView hideToolBar];
	[activeCalendarView setUserInteractionEnabled:NO];
	
	//!!!! hide hideToorBarBu
	//hideToolBarBu.hidden = YES;
	
	//show month selection bar
    
	monthSelectionBar.hidden = NO;
	monthSelectionBar.frame = monthSelectionBarShowFrame;
    
    [self updateMonthBarWithAnimated:NO];
    
}
-(void)updateMonthBarWithAnimated:(BOOL) animated{
    NSLog(@"activeMonth %d",activeMonth);
    int secondPageEdge = 8;
    if(animated && lastMonth>5 && lastMonth <9){
        secondPageEdge = 9;
    }
    
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if(activeMonth >=1 && activeMonth<=5){
            
            if([monthSelectionSV isKindOfClass:NSClassFromString(@"UIScrollView")]){
                UIScrollView *scrollView = (UIScrollView*)monthSelectionSV;
                [scrollView scrollRectToVisible:CGRectMake(0, 40, 220, 40) animated:animated];
            }
            
        }else if (activeMonth >5 && activeMonth<secondPageEdge){
            if([monthSelectionSV isKindOfClass:NSClassFromString(@"UIScrollView")]){
                UIScrollView *scrollView = (UIScrollView*)monthSelectionSV;
                [scrollView scrollRectToVisible:CGRectMake(40*4, 40, 220, 40) animated:animated];
            }
            
        }else if (activeMonth >=secondPageEdge && activeMonth<=12){
            if([monthSelectionSV isKindOfClass:NSClassFromString(@"UIScrollView")]){
                UIScrollView *scrollView = (UIScrollView*)monthSelectionSV;
                [scrollView scrollRectToVisible:CGRectMake(40*10, 40, 220, 40) animated:animated];
            }
            
        }
    }
}

- (void)showToolBar {
	[activeCalendarView showToolBar];
	[activeCalendarView setUserInteractionEnabled:YES];
	
	//!!!! show hideToorBarBu
	//hideToolBarBu.hidden = NO;
	
	//hide month selection bar
	monthSelectionBar.hidden = YES;
	monthSelectionBar.frame = monthSelectionBarHideFrame;
    [self updateMonthBarWithAnimated:NO];
}

- (void)infoButtonOnPressed {
	if (activeCalendarView.usingTool) {
		return;
	}
	
	/*
	[[ [[UIAlertView alloc] initWithTitle:nil
								  message:@"This app is developed by\nDream Cortex"
								 delegate:self
						cancelButtonTitle:nil
						otherButtonTitles:@"View More", @"Close", nil]
	  autorelease] show];
	*/
	
    [[MunerisManager sharedManager]logEvent:FLURRY_EVENT_Tap_INFO withParameters:nil];
    
//	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://target.georiot.com/Proxy.ashx?grid=1173&GR_URL=http%3A%2F%2Fitunes.apple.com%2Fartist%2Fdream-cortex%2Fid310152451%3Fmt%3D8&TRACK=CAL2012"]];
    
    [self showMoreApps];
}

/*
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.com/apps/dreamcortex"]];
	}
}
 */

#pragma mark - Muneris Navigator

//- (void)showOffers;
//{
//    [[MunerisManager sharedManager] showOffers:self delegate:self];
//}


- (void)refreshAds;
{
    if (_munerisAdsView) {
        [_munerisAdsView refreshAds]; 
    }
}


- (void)checkMessages;
{
    [[MunerisManager sharedManager] checkMessages:[NSArray arrayWithObjects:[NSNumber numberWithInt:MunerisCreditsMessage],[NSNumber numberWithInt:MunerisTextMessage],nil] delegate:self];
}


//- (void)facebookConnect:(id)sender;
//{
//    [[MunerisManager sharedManager] fbAccess:self];
//}


- (void)checkVersion;
{
    [[MunerisManager sharedManager] checkAppVersion];
}


- (void)getEnvars;
{
    [[MunerisManager sharedManager] updateEnvars];
}


- (void)showTakeOver;
{
    if([self shouldShowAdv]){
        [[MunerisManager sharedManager] loadTakeover:@"featured" withViewController:self delegate:self];
    }
}


- (void)showMoreApps;
{
    [[MunerisManager sharedManager] loadTakeover:@"moreapps" withViewController:self delegate:self];
}

- (void)removeAds;
{
    if (_munerisAdsView) {
        [_munerisAdsView removeFromSuperview];
    }
    _munerisAdsView = nil;
    
}

- (void)loadAds;
{
    if([self shouldShowAdv]){
        if (!_munerisAdsView) {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                [[MunerisManager sharedManager] loadAdswithBannerSize:MunerisBannerAdsSizes728x90 withZone:@"zone" withDelegate:self withUIViewController:self];
            }
            else{
                [[MunerisManager sharedManager] loadAdswithBannerSize:MunerisBannerAdsSizes320x50 withZone:@"zone" withDelegate:self withUIViewController:self];
            }
        }
        
    }
}

#pragma mark - Muneris Delegates

//// Muneris Facebook Delegate
//
//-(void) fbReady:(Facebook*) facebook;
//{
//    MLOG(@"from app facebook %@" ,  facebook.accessToken);
//    [facebook requestWithGraphPath:@"me" andDelegate:self];
//}
//
//
//-(void)request:(FBRequest *)request didLoad:(id)result;
//{
//    MLOG(@"%@",result);
//}

// Muneris Ads Delegate

-(void) didReceiveAds:(MunerisAdsView *)view;
{
    view.frame = CGRectMake(self.view.frame.size.width/2 - view.frame.size.width/2, 0, view.frame.size.width, view.frame.size.height);
    _munerisAdsView = view;
    [self.view addSubview:_munerisAdsView];
}


-(void) didFailToReceiveAds:(NSError *)error;
{
    _munerisAdsView = nil;
}


// Muneris Messages Delegate

-(void) didRecievedMessage:(MunerisMessages *)msgs;
{
    MLOG(@"Viewcontroller received inbox %@", msgs.messages);
    
    for (NSDictionary* msg in msgs.messages) {
        switch ([[msg objectForKey:@"MunerisMessageType"] intValue]) {
            case MunerisCreditsMessage:{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",[msg objectForKey:@"subj"]] message:[NSString stringWithFormat:@"%@", [[msg objectForKey:@"body"] objectForKey:@"credits"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
                break;
                
            case MunerisTextMessage:{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",[msg objectForKey:@"subj"]] message:[NSString stringWithFormat:@"%@", [[msg objectForKey:@"body"] objectForKey:@"te0xt"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }
                break;
                
            default:
                break;
        }
    }
}

-(BOOL)shouldShowAdv{
   return [DataManager getBoolForKey:SHOULD_SHOW_ADV setDefault:YES];
}

-(void)setShouldShowAdv:(BOOL)shouldShow{
    
    [DataManager setBool:shouldShow forkey:SHOULD_SHOW_ADV];
}



// Muneris Takeover Delegate
-(BOOL) shouldShowTakeover:(NSDictionary*) takeover;
{
    MLOG(@"shouldshow %@",takeover);
    return YES;
}

-(void) didFinishedLoadingTakeover:(NSDictionary*) takeoverInfo;
{
    
    
    //  MunerisWebView* webView = [[takeoverInfo objectForKey:@"moreapps"] objectForKey:@"MunerisWebView"];
    //  [webView.titleBarItem setTitle:@"More Apps"];
    //  webView.titleBarItem.rightBarButtonItem = nil;
    //  [webView setTitleBarColor:[UIColor whiteColor]];
    //[self checkMessages];
    MLOG(@"takeover loading finshed %@" , takeoverInfo);
}


-(void) didFailToLoadTakeover:(NSDictionary*) takeoverInfo;
{
    MLOG(@"takeover *FAILED* %@", takeoverInfo);  
}


@end
