    //
//  PaintingViewController.m
//  BabyPainter
//
//  Created by Dream Cortex on 13/10/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PaintingViewController.h"


@implementation PaintingViewController

- (IBAction) quitOnClick {
	[self dismissModalViewControllerAnimated:YES];
}


 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
		NSLog(@"Alloc %@", self.description);
    }
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
    NSLog(@"PaintingViewController %f , %f",self.view.frame.size.width,self.view.frame.size.height);
	/*
	[self setCurrentPaintingTool:[[ZapTool alloc] init]];
	//float f = 255.0f;
	//((ZapTool*)self.currentPaintingTool).strokeColor = [UIColor colorWithRed:(0/f) green:(0/f) blue:(0/f) alpha:1.0f];
	((ZapTool*)self.currentPaintingTool).strokeColor = [UIColor blackColor];
	((ZapTool*)self.currentPaintingTool).lineWidth = 1;
	*/
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return NO;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	[super touchesMoved:touches withEvent:event];
	
	if (self.view.userInteractionEnabled == NO) {
		[self touchesEnded:touches withEvent:event];
	}
}


- (void)dealloc {
    [super dealloc];
}
@end
