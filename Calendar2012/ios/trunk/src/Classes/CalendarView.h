//
//  CalendarView.h
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 Dream Cortex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaintingViewController.h"
#import "StampSelector.h"
#import "TextEntryView.h"
#import "ResizableImageView.h"

@interface CalendarView : UIView <UITableViewDelegate>{
	NSUInteger					activeMonth;		//Month to display
	int							touchCount;			//Count current touches
	NSString					*activeStampPath;
	ResizableImageView			*activeStamp;
	UIImageView					*calendarBG;		//Background image
	UIImageView					*calendarFG;		//Calendar grid
	UIImageView					*todayMarker;		//red circle around today
	PaintingViewController		*paintLayer;		//Freehand painting library
	UIView						*overlay;			//Used for placement of text/stamps
	
	NSURL *drawFileURL;
	
	IBOutlet UIView				*ctrlBar;			//control bar
	IBOutlet UIButton			*penBu, *erasorBu, *textBu ,*soundBtn;
	StampSelector				*stampSelector;
	UIButton					*currentTool;
	//BOOL				_isZooming;
	int							penSizeLevel, erasorSizeLevel;
	TextEntryView				*textEntryView;
	
	CGPoint				_cachedCenter;
	CGRect				_cachedBounds;
	CGAffineTransform	_cachedTransform;
	BOOL				_stampMoving;
	
    CGSize ctrlBarSize;
    CGPoint textEntryViewPos;
    
    IBOutlet UIView *penTools;
    
    
}

//custom initialization
-(id) initWithFrame:(CGRect)frame forMonth:(NSUInteger)month;

- (BOOL)usingTool;
- (void)hideToolBar;
- (void)showToolBar;
- (void)drawView:(UIView *)pLayer;
-(IBAction) onSoundButtonClick;
-(IBAction) onPenButtonClick;
-(IBAction) onEraserButtonClick;
-(IBAction) onTextButtonClick;
- (void)onTextEntryViewCommit:(UIView *)resultView;
-(IBAction) onStampLeftClick;
-(IBAction) onStampRightClick;
-(void)updateSoundBtn;
@end
