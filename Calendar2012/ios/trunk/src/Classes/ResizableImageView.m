//  ResizableImageView.m
//  TouchExplorer
//
//  Created by Yen Liew on 28/06/2009.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ResizableImageView.h"
#import "CGPointUtils.h"


@implementation ResizableImageView

@synthesize isBeingTouched;
@synthesize highlight;
@synthesize vAngle, vRatio, image;
@synthesize vID, vType, uid, key;
@synthesize iLabel;
@synthesize iFrameTL, iFrameTR, iFrameBL, iFrameBR, iFrameBG;

- (void) dealloc
{
	CGColorRelease(highlightColor);
	[image release];
	[iFrameTL release];
	[iFrameTR release];
	[iFrameBL release];
	[iFrameBR release];
	[iFrameBG release];
	[iLabel release];
	[super dealloc];
}


-(id)initWithImage:(UIImage*) myImage
{
	vAngle = 0;
	vRatio = 1;
	vType = 0;
	verbose = FALSE;
	image = [myImage retain];
	
	CGRect frame = CGRectMake(0,0, image.size.width+(GLOW_WIDTH*2), image.size.height+(GLOW_WIDTH*2));
	if (self = [self initWithFrame:frame])
	{
		self.contentMode = UIViewContentModeCenter;
		[self setUserInteractionEnabled:YES];
		[self setMultipleTouchEnabled:YES];
		self.transform = CGAffineTransformIdentity;
	}
	isBeingTouched = FALSE;
	highlight 	   = FALSE;
	self.opaque		= NO;
	

	return self;
}


-(void) setNeedHighlight:(BOOL)enable
{
	
	if (highlight != enable) {
		highlight = enable;
		[self setNeedsDisplay];	
	}
	[self fnUpdateFrame];
}

-(void) fnSetFrame
{
	int tLen = 10;
	CGRect tFrame;
	if (iFrameTL == NULL) {
		
		iFrameBG = [[UIImageView alloc] init];
		//iFrameBG.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"mask" ofType:@"png"]];
		[iFrameBG setUserInteractionEnabled:NO];
		//iFrameBG.opaque = YES;
		iFrameBG.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.2];
		tFrame = iFrameBG.frame;
		tFrame.size = self.frame.size;
		iFrameBG.frame = tFrame;
		[self addSubview:iFrameBG];
		[self sendSubviewToBack:iFrameBG];
		
		iFrameTL = [[UIImageView alloc] init];	
		iFrameTL.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"corner" ofType:@"png"]];
		tFrame = iFrameTL.frame;
		tFrame.size.width = 10; 
		tFrame.size.height = 10;
		//iFrameTL.image.size;
		iFrameTL.frame = tFrame;		
		[iFrameTL setUserInteractionEnabled:NO];
		[self addSubview:iFrameTL];
		[self bringSubviewToFront:iFrameTL];
		//iFrameTL.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
		
		iFrameTR = [[UIImageView alloc] init];	
		iFrameTR.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"corner" ofType:@"png"]];
		tFrame = iFrameTL.frame;
		tFrame.origin.x = self.frame.size.width-tLen;
		tFrame.size.width = 10; 
		tFrame.size.height = 10;
		//tFrame.size = iFrameTR.image.size;
		iFrameTR.frame = tFrame;		
		[iFrameTR setUserInteractionEnabled:NO];
		[self addSubview:iFrameTR];
		[self bringSubviewToFront:iFrameTR];
		
		iFrameBL = [[UIImageView alloc] init];	
		iFrameBL.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"corner" ofType:@"png"]];
		tFrame = iFrameBL.frame;
		tFrame.origin.y = self.frame.size.height-tLen;
		tFrame.size.width = 10; 
		tFrame.size.height = 10;
		//tFrame.size = iFrameBL.image.size;
		iFrameBL.frame = tFrame;		
		[iFrameBL setUserInteractionEnabled:NO];
		[self addSubview:iFrameBL];
		[self bringSubviewToFront:iFrameBL];
		
		iFrameBR = [[UIImageView alloc] init];	
		iFrameBR.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"corner" ofType:@"png"]];
		tFrame = iFrameBR.frame;
		tFrame.origin.x = self.frame.size.width-tLen;
		tFrame.origin.y = self.frame.size.height-tLen;
		tFrame.size.width = 10; 
		tFrame.size.height = 10;
		//tFrame2.size = iFrameBR.image.size;
		iFrameBR.frame = tFrame;		
		[iFrameBR setUserInteractionEnabled:NO];
		[self addSubview:iFrameBR];
		[self bringSubviewToFront:iFrameBR];
		
	}
	[self fnUpdateFrame];
}

-(void) fnUpdateFrame
{
	
	if (highlight) {
		if (iFrameTL == NULL) {
			NSLog(@"add frame");
			[self fnSetFrame];
		}
		else {
			[self sendSubviewToBack:iFrameBG];
			iFrameTL.hidden = NO;
			iFrameTR.hidden = NO;
			iFrameBL.hidden = NO;
			iFrameBR.hidden = NO;
			iFrameBG.hidden = NO;
		}
	}
	else {
		if (iFrameTL != NULL) {
			iFrameTL.hidden = YES;
			iFrameTR.hidden = YES;
			iFrameBL.hidden = YES;
			iFrameBR.hidden = YES;
			iFrameBG.hidden = YES;
			/*[iFrameTL removeFromSuperview];
			[iFrameTL release];
			iFrameTL = NULL;
			[iFrameTR removeFromSuperview];
			[iFrameTR release];
			iFrameTR = NULL;
			[iFrameBL removeFromSuperview];
			[iFrameBL release];
			iFrameBL = NULL;
			[iFrameBR removeFromSuperview];
			[iFrameBR release];
			iFrameBR = NULL;*/
		}
	}	
}

-(void) setHightlightColor:(CGColorRef)color
{
	if (color != nil)
		highlightColor = CGColorRetain(color);
}

- (void)rotateImage:(CGPoint)prevFirstPoint prevSecondPoint:(CGPoint)prevSecondPoint
					firstPoint:(CGPoint)firstPoint secondPoint:(CGPoint)secondPoint
{
	CGFloat newAngle = 0.0f;
	// Get distance between points
	CGFloat previousDistance = distanceBetweenPoints(prevFirstPoint, prevSecondPoint);
	CGFloat currentDistance = distanceBetweenPoints(firstPoint, secondPoint);

	if (previousDistance ==0 || currentDistance ==0)
	{
		VERBOSE(verbose, @"prevent divided by 0!!");
		return;
	}
	CGPoint previousDifference = vectorBetweenPoints(prevFirstPoint, prevSecondPoint);
	CGFloat xDifferencePrevious = previousDifference.x;

	// Use acos to get angle to avoid issue with the denominator being 0
	CGFloat previousRotation = acos(xDifferencePrevious / previousDistance); // adjacent over hypotenuse
	if (previousDifference.y < 0) 
	{
		// account for acos of angles in quadrants III and IV
		// 
		previousRotation *= -1;
	}

	// Get current angle
	CGPoint currentDifference = vectorBetweenPoints(firstPoint, secondPoint);
	CGFloat xDifferenceCurrent = currentDifference.x;

	// Use acos to get angle to avoid issue with the denominator being 0
	CGFloat currentRotation = acos(xDifferenceCurrent / currentDistance); // adjacent over hypotenuse
	if (currentDifference.y < 0) {
	// account for acos of angles in quadrants III and IV
	currentRotation *= -1;
	}

	newAngle = currentRotation - previousRotation;
	
	
	if (fabs(newAngle) > 0.009f && fabs(newAngle) < 0.2f)
	{ 
		self.transform = CGAffineTransformRotate(self.transform, newAngle);
		VERBOSE(verbose, @"RotateImage: Image is rotated by radian =%f", 
			newAngle);
		vAngle += newAngle;
	}
	else
		VERBOSE(verbose, @"RotateImage: Skip rotation as too small =%f", 
				newAngle);
	if (vAngle > 6.283) {
		vAngle -= 6.283;
	}
	if (vAngle < -6.283) {
		vAngle += 6.283;
	}
	//NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];		
	//[prefs setFloat:vAngle forKey:[[NSString alloc] initWithFormat:@"img%da", self.vID]];
}

- (void)rotateImage:(CGFloat)byRadian
{
	self.transform = CGAffineTransformRotate(self.transform, byRadian);
	vAngle = byRadian;
	//NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];		
	//[prefs setFloat:byRadian forKey:[[NSString alloc] initWithFormat:@"img%da", self.vID]];
}

- (void) resizeImage: (CGFloat)byRatio
{
	
	
	// This is to prevent the false resize call
	if (byRatio < 0.8 || byRatio > 1.5)
		return;
	CGFloat ratio =1.0f;
	
	
	
	if (vType == 0) {
		if (vRatio * byRatio > 1) {
			byRatio = 1 / vRatio;
		}
		else if (vRatio * byRatio < (1.0/6.0)) {
			NSLog(@"small");
			return;
			byRatio = (1.0/6.0) / vRatio;
		}
	}
	else if (vType == 2) {
		if (vRatio * byRatio < (1.0/1.0) && byRatio < 1) {
			CGRect tFrame = self.frame;
			if (tFrame.size.width <= 20 && tFrame.size.height <= 20) {
				return;
			}

		}
	}
	else if (vType == 3) {
		if (vRatio * byRatio > 1) {
			byRatio = 1 / vRatio;
		}
		else if (vRatio * byRatio < (1.0/6.0)) {
			NSLog(@"small");
			return;
			byRatio = (1.0/6.0) / vRatio;
		}
	}
	
	ratio = byRatio;
	vRatio *= byRatio;
	
	self.transform = CGAffineTransformScale(self.transform, 
											ratio, ratio);
	
	
	
	VERBOSE(verbose, @"ResizeImage: Resize Ratio =%f. ", 
					ratio);
	
	//NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];	

	//[prefs setFloat:vRatio forKey:[[NSString alloc] initWithFormat:@"img%dr", self.vID]];

}

// since resizeImage will skip when byRatio < 0.8 || > 1.5
- (void) mustResize: (CGFloat)byRatio
{

	CGFloat ratio =1.0f;
	ratio = byRatio;
	
	self.transform = CGAffineTransformScale(self.transform, 
											ratio, ratio);
	
	vRatio *= byRatio;
	
	VERBOSE(verbose, @"ResizeImage: Resize Ratio =%f. ", 
			ratio);
	
	//NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];	
	
	//[prefs setFloat:vRatio forKey:[[NSString alloc] initWithFormat:@"img%dr", self.vID]];
	
}

//
// TBD: Do we allow it to move out of screen ?
- (void)moveImage:(CGPoint)origLoc newLoc:(CGPoint)newLoc
{
	CGPoint currentlocation = self.center;

	CGFloat moveX = newLoc.x - origLoc.x;
	CGFloat moveY = newLoc.y - origLoc.y;
	
	if (moveX == 0 && moveY == 0) {
		return;
	}

	if (fabs(moveX) > 25.0f || fabs(moveY) > 50.0f)
		return;

	currentlocation.y += moveY;
	currentlocation.x += moveX;
	
	VERBOSE(verbose, @"Image will be moved by x=%f, y =%f", 
			moveX, moveY);
	self.center = currentlocation;
	
	//NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];	
	
	//memory leak!
	//[prefs setFloat:self.center.x forKey:[[NSString alloc] initWithFormat:@"img%dx", self.vID]];
	//[prefs setFloat:self.center.y forKey:[[NSString alloc] initWithFormat:@"img%dy", self.vID]];
	
}

- (void)placeImage:(CGPoint)tarLoc {
	self.center = tarLoc;
}

-(void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
	VERBOSE(verbose, @"touch began detectionn  in Resizable Image");
	isBeingTouched = TRUE;
#ifdef NOUSED
	tapLocation = [[touches anyObject] locationInView:self.superview];
#endif
	[self.nextResponder touchesBegan:touches withEvent:event];
}
-(void) touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
	VERBOSE(verbose, @"touch ended detectionn  in Resizable Image");
	isBeingTouched = FALSE;
	[self.nextResponder touchesEnded:touches withEvent:event];
}

-(void) touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event
{
	isBeingTouched = FALSE;
	[self.nextResponder touchesCancelled:touches withEvent:event];
}

-(void) drawRect: (CGRect) rect
{
	CGContextRef currentContext = UIGraphicsGetCurrentContext();
	CGContextSaveGState(currentContext);
	
	if (highlight == TRUE)
	{
		[self sendSubviewToBack:iFrameBG];
		CGSize shadowOffset = CGSizeMake(0, 0);

		if (highlightColor == nil)
		{
			//use our own color if it is not set 
			float	colorVals[] = {0,0,1,  1.0};
			CGColorSpaceRef colorSpace ;
			colorSpace = CGColorSpaceCreateDeviceRGB ();
    		highlightColor = CGColorCreate(colorSpace, colorVals);
			CGColorSpaceRelease(colorSpace);
		}

		CGContextSetShadowWithColor(currentContext, shadowOffset, 
									GLOW_WIDTH, 
									highlightColor);
	}
	CGPoint pt = CGPointMake(rect.origin.x+GLOW_WIDTH, 
	 						rect.origin.y+GLOW_WIDTH);
	[image drawAtPoint:pt];
	CGContextRestoreGState(currentContext);
}


@end
