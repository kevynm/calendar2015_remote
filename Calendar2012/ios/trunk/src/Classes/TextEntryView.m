//
//  TextEntryView.m
//  Calendar2011
//
//  Created by Jonathan Fung on 28/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TextEntryView.h"


@implementation TextEntryView

@synthesize textEntryViewDelegate;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [[NSBundle mainBundle] loadNibNamed:@"TextEntryView" owner:self options:nil];
        }else{
            [[NSBundle mainBundle] loadNibNamed:@"TextEntryView-iphone" owner:self options:nil];
        }
		[self addSubview:mainView];
		moveIndicator.hidden = YES;
		[textView becomeFirstResponder];
    }
    return self;
}

- (void)dealloc {
	[mainView release];
	[textView release];
	[moveIndicator release];
	
    [super dealloc];
}

- (IBAction)doneButtonOnPressed {
	if (resultView == nil) {
		//image is not generated, create and go to move mode
		UIImage *resultImg;
		
		//make new text view
		UITextView *newTextView = [[UITextView alloc] initWithFrame:textView.frame];
		[newTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
		newTextView.backgroundColor = [UIColor clearColor];
		newTextView.font = textView.font;
		newTextView.text = textView.text;
		
		//draw image
		UIGraphicsBeginImageContext(newTextView.frame.size);
		[newTextView.layer renderInContext:UIGraphicsGetCurrentContext()];
		CGContextFlush(UIGraphicsGetCurrentContext());
		resultImg = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		[newTextView release];
		
		CGRect fr = textView.frame;
		
		//remove edit view
		//[mainView removeFromSuperview];
		for (UIView *subView in [mainView subviews]) {
			if (subView != moveIndicator) {
				[subView removeFromSuperview];
			}
		}
		
		//Add the draggable image view
		resultView = [[ResizableImageView alloc] initWithImage:resultImg];
		resultView.frame = fr;
		[mainView addSubview:resultView];
		//not to release resultView now as it have to be returned later
		
		//show indicator
		moveIndicator.hidden = NO;
	/*
	} else {
		//commit the image
		[textEntryViewDelegate performSelector:@selector(onTextEntryViewCommit:) withObject:[resultView autorelease]];
	 */
	}
}

#pragma mark Touch Event

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if (resultView != nil) {
		moveIndicator.hidden = YES;
		
		UITouch *touch = [touches anyObject];
		[resultView moveImage:[touch previousLocationInView:self] newLoc:[touch locationInView:self]];
	}
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	//commit the image
	if (resultView != nil) {
		[textEntryViewDelegate performSelector:@selector(onTextEntryViewCommit:) withObject:[resultView autorelease]];
	}
}

@end
