//
//  IAPManager.h
//  Farm
//
//  Created by Yu Yick Lam on 27/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <StoreKit/StoreKit.h>

@protocol IAPManagerDelegate

-(void) IAPManagerStartTransaction;
-(void) IAPManager:(id)_sender transactionSucceed:(SKPaymentTransaction*)transaction;
-(void) IAPManager:(id)_sender transactionFailed:(SKPaymentTransaction*)transaction;
-(void) IAPManager:(id)_sender transactionError:(SKPaymentTransaction*)transaction;
@end


@interface IAPManager : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>{
	SKProduct			*proUpgradeProduct;
    SKProductsRequest	*productsRequest;
	NSMutableSet*		delegateList;
}

+(id) sharedManager;
+(void) addDelegate:(id<IAPManagerDelegate>)delegate;
+(void) removeDelegate:(id<IAPManagerDelegate>)delegate;

-(void) addDelegate:(id<IAPManagerDelegate>)delegate;
-(void) removeDelegate:(id<IAPManagerDelegate>)delegate;

-(void) purchaseWithProduct:(NSString*)identifier;

//check if the device can make purchase
-(BOOL) canMakePurchases;

// called when the transaction was successful
-(void) completeTransaction:(SKPaymentTransaction *)transaction;
// called when a transaction has been restored and and successfully completed
-(void) restoreTransaction:(SKPaymentTransaction *)transaction;
// called when a transaction has failed
- (void)failedTransaction:(SKPaymentTransaction *)transaction;
// removes the transaction from the queue and posts a notification with the transaction result
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful;



@end
