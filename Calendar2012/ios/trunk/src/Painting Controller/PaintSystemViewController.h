//
//  PaintSystemViewController.h
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaintingSystem.h"


@interface PaintSystemViewController : UIViewController {
	
	PaintingView *paintingView;
	SketchingView *sketchingView;
	
	PaintingTool *currentPaintingTool;
	
	//BOOL usingTool;
	
}

@property(nonatomic, readwrite, assign) IBOutlet PaintingView *paintingView;
@property(nonatomic, readwrite, assign) IBOutlet SketchingView *sketchingView;

@property(nonatomic, readwrite, retain) PaintingTool *currentPaintingTool;
@property(nonatomic, readwrite) BOOL isPaintingStroke;

@property(nonatomic, readonly) BOOL usingTool;

- (void)undo;
- (void)redo;

- (void)onPaintingViewInsertedImage;

//For tool which will modify the current image of paint view
- (void)prepareModifyCurrentImage;
- (void)endModifyCurrentImage;

@end
