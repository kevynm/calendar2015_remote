//
//  PenAction.h
//  PaintingController
//
//  Created by outblaze on 02/08/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LineAction.h"


@interface PenAction : LineAction {
	
	CGFloat pointDistant;

}

@property(nonatomic, readwrite) CGFloat pointDistant;

@end
