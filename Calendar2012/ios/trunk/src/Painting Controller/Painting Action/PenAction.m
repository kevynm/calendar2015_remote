//
//  PenAction.m
//  PaintingController
//
//  Created by outblaze on 02/08/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "PenAction.h"

#define controlPointLengthRatio 0.5

CGPoint rescaleCGPoint(CGPoint vector, CGFloat length) {
	CGFloat cpvLength = sqrt(vector.x*vector.x + vector.y*vector.y);
	vector.x = vector.x * length / cpvLength;
	vector.y = vector.y * length / cpvLength;
	return vector;
}

@implementation PenAction

@synthesize pointDistant;

#pragma mark Lifecycle

- (id) init
{
	self = [super init];
	if (self != nil) {
		pointDistant = 1;
	}
	return self;
}


#pragma mark Override abstract method

-(void) paintInContext:(CGContextRef)context {
	if ([points count] <= 2) {
		//Draw a stright line instead
		[super paintInContext:context];
		return;
	}
	
	//At least three points to draw a curve
	//The second point and the second-last point are meaning the same point if exactly 3 three points there
	//Control points for first and last point are tending to the adjoint point
	//and the control points of others are parallel to the line of adjoint points (line from perivous point to the next point)
	//The lengths between the points and control points are depending the kength between the points and the adjoint points, and controlPointLengthRatio
	
	//!!!! static length is used
	CGFloat controlPointLength = pointDistant * controlPointLengthRatio;
	
	//set color and line width
	CGContextSetStrokeColorWithColor(context, [self.strokeColor CGColor]);
	CGContextSetLineWidth(context, lineWidth);
	
	CGPoint point0, point1, point2, controlPointVector0, controlPointVector1;
	
	//First point to second point
	point0 = [(NSValue *)[points objectAtIndex:0] CGPointValue];
	CGContextMoveToPoint(context, point0.x, point0.y);
	point1 = [(NSValue *)[points objectAtIndex:1] CGPointValue];
	//controlPointVector directing from point0 to point 1
	controlPointVector0 = CGPointMake(point1.x - point0.x, point1.y - point0.y);
	//Rescale the length of vector
	controlPointVector0 = rescaleCGPoint(controlPointVector0, controlPointLength);
	
	NSLog(@"length check:%f", sqrt(controlPointVector0.x*controlPointVector0.x + controlPointVector0.y*controlPointVector0.y));
	
	for (int i=2; i<[points count]; i++) {
		//loop start with the point0 and point1 is set
		//so set the point2 now
		point2 = [(NSValue *)[points objectAtIndex:i] CGPointValue];
		
		//controlPointVector directing from point0 to point 2
		controlPointVector1 = CGPointMake(point2.x - point0.x, point2.y - point0.y);
		
		//Rescale the length of vector
		controlPointVector1 = rescaleCGPoint(controlPointVector1, controlPointLength);
		
		//!!!! Another control point length approach:
		//		calculate the dot product between the controlPointVector and the point-adjoint vector to get the "component length"
		//		and then divide that by a factor
		
		CGContextAddCurveToPoint(context,
								 point0.x + controlPointVector0.x, point0.y + controlPointVector0.y,	//control point 0 = point0 + controlPointVector0
								 point1.x - controlPointVector1.x, point1.y - controlPointVector1.y,	//control point 1 = point1 - controlPointVector1
								 point1.x, point1.y);
		
		//reset point0 and point1 before next loop
		point0 = point1;
		point1 = point2;
		controlPointVector0 = controlPointVector1;
	}
	
	//Second-last point and last point
	//controlPointVector directing from point0 to point 1
	controlPointVector1 = CGPointMake(point1.x - point0.x, point1.y - point0.y);
	//Rescale the length of vector
	controlPointVector1 = rescaleCGPoint(controlPointVector1, controlPointLength);
	
	CGContextAddCurveToPoint(context,
							 point0.x + controlPointVector0.x, point0.y + controlPointVector0.y,	//control point 0 = point0 + controlPointVector0
							 point1.x - controlPointVector1.x, point1.y - controlPointVector1.y,	//control point 1 = point1 - controlPointVector1
							 point1.x, point1.y);
	
	//stroke path
	CGContextStrokePath(context);
}

@end
