//
//  PALine.m
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "LineAction.h"


@implementation LineAction

@synthesize points, strokeColor, lineWidth, lineJoin, lineCap;

#pragma mark Lifecycle

-(id) init {
	if ((self=[super init]) != nil) {
		points = [[NSMutableArray alloc] init];
		
		//default setting
		self.strokeColor = [UIColor blackColor];
		self.lineWidth = 10;
		self.lineJoin = kCGLineJoinRound;
		self.lineCap = kCGLineCapRound;
	}
	
	return self;
	
}

-(void) dealloc{
	[points release];
	self.strokeColor = nil;
	
	[super dealloc];
	
}

#pragma mark Override abstract method

- (UIColor *)mainColor {
	return self.strokeColor;
}

- (void)setMainColor:(UIColor *)value {
	self.strokeColor = value;
}

-(void) paintInContext:(CGContextRef)context {
	if ([points count] < 2) {
		//Not enough points for painting
		return;
	}
	
	//set color and line width
	CGContextSetStrokeColorWithColor(context, [self.strokeColor CGColor]);
	CGContextSetLineWidth(context, lineWidth);
	CGContextSetLineJoin(context, lineJoin);
	CGContextSetLineCap(context, lineCap);
	
	//draw path
	BOOL isFirst = YES;
	for (NSValue *val in points) {
		CGPoint point = [val CGPointValue];
		
		if (isFirst) {
			isFirst = NO;
		} else {
			//Draw line from previous point to this one
			CGContextAddLineToPoint(context, point.x, point.y);
		}

		//Move to the point to draw next line
		CGContextMoveToPoint(context, point.x, point.y);
		
	}
	
	//stroke path
	CGContextStrokePath(context);
	
}

@end
