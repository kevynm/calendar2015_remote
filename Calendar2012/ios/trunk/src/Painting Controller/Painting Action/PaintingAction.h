//
//  PaintingAction.h
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PaintingAction : NSObject {

}

@property(nonatomic, readwrite, retain) UIColor *mainColor;

-(void) paintInContext:(CGContextRef)context;

@end
