
//
//  BrushAction.m
//  BabyWriter
//
//  Created by Jonathan Fung on 09/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BrushAction.h"


@implementation BrushAction


-(void) paintInContext:(CGContextRef)context {
	if ([points count] < 2) {
		//Not enough points for painting
		return;
	}
	
	//set color and line width
	CGContextSetStrokeColorWithColor(context, [self.strokeColor CGColor]);
	CGContextSetLineWidth(context, lineWidth);
	CGContextSetLineJoin(context, lineJoin);
	CGContextSetLineCap(context, lineCap);
	
	//Load Brush Image
	UIImage *brushImg = [UIImage imageNamed:@"brush.png"];
	
	//draw path
	//BOOL isFirst = YES;
	for (NSValue *val in points) {
		CGPoint point = [val CGPointValue];
		CGContextSaveGState(context);
		CGContextTranslateCTM(context, point.x, point.y);
		CGContextRotateCTM(context, drand48()*2*M_PI - M_PI);
		
		CGContextDrawImage(context, CGRectMake(-12.5, -12.5, 25, 25), brushImg.CGImage);
		CGContextRestoreGState(context);
		/*if (isFirst) {
			isFirst = NO;
		} else {
			//Draw line from previous point to this one
			CGContextAddLineToPoint(context, point.x, point.y);
		}
		
		//Move to the point to draw next line
		CGContextMoveToPoint(context, point.x, point.y);*/
		
	}
	
	//stroke path
	//CGContextStrokePath(context);
	
}

@end
