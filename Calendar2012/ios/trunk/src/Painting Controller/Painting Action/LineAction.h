//
//  PALine.h
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaintingAction.h"


@interface LineAction : PaintingAction {
	
	NSMutableArray *points;
	
	UIColor *strokeColor;
	CGFloat lineWidth;
	CGLineJoin lineJoin;
	CGLineCap lineCap;

}

@property(nonatomic, readonly) NSMutableArray *points;
@property(nonatomic, readwrite, retain) UIColor *strokeColor;
@property(nonatomic, readwrite) CGFloat lineWidth;
@property(nonatomic, readwrite) CGLineJoin lineJoin;
@property(nonatomic, readwrite) CGLineCap lineCap;

@end
