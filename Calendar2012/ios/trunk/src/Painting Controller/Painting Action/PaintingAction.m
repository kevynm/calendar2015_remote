//
//  PaintingAction.m
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "PaintingAction.h"


@implementation PaintingAction

@dynamic mainColor;

#pragma mark Abstract method

//Main color label for generic usage
- (UIColor *)mainColor {
	//Abstract method
	return nil;
}

- (void)setMainColor:(UIColor *)value {
	//Abstract method
}

-(void) paintInContext:(CGContextRef)context {
	//Abstract method
}

@end
