//
//  PaintSystemViewController.m
//  PaintingController
//
//  Created by outblaze on 30/07/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PaintSystemViewController.h"


@implementation PaintSystemViewController

@synthesize paintingView, sketchingView, currentPaintingTool, isPaintingStroke, usingTool;

#pragma mark -
#pragma mark Accessor

- (BOOL)usingTool {
	return currentPaintingTool.state == PaintToolState_Using;
}

#pragma mark -
#pragma mark Lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
	//!!!! default handling
    [super didReceiveMemoryWarning];
    
}


- (void)viewDidUnload {
    [super viewDidUnload];
	
	//!!!! default handling
}


- (void)dealloc {
	self.currentPaintingTool = nil;
	self.paintingView = nil;
	self.sketchingView = nil;
	
    [super dealloc];
}

#pragma mark -
#pragma mark Accessor

//Not to release the tool immediately after re-assignment
//commit the sketch before release
//and set the new tool's controller to self
- (void)setCurrentPaintingTool:(id)tool {
	if (currentPaintingTool != nil) {
		//Force commit sketch when deselected
		[currentPaintingTool commitSketch];
		
		//Note: the previous paint tool have to be released before retain the new one
		//as some setting may be changed when the previous one release
		//E.g.: Blend mode
		[currentPaintingTool release];
	}
	
	currentPaintingTool = tool;
	if (currentPaintingTool != nil) [currentPaintingTool retain];
	[currentPaintingTool setController:self];
}

#pragma mark -
#pragma mark Touches Event

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	if (currentPaintingTool != nil) [(PaintingTool *)currentPaintingTool touchesBegan:touches withEvent:event];
	isPaintingStroke = YES;
	[super touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	if (currentPaintingTool != nil) [(PaintingTool *)currentPaintingTool touchesMoved:touches withEvent:event];
	[super touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	if (currentPaintingTool != nil) [(PaintingTool *)currentPaintingTool touchesEnded:touches withEvent:event];
	isPaintingStroke = NO;
	[super touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	if (currentPaintingTool != nil) [(PaintingTool *)currentPaintingTool touchesCancelled:touches withEvent:event];
	isPaintingStroke = NO;
	[super touchesCancelled:touches withEvent:event];
}

#pragma mark -
#pragma mark Interface

- (void)undo {
	//Stop drawing tool to draw thing
	//self.currentPaintingTool = nil;
	
	[paintingView undo];
}

- (void)redo {
	//Stop drawing tool to draw thing
	//self.currentPaintingTool = nil;
	
	[paintingView redo];
}

//delegate callback by painting view
- (void)onPaintingViewInsertedImage {
	/*
	 //save draw image
	 if (paintToolIndex == PT_ZAP) {
	 UIImage *paintImage = [drawView.imageStack lastObject];
	 currentLevel.page.drawImage = paintImage;
	 }
	 
	 [self updateUndoButton];
	 */
}

//For tool which will modify the current image of paint view
- (void)prepareModifyCurrentImage {
	UIImage *currentImage = [paintingView.imageStack lastObject];
	[sketchingView setPreCommitImage:currentImage];
	paintingView.hidden = YES;
}

- (void)endModifyCurrentImage {
	paintingView.hidden = NO;
}

@end
