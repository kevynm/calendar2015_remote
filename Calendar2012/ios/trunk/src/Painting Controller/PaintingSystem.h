/*
 *  PaintingSystem.h
 *  PaintingController
 *
 *  Created by outblaze on 29/07/2010.
 *  Copyright 2010 DreamCortex. All rights reserved.
 *
 */

//Paint View
#import "PaintingView.h"
#import "SketchingView.h"

//Paint Action
#import "LineAction.h"
#import "PenAction.h"

//Paint Tool
#import "LineTool.h"
#import "PenTool.h"
#import "ZapEraseTool.h"

//Paint System Controller
#import "PaintSystemViewController.h"


#define MIN_ADJOINT_DISTANT 3