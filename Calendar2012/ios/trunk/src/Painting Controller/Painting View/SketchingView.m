//
//  SketchingView.m
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "SketchingView.h"


@implementation SketchingView

@synthesize actions, testColor;
//currentAction

#pragma mark Lifecycle

//Overriding super initiator
-(void) initCommon {
	[super initCommon];
	actions = [[NSMutableArray alloc] init];
	CGRect pcivframe = self.frame;
	pcivframe.origin.x = 0;
	pcivframe.origin.y = 0;
	preCommitImageView = [[[UIImageView alloc] initWithFrame:pcivframe] autorelease];
	[self addSubview:preCommitImageView];
	[self clearActions];
	
}

- (void)dealloc {
	if (preCommitImage != nil) [preCommitImage release];
	[actions release];
	
	[super dealloc];
}

#pragma mark Override abstract method

-(void) drawInContext:(CGContextRef)context {
	CGContextSetBlendMode(UIGraphicsGetCurrentContext(), self.blendMode);
	
	for (int i=commitedActionCount; i<[actions count]; i++) {
		PaintingAction *action = [actions objectAtIndex:i];
		[action paintInContext:context];
	}
	
}

#pragma mark Interface

-(void) addAction:(PaintingAction *)action {
	//!!!! testing
	if (testColor != nil) {
		action.mainColor = testColor;
	}
	
	[actions addObject:action];
	//currentAction = action;
	
}

-(void)removeAction:(PaintingAction *)action {
	[actions removeObject:action];
}

-(void) clearActions {
	if (commitedActionCount > 0) {
		if (preCommitImage != nil) [preCommitImage release];
		//[self setPreCommitImage:[GeneralUtility clearImageWithFrame:self.frame.size]];
		[self setPreCommitImage:[UIImage imageNamed:@"paintingToolNone.png"]];
	}
	commitedActionCount = 0;
	
	[actions removeAllObjects];
}

- (void)setPreCommitImage:(UIImage *)image {
	preCommitImage = [image retain];
	preCommitImageView.image = preCommitImage;
}

- (void)precommitActions {
	//draw the pre committed image and release it
	//draw the un-committed image
	//commite and save the action
	UIGraphicsBeginImageContext(preCommitImageView.frame.size);
	[preCommitImage drawInRect:CGRectMake(0, 0, preCommitImage.size.width, preCommitImage.size.height)];
	[preCommitImage release];
	[self drawInContext:UIGraphicsGetCurrentContext()];
	CGContextFlush(UIGraphicsGetCurrentContext());
	[self setPreCommitImage:UIGraphicsGetImageFromCurrentImageContext()];
	UIGraphicsEndImageContext();
	
	commitedActionCount = [actions count];
}

@end
