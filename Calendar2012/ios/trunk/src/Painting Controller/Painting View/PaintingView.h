//
//  PaintingView.h
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuartzView.h"
#import "PaintingAction.h"


@interface PaintingView : QuartzView {
	
	id paintingViewDelegate;
	
	NSMutableArray *actions;
	NSMutableArray *imageStack, *redoImageStack;
	int imageStackSize;
	UIImageView *paintingImageView;
	
}

@property(nonatomic, readwrite, assign) IBOutlet id paintingViewDelegate;
@property(nonatomic, readwrite) int imageStackSize;
@property(nonatomic, readonly) NSMutableArray *imageStack;


#pragma mark Overriding
-(void)drawInContext:(CGContextRef)context;

#pragma mark Interface
- (BOOL)canUndo;
- (void)undo;
- (BOOL)canRedo;
- (void)redo;
- (void)setRootImage:(UIImage *)image;
- (void)addAction:(PaintingAction *)action;
- (void)insertImage:(UIImage *)image;
- (void)clearImage;
- (void)clearHistory;
- (void)setBlendMode:(CGBlendMode) mode;

@end
