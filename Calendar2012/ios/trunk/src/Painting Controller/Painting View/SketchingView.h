//
//  SketchingView.h
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuartzView.h"
#import "PaintingAction.h"


@interface SketchingView : QuartzView {
	
	NSMutableArray *actions;
	//PaintingAction *currentAction;
	
	//pre-commit action
	UIImage *preCommitImage;
	UIImageView *preCommitImageView;
	int commitedActionCount;
	
	//!!!! testing
	UIColor *testColor;

}

@property(nonatomic, readonly) NSMutableArray *actions;
//@property(nonatomic, readonly) PaintingAction *currentAction;
@property(nonatomic, readwrite, retain) UIColor *testColor;

#pragma mark Overriding
-(void) drawInContext:(CGContextRef)context;

#pragma mark Interface
-(void) addAction:(PaintingAction *)action;
-(void) removeAction:(PaintingAction *)action;
-(void) clearActions;
- (void)setPreCommitImage:(UIImage *)image;
- (void)precommitActions;

@end
