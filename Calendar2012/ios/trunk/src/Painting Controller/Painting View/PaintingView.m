//
//  PaintingView.m
//  PaintingController
//
//  Created by outblaze on 29/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "PaintingView.h"


@implementation PaintingView

@synthesize imageStack, imageStackSize, paintingViewDelegate;

#pragma mark Lifecycle

//Overriding super initiator
-(void) initCommon {
	[super initCommon];
	
	blendMode = kCGBlendModeNormal;
	
	self.clearsContextBeforeDrawing = NO;
	actions = [[NSMutableArray alloc] init];
	imageStack = [[NSMutableArray alloc] init];
	redoImageStack = [[NSMutableArray alloc] init];
	imageStackSize = 0;
	
	//make a clear image at first
	CGRect pivvframe = self.frame;
	pivvframe.origin.x = 0;
	pivvframe.origin.y = 0;
	paintingImageView = [[[UIImageView alloc] initWithFrame:pivvframe] autorelease];
	[self clearImage];
	[self addSubview:paintingImageView];
	
}

- (void)dealloc {
	[actions release];
	[imageStack release];
	[redoImageStack release];
	[paintingImageView removeFromSuperview];
	
	[super dealloc];
}

#pragma mark Override abstract method

-(void)drawInContext:(CGContextRef)context {
	//!!!! The image is not draw on the requested context
	
	UIImage *img;
	
	//******** image rendering *********//
	UIGraphicsBeginImageContext(paintingImageView.frame.size);
	
	CGContextSaveGState(UIGraphicsGetCurrentContext());
	CGContextSetBlendMode(UIGraphicsGetCurrentContext(), self.blendMode);
	
	//load the latest image
	img = [imageStack lastObject];
	[img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
	
	//push and resolve all objects
	for (PaintingAction *action in actions) {
		[action paintInContext:UIGraphicsGetCurrentContext()];
	}
	CGContextFlush(UIGraphicsGetCurrentContext());
		
	//save the image for any changes
	if ([actions count] > 0) {
		img = UIGraphicsGetImageFromCurrentImageContext();
		[redoImageStack removeAllObjects];
		[self insertImage:img];
	}
	CGContextRestoreGState(UIGraphicsGetCurrentContext());	
	UIGraphicsEndImageContext();
	
	//Clear actions
	[actions removeAllObjects];
	

	//******** context rendering *********//
	//[img drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}

#pragma mark Interface

- (BOOL)canUndo {
	return [imageStack count] > 1 || [actions count] > 0;
}

- (void)undo {
	//Cannot undo back b4 history root
	if (![self canUndo]) return;
	
	//add to redo list
	[redoImageStack addObject:[imageStack lastObject]];
	
	[imageStack removeLastObject];
	paintingImageView.image = [imageStack lastObject];
	
	//image change callback
	[paintingViewDelegate performSelector:@selector(onPaintingViewInsertedImage)];
}

- (BOOL)canRedo {
	return [redoImageStack count] > 0;
}

- (void)redo {
	if (![self canRedo]) return;
	
	//insertImage should be called at last for callback at last
	UIImage *image = [[redoImageStack lastObject] retain];
	[redoImageStack removeLastObject];
	[self insertImage:image];
	[image release];
}

- (void)setRootImage:(UIImage *)image {
	[imageStack removeAllObjects];
	[imageStack addObject:image];
	paintingImageView.image = image;
}

- (void)addAction:(PaintingAction *)action {
	[actions addObject:action];
	
}

- (void)insertImage:(UIImage *)image {
	[imageStack addObject:image];
	
	//Retain image stack size
	if ([imageStack count] - 1 > imageStackSize) {
		[imageStack removeObjectAtIndex:0];
	}
	
	paintingImageView.image = image;
	
	//!!!!
	[paintingViewDelegate performSelector:@selector(onPaintingViewInsertedImage)];
}

- (void)clearImage {
	//UIImage *image = [GeneralUtility clearImageWithFrame:self.frame.size];
	//UIImage *image = [UIImage imageNamed:@"paintingToolNone.png"];
	UIImage *image = [[[UIImage alloc] init] autorelease]; //EDIT: autoreleased - image should be retained by the imageStack alone
	[imageStack addObject:image];
	paintingImageView.image = image;
}

- (void)clearHistory {
	[imageStack removeAllObjects];
	[self clearImage];
}

- (void)setBlendMode:(CGBlendMode)mode {
	blendMode = mode;
}

@end
