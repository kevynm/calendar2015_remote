//
//  ZapEraseTool.m
//  Calendar2011
//
//  Created by Kay Chan on 29/12/2010.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PaintingTool2.h"
#import "ZapEraseTool.h"

@interface ZapEraseTool (hidden)

- (void)startTool;
- (void)endTool;

@end




@implementation ZapEraseTool

#pragma mark Accessor

//for blend mode setting

-(void) setController:(UIViewController *)value {
	if (value == self.controller) return;
	
	//restore previous controller
	if (self.controller != nil) {
		[self.controller.sketchingView setBlendMode:kCGBlendModeNormal];
		[self.controller.paintingView setBlendMode:kCGBlendModeNormal];
	}
	
	[super setController:value];
	
	//set blend mode for new controller
	if (self.controller != nil) {
		[self.controller.sketchingView setBlendMode:kCGBlendModeClear];
		[self.controller.paintingView setBlendMode:kCGBlendModeClear];
	}
	
}

#pragma mark Touch event

- (BOOL)targetedTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	[self startTool];
	
	return [super targetedTouchBegan:touch withEvent:event];
}

- (void)targetedTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	[super targetedTouchEnded:touch withEvent:event];
	
	[self endTool];
}

- (void)targetedTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
	[super targetedTouchCancelled:touch withEvent:event];
	
	[self endTool];
}

- (void)startTool {
	[self.controller prepareModifyCurrentImage];
}

- (void)endTool {
	[self.controller endModifyCurrentImage];
}

#pragma mark Overriding method


- (BOOL)checkIfNeedPrecommit {
	return YES;
}

@end
