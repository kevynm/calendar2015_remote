//
//  PaintingTool.m
//  PaintingController
//
//  Created by outblaze on 30/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "PaintingTool2.h"

@implementation PaintingTool (Pointer)

@dynamic controller, state;

- (PaintSystemViewController *)controller {
	return (PaintSystemViewController *)_controller;
}

- (void)setState:(PaintToolState)pValue {
	state = pValue;
}

@end



@implementation PaintingTool

@synthesize state;

#pragma mark -
#pragma mark LifeCycle

- (id)init {
	if ((self = [super init]) != nil) {
		state = PaintToolState_Idle;
	}
	
	return self;
}

- (void)dealloc {
	self.controller = nil;
	
	[super dealloc];
}


#pragma mark -
#pragma mark Accessor

- (void)setController:(UIViewController *)value {
	if (value != nil && ! [[value class] isSubclassOfClass:[PaintSystemViewController class]]) {
		[[NSException exceptionWithName:@"InvalidArgumentClass" reason:@"Invalid argument class assigned to controller" userInfo:nil] raise];
	}
	
	_controller = value;
}


#pragma mark -
#pragma mark Work Flow Control

//The action by tool should be done in sketch view first
//and then draw to paint view after a "step" of tool
- (void)commitSketch {
	if ([self.controller.sketchingView.actions count] <= 0) return;
	
	NSMutableArray *convertedActions = [self convertSketchToPaint:self.controller.sketchingView.actions];
	
	if (convertedActions != nil) {
		for (PaintingAction *action in convertedActions) {
			[self.controller.paintingView addAction:action];
		}
		[self.controller.paintingView setNeedsDisplay];
	}
	
	[self.controller.sketchingView clearActions];
	[self.controller.sketchingView setNeedsDisplay];
	
}

- (NSMutableArray *)convertSketchToPaint:(NSMutableArray *)actions {
	//Default action
	return actions;
}

@end
