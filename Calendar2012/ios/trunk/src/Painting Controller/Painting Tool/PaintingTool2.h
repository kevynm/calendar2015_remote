/*
 *  PaintingTool2.h
 *  PaintingController
 *
 *  Created by outblaze on 02/08/2010.
 *  Copyright 2010 DreamCortex. All rights reserved.
 *
 *  This is a header for subclass only
 *
 */

#import "PaintingTool.h"
#import "PaintingSystem.h"

@interface PaintingTool (Pointer)

@property (nonatomic, getter = controller, assign) PaintSystemViewController *controller;
@property (nonatomic, readwrite) PaintToolState state;

@end