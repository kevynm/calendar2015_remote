//
//  CurveTool.m
//  PaintingController
//
//  Created by outblaze on 02/08/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "PaintingTool2.h"
#import "PenTool.h"
#import "PaintingSystem.h"

@implementation PenTool

#pragma mark Work Flow Control

- (NSMutableArray *)convertSketchToPaint:(NSMutableArray *)actions {
	//!!!! assuming 1 action only
	LineAction *lineAction = (LineAction *) [actions objectAtIndex:0];
	PenAction *penAction = [[[PenAction alloc] init] autorelease];
	if (strokeColor != nil) penAction.strokeColor = strokeColor;
	if (lineWidth >= 0) penAction.lineWidth = lineWidth;
	
	//!!!testing
	/*
	[penAction.points addObject:[NSValue valueWithCGPoint:CGPointMake(100, 400)]];
	[penAction.points addObject:[NSValue valueWithCGPoint:CGPointMake(150, 350)]];
	[penAction.points addObject:[NSValue valueWithCGPoint:CGPointMake(200, 400)]];
	[penAction.points addObject:[NSValue valueWithCGPoint:CGPointMake(250, 350)]];
	penAction.pointDistant = sqrt(50*50*2);
	 */
	
	penAction.pointDistant = MIN_ADJOINT_DISTANT;
	for (NSValue *value in lineAction.points) {
		[penAction.points addObject:value];
	}
	
	
	return [NSMutableArray arrayWithObject:penAction];
}


@end
