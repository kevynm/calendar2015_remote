//
//  ZapTool.h
//  PaintingController
//
//  Created by outblaze on 04/08/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "LineTool.h"


@interface ZapTool : LineTool {
	
	int lineSegmentCount;
	
}

@end
