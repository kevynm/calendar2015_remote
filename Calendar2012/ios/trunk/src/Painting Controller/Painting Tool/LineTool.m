//
//  LineTool.m
//  PaintingController
//
//  Created by outblaze on 30/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "PaintingTool2.h"
#import "PaintingSystem.h"
#import "BrushAction.h"



@implementation LineTool

@synthesize currentAction, strokeColor, lineWidth, lineJoin, lineCap;

#pragma mark Lifecycle

- (id)init {
	if ((self = [super init]) != nil) {
		targetedTouch = nil;
		
		//invalid init for default setting
		strokeColor = nil;
		lineWidth = -1;
	}
	
	return self;
}

- (void)dealloc {
	self.currentAction = nil;
	
	[super dealloc];
}

#pragma mark Touch event

- (BOOL)targetedTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	if (targetedTouch != nil) return NO;
	
	self.state = PaintToolState_Using;
	
	targetedTouch = touch;
	
	[self newLineAction];
	
	[self initLineTool:touch withEvent:event];
	
	return YES;
	
}

- (void)targetedTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
	NSValue *pointValue = [NSValue valueWithCGPoint:[touch locationInView:self.controller.sketchingView]];
	[currentAction.points replaceObjectAtIndex:1 withObject:pointValue];
	
	[self.controller.sketchingView setNeedsDisplay];
	
}

- (void)targetedTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	if ([self checkIfNeedCommit]) {
		[self commitSketch];
	} else {
		//The line is not draw yet
		[self.controller.sketchingView removeAction:currentAction];
	}
	
	self.currentAction = nil;
	
	targetedTouch = nil;
	
	self.state = PaintToolState_Idle;
	
}

- (void)targetedTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
	targetedTouch = nil;
}

#pragma mark Procedure
//be more convenient to be overrided

- (void)newLineAction {
	self.currentAction = [[[LineAction alloc] init] autorelease];
	//self.currentAction = [[[BrushAction alloc] init] autorelease];
	if (strokeColor != nil) currentAction.strokeColor = strokeColor;
	if (lineWidth >= 0) currentAction.lineWidth = lineWidth;
	[self.controller.sketchingView addAction:currentAction];
}

- (void)initLineTool:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint pt = [touch locationInView:self.controller.sketchingView];
	NSValue *pointValue = [NSValue valueWithCGPoint:pt];
	[currentAction.points addObject:pointValue];
	
	pt.x += 0.001f;
	pt.y += 0.001f;
	pointValue = [NSValue valueWithCGPoint:pt];
	[currentAction.points addObject:pointValue];
	[self.controller.sketchingView setNeedsDisplay];
	
}

- (BOOL)checkIfNeedCommit {
	return [currentAction.points objectAtIndex:0] != [currentAction.points objectAtIndex:1];
	
}

@end
