//
//  LineTool.h
//  PaintingController
//
//  Created by outblaze on 30/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaintingTool.h"
#import "LineAction.h"


@interface LineTool : PaintingTool {
	
	//Targeted Touch
	UITouch *targetedTouch;
	
	LineAction *currentAction;
	UIColor *strokeColor;
	CGFloat lineWidth;
	CGLineJoin lineJoin;
	CGLineCap lineCap;

}

@property(nonatomic, readwrite, retain) LineAction *currentAction;
@property(nonatomic, readwrite, retain) UIColor *strokeColor;
@property(nonatomic, readwrite) CGFloat lineWidth;
@property(nonatomic, readwrite) CGLineJoin lineJoin;
@property(nonatomic, readwrite) CGLineCap lineCap;

- (void)newLineAction;
- (void)initLineTool:(UITouch *)touch withEvent:(UIEvent *)event;
- (BOOL)checkIfNeedCommit;

@end
