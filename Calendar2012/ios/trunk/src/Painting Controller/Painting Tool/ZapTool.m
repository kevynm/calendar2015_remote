//
//  ZapTool.m
//  PaintingController
//
//  Created by outblaze on 04/08/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import "PaintingTool2.h"
#import "ZapTool.h"
#import "PaintingSystem.h"

#define ZAPTOOL_LS_COUNT_TO_PRECMT 10

@interface ZapTool (hidden)

- (BOOL)checkIfNeedPrecommit;

@end





@implementation ZapTool

#pragma mark Overriding method

- (void)initLineTool:(UITouch *)touch withEvent:(UIEvent *)event {
	[super initLineTool:touch withEvent:event];
	
	//[self newLineAction];
	CGPoint pt = [touch locationInView:self.controller.sketchingView];
	NSValue *pointValue = [NSValue valueWithCGPoint:pt];
	[currentAction.points addObject:pointValue];
	
	lineSegmentCount = 1;
}

- (void)targetedTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint point = [touch locationInView:self.controller.sketchingView];
	CGPoint previousPoint = [(NSValue *)[currentAction.points lastObject] CGPointValue];
	
	//Distant between two points
	CGFloat dx = point.x - previousPoint.x;
	CGFloat dy = point.y - previousPoint.y;
	CGFloat dist = sqrt(dx*dx + dy*dy );
	
	//Draw line if the distant is long enough
	if (dist > MIN_ADJOINT_DISTANT) {
		NSValue *pointValue = [NSValue valueWithCGPoint:point];
		[currentAction.points addObject:pointValue];
		++lineSegmentCount;
		if ([self checkIfNeedPrecommit]) {
			[self.controller.sketchingView precommitActions];
		}
		[self newLineAction];
		[currentAction.points addObject:pointValue];
		[self.controller.sketchingView setNeedsDisplay];
	}
	
}

- (BOOL)checkIfNeedCommit {
	return lineSegmentCount > 0;
}

- (void)commitSketch {
	//the last action has not enough point to draw
	[self.controller.sketchingView removeAction:currentAction];
	
	[super commitSketch];
}

#pragma mark Procedure
//be more convenient to be overrided

- (BOOL)checkIfNeedPrecommit {
	return lineSegmentCount % ZAPTOOL_LS_COUNT_TO_PRECMT == 0;
}

@end
