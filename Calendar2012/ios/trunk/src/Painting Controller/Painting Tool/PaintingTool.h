//
//  PaintingTool.h
//  PaintingController
//
//  Created by outblaze on 30/07/2010.
//  Copyright 2010 DreamCortex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaintingAction.h"

#import "TargetedTouchController.h"

typedef enum {
	PaintToolState_Idle, PaintToolState_Using
} PaintToolState;


@interface PaintingTool : TargetedTouchController {
	
	UIViewController *_controller;
	PaintToolState state;

}

@property (nonatomic, readonly) PaintToolState state;

- (void)setController:(UIViewController *)value;

- (void)commitSketch;
- (NSMutableArray *)convertSketchToPaint:(NSMutableArray *)actions;

@end
