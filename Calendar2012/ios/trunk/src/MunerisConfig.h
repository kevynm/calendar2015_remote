//
//  MunerisConfig.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/14/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface MunerisConfig : NSObject {
    
}

@property (readonly,nonatomic) NSDictionary* config;

@end
