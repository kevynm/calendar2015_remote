//
//  MunerisConfig.m
//  MunerisKit
//
//  Created by Jacky Yuk on 10/14/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisConfig.h"


@implementation MunerisConfig

@dynamic config;


-(NSDictionary*) config;
{
  return [NSDictionary dictionary];
}

@end
