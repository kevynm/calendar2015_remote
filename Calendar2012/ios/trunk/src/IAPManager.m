//
//  IAPManager.m
//  Farm
//
//  Created by Yu Yick Lam on 27/03/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "IAPManager.h"

#import "IAPCommonValues.h"

static IAPManager* Instance = nil;

@implementation IAPManager

#pragma mark -
#pragma mark Class Methods
+(id) sharedManager {
	if(!Instance){
		Instance = [[self alloc] init];
	}
	return Instance;
}

+(void) addDelegate:(id<IAPManagerDelegate>)delegate {
	[[self sharedManager] addDelegate:delegate];
}
+(void) removeDelegate:(id<IAPManagerDelegate>)delegate {
	[[self sharedManager] removeDelegate:delegate];
}

#pragma mark -
#pragma mark Initialization and Destruction
-(id) init {
	if((self = [super init])){		
		[[SKPaymentQueue defaultQueue] addTransactionObserver:self];
		delegateList = [[NSMutableSet set] retain];
	}
	return self;
}

-(void) addDelegate:(id<IAPManagerDelegate>)delegate {
	if(![delegateList containsObject:delegate]){
		[delegateList addObject:delegate];
	}
}

-(void) removeDelegate:(id<IAPManagerDelegate>)delegate {
	[delegateList removeObject:delegate];
}

#pragma mark Purchase
-(void) purchaseWithProduct:(NSString*)identifier {
	if([self canMakePurchases]){
        for(id delegate in delegateList){
			[delegate IAPManagerStartTransaction];
		}
		[[NSNotificationCenter defaultCenter] postNotificationName:OBS_LOADING_START object:nil];
		SKPayment *payment = [SKPayment paymentWithProductIdentifier:identifier];
		[[SKPaymentQueue defaultQueue] addPayment:payment];

	}
    else
    {
        /*
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                            message:@"We cannot connect to iTunes store at this moment, please try again later."
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        */
        for(id delegate in delegateList){
			[delegate IAPManager:self transactionError:nil];
		}
        NSLog(@"Error, can't connect to the iTunes store");
    }
}

-(BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
	//TODO: record the transation
	//TODO: provide the content
    [self finishTransaction:transaction wasSuccessful:YES];
	[[NSNotificationCenter defaultCenter] postNotificationName:OBS_LOADING_STOP object:nil];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    //TODO: record the transation
	//TODO: provide the content
    [self finishTransaction:transaction wasSuccessful:YES];
	[[NSNotificationCenter defaultCenter] postNotificationName:OBS_LOADING_STOP object:nil];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    /*SKErrorUnknown,
    SKErrorClientInvalid,       // client is not allowed to issue the request, etc.
    SKErrorPaymentCancelled,    // user cancelled the request, etc.
    SKErrorPaymentInvalid,      // purchase identifier was invalid, etc.
    SKErrorPaymentNotAllowed*/
  /*  
    NSString *errorCodeStr;
    switch (transaction.error.code) {
        case SKErrorUnknown:
            errorCodeStr = @"SKErrorUnknown";
            break;
        
        case SKErrorClientInvalid:
            errorCodeStr = @"SKErrorClientInvalid";
            break;
        
        case SKErrorPaymentCancelled:
            errorCodeStr = @"SKErrorPaymentCancelled";
            break;
            
        case SKErrorPaymentInvalid:
            errorCodeStr = @"SKErrorPaymentInvalid";
            break;
            
        case SKErrorPaymentNotAllowed:
            errorCodeStr = @"SKErrorPaymentNotAllowed";
            break;
            
        default:
            errorCodeStr = @"No error code found";
            break;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Transaction Failed" 
                                                        message:[NSString stringWithFormat:@"Error Code: %@", errorCodeStr]
                                                       delegate:nil 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
    [alertView show];
    [alertView release];
*/

    
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        // error!
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                            message:@"We cannot connect to iTunes store at this moment, please try again later."
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else
    {
        // this is fine, the user just cancelled, so don’t notify
        [self finishTransaction:transaction wasSuccessful:NO];
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
	[[NSNotificationCenter defaultCenter] postNotificationName:OBS_LOADING_STOP object:nil];
}

- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
	[userInfo setObject:[transaction transactionIdentifier] forKey:@"identifer"];
    if (wasSuccessful)
    {
        // send out a notification that we’ve finished the transaction
		NSLog(@"Finished Transaction Successful: %@", [transaction transactionIdentifier]);
		for (id delegate in delegateList) {
			[delegate IAPManager:self transactionSucceed:transaction];
		}
    }
    else
    {
        // send out a notification for the failed transaction
		NSLog(@"Finished Transaction Fail: %@", [transaction transactionIdentifier]);
		for (id delegate in delegateList) {
			[delegate IAPManager:self transactionFailed:transaction];
		}
    }
}


#pragma mark -
#pragma mark SKPaymentTransactionObserver methods
// called when the transaction status is updated
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
			case SKPaymentTransactionStatePurchasing:
				NSLog(@"Just Started Purchasing");
				break;
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
} 

#pragma mark SKProductsRequestDelegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
	if(response){
		NSArray *products = response.products;
		proUpgradeProduct = [products count] == 1 ? [[products objectAtIndex:0] retain] : nil;
		if (proUpgradeProduct)
		{
			NSLog(@"Product title: %@" , proUpgradeProduct.localizedTitle);
			NSLog(@"Product description: %@" , proUpgradeProduct.localizedDescription);
			NSLog(@"Product price: %@" , proUpgradeProduct.price);
			NSLog(@"Product id: %@" , proUpgradeProduct.productIdentifier);
		}
		
		for (NSString *invalidProductId in response.invalidProductIdentifiers)
		{
			NSLog(@"Invalid product id: %@" , invalidProductId);
		}
	}else{
		NSLog(@"Receive NULL response");
	}
	
	// finally release the reqest we alloc/init’ed in requestProUpgradeProductData
	[productsRequest release];
	
	//[[NSNotificationCenter defaultCenter] postNotificationName:kInAppPurchaseManagerProductsFetchedNotification object:self userInfo:nil];
} 

@end
