//
//  MunerisIadPlugin.m
//  MunerisKit
//
//  Created by Casper on 09/09/2011.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisIadPlugin.h"
#import "MunerisMacro.h"
#import "MunerisAdsView.h"
#import "MunerisManager.h"
#import "MunerisHelper.h"

@implementation MunerisIadPlugin

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisBannerAdsPluginProtocol))];
    [self updateConfiguration:dictionary];
  }
  return self;
}

-(void) loadAdsWithSize:(MunerisBannerAdsSizes)size withZone:(NSString*)zone WithDelegate:(id<MunerisBannerAdsDelegate>)delagate withUIViewController:(UIViewController*) viewController;
{

  //#TODO FIX THIS MEMORY LEAK - fixed.
  //  MunerisIad* iaa = [[[MunerisIad alloc] initWithSize:_bannerSize] autorelease];
  MunerisIad* iaa = [[[MunerisIad alloc] initWithDictionary:_info withSize:size] autorelease];
  
  [iaa loadAdsWithDelegate:delagate];
  
}

-(void) dealloc;
{
  if (_info) {
    [_info release];
    _info = nil;
  }
  [super dealloc];
}

@end

@interface MunerisIad (private)

- (void) cleanup;

@end

@implementation MunerisIad

//-(id) initWithSize:(NSString*) size;
//{
//  if ((self = [self init])) {
//    _bannerSize = [size retain];
//  }
//  return self;
//}

- (id) initWithDictionary:(NSDictionary *)dictionary withSize:(MunerisBannerAdsSizes)bannerSize {
  //  [self retain];
  if ((self = [self init])) {
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
      if ([[dictionary objectForKey:@"retryCount"] copyWithZone:[self zone]] != nil) {
        _retryCount = [[[dictionary objectForKey:@"retryCount"] copyWithZone:[self zone]] intValue];   
      }
    }
    
    _bannerSize = bannerSize;
  }
  return self;
  
}

-(void) loadAdsWithDelegate:(id<MunerisBannerAdsDelegate>)delagate;
{
  obmAdsView = [[[MunerisAdsView alloc] initWithSize:_bannerSize] autorelease];
  obmAdsView.delegate = self;
  Class AdView = NSClassFromString(@"ADBannerView");
  
  if (!AdView) {
    [delagate didFailToReceiveAds:[NSError errorWithDomain:@"muneris" code:123 userInfo:nil]];
    return;
  }
  _bannerView = [[AdView alloc] init];
  //Setup iAd Content Size id
  MLOG(@"%@",[[[[MunerisManager sharedManager] getHeader] objectForKey:@"os"] objectForKey:@"version"]);
  /*
  if ([MunerisHelper isIOSVersionGreaterThan:@"4.2"]) {
    _bannerView.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifierPortrait, ADBannerContentSizeIdentifierLandscape, nil];
  }else{
    _bannerView.requiredContentSizeIdentifiers = [NSSet setWithObjects:ADBannerContentSizeIdentifier320x50, ADBannerContentSizeIdentifier480x32, nil];
  }
*/
  _bannerView.delegate = self;
  
  _bannerView.frame = obmAdsView.frame;
  
  _bannerAdsdelegate = [delagate retain];
  
  [obmAdsView addSubview:_bannerView];
  
  [_bannerAdsdelegate didReceiveAds:obmAdsView];
  if (obmAdsView.superview) {
    [obmAdsView.superview bringSubviewToFront:obmAdsView];
  }
}

- (void)bannerViewDidLoadAd:(ADBannerView *)bannerView;
{
  MLOG(@"Muneris : PLUGIN IAD Banner View Action Did Loaded");
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)bannerView willLeaveApplication:(BOOL)willLeave;
{
  return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)bannerView
{
  MLOG(@"Muneris : PLUGIN IAD Banner View Action Did Finish");
}

- (void)bannerView:(ADBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error
{
  MLOG(@"Muneris : PLUGIN IAD Banner View Action Did Fail");
  
  error = [NSError errorWithDomain:@"muneris" code:38 userInfo:[NSDictionary dictionaryWithObject:obmAdsView forKey:@"MunerisAdsView"]];
  
  [_bannerAdsdelegate didFailToReceiveAds:error];

}

- (void) refreshAds
{
  MLOG(@"Muneris: PLUGIN IAD Banner Does NOT Support Refresh Action");
}

-(void) dealloc;
{ 
  
  MLOG(@"* DEALLOC Muneris: PLUGIN IAD");
  
  if (_bannerView) {
    [_bannerView release];
    _bannerView = nil;
  }
  
  if (_bannerAdsdelegate) {
    [_bannerAdsdelegate release];
    _bannerAdsdelegate = nil;
  }
  
  
  if (_viewController) {
    [_viewController release];
    _viewController = nil;
  }
  
  
  [super dealloc];
}

@end
