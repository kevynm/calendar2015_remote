//
//  MunerisIadPlugin.h
//  MunerisKit
//
//  Created by Casper on 09/09/2011.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractMunerisPlugin.h"
#import "MunerisBannerAdsPluginProtocol.h"
#import "MunerisPluginProtocol.h"
#import "iAd/ADBannerView.h"
#import "MunerisConstant.h"

@interface MunerisIadPlugin : AbstractMunerisPlugin<MunerisBannerAdsPluginProtocol> {
  
}

@end

@interface MunerisIad : NSObject<ADBannerViewDelegate> {
  
  ADBannerView *_bannerView;
  id<MunerisBannerAdsDelegate> _bannerAdsdelegate;
  MunerisAdsView* obmAdsView;
  MunerisBannerAdsSizes _bannerSize;
  UIViewController* _viewController;
  int _retryCount;
  int noOfRetries;
  
}

//- (id) initWithSize:(NSString*) size;
-(id) initWithDictionary:(NSDictionary*) dictionary withSize:(MunerisBannerAdsSizes) bannerSize;

- (void) loadAdsWithDelegate:(id<MunerisBannerAdsDelegate>)delagate;

@end