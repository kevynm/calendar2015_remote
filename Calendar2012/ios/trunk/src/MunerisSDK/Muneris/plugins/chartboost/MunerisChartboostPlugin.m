//
//  MunerisChartboostPlugin.m
//  MunerisKit
//
//  Created by Casper Lee on 28/10/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisChartboostPlugin.h"
#import "MunerisMacro.h"

@interface MunerisChartboostPlugin(private)

-(void) cleanUp;
@end

@implementation MunerisChartboostPlugin

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if (self = [self init]) {
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisPluginProtocol))];
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisTakeoverPluginProtocol))];
    
    [self updateConfiguration:dictionary];
  }
  return self;
}

//-(void) loadingTimeOut;
//{
//  MLOG(@"Muneris: CHARTBOOST Loading Timeout");
//  
//  if (_timeoutChecker) {
//    [_timeoutChecker invalidate];
//    _timeoutChecker = nil; 
//  }
//  
//  if (_delegate) {
//    [_delegate didFailToLoadTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_area forKey:@"zone"] forKey:@"chartboost"]];
//  }
//  
//  [self cleanUp];
//}

-(void) updateConfiguration:(NSDictionary *)dict ;
{
  ChartBoost* chartboost = [ChartBoost sharedChartBoost];
  chartboost.appId = [dict objectForKey:@"appId"];
  chartboost.appSignature = [dict objectForKey:@"appSecret"];
  [chartboost setDelegate:self];
}

- (void) applicationDidBecomeActive:(UIApplication *)application
{
  MLOG(@"Muneris: CHARTBOOST APPDELEGATE application did become active");
  [[ChartBoost sharedChartBoost] install];
}


- (void) loadTakeover:(NSString *)area withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate
{
  if (_delegate) {
    MLOG(@"Muneris: CHARTBOOST already running");
    [delegate didFailToLoadTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:area forKey:@"zone"] forKey:@"chartboost"]];
    return;
  }
  _delegate = [delegate retain];
  _area = [area copy];
  
  if ([area isEqualToString:@"moreapps"]) {
    [[ChartBoost sharedChartBoost] loadMoreApps];
  } else {
    [[ChartBoost sharedChartBoost] loadInterstitial];
  }
//  _timeoutChecker = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(loadingTimeOut) userInfo:nil repeats:NO];
}

- (BOOL)shouldDisplayInterstitial:(UIView *)interstitialView;
{
  if (!_delegate) {
    return YES;
  }
  
  BOOL show  = [_delegate shouldShowTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_area forKey:@"zone"] forKey:@"chartboost"]];
  
  if (!show) {
    [self cleanUp];
  }
  
//  if (_timeoutChecker) {
//    [_timeoutChecker invalidate];
//    _timeoutChecker = nil; 
//  }
  
  return show;
}

- (void)didReturnWithNoInterstitial;
{
  if (_delegate) {
    [_delegate didFailToLoadTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_area forKey:@"zone"] forKey:@"chartboost"]];
  }
  [self cleanUp];
}

- (void) didDismissInterstitial:(UIView *)interstitialView {
  if (_delegate) {
    [_delegate didFinishedLoadingTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_area forKey:@"zone"] forKey:@"chartboost"]]; 
  }
  [self cleanUp];
}

-(void) cleanUp;
{
  if (_area) {
    [_area release];
    _area = nil;
  }
  
  if (_delegate) {
    [_delegate release];
    _delegate = nil;
  }
}

-(void) dealloc;
{
  [self cleanUp];
  [super dealloc];
}

@end