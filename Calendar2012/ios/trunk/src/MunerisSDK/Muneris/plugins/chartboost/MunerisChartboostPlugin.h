//
//  MunerisChartboostPlugin.h
//  MunerisKit
//
//  Created by Casper Lee on 28/10/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractMunerisPlugin.h"
#import "MunerisPluginProtocol.h"
#import "MunerisTakeoverPluginProtocol.h"
#import "MunerisConstant.h"
#import "ChartBoost.h"

@interface MunerisChartboostPlugin : AbstractMunerisPlugin<MunerisPluginProtocol, MunerisTakeoverPluginProtocol, ChartBoostDelegate> {
  id<MunerisTakeoverDelegate> _delegate;
  NSString* _area;
//  NSTimer* _timeoutChecker;
}

@end