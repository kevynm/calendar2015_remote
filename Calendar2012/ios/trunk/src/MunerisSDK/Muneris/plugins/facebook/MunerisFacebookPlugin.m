//
//  MunerisFacebookPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/16/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisFacebookPlugin.h"
#import "MunerisManager.h"
#import "MunerisMacro.h"
#import "MunerisApi.h"

@interface MunerisFacebookPlugin(private)

-(void) setAccessTokenToApiRequest;

-(BOOL) isAccessTokenValid;

-(void) checkLogin;

-(void) logout;

-(void) clearFbDelegate;

-(void) setFbApi;

@end

@implementation MunerisFacebookPlugin

const NSString* KAccessToken = @"accessToken";

const NSString* KAccessTokenExpiry = @"accessTokenExpiry";

const NSString* kSetFbFlag = @"setFb";

const NSString* KApiId = @"apiId";

const NSString* kPermissionKey = @"permissions";

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    [self updateConfiguration:dictionary];
    
  }
  return self;
}


-(void) updateConfiguration:(NSDictionary *)dict;
{
  [super updateConfiguration:dict];
  
  
  Facebook* newfb = [[[Facebook alloc] initWithAppId:self.apiId andDelegate:self] autorelease];
  
  _facebook = [self replaceConfigurationWith:newfb oldConfigutration:_facebook];
  
  if ([self isAccessTokenValid]) {
    [_facebook setAccessToken:self.accessToken];
    [_facebook setExpirationDate:self.accessTokenExpiry];
    [self setAccessToken:self.accessToken];
  }
}

-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  [self setFbApi];
  return YES;
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;
{
  
  return [_facebook handleOpenURL:url];
}


-(BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
{
  return [self application:application handleOpenURL:url];
}

-(void) applicationWillTerminate:(UIApplication *)application;
{
  [self storeSave];
}

-(void) checkLogin;
{
  if (![_facebook isSessionValid]) {
    [_facebook authorize:self.permissions];
  }else{
    [self fbDidLogin];
  }
}

-(void) logout;
{
  [_facebook logout:self];
}

-(void) fbAccessWithDelegate:(id<MunerisFacebookDelegate>)delegate;
{
  _fbDelegate = [delegate retain];
  [self checkLogin];
}



-(void) clearFbDelegate;
{
  if (_fbDelegate) {
    [_fbDelegate release];
    _fbDelegate = nil;
  }

}


-(void) fbDidLogin;
{
  if ([_facebook isSessionValid]) {
    [self setAccessToken:[_facebook accessToken]];
    [self setAccessTokenExpiry:[_facebook expirationDate]];
    [_store setObject:[NSNumber numberWithBool:YES] forKey:kSetFbFlag];
    [self setFbApi];
    [self storeSave];
    MLOG(@"Muneris: FACEBOOK LOGIN SAVE SESSION - %@", _store );
  }
  if ([_fbDelegate respondsToSelector:@selector(fbReady:)]) {
    [_fbDelegate fbReady:_facebook];
  }
  [self clearFbDelegate];
}

-(void) fbDidLogout;
{
  if (![_facebook isSessionValid]) {
    [self setAccessTokenExpiry:nil];
    [self setAccessToken:nil];
    [self storeSave];
     MLOG(@"Muneris: FACEBOOK LOGOUT REMOVE SESSION - %@", _store );
  }
  if ([_fbDelegate respondsToSelector:@selector(fbDidLogout:)]) {
    [_fbDelegate fbDidLogout:_facebook];
  }
  [self clearFbDelegate];
}

-(void) fbDidNotLogin:(BOOL)cancelled;
{
  MLOG(@"Muneris: FACEBOOK LOGOUT REMOVE SESSION - %@", _store );
  if ([_fbDelegate respondsToSelector:@selector(fbDidNotLogin:userCancelled:)]) {
    [_fbDelegate fbDidNotLogin:_facebook userCancelled:cancelled];
  }
  [self clearFbDelegate];
}


-(void) setAccessToken:(NSString *)accessToken;
{
  [_store setValue:accessToken forKey:[NSString stringWithFormat:@"%@",KAccessToken]];
  [[[MunerisManager sharedManager].apiInfo objectForKey:@"user"] setValue:accessToken forKey:@"fbtok"];
}


-(NSString*) accessToken;
{
  return [_store objectForKey:KAccessToken];
}

-(void) setAccessTokenExpiry:(NSDate *)accessTokenExpiry;
{
  [_store setValue:accessTokenExpiry forKey:[NSString stringWithFormat:@"%@",KAccessTokenExpiry]];
}


-(NSDate*) accessTokenExpiry;
{
  return [_store objectForKey:KAccessTokenExpiry];
}

-(BOOL) isAccessTokenValid;
{
  return (self.accessToken && [self.accessToken length] > 0 && self.accessTokenExpiry);
}

-(NSString*) apiId;
{
  return [_info objectForKey:KApiId];
}

-(NSArray*) permissions;
{
  return [_info objectForKey:kPermissionKey];
}

-(void) setFbApi;
{
  NSNumber* setFb = [_store objectForKey:kSetFbFlag];
  if (setFb) {
    if ([setFb boolValue]) {
      [MunerisApi apiMethod:@"setFb" payload:nil delegate:self];
    }
  }
}

-(void) didSuccessfulCallingApiWithDictionary:(NSDictionary*) data;
{
  MLOG(@"Muneris: FACEBOOK PLUGIN setFB ok");
  NSNumber* setFb = [_store objectForKey:kSetFbFlag];
  if (setFb) {
    if ([setFb boolValue]) {
      //request done remove flag
      [_store removeObjectForKey:kSetFbFlag];
      [self storeSave];
    }
  }
}

-(void) didFailCallingApiWithError:(NSError*) error;
{
  MLOG(@"Muneris: FACEBOOK PLUGIN setFB failed");
}

-(void) dealloc;
{
  
  [self clearFbDelegate];
  
  if (_facebook) {
    [_facebook release];
    _facebook = nil;
  }
  [super dealloc];
}

@end
