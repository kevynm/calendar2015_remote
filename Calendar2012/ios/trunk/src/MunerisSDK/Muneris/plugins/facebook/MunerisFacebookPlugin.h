//
//  MunerisFacebookPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/16/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Facebook.h"
#import "MunerisPluginProtocol.h"
#import "AbstractMunerisPlugin.h"
#import "MunerisFacebookDelegate.h"
#import "MunerisDelegates.h"

@interface MunerisFacebookPlugin : AbstractMunerisPlugin<MunerisPluginProtocol,FBSessionDelegate, MunerisApiCallDelegate> {

  Facebook* _facebook;
  id<MunerisFacebookDelegate> _fbDelegate;
}

@property (assign) NSString* accessToken;

@property (assign) NSDate* accessTokenExpiry;

@property (readonly) NSString* apiId;

@property (readonly) NSArray* permissions;

-(void) fbAccessWithDelegate:(id<MunerisFacebookDelegate>)delegate;

@end
