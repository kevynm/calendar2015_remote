//
//  MunerisManager+FacebookAddition.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/13/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisManager.h"
#import "MunerisFacebookDelegate.h"

@interface MunerisManager(FacebookAddition)

-(void) fbAccess:(id<MunerisFacebookDelegate>)delegate;

@end
