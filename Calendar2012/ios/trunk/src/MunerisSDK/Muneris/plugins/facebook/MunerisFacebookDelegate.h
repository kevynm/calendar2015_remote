//
//  MunerisFacebookDelegate.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/13/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Facebook.h"

@protocol MunerisFacebookDelegate <NSObject>

@optional
-(void) fbReady:(Facebook*) facebook;

-(void) fbDidNotLogin:(Facebook*) facebook userCancelled:(BOOL)cancelled;

-(void) fbDidLogout:(Facebook*) facebook;

@end
