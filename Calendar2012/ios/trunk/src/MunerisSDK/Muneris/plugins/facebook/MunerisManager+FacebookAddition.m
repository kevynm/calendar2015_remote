//
//  MunerisManager+FacebookAddition.m
//  MunerisKit
//
//  Created by Jacky Yuk on 10/13/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisManager+FacebookAddition.h"
#import "MunerisFacebookPlugin.h"

@implementation MunerisManager(FacebookAddition)

-(void) fbAccess:(id<MunerisFacebookDelegate>)delegate;
{
  for (id<MunerisPluginProtocol> plugin in [self getPluginsByNamespace:@"facebook"]) {
    if ([plugin isKindOfClass:[MunerisFacebookPlugin class]]) {
      if ([plugin respondsToSelector:@selector(fbAccessWithDelegate:)]) {
        [plugin performSelector:@selector(fbAccessWithDelegate:) withObject:delegate];
        return;
      }
    }
  }
}

@end
