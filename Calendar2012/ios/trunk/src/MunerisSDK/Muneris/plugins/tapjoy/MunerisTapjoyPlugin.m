//
//  MunerisTapjoyPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisTapjoyPlugin.h"
#import "MunerisMacro.h"
#import "MunerisConstant.h"
#import "MunerisDelegates.h"
#import "MunerisAdsView.h"
#import "MunerisManager.h"

@interface MunerisTapjoyPlugin(private)


- (void) cleanUpOfferWallDelegate;

- (void) showVideo;


@end



@implementation MunerisTapjoyPlugin


-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisBannerAdsPluginProtocol))];
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisInboxPluginProtocol))];
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisTakeoverPluginProtocol))];
    
    
    
    MLOG(@"Muneris : TAPJOY Plugin Store %@",_store);
    [self updateConfiguration:dictionary];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjcConnectSuccess:) name:TJC_CONNECT_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjcFeatureAppResponse:) name:TJC_FEATURED_APP_RESPONSE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjcOffersDidClose:) name:TJC_SHOW_BOX_CLOSE_NOTIFICATION object:nil];
    
  }
  
  return self;
}

-(void) updateConfiguration:(NSDictionary *)dict;
{
  [super updateConfiguration:dict];
 
  id tmpa  = _appId;
  id tmps  = _appSecret;
  
  _appSecret = [[dict objectForKey:@"appSecret"] copyWithZone:[self zone]];
  _appId = [[dict objectForKey:@"appId"] copyWithZone:[self zone]];
  
  if (tmpa) {
    [tmpa release];
  }
  
  if (tmps) {
    [tmps release];
  }
  
  if ([dict objectForKey:@"TJCTransitionEnum"]) {
    [TapjoyConnect setTransitionEffect:[[dict objectForKey:@"TJCTransitionEnum"] intValue]];
  }
  
  [TapjoyConnect requestTapjoyConnect:_appId secretKey:_appSecret];
  
  [self showVideo];
}


-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  MLOG(@"Muneris: TAPJOY APPDELEGATE application did finish launching");
  return YES;
}

-(void) loadAdsWithSize:(MunerisBannerAdsSizes)size withZone:(NSString*)zone WithDelegate:(id<MunerisBannerAdsDelegate>)delagate withUIViewController:(UIViewController *)viewController;
{
  
  //#TODO Fix memory leak here by creating a MunerisAdsView and del
  MunerisTayjoy* tja = [[[MunerisTayjoy alloc] initWithDictionary:_info withSize:size] autorelease];
  [tja loadAdsWithDelegate:delagate];
  
}

-(void) readInbox:(NSArray *)types withDelegate:(id<MunerisInboxDelegate>)delegate;
{  
  if (_tjp) {
    [_tjp readInbox:types withDelegate:delegate];
  }else{
    [delegate didRecievedMessage:nil];
  }
}


//deprecated
-(void) markUnread:(MunerisMessages *)msgs;
{
//  for (NSDictionary* m in msgs.readMessage) {
//    //NSString* type = [m objectForKey:@"ty"];
//    NSNumber* type = [m objectForKey:@"MunerisMessageType"];
//    if (type) {
//      switch ([type intValue]) {
//        case MunerisCreditsMessage : {
//          // spend the point [_pointsHandler ];
//          NSDictionary* data = [m objectForKey:@"body"];
//          int points = [[data objectForKey:@"credits"] intValue];
//          MunerisTayjoyPoints* tjp = [[[MunerisTayjoyPoints alloc] init] autorelease];
//          [tjp performSelector:@selector(spend:) withObject:[NSNumber numberWithInt:points] afterDelay:0.5];
//          break;
//        }
//        default:
//          break;
//      }
//    }
//  }
}


-(void) showOfferWall:(UIViewController*) viewController;
{
  if (viewController) {
    [TapjoyConnect showOffersWithViewController:viewController]; 
  }else{
    [TapjoyConnect showOffers];
  }
}

-(void) showFeaturedApp:(UIViewController*) viewController delegate:(id<MunerisTakeoverDelegate>)delegate;
{
  MunerisTapjoyFeatureApps* fa = [[[MunerisTapjoyFeatureApps alloc] init] autorelease];
  [fa loadFeaturedAppWith:viewController andDelegate:delegate];
}

-(void) loadTakeover:(NSString*)zone withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate;
{
  NSString* oldZone = zone;
  NSDictionary* toDict = [_info objectForKey:@"takeovers"];
  if (toDict) {
    NSDictionary* zones = [toDict objectForKey:@"zones"];
    if (zones) {
      NSString* takeovers = [zones objectForKey:zone];
      if (takeovers) {
        zone = takeovers;
      }
    }
  }
  
  
  
  if ([zone isEqualToString:@"offers"]) {
    if ([delegate shouldShowTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:zone forKey:@"zone"] forKey:@"tapjoy"]]) {
      if (!_offerWallTakeoverDelegate) {
        _offerWallTakeoverDelegate = [delegate retain];
        _zone = [oldZone copy];
        [self showOfferWall:viewController];
      } else {
        MLOG(@"Muneris TAPJOY: OfferWall Already Running");
      }
    }
  }else{
    MunerisTapjoyTakeoverFeaturedAppDelegateWrapper* wrapper = [[[MunerisTapjoyTakeoverFeaturedAppDelegateWrapper alloc] initWithDelegate:delegate withZone:oldZone] autorelease];
    [self showFeaturedApp:viewController delegate:wrapper];
  }
  
}


-(void) showVideo;
{
  NSNumber* videoCount= [_info objectForKey:@"videoCount"];
  if (videoCount) {
    int vc = [videoCount intValue];
    if (vc > 0) {
      [TapjoyConnect initVideoAdWithDelegate:self];
      [TapjoyConnect setVideoCacheCount:vc];
    }
    
  }
}

// Called when the video ad begins playing.
- (void) videoAdBegan;
{

}

// Called when the video ad is closed.
- (void)videoAdClosed;
{
//  if (_offerWallTakeoverDelegate) {
//    [_offerWallTakeoverDelegate didFinishedLoadingTakeover:[NSDictionary dictionaryWithObject:@"offerwall" forKey:@"tapjoy"]];
//  }
//  [self cleanUpOfferWallDelegate];
}

- (void) cleanUpOfferWallDelegate;
{
  if (_offerWallTakeoverDelegate) {
    [_offerWallTakeoverDelegate release];
    _offerWallTakeoverDelegate = nil;
  }
  
  if (_zone) {
    [_zone release];
    _zone = nil;
  }
}

- (void)tjcOffersDidClose:(NSNotification*)notifyObj {
  MLOG(@"Muneris TAPJOY: OFFERWALL DID CLOSE");
  if (_offerWallTakeoverDelegate) {
    [_offerWallTakeoverDelegate didFinishedLoadingTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"tapjoy"]];
  }
  [self cleanUpOfferWallDelegate];
}


-(void) setUserDefinedColorWithRed:(int)red WithGreen:(int)green WithBlue:(int)blue;
{
  [TapjoyConnect setUserDefinedColorWithRed:red WithGreen:green WithBlue:blue];
}

-(void) tjcFeatureAppResponse:(NSNotification*)notifyObj;
{
  MLOG(@"Muneris TAPJOY: FEATURED APP %@",notifyObj.object);
}


-(void) tjcConnectSuccess:(NSNotification*)notifyObj;
{
  MLOG(@"Muneris TAPJOY: CONNECT SUC %@",notifyObj.object);
  if (!_tjp) {
     _tjp = [[MunerisTayjoyPoints alloc] init];
  }

}

-(void) dealloc;
{
  if (_tjp) {
    [_tjp release];
    _tjp = nil;
  }
  if (_appId) {
    [_appId release];
    _appId = nil;
  }
  
  if (_appSecret) {
    [_appSecret release];
    _appSecret = nil;
  }
  [super dealloc];
}

@end



@interface MunerisTayjoy(private)

-(void) cleanUp;

@end

@implementation MunerisTayjoy


- (id) initWithDictionary:(NSDictionary *)dictionary withSize:(MunerisBannerAdsSizes)bannerSize;
{
  if ((self = [self init])) {
    _bannerSize = bannerSize;
    if ([dictionary objectForKey:@"retryCount"]  != nil) {
      _retryCount = [[dictionary objectForKey:@"retryCount"] intValue];   
    }
    
    if ([dictionary objectForKey:@"shouldReload"] != nil) {
      _shouldReload = [[dictionary objectForKey:@"shouldReload"] boolValue];   
    }
  }
  return self;
}

-(void) loadAdsWithDelegate:(id<MunerisBannerAdsDelegate>)delagate;
{
  
  MunerisAdsView* obmAdsView = [[[MunerisAdsView alloc] initWithSize:_bannerSize] autorelease];
  
  obmAdsView.delegate = self;
  
  _bannerAdsdelegate = [delagate retain];
  
  switch (_bannerSize) {
    case MunerisBannerAdsSizes320x50:
      _tjBannerSize = TJC_AD_BANNERSIZE_320X50;
      break;
    case MunerisBannerAdsSizes728x90:
          _tjBannerSize = TJC_AD_BANNERSIZE_768X90;
          break;      
    default:
      _tjBannerSize = TJC_AD_BANNERSIZE_320X50;
      break;
  }
  
  TJCAdView* tjADView = [TapjoyConnect getDisplayAdView];
  
  [tjADView getAdWithDelegate:self currencyID:nil];
  
  [tjADView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
  
  [obmAdsView addSubview:tjADView];
  
  
  [_bannerAdsdelegate didReceiveAds:obmAdsView];
  
}



- (void) didReceiveAd:(TJCAdView*)adView;
{
  MLOG(@"Muneris PLUGIN : TAPJOY Ads Success.");
}

-(void) didFailWithMessage:(NSString*)msg;
{
  MLOG(@"Muneris : PLUGIN TAPJOY Banner View Action Did Fail.");
  _noOfRetries++;
  // fail fast now
  if (_noOfRetries >= _retryCount) {
    NSError* error = [NSError errorWithDomain:@"muneris" code:38 userInfo:nil];
    [_bannerAdsdelegate didFailToReceiveAds:error];
    return;
  }
}

- (void) refreshAds;
{
  MLOG(@"Muneris: PLUGIN TAPJOY Do not support manual reloading ads");
}

- (NSString*) adContentSize;
{
  return _tjBannerSize;
}


- (BOOL) shouldRefreshAd;
{
  return _shouldReload;
}


-(void) dealloc;
{
  MLOG(@"* DEALLOC Muneris : PLUGIN TAPJOY");
  [[TapjoyConnect getDisplayAdView] getAdWithDelegate:nil];
  if (_tjBannerSize) {
    [_tjBannerSize release];
    _tjBannerSize = nil;
  }
  if (_bannerAdsdelegate) {
    [_bannerAdsdelegate release];
    _bannerAdsdelegate = nil;
  }
  [super dealloc];
}

@end

@interface MunerisTayjoyPoints(private)

-(void) cleanUp;

-(void) reply;

@end


@implementation MunerisTayjoyPoints

-(id) init;
{
  if ((self = [super init])) {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjcConnectSuccess:) name:TJC_CONNECT_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedPoints:) name:TJC_TAP_POINTS_RESPONSE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(spentPoints:) name:TJC_SPEND_TAP_POINTS_RESPONSE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(awardPoints:) name:TJC_AWARD_TAP_POINTS_RESPONSE_NOTIFICATION  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(earnPoints:) name:TJC_TAPPOINTS_EARNED_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjError:) name:TJC_TAP_POINTS_RESPONSE_NOTIFICATION_ERROR object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjError:) name:TJC_SPEND_TAP_POINTS_RESPONSE_NOTIFICATION_ERROR object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjError:) name:TJC_AWARD_TAP_POINTS_RESPONSE_NOTIFICATION_ERROR object:nil];
    _messages = nil;
  }
  return self;
}

-(void) readInbox:(NSArray *)types withDelegate:(id<MunerisInboxDelegate>)delegate;
{
  if (!_inboxdelegate) {
    
    _inboxdelegate = [delegate retain];
    
    BOOL hasCredit;
    for (NSString* type in types) {
      if ([type isEqualToString:@"c"]) {
        hasCredit = YES;
      }
    }
    if (hasCredit) {
      //prepare dictionary
      [TapjoyConnect getTapPoints];
    }else{
      [self reply];
      [self cleanUp];
    }
  }else{
    [delegate didRecievedMessage:nil];
    MLOG(@"Muneris TAPJOY : checking tapjoy points already!");
  }
  
}

-(void) tjcConnectSuccess:(NSNotification*) obj;
{
  MLOG(@"Muneris: TapJoyPoint connected %@",obj);
}

-(void) spentPoints:(NSNotification*) obj;
{
  MLOG(@"Muneris: TapJoyPoint spent %@",obj);
  
  
  if (_inboxdelegate && _points > 0) {
    _messages = [NSMutableArray array];
  
    [_messages addObject:[MunerisMessages createCreditMessageWithPlugin:@"tapjoy" credits:_points reason:@"tapjoyPoints"]];
    
  }
  
  [self reply];
  [self cleanUp];
}

-(void) awardPoints:(NSNotification*) obj;
{
  MLOG(@"Muneris: TapJoyPoint award %@",obj);
}

-(void) earnPoints:(NSNotification*) obj;
{
  MLOG(@"Muneris: TapJoyPoint earned %@",obj);
}

-(void) updatedPoints:(NSNotification*) obj;
{
  MLOG(@"Muneris: TapJoyPoint updated %@",obj);
  NSNumber* tp = obj.object;
  
  _points = [tp intValue];
  if (_points > 0 && _inboxdelegate) {
    [TapjoyConnect spendTapPoints:_points];
  }else{
    [self reply];
    [self cleanUp];
  }
  
}


-(void) tjError:(NSError*)error;
{
  MLOG(@"Muneris: TapJoyPoint error %@",error);
  [self reply];
  [self cleanUp];
}

-(void) reply;
{
  if (_inboxdelegate) {
    if (_messages) {
      [_inboxdelegate didRecievedMessage:[MunerisMessages messages:_messages]];
    }else{
      [_inboxdelegate didRecievedMessage:nil];
    }
  }
  
}

-(void) cleanUp;
{
  _points = 0;
  
  if (_inboxdelegate) {
    [_inboxdelegate release];
    _inboxdelegate = nil;
  }
  
  if (_messages) {
    _messages = nil;
  }
}


-(void) dealloc;
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  
  MLOG(@"* DEALLOC: MUNERIS TapJoyPoints");

  if (_inboxdelegate) {
    [_inboxdelegate release];
    _inboxdelegate = nil;
  }
  
  
  [super dealloc];
}

@end


@implementation MunerisTapjoyFeatureApps

-(id) init;
{
  if ((self = [super init])) {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjcFeatureAppResponse:) name:TJC_FEATURED_APP_RESPONSE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tjcFeatureAppClose:) name:TJC_VIEW_CLOSED_NOTIFICATION object:nil];
  }
  return self;
}

-(void) loadFeaturedAppWith:(UIViewController*) viewController andDelegate:(id<MunerisTakeoverDelegate>)delagate;
{
  if (delagate) {
      _featuredAppDelegate = [delagate retain];
  }
  _viewController = [viewController retain];
  [TapjoyConnect getFeaturedApp];
  [self retain];
}

-(void) tjcFeatureAppResponse:(NSNotification*) notifyObj;
{
  BOOL show = NO;
  if (_featuredAppDelegate) {
    if ([_featuredAppDelegate shouldShowTakeover:nil]) {
      show = YES;
    }
  }else{
    show = YES;
  }
  
  
  if (show) {
    if (_viewController) {
      [TapjoyConnect showFeaturedAppFullScreenAdWithViewController:_viewController];
    }else{
      [TapjoyConnect showFeaturedAppFullScreenAd];
    }
  }
}

-(void) tjcFeatureAppClose:(NSNotification*) notifyObj;
{
  [_featuredAppDelegate didFinishedLoadingTakeover:nil];
  [self release];
}

-(void) dealloc;
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  MLOG(@"* DEALLOC Muneris : PLUGIN TAPJOY FEATURED_APP");
  if (_featuredAppDelegate) {
    [_featuredAppDelegate release];
    _featuredAppDelegate = nil;
  }
  
  if (_viewController) {
    [_viewController release];
    _viewController = nil;
  }
  [super dealloc];
}

@end



@implementation MunerisTapjoyTakeoverFeaturedAppDelegateWrapper

-(id) initWithDelegate: (id<MunerisTakeoverDelegate>) delegate withZone:(NSString*)zone;
{
  if ((self = [self init])) {
    if (delegate) {
      _takeoverDelegate = [delegate retain];
    }
    
    if (zone) {
      _zone = [zone copy];
    }
    
  }
  return self;
}

-(BOOL) shouldShowTakeover:(NSDictionary *)takeover;
{
  if (!_takeoverDelegate) {
    return YES;
  }
  return [_takeoverDelegate shouldShowTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"tapjoy"]];
}

-(void) didFailToLoadTakeover:(NSDictionary*) takeoverInfo;
{
  [_takeoverDelegate didFailToLoadTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"flurry"]];
}

-(void) didFinishedLoadingTakeover:(NSDictionary *)takeoverInfo;
{
  [_takeoverDelegate didFinishedLoadingTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"tapjoy"]];
}

-(void) dealloc;
{
  MLOG(@"* DEALLOC Muneris : PLUGIN TAPJOY TAKEOVER WRAPPER");
  if (_takeoverDelegate) {
    [_takeoverDelegate release];
    _takeoverDelegate = nil;
  }
  
  if (_zone) {
    [_zone release];
    _zone = nil;
  }
  [super dealloc];
}

@end