//
//  MunerisTapjoyPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractMunerisPlugin.h"
#import "MunerisInboxPlugin.h"
#import "MunerisPluginProtocol.h"
#import "MunerisBannerAdsPluginProtocol.h"
#import "MunerisTakeoverPluginProtocol.h"
#import "TapjoyConnect.h"
#import "MunerisConstant.h"

@interface MunerisTayjoyPoints : NSObject {
  
  id<MunerisInboxDelegate> _inboxdelegate;
  
  NSMutableArray* _messages;
  
  int _points;
}

-(void) readInbox:(NSArray*)type withDelegate:(id<MunerisInboxDelegate>) delegate;

@end

@interface MunerisTapjoyPlugin : AbstractMunerisPlugin<MunerisBannerAdsPluginProtocol,MunerisInboxPluginProtocol, MunerisTakeoverPluginProtocol, TJCVideoAdDelegate> {
  
  NSString* _appId;
  NSString* _appSecret;
  id<MunerisTakeoverDelegate> _offerWallTakeoverDelegate;
  NSString* _zone;
  MunerisTayjoyPoints* _tjp;
}

-(void) setUserDefinedColorWithRed:(int)red WithGreen:(int)green WithBlue:(int)blue;

-(void) showFeaturedApp:(UIViewController*) viewController delegate:(id<MunerisTakeoverDelegate>)delegate;

-(void) showOfferWall:(UIViewController*) viewController;

@end

@interface MunerisTayjoy : NSObject<TJCAdDelegate>{
  
  id<MunerisBannerAdsDelegate> _bannerAdsdelegate;
  MunerisBannerAdsSizes _bannerSize;
  NSString* _tjBannerSize;
  int _retryCount;
  int _noOfRetries;
  BOOL _shouldReload;
}

- (id) initWithDictionary:(NSDictionary *)dictionary withSize:(MunerisBannerAdsSizes)bannerSize;

-(void) loadAdsWithDelegate:(id<MunerisBannerAdsDelegate>)delagate;

@end


@interface MunerisTapjoyFeatureApps : NSObject {
  
  id<MunerisTakeoverDelegate> _featuredAppDelegate;
  
  UIViewController* _viewController;
  
}

-(void) loadFeaturedAppWith:(UIViewController*) viewController andDelegate:(id<MunerisTakeoverDelegate>)delagate;

@end

@interface MunerisTapjoyTakeoverFeaturedAppDelegateWrapper : NSObject<MunerisTakeoverDelegate> {
  
  id<MunerisTakeoverDelegate> _takeoverDelegate;
 
  NSString* _zone;
}

-(id) initWithDelegate: (id<MunerisTakeoverDelegate>) delegate withZone:(NSString*)zone;

@end
