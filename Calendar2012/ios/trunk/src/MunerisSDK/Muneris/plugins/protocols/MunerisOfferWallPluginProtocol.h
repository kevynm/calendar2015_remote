//
//  MunerisOfferWallPluginProtocol.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/8/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import "MunerisPluginProtocol.h"

@protocol MunerisOfferWallPluginProtocol <NSObject,MunerisPluginProtocol>


-(void) showOfferWall:(UIViewController*) viewController;

@end
