//
//  MunerisBannerAdsPluginProtocol.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisDelegates.h"
#import "MunerisPluginProtocol.h"
#import "MunerisConstant.h"
#import <UIKit/UIKit.h>

@protocol MunerisBannerAdsPluginProtocol <NSObject,MunerisPluginProtocol>


@required

-(void) loadAdsWithSize:(MunerisBannerAdsSizes)size withZone:(NSString*)zone WithDelegate:(id<MunerisBannerAdsDelegate>)delagate withUIViewController:(UIViewController*) viewController;


@end