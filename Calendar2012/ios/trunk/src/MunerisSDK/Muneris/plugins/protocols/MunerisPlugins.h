//
//  MunerisPlugins.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/5/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//


#import "MunerisEnvarsPlugin.h"

#import "MunerisUdidPlugin.h"

#import "MunerisInboxPlugin.h"

#import "MunerisMediationPlugin.h"

#import "MunerisUrlschemePlugin.h"

#import "MunerisVersionPlugin.h"

#import "MunerisAnalyticsPlugin.h"

#import "MunerisOffersPlugin.h"

//plugin interfaces

#import "MunerisPluginProtocol.h"

#import "MunerisBannerAdsPluginProtocol.h"

#import "MunerisInboxPluginProtocol.h"


#import "MunerisAnalyticsPluginProtocol.h"

#import "MunerisTakeoverPluginProtocol.h"



