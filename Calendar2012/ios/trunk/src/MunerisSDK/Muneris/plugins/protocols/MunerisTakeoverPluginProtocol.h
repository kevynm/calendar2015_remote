//
//  MunerisTakeoverPluginProtocol.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/25/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisDelegates.h"

@protocol MunerisTakeoverPluginProtocol <NSObject>

-(void) loadTakeover:(NSString*)area withViewController:(UIViewController*) viewController delegate:(id<MunerisTakeoverDelegate>) delegate;


@end
