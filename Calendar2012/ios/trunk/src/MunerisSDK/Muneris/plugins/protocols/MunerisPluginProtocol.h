//
//  MunerisModule.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/5/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol MunerisPluginProtocol <NSObject,UIApplicationDelegate>

@required

-(void) addNamespace:(NSString*) ns;

-(NSArray*) namespaces;

-(id) initPluginWithDictionary:(NSDictionary*) dictionary;

-(void) updateConfiguration:(NSDictionary*) dict;

@end