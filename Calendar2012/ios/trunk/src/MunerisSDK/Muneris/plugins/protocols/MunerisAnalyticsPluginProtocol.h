//
//  MunerisAnalyticsProtocol.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/8/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol MunerisAnalyticsPluginProtocol <NSObject>


@required

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;

- (void)logStartEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;

- (void)logEndEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;

@end
