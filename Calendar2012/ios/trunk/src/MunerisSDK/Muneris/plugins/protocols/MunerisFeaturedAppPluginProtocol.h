//
//  MunerisFeatureAppsProtocol.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/8/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import "MunerisPluginProtocol.h"
#import "MunerisDelegates.h"

@protocol MunerisFeaturedAppPluginProtocol <NSObject,MunerisPluginProtocol>


-(void) showFeaturedApp:(UIViewController*) viewController delegate:(id<MunerisFeaturedAppDelegate>)delegate;

@end
