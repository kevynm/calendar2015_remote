//
//  MunerisInboxPluginProtocol.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisPluginProtocol.h"
#import "MunerisDelegates.h"

@class MunerisMessages;

@protocol MunerisInboxPluginProtocol <NSObject,MunerisPluginProtocol>

-(void) readInbox:(NSArray*)type withDelegate:(id<MunerisInboxDelegate>) delegate;

-(void) markUnread:(MunerisMessages*)msgs;


@end
