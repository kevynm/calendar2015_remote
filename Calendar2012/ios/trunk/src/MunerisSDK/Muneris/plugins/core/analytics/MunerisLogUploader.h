//
//  MunerisLogUploader.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/11/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisDelegates.h"
@class MunerisLogger;

@interface MunerisLogUploader : NSObject<MunerisApiCallDelegate> {
  
  MunerisLogger* _logger;
  
  BOOL _isUploading;
  
  NSArray* _logFilesUploading;
  
}


-(id) initWithLogger:(MunerisLogger*) logger;

-(void) upload:(BOOL) nonblocking;

@end
