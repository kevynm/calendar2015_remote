//
//  MunerisInboxPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractMunerisPlugin.h"
#import "MunerisInboxPluginProtocol.h"
#import "MunerisDelegates.h"

@interface MunerisInboxPlugin : AbstractMunerisPlugin<MunerisInboxPluginProtocol> {


  
}

@end


@interface MunerisInbox : AbstractMunerisPlugin<MunerisApiCallDelegate,MunerisInboxDelegate, MunerisInboxPluginProtocol> {
  
  

  
  id<MunerisInboxDelegate> _delegate;
  
  int resultCount;
  
  int requestCount;
  
  int currentRequestDone;
  
  NSMutableArray* _messages;
  
}


@end