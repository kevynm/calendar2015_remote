//
//  MunerisMediationPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/14/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisDelegates.h"
#import "AbstractMunerisPlugin.h"
#import "MunerisPluginProtocol.h"
#import <UIKit/UIKit.h>
#import "MunerisConstant.h"

@interface MunerisMediationPlugin : AbstractMunerisPlugin<MunerisPluginProtocol> {

}

-(void) loadAdswithBannerSize:(MunerisBannerAdsSizes)size withZone:(NSString *)zone withDelegate:(id<MunerisBannerAdsDelegate>)delegate withUIViewController:(UIViewController *)viewController;

-(void) loadTakeover:(NSString*)zone withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate;

-(BOOL) canMediates:(NSString*)type withZone:(NSString*)zone;

@end


@interface MunerisMediation: AbstractMunerisPlugin
{
  id _delegate;
  
  NSDictionary* _evalInfo;
  
  NSMutableDictionary* _plugins;
  
  int _noOfRetries;
}

@property (retain,nonatomic) id delegate;

-(id<MunerisPluginProtocol>) evaluate;

-(NSMutableDictionary*) getPluginsThatConformsTo:(Protocol*) protocol;

@end


@interface MunerisMediateBannerAd : MunerisMediation<MunerisBannerAdsDelegate>
{    
  MunerisAdsView* obmAdsView;
  MunerisBannerAdsSizes obmBannerSize;
  UIViewController* _viewController;
  NSString* _zone;
  
}

-(void) loadAdswithBannerSize:(MunerisBannerAdsSizes)size withZone:(NSString*)zone  withDelegate:(id<MunerisBannerAdsDelegate>) delegate withUIViewController:(UIViewController*) viewController;

@end

@interface MunerisMediateTakeover : MunerisMediation<MunerisTakeoverDelegate>
{
  NSString* _area;
  UIViewController* _viewController;
  NSMutableDictionary* _failedPlugins;
}

-(void) loadTakeover:(NSString*)area withViewController:(UIViewController*) viewController delegate:(id<MunerisTakeoverDelegate>) delegate;



@end