//
//  MunerisLogUploader.m
//  MunerisKit
//
//  Created by Jacky Yuk on 10/11/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisLogUploader.h"
#import "MunerisLogger.h"
#import "MunerisMacro.h"
#import "MunerisApi.h"

@interface MunerisLogUploader(private)

-(void) complete;

-(void) removeFiles:(NSArray*) files;

-(NSArray*) files;

-(NSArray*) logsFromFile:(NSArray*) files; 

@end

@implementation MunerisLogUploader

-(id) initWithLogger:(MunerisLogger*) logger;
{
  if ((self = [self init])) {
    _logger = [logger retain];
  }
  return self;
}


-(void) upload:(BOOL) nonblocking;
{
  if (_isUploading) {
    MLOG(@"Muneris : LOG Uploader upload exits, already uploading!");
    return;
  }
  _isUploading = false;
  
  [_logger spoolToDisk];
  
  _logFilesUploading = [[self files] retain];
  
  NSArray* logs = [self logsFromFile:_logFilesUploading];
  
  //need to deserialise all NSDicts send  files to server if success remove it;
  if (nonblocking) {
    [MunerisApi apiMethod:@"logEvents" payload:[NSDictionary dictionaryWithObject:logs forKey:@"events"] delegate:self];
  }else{
    [MunerisApi apiMethod:@"logEvents" payload:[NSDictionary dictionaryWithObject:logs forKey:@"events"] delegate:self options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:@"sync"]];
  }
  
}

-(void) didSuccessfulCallingApiWithDictionary:(NSDictionary *)data;
{
  MLOG(@"Muneris : Uploader success %@",data);
  [self removeFiles:_logFilesUploading];
  [self complete];
}

-(void) didFailCallingApiWithError:(NSError *)error;
{
  MLOG(@"Muneris : Uploader failed %@",error);
  [self complete];
}

-(void) removeFiles:(NSArray*) files;
{
  @synchronized(self){
    NSString* basepath = [_logger getPath];
    if (files) {
      NSFileManager* fm = [NSFileManager defaultManager];
      for (NSString* file in files) {
        NSString* fullpath = [basepath stringByAppendingPathComponent:file];
        [fm removeItemAtPath:fullpath error:nil];
      }
    }
  }
}

-(void) complete;
{
  _isUploading = NO;
  if (_logFilesUploading) {
    [_logFilesUploading release];
    _logFilesUploading = nil;
  }

}

-(NSArray*) logsFromFile:(NSArray*) files;
{
  NSFileManager* fm = [NSFileManager defaultManager];
  NSMutableArray* arr = [NSMutableArray array];
  for (NSString* file in files) {
    NSString* path = [[_logger getPath] stringByAppendingPathComponent:file];
    if ([fm fileExistsAtPath:path]) {
      NSDictionary* content = [NSDictionary dictionaryWithContentsOfFile:path];
      if (content) {
        NSArray* logs = [content objectForKey:@"logs"];
        if (content) {
          for (NSDictionary* log in logs) {
            [arr addObject:log];
          }
        }
      }
    }
  }
  return arr;
}

-(NSArray*) files;
{
  NSFileManager* fm = [NSFileManager defaultManager];
  NSArray* files = [fm contentsOfDirectoryAtPath:[_logger getPath] error:nil];
  if (!files) {
    return nil;
  }
  if ([files count] < 1) {
    return nil;
  }
  return files;
}


-(void) dealloc;
{
  
  [self complete];
  
  if (_logger) {
    [_logger release];
    _logger = nil;
  }
  [super dealloc];
}


@end
