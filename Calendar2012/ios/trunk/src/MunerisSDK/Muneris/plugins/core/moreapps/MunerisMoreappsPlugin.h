//
//  MunerisMoreappsPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/25/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import "AbstractMunerisPlugin.h"
#import "MunerisPluginProtocol.h"
#import "MunerisTakeoverPluginProtocol.h"
#import "MunerisDelegates.h"
#import "MunerisWebView.h"
//#import "MBProgressHUD.h"


@interface MunerisMoreappsPlugin : AbstractMunerisPlugin<MunerisPluginProtocol, MunerisTakeoverPluginProtocol>


@end

@interface MunerisMoreapps : NSObject <MunerisTakeoverPluginProtocol>
{
//  id<MunerisTakeoverDelegate> _delegate;
  
  MunerisWebView* _webview;
  NSDictionary* _info;
  
//  MBProgressHUD* _hud;
  
//  UIViewController* _viewController;
  
  NSString* _url;
}

- (id) initWithDictionary: (NSDictionary*) dictionary;

@property (copy,nonatomic) NSString* url;

@end