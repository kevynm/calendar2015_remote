//
//  MunerisLogger.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/11/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MunerisLogger : NSObject {
  
  NSMutableArray* _logs;
  
  NSString* _logpath;
  
  NSString* _sessId;
  
  NSDate* _date;
  
  int _logSize;
  
  
}

-(id) initWithSize:(int) size;

-(void) logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;

-(void) spoolToDisk;

-(NSString*) getPath;

@end
