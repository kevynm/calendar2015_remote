//
//  MunerisPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/6/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "AbstractMunerisPlugin.h"
#import "MunerisManager.h"
#import "MunerisMacro.h"

@interface AbstractMunerisPlugin(private)

-(NSMutableDictionary*) store;

@end


@implementation AbstractMunerisPlugin

-(id) init;
{
  if ((self = [super init])) {
    _namespaces = [[NSMutableArray array] retain];
    _store = [[self store] retain];
  }
  return self;
}


-(void) addNamespace:(NSString *)ns;
{
  [_namespaces addObject:ns];
}

-(NSArray*) namespaces;
{
  return _namespaces;
}

-(NSArray*) getPluginsByProtocol:(Protocol*) protocol;
{
  if ([[MunerisManager sharedManager] respondsToSelector:@selector(getPluginsByProtocol:)]) {
    return (NSArray*)[[MunerisManager sharedManager] performSelector:@selector(getPluginsByProtocol:) withObject:protocol];
  }
  return [NSArray array];
}


-(NSArray*) getPluginsByNamespace:(NSString*) namespace;
{
  if ([[MunerisManager sharedManager] respondsToSelector:@selector(getPluginsByNamespace:)]) {
    return (NSArray*)[[MunerisManager sharedManager] performSelector:@selector(getPluginsByNamespace:) withObject:namespace];
  }
  return [NSArray array];
}

-(NSMutableDictionary*) store;
{
  NSString* ns = NSStringFromClass([self class]);
  NSMutableDictionary* store = [[MunerisManager sharedManager].state objectForKey:ns];
  
  
  if (!store) {
    store = [NSMutableDictionary dictionary];
    [[MunerisManager sharedManager].state setValue:store forKey:ns];
    [[MunerisManager sharedManager] saveState];
  }
  MLOG(@"Muneris: Store For Plugin %@  -  %@",NSStringFromClass([self class]), store);
  return store;
}

-(void) updateConfiguration:(NSDictionary *)dict;
{
  _info = [self replaceConfigurationWith:dict oldConfigutration:_info];
}

-(void) storeSave;
{
  [[MunerisManager sharedManager] saveState];
}

-(id) replaceConfigurationWith:(id) newConfig oldConfigutration:(id) oldConfig;
{
  id tmp = oldConfig;
  if (tmp) {
    [tmp release];
  }
  return [newConfig retain];
}

-(void) dealloc;
{
  if (_info) {
    [_info release];
    _info = nil;
  }
  
  if (_store) {
    [_store release];
    _store = nil;
  }
  
  if (_namespaces) {
    [_namespaces release];
    _namespaces = nil;
  }
  [super dealloc];
}




@end
