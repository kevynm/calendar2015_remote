//
//  MunerisLogger.m
//  MunerisKit
//
//  Created by Jacky Yuk on 10/11/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisLogger.h"
#import "MunerisConstant.h"
#import "MunerisMacro.h"

@interface MunerisLogger(private)

-(NSString *) getRandomUUID;

-(int) getTimestamp;

-(BOOL) shouldSpool;

@end

@implementation MunerisLogger


-(id) initWithSize:(int) size;
{
  if((self = [self init]))
  {
    _logSize = size;
  }
  return self;
}

-(id) init;
{
  if ((self = [super init])) {
    _sessId = [[self getRandomUUID] retain];
    _logpath = [self getPath];
    _date = [[NSDate date] retain];
    _logs = [[NSMutableArray array] retain];
    if (!_logSize) {
      _logSize = 10;
    }
  }
  return self;
}

-(void) logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  NSMutableDictionary* logEntry = [NSMutableDictionary dictionary];
  if (parameters) {
    [logEntry setObject:parameters forKey:@"params"];
  }
  if (eventName) {
    [logEntry setObject:eventName forKey:@"ev"];
  }
  [logEntry setObject:_sessId forKey:@"sid"];
  [logEntry setObject:[NSNumber numberWithInt:[self getTimestamp]] forKey:@"ts"];
  @synchronized(_logs)
  {
    [_logs addObject:logEntry];
  }
  if ([self shouldSpool]) {
    [self spoolToDisk];
  }
}

-(BOOL) shouldSpool;
{
  BOOL spool;
  @synchronized (_logs)
  {
    spool = ([_logs count] >= _logSize);
  }
  return spool;
}

-(void) spoolToDisk;
{
  @synchronized(_logs)
  {
    //write to disk and creats a new _log array;
    if ([_logs count] < 1) {
      MLOG(@"Muneris : Logger trying to spool empty array");
      return;
    }
    NSString* file = [[self getPath] stringByAppendingPathComponent:
     [NSString stringWithFormat:@"%@-%i.log",_sessId,[self getTimestamp]]
     ];
    
    NSDictionary* dict = [NSDictionary dictionaryWithObject:_logs forKey:@"logs"];
    
    NSString *errorStr = nil;
    
    NSData* data = [NSPropertyListSerialization dataFromPropertyList:dict format:NSPropertyListBinaryFormat_v1_0 errorDescription:&errorStr];
    
    if (errorStr) {
      NSLog(@"Muneris : Logger Failed to serialize logs %@",dict);
      return;
    }
    
    [data writeToFile:file atomically:NO];
    MLOG(@"Muneris : Logger write streams to file data %@ file %@", data , file);
    [_logs removeAllObjects];
  }
}

-(int) getTimestamp;
{
  return [[NSDate date] timeIntervalSince1970];
}

-(NSString *) getRandomUUID;
{
	CFUUIDRef theUUID = CFUUIDCreate(NULL);
	CFStringRef stringUUID = CFUUIDCreateString(NULL, theUUID);
	CFRelease(theUUID);
	return [(NSString *)stringUUID autorelease];
}

-(NSString*) getPath;
{
  if (!_logpath) {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* directory = [[[paths objectAtIndex:0] stringByAppendingPathComponent:kMuenrisFolder] stringByAppendingPathComponent:@"analytics"];    
    NSFileManager* fm = [NSFileManager defaultManager];
    BOOL isDir = YES;
    if(![fm fileExistsAtPath:directory isDirectory:&isDir])
      if(![fm createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:nil])
        NSLog(@"Muneris Logger Error: Creating folder %@ failed.",directory);
    _logpath = [directory copy];
  }
  return _logpath;
}

-(void) dealloc;
{
  if (_sessId) {
    [_sessId release];
    _sessId = nil;
  }
  
  if (_logpath) {
    [_logpath release];
    _logpath = nil;
  }
  
  if (_logs) {
    [_logs release];
    _logs = nil;
  }
  if (_date) {
    [_date release];
    _date = nil;
  }
  [super dealloc];
}

@end
