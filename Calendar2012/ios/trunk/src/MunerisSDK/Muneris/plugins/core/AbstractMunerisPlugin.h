//
//  MunerisPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/6/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AbstractMunerisPlugin : NSObject {
  NSMutableArray* _namespaces;
  NSMutableDictionary* _store;
  NSDictionary* _info;
  
}


-(void) addNamespace:(NSString*) ns;

-(NSArray*) namespaces;

-(NSArray*) getPluginsByProtocol:(Protocol*) protocol;

-(NSArray*) getPluginsByNamespace:(NSString*) ns;

-(void) updateConfiguration:(NSDictionary*) dict;

-(id) replaceConfigurationWith:(id) newConfig oldConfigutration:(id) oldConfig;

-(void) storeSave;

@end
