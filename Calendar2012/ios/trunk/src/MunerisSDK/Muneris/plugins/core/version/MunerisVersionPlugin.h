//
//  MunerisVerisonPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/27/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractMunerisPlugin.h"
#import "MunerisPluginProtocol.h"
#import "MunerisDelegates.h"
@interface MunerisVersionPlugin : AbstractMunerisPlugin<MunerisPluginProtocol, MunerisApiCallDelegate> {
    
}

-(void) checkAppVersion;

@end


@interface MunerisVersionUpdateViewDelegate : NSObject {
  
  NSString* _appleId;

  BOOL _criticalUpdate;
}

-(id) initWithCriticalUpdates;

@property (copy) NSString* appleId;


@end