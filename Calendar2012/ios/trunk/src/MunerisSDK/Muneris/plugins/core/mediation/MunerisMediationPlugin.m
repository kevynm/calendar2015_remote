//
//  MunerisMediationPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/14/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisMediationPlugin.h"
#import "MunerisBannerAdsPluginProtocol.h"
#import "MunerisTakeoverPluginProtocol.h"
#import "MunerisMacro.h"
#import "MunerisManager.h"


#pragma mark MunerisMediation
@implementation MunerisMediation

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    _evalInfo = [dictionary retain];
  }
  return self;
}


-(id<MunerisPluginProtocol>) evaluate;
{
  
  // Calculate the total weight
  if ([_plugins count] < 1) {
    return nil;
  }
  
  float totalWeight = 0.0;
  for (NSString* key in _plugins) {
    float keyWeight = [[[_plugins objectForKey:key] objectForKey:@"weight"] floatValue];
    totalWeight += keyWeight;
  }
  
  //Setup the Number Format
  NSNumberFormatter *formatter = [[[NSNumberFormatter alloc] init] autorelease];
  [formatter setMaximumFractionDigits:1];
  [formatter setRoundingMode:NSNumberFormatterRoundHalfEven];
  
  // Initiate the plugin dictionary
  NSMutableArray * pluginPtr = [NSMutableArray array];
  NSMutableArray * pluginKeys = [NSMutableArray array];

  for (NSString* key in _plugins) {
    float keyWeight = [[[_plugins objectForKey:key] objectForKey:@"weight"] floatValue];
    int propotion = [[formatter stringFromNumber:[NSNumber numberWithFloat:keyWeight]] intValue];
    for (int i = 0; i < propotion; i++) {
      [pluginPtr addObject:[[_plugins objectForKey:key] objectForKey:@"plugin"]];
      [pluginKeys addObject:key];
    }
  }
  

  
  // Random Number
  if ([pluginPtr count] < 1) {
    return nil;
  }
  int randomNumber = arc4random() % [pluginPtr count];
  
  MLOG(@"Muneris Mediation Plugins: %@", pluginPtr);
  MLOG(@"Muneris Mediation Key: %@", pluginKeys);
  MLOG(@"Muneris Mediation Seed: %i", randomNumber);
  
  [_plugins removeObjectForKey:[pluginKeys objectAtIndex:randomNumber]];
  
  id<MunerisPluginProtocol> selPlugin = [pluginPtr objectAtIndex:randomNumber];
  
  MLOG(@"Muneris : Mediation Selected this plugin - %@",selPlugin);
  
  return selPlugin;
  
}


-(NSMutableDictionary*) getPluginsThatConformsTo:(Protocol*) protocol;
{
  NSMutableDictionary* weightedPlugins = [NSMutableDictionary dictionary];
  id key[2];
  key[0] = @"plugin";
  key[1] = @"weight";
  for (NSString* pluginName in [_evalInfo allKeys]) {
    for (id<MunerisPluginProtocol> plugin in [self getPluginsByNamespace:pluginName]) {
      if ([plugin conformsToProtocol:protocol]) {
        id val[2];
        val[0] = plugin;
        val[1] = [_evalInfo objectForKey:pluginName];
        [weightedPlugins setObject:[NSDictionary dictionaryWithObjects:val forKeys:key count:2] forKey:pluginName];
      }
    }
  }
  MLOG(@"Muneris : Mediation getPlugin %@",weightedPlugins);
  if (!_plugins) {
    _plugins = [weightedPlugins retain];
  }
  return weightedPlugins;
}

@synthesize delegate = _delegate;

-(void) dealloc;
{
  if (_delegate) {
    [_delegate release];
    _delegate = nil;
  }
  
  if (_evalInfo) {
    [_evalInfo release];
    _evalInfo = nil;
  }
  
  if (_plugins) {
    [_plugins release];
    _plugins = nil;
  }
  [super dealloc];
}

@end


#pragma mark MediationPlugin
@implementation MunerisMediationPlugin

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    [self updateConfiguration:dictionary];
  }
  return self;
}


-(void) loadAdswithBannerSize:(MunerisBannerAdsSizes)size withZone:(NSString *)zone withDelegate:(id<MunerisBannerAdsDelegate>)delegate withUIViewController:(UIViewController *)viewController;
{
  
  NSDictionary* adInfo = [_info objectForKey:@"ads"];
  if (adInfo) {
    NSDictionary* info = [adInfo objectForKey:zone];
    
    MunerisMediateBannerAd* ad = [[[MunerisMediateBannerAd alloc] initPluginWithDictionary:info] autorelease];
    
    [ad loadAdswithBannerSize:size withZone:zone withDelegate:delegate withUIViewController:viewController];
  }
  
  
}


-(void) loadTakeover:(NSString*)zone withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate;
{
  NSDictionary* toinfo = [_info objectForKey:@"takeovers"];
  MLOG(@"asd %@",_info);
  if (toinfo) {
    NSDictionary* info = [toinfo objectForKey:zone];
    MunerisMediateTakeover* to = [[[MunerisMediateTakeover alloc] initPluginWithDictionary:info] autorelease];
    [to loadTakeover:zone withViewController:viewController delegate:delegate];
  }
}

-(BOOL) canMediates:(NSString*)type withZone:(NSString*)zone;
{
  NSDictionary* toinfo = [_info objectForKey:type];
  int weight = 0;
  if (toinfo) {
    NSDictionary* info = [toinfo objectForKey:zone];
    if (info) {
      for (NSString* dict in  [info allKeys]) {
        int num = [[info objectForKey:dict] intValue];
        weight = weight + num;
      }
    }
  }
  if (weight > 0) {
    return YES;
  }else{
    return NO;
  }
}

-(void) dealloc;
{
  [super dealloc];
}

@end






#pragma mark Mediates BannerAds
@interface MunerisMediateBannerAd(private)

-(id<MunerisBannerAdsPluginProtocol>) evaluate;
-(void)loadAds;

@end 


@implementation MunerisMediateBannerAd


-(id<MunerisBannerAdsPluginProtocol>) evaluate;
{
  return (id<MunerisBannerAdsPluginProtocol>) [super evaluate];
}



-(void) loadAdswithBannerSize:(MunerisBannerAdsSizes)size withZone:(NSString*)zone  withDelegate:(id<MunerisBannerAdsDelegate>) delegate withUIViewController:(UIViewController*) viewController;
{
  

  [self getPluginsThatConformsTo:@protocol(MunerisBannerAdsPluginProtocol)];
  
  self.delegate = delegate;
  
  obmAdsView = [[[MunerisAdsView alloc] initWithSize:size] autorelease];
  
  obmAdsView.delegate = self;
  
  _viewController = [viewController retain];
  
  obmBannerSize = size;
  
  _zone = [zone copy];
  
  [self loadAds];
}

-(void) loadAds;
{
  id<MunerisBannerAdsPluginProtocol> plugin = [self evaluate];  
  
  if (!plugin) {
    //to clean up
    NSError* medAdError = [NSError errorWithDomain:@"muneris" code:123 userInfo:[NSDictionary dictionaryWithObject:obmAdsView forKey:@"MunerisAdView"]];
    if (self.delegate) {
      [self.delegate didFailToReceiveAds:medAdError];
    }
    [obmAdsView removeFromSuperview];
    return;
  }
  
  [[self delegate] didReceiveAds:obmAdsView];
  
  [plugin loadAdsWithSize:obmBannerSize withZone:_zone WithDelegate:self withUIViewController:_viewController];
}

-(void) didReceiveAds:(MunerisAdsView *)view;
{
  if ([obmAdsView isKindOfClass:[MunerisAdsView class]]) {
    [obmAdsView addSubview:view];
    if (obmAdsView.superview) {
      [obmAdsView.superview bringSubviewToFront:view];
    }
  }
  
}

-(void) didFailToReceiveAds:(NSError *)error;
{
  //retry done
  if (obmAdsView) {
    for (UIView* view  in [obmAdsView subviews]) {
      [view removeFromSuperview];
    }
  }
  
  [self loadAds];
}

- (void) refreshAds;
{
  for (MunerisAdsView* view in [obmAdsView subviews]) {
    if ([view respondsToSelector:_cmd]) {
      [view performSelector:_cmd];
    }
  }
}

-(void) reloadAdsWithViewController: (UIViewController*) viewController;
{
  for (MunerisAdsView* view in [obmAdsView subviews]) {
    if ([view respondsToSelector:_cmd]) {
      [view performSelector:_cmd withObject:viewController];
    }
  }
}

-(id<MunerisBannerAdsDelegate>) delegate;
{
  return [super delegate];
}

-(void) dealloc;
{
  MLOG(@"* DEALLOC MUNERIS: MEDIATION ADS");
  if (_zone) {
    [_zone release];
    _zone = nil;
  }
  if (_viewController) {
    [_viewController release];
    _viewController = nil;
  }
  
  [super dealloc];
}
@end


#pragma mark Mediates Takeover
@implementation MunerisMediateTakeover

-(void) loadTakeover:(NSString*)area withViewController:(UIViewController*) viewController delegate:(id<MunerisTakeoverDelegate>) delegate;
{ 
  
  
  if (delegate) {
    [self setDelegate:delegate];
  }
  
  
  
  [self getPluginsThatConformsTo:@protocol(MunerisTakeoverPluginProtocol)];
  
  id<MunerisTakeoverPluginProtocol> plugin = (id<MunerisTakeoverPluginProtocol>)[self evaluate];
  
  if (area) {
    _area = [area retain];
  }
  
  if(!_failedPlugins)
  {
    _failedPlugins = [[NSMutableDictionary dictionaryWithObject:@"depleted" forKey:@"all"] retain];
  }
  
  if (viewController) {
    _viewController = [viewController retain];
  }
  
  if(plugin){
    [self retain];
    [plugin loadTakeover:_area withViewController:_viewController delegate:self];
  }else{
    MLOG(@"Muneris : MEDIATE TAKEOVER no plugin found.");
    [self.delegate didFailToLoadTakeover:_failedPlugins];
  }
  
}

-(void) didFinishedLoadingTakeover:(NSDictionary *)takeoverInfo;
{
  [self.delegate didFinishedLoadingTakeover:takeoverInfo];
  [self release];
}

-(BOOL) shouldShowTakeover:(NSDictionary *)takeover;
{
  _noOfRetries = 0;
  if (self.delegate) {
    //    id<MunerisTakeoverDelegate> del = self.delegate;
    return [self.delegate shouldShowTakeover:takeover];
  }
  [self release];
  return YES;
}

-(void) didFailToLoadTakeover:(NSDictionary*) takeoverInfo;
{
  for (NSString* toKey in [takeoverInfo allKeys]) {
    [_failedPlugins setObject:[takeoverInfo objectForKey:toKey] forKey:toKey];
  }
  _noOfRetries ++;
  if (_noOfRetries <= 3) {
    MLOG(@"Muneris TAKEOVER : retry no. %d",_noOfRetries);
    //rotated and mediate
    [self loadTakeover:nil withViewController:nil delegate:nil];
  }else{
    id<MunerisTakeoverDelegate> del = self.delegate;
    MLOG(@"Muneris TAKEOVER : failed retries %d",_noOfRetries);
    [del didFailToLoadTakeover:takeoverInfo];
  }
  
  [self release];
}


-(void) dealloc;
{
  
  MLOG(@"* Muneris DEALLOC : TAKEOVER MEDIATION");
  if (_area) {
    [_area release];
    _area = nil;
  }
  
  if(_failedPlugins)
  {
    [_failedPlugins release];
    _failedPlugins = nil;
  }
  
  if (_viewController) {
    [_viewController release];
    _viewController = nil;
  }
  
  if (_delegate) {
    [_delegate release];
    _delegate = nil;
  }
  
  [super dealloc];
}

@end




