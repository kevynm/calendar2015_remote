//
//  MunerisOffersPlugin.m
//  MunerisKit
//
//  Created by Casper Lee on 20/10/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisOffersPlugin.h"
#import "MunerisMacro.h"
#import "MunerisManager.h"
#import "MunerisAlert.h"
#import "MunerisHelper.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"

@interface MunerisOffersPlugin(private)
-(void) cleanUp;
@end

@implementation MunerisOffersPlugin

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    [self updateConfiguration:dictionary];
  }
  return self;
}

-(void) updateConfiguration:(NSDictionary *)dict;
{
  [super updateConfiguration:dict];
  
  if (_offersArray) {
    [_offersArray release];
    _offersArray = nil;
  }
  _offersArray = [[NSArray alloc] initWithArray:[_info objectForKey:@"offeringItems"]];
}

//-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
//{
//  //Caching Image
//  for (NSDictionary* dict in _offersArray) {
////    MLOG(@"%@", dict);
//    
//    if ([dict objectForKey:@"zone"]) {
//      MLOG(@"%@", [dict objectForKey:@"image"]);
//      UIImage *cachedImage = [[SDWebImageManager sharedManager] imageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]]];
//      [[SDImageCache sharedImageCache] storeImage:cachedImage forKey:[dict objectForKey:@"zone"] toDisk:YES]; 
//    }
//  }
//  return YES;
//}

-(BOOL) hasOffers;
{
  BOOL offersAvailable = NO;
  
  for (NSDictionary *offers in _offersArray) {
    if ([[MunerisManager sharedManager] hasTakeover: ([offers objectForKey:@"zone"])]) {
      offersAvailable = YES;
    }
  }
  return offersAvailable;
}

#pragma mark - Call Plugins

-(void) showOffers:(UIViewController*) viewController delegate:(id<MunerisOffersDelegate>) delegate;
{   
  if (!_viewController && !_delegate) {
    if (viewController) {
      _viewController = [viewController retain];
    }
    if (delegate) {
      _delegate = [delegate retain];
    }
  }else{
    MLOG(@"Muneris Offers Plugin: Plugin already in used");
    return;
  }
  
  if (![MunerisHelper isInternetReachable] || [_offersArray count] == 0) {
    NSString* alertTitle = [[_info objectForKey:@"messages"] objectForKey:@"title"];
    NSString* alertErrMsg = [[_info objectForKey:@"messages"] objectForKey:@"errorMessage"];
    MunerisAlert* errorAlert = [[[MunerisAlert alloc] initWithTitle:alertTitle message:alertErrMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [errorAlert show];
  } else {
    if ([_offersArray count] == 1) {
      NSString* zone = [[_offersArray objectAtIndex:0] objectForKey:@"zone"];
      [[MunerisManager sharedManager] loadTakeover:zone withViewController:_viewController delegate:_delegate];
      [self cleanUp];
    } else {
      MunerisOffersView* offersView = [[[MunerisOffersView alloc] initWithDictionary:_info andViewController:_viewController] autorelease];
      offersView.delegate = [_delegate retain];
      [offersView.view setAlpha:0.0];
      [_viewController.view addSubview:offersView.view];
      
      [UIView beginAnimations:@"displayOffers" context:nil];
      [offersView.view setAlpha:1.0];
      [UIView commitAnimations];
      
      [self cleanUp];  
    }
  }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (buttonIndex == 0) {
    [_delegate didClosedOffersView]; 
  }
  [self cleanUp];
}

-(void) cleanUp;
{
  if (_viewController) {
    [_viewController release];
    _viewController = nil;
  }
  
  if (_delegate) {
    [_delegate release];
    _delegate = nil;
  }
  
}

- (void) dealloc {
  if (_offersArray) {
    [_offersArray release];
    _offersArray = nil;
  }
  
  [super dealloc];
}

@end

#pragma mark -
#pragma mark Muneris Offers View

@interface MunerisOffersView (private)
- (void) positionView;
@end

@implementation MunerisOffersView

@synthesize info = _info, viewController = _viewController, delegate = _delegate;
@synthesize offersTitle = offersTitle, offersCloseMsg = offersCloseMsg, offersTableView = offersTableView;

-(id) initWithDictionary:(NSDictionary *)dictionary andViewController: (UIViewController*) viewController;
{
  
  if ((self = [super initWithNibName:@"MunerisOffersView" bundle:[NSBundle mainBundle]])) {
    _info = [dictionary retain];
    _offersArray = [[NSMutableArray alloc] init];
    
    NSArray* offeringItems = [[NSArray alloc] initWithArray:[_info objectForKey:@"offeringItems"]];
    
    for (NSDictionary* dict in offeringItems) {
      if ([[MunerisManager sharedManager] hasTakeover:[dict objectForKey:@"zone"]]) {
        [_offersArray addObject:dict];
      }
    }
    
    _viewController = [viewController retain];
    
    [self positionView];
  }
  return self;
}

-(void) positionView;
{
  CGRect rect = [[UIScreen mainScreen] bounds];
  if (_viewController) {
    rect = _viewController.view.bounds;
  }
  [self.view setFrame:rect];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [self retain];
  [offersTitle setText:[NSString stringWithFormat:@"  %@%", [[_info objectForKey:@"messages"] objectForKey:@"title"]]];
  [offersCloseMsg setTitle:[[_info objectForKey:@"messages"] objectForKey:@"closeButton"] forState:UIControlStateNormal];
  
  [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.7]];
  
  [super viewDidLoad];
}

- (void) viewDidUnload;
{
  [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  return YES;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
  [self positionView];
}

#pragma mark - Table View Data Source Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  
  UITableViewCell *cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
  CGFloat tableHeight = [_offersArray count] * cell.frame.size.height;
  int maxRow = [[[_info objectForKey:@"messages"] objectForKey:@"maxRows"] intValue];
  
  
  if (tableHeight > cell.frame.size.height * maxRow) {
    tableHeight = cell.frame.size.height * maxRow + 10;
    
    UILabel *paddingLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, tableHeight + 50, tableView.superview.frame.size.width, 10)] autorelease];
    paddingLabel.backgroundColor = [offersTitle backgroundColor];
    
    [tableView.superview addSubview:paddingLabel];
    [tableView setScrollEnabled:YES];
  } else {
    tableHeight += 10; 
  }
  
  tableView.frame = CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, tableHeight);
  tableView.backgroundColor = [offersTitle backgroundColor];
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [_offersArray count];
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 2;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
  [[cell textLabel] setBackgroundColor:[UIColor clearColor]];
  [[cell detailTextLabel] setBackgroundColor:[UIColor clearColor]];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
  UITableViewCell *cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
	NSString* offerTitle = [[_offersArray objectAtIndex:[indexPath row]] objectForKey:@"title"];
  
  [cell.textLabel setText:offerTitle];
  [cell.textLabel setTextColor:[UIColor whiteColor]];
  [cell.textLabel setFont:[UIFont boldSystemFontOfSize:14]];
  [cell.textLabel setBackgroundColor:[UIColor clearColor]];
  
  [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
  [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
  
  UIView *cellBg = [[[UIView alloc] initWithFrame:cell.frame] autorelease];
  cellBg.backgroundColor = [UIColor grayColor];
  cell.backgroundView = cellBg;
  
//  NSString* offerImage = [[_offersArray objectAtIndex:[indexPath row]] objectForKey:@"zone"];
//  UIImage* cellImage = [[SDImageCache sharedImageCache] imageFromKey:offerImage];
//  [cell.imageView setImage:cellImage];
  
  return cell;
}

#pragma mark - User Actions
- (IBAction)closeOffers:(id)sender;
{
  [self.view setUserInteractionEnabled:NO];
  [UIView beginAnimations:@"closeOffers" context:nil];
  [self.view setAlpha:0.0];
  [UIView setAnimationDelegate:self];
  [UIView setAnimationDidStopSelector:@selector(removeOffersView)];
  [UIView commitAnimations];
}

- (void) removeOffersView;
{
  [self.view removeFromSuperview];
  [self.delegate didClosedOffersView];
  [self release];
}

#pragma mark - Table View Delegate Methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSString* zone = [[_offersArray objectAtIndex:indexPath.row] objectForKey:@"zone"];
  
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
//  [self closeOffers:indexPath];7
  
  if (!_hud) {
    _hud = [[MBProgressHUD alloc] initWithView:self.view];
  }

  [self.view addSubview:_hud];
  [_hud setLabelText:@"Loading..."];
  [_hud setDimBackground:YES];
  [_hud show:YES];

  [[MunerisManager sharedManager] loadTakeover:zone withViewController:_viewController delegate:self];
}

- (void) dismissOffersLoadingView;
{
  [_hud hide:YES];
}

#pragma mark - Takeover Delegate

-(BOOL) shouldShowTakeover:(NSDictionary*) takeoverInfo;
{
  [self dismissOffersLoadingView];
  if (![_delegate shouldShowTakeover:takeoverInfo]) {
    return NO;
  }
  return YES;
}

-(void) didFailToLoadTakeover:(NSDictionary*) takeoverInfo;
{
  [self dismissOffersLoadingView];
  [_delegate didFailToLoadTakeover:takeoverInfo];
}

-(void) didFinishedLoadingTakeover:(NSDictionary*) takeoverInfo;
{
  [self dismissOffersLoadingView];
  [_delegate didFinishedLoadingTakeover:takeoverInfo];
}

#pragma mark - 

- (void)dealloc
{
  if (_info) {
    [_info release];
    _info = nil;
  }
  
  if (offersTitle) {
    [offersTitle release];
    offersTitle = nil;
  }
  
  if (offersCloseMsg) {
    [offersCloseMsg release];
    offersCloseMsg = nil;
  }
  
  if (_viewController) {
    [_viewController release];
    _viewController = nil;
  }
  
  if (_delegate) {
    [_delegate release];
    _delegate = nil;
  }
  
  if (_offersArray) {
    [_offersArray release];
    _offersArray = nil;
  }
  
  [super dealloc];
}

@end