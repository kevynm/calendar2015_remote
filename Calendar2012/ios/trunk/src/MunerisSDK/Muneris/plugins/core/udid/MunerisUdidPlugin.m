
//
//  MunerisUDIDPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/9/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisUdidPlugin.h"
#import "MunerisManager.h"
#import "MunerisStateManager.h"
#import "MunerisState.h"
#import "UIDevice+IdentifierAddition.h"

@interface MunerisUdidPlugin(private)


@end


@implementation MunerisUdidPlugin

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    [self getUDID];
  }
  return self;
}


-(NSString*) getUDID;
{
  if (![MunerisManager sharedManager].stateManager.state.udid) {
    if([[UIDevice currentDevice] respondsToSelector:@selector(uniqueIdentifier)]){
      if ([[UIDevice currentDevice] uniqueIdentifier]) {
        [MunerisManager sharedManager].stateManager.state.udid = [[UIDevice currentDevice] uniqueIdentifier];
        [[MunerisManager sharedManager] saveState];
        return [MunerisManager sharedManager].udid;
      }
    }else{
      [MunerisManager sharedManager].stateManager.state.udid = [[UIDevice currentDevice] macaddress];
      [[MunerisManager sharedManager] saveState];
    }
  }
  return [MunerisManager sharedManager].stateManager.state.udid;
}


@end