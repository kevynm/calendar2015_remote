//
//  MunerisInboxPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisInboxPlugin.h"
#import "MunerisManager.h"
#import "MunerisApi.h"
#import "MunerisMacro.h"
#import "MunerisAlert.h"



@implementation MunerisInboxPlugin

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  
  if ((self = [self init])) {
    [self updateConfiguration:dictionary];
  }
  return self;
}

-(void) readInbox:(NSArray*)type withDelegate:(id<MunerisInboxDelegate>)delegate;
{
  MunerisInbox* inb = [[[MunerisInbox alloc] initPluginWithDictionary:_info] autorelease];
  [inb readInbox:type withDelegate:delegate];

}

-(void) markUnread:(MunerisMessages *)msgs;
{


}

-(void) dealloc;
{
  [super dealloc];
}

@end


@interface MunerisInbox (private)

-(void) callback:(NSArray*) msgs;

-(NSMutableDictionary*) convertMessageType:(NSMutableDictionary*) msg;
@end


@implementation MunerisInbox


-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    _info = [dictionary retain];
  }
  return self;
}

-(id) init;
{
  if ((self = [super init])) {
    _messages = [[NSMutableArray alloc] init];
  }
  return self;
}

-(void) readInbox:(NSArray*)type withDelegate:(id<MunerisInboxDelegate>)delegate;
{
  [self retain];
  
  NSString* types[[type count]];
  
  for (id ty in type) {
    int index = [type indexOfObject:ty];
    if ([ty isKindOfClass:[NSString class]]) {
      types[index] = ty;
      break;
    }
    if ([ty isKindOfClass:[NSNumber class]]) {
      switch ([ty intValue]) {
        case MunerisCreditsMessage:
          types[index] = @"c";
          break;
        case MunerisTextMessage:
          types[index] = @"t";
          break;
        default:
          break;
      }
    }
    
  }
  
  NSArray* msgType = [NSArray arrayWithObjects:types count:[type count]];
  
  requestCount = 1;
  
  _delegate = [delegate retain];
  
  NSMutableDictionary* payload = [NSMutableDictionary dictionaryWithDictionary:_info];
  [payload setObject:msgType forKey:@"types"];
  
  
  
  MunerisApi* req = [MunerisApi apiMethod:@"readMessages" payload:payload delegate:self zone:[self zone]];
  
  [req retain];
  
  MLOG(@"Muneris : INBOX PLUGIN READ with api request %@", req);
  
  NSArray* plugins = [self getPluginsByProtocol:@protocol(MunerisInboxPluginProtocol)];
  
  MLOG(@"Muneris : INBOX PLUGIN LOOP %@",plugins);
  
  
  for (id<MunerisInboxPluginProtocol> plugin in plugins) {
    if ([plugin class] != [MunerisInboxPlugin class]) {
      requestCount++;
      
      [plugin readInbox:msgType withDelegate:self];
    }
  }
  [req release];
}


-(void) didSuccessfulCallingApiWithDictionary:(NSDictionary*) data;
{
  //parse payload
  NSDictionary* payload = [data objectForKey:@"params"];
  if (!payload) {
    [self callback:nil];
    return;
  }
  
  NSArray* msgs = [payload objectForKey:@"messages"];
  
  if (![msgs isKindOfClass:[NSArray class]]) {
    [self callback:nil];
    return;
  }
  [self callback:msgs];
}

-(void) didFailCallingApiWithError:(NSError*) error;
{
  [self callback:nil];
}

-(void) didRecievedMessage:(MunerisMessages*) msgs;
{
  
  [self callback:(msgs)?msgs.messages:nil];
}

-(void) didFailedToRecieveMessage:(NSError*) error;
{
  [self callback:nil];
}

-(void) callback:(NSArray*)msgs;
{
  if (msgs) {
    //alert  not done, should decouple this.
    for (NSDictionary* msg in msgs) {
      
      resultCount++;
      if (msg) {
        
        msg = [self convertMessageType:[NSMutableDictionary dictionaryWithDictionary:msg]];
        
//        MunerisMessageType type = [[msg objectForKey:@"MunerisMessageType"] intValue];
//        
//        NSString* message;
//        
//        switch (type) {
//          case MunerisVersionUpdateMessage:
//            
//            message = [msg objectForKey:@"message"];
//            
//            MunerisVersionUpdateViewDelegate* del = [[[MunerisVersionUpdateViewDelegate alloc] init] autorelease];
//            
//            MunerisAlert* v = [[[MunerisAlert alloc] initWithTitle:@"Version Update" message:message delegate:del cancelButtonTitle:@"Cancel" otherButtonTitles:@"Update", nil] autorelease];
//            
//            [v show];
//            
//            break;
//          default:
//            break;
//        }
        

        [_messages addObject:msg];
      }
    }
  } 
  
  currentRequestDone += 1;
  
  if (currentRequestDone >= requestCount ) {
    MLOG(@"Muneris Messages : %@",_messages);
    MunerisMessages* msgs = [MunerisMessages messages:_messages];
    
    [_delegate didRecievedMessage:msgs];
    [_delegate release];
    //report;
    
    [self markUnread:msgs];
    
    [self release];
  }
}

-(void) markUnread:(MunerisMessages*)msgs;
{
  
  NSArray* readMessages = msgs.readMessage;
  
  for (NSDictionary* m in readMessages) {
    NSDictionary* plugin = [m objectForKey:@"plugin"];
    
    if (plugin) {
      NSString* pluginName = [plugin objectForKey:@"pluginName"];
      
      NSArray* inboxes = [self getPluginsByNamespace:pluginName];
      for (id<MunerisInboxPluginProtocol>inbox in inboxes) {
        MLOG(@"Muneris: INBOX REPORT handles with plugin %@",inbox);
        if ([inbox conformsToProtocol:@protocol(MunerisInboxPluginProtocol)]) {
          [inbox markUnread:[MunerisMessages messages:[NSArray arrayWithObject:m]]];
        }
      }
    }
  }
}

-(NSMutableDictionary*) convertMessageType:(NSMutableDictionary*) msg;
{
  [msg setObject:[NSNumber numberWithInt:MunerisUnKnownMessage] forKey:@"MunerisMessageType"];
  
  NSString* type = [msg objectForKey:@"ty"];
  if ([type isEqualToString:@"c"]) {
    [msg setObject:[NSNumber numberWithInt:MunerisCreditsMessage] forKey:@"MunerisMessageType"];
    return msg;
  }

  if ([type isEqualToString:@"t"]) {
    [msg setObject:[NSNumber numberWithInt:MunerisTextMessage] forKey:@"MunerisMessageType"];
    return msg;
  }
  
  return msg;
}

-(void) dealloc;
{
  MLOG(@"* DEALLOC Muneris PLUGIN: INBOX");
  if (_info) {
    [_info release];
    _info = nil;
  }
  
  
  if (_messages) {
    [_messages release];
    _messages = nil;
  }
  [super dealloc];
}

@end







