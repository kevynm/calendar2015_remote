//
//  MunerisUrlschemePlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/15/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractMunerisPlugin.h"
#import "MunerisPluginProtocol.h"
@interface MunerisUrlschemePlugin : AbstractMunerisPlugin<MunerisPluginProtocol> {
    
}

@end
