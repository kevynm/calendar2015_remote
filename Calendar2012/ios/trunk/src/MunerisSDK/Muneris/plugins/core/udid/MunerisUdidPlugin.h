//
//  MunerisUDIDPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/9/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisPluginProtocol.h"
#import "AbstractMunerisPlugin.h"

@interface MunerisUdidPlugin : AbstractMunerisPlugin<MunerisPluginProtocol> {
  
}

-(NSString*) getUDID;

@end