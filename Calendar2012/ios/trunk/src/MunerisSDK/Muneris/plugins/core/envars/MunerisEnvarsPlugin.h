//
//  MunerisEnvarsPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/5/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisPluginProtocol.h"
#import "MunerisDelegates.h"
#import "AbstractMunerisPlugin.h"
@interface MunerisEnvarsPlugin : AbstractMunerisPlugin<MunerisPluginProtocol,MunerisApiCallDelegate> {
    
  NSDictionary* _defaultValues;
  
  id<MunerisApiCallDelegate> _delegate;
  
}

@property(retain,nonatomic) id<MunerisApiCallDelegate> delegate;

-(NSDictionary*) getEnvars;

-(void) updateEnvars;

@end
