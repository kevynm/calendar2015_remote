//
//  MunerisMoreappsPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 10/25/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisMoreappsPlugin.h"
#import "MunerisMacro.h"
#import "MunerisWebView.h"
#import "MunerisManager.h"
#import "MunerisApiRequest.h"
#import "MunerisHelper.h"

@implementation MunerisMoreappsPlugin


-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    [self updateConfiguration:dictionary];
  }
  return self;
}

-(void) loadTakeover:(NSString *)zone withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate;
{
  MunerisMoreapps* apps = [[[MunerisMoreapps alloc] initWithDictionary:_info] autorelease];
  if ([_info objectForKey:@"baseUrl"]) {
    [apps setUrl:[_info objectForKey:@"baseUrl"]];
  }
  
  NSString* udid = [[MunerisManager sharedManager] udid];
  NSString* bundleId = [[MunerisHelper getApp] objectForKey:@"bundleId"];
  if ([_info objectForKey:@"baseUrlFormat"]) {
    [apps setUrl:[NSString stringWithFormat:[_info objectForKey:@"baseUrlFormat"],bundleId,udid,zone]];
  }else{
    [apps setUrl:[NSString stringWithFormat:@"https://muneris.animoca.com/moreapps/ios/?aid=%@&uid=%@&zone=%@",bundleId,udid,zone]];
  }
  [apps loadTakeover:zone withViewController:viewController delegate:delegate];
}

@end


@implementation MunerisMoreapps

@synthesize url = _url;

- (id) initWithDictionary: (NSDictionary*) dictionary;
{
  if (self = [self init]) {
    if (dictionary) {
      _info = [dictionary retain];
    }
  }
  return self;
}

//-(void) didSuccessfulCallingApiWithDictionary:(NSDictionary *)data;
//{
//  MLOG(@"Muneris Moreapps : API OK %@ ", data);
//  [_hud hide:NO];
//}
//
//-(void) didFailCallingApiWithError:(NSError *)error;
//{
//  MLOG(@"Muneris Moreapps : API ERROR %@", error);
//  [_hud setCustomView:[[[UIView alloc] init] autorelease]];
//  [_hud setAnimationType:MBProgressHUDAnimationFade];
//  [_hud setMode:MBProgressHUDModeCustomView]; 
//  [_hud setLabelText:@"ERROR"];
//  [_hud setDetailsLabelText:@"Please try again later"];
//  [_hud hide:YES afterDelay:3];
// 
//  
//}


-(void) loadTakeover:(NSString *)zone withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate;
{
  //  _viewController = [viewController retain];
  //  [MunerisApi apiMethod:@"moreapps" payload:nil delegate:self];
  //  UIWindow* win = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
  //  _hud = [[MBProgressHUD alloc] initWithWindow:win];
  //  _hud.animationType =  MBProgressHUDAnimationZoom;
  //  _hud.removeFromSuperViewOnHide = YES;
  //  _hud.dimBackground = YES;
  //  
  //  [win addSubview:_hud];
  //  [_hud show:YES];
  
  if (_url) {
    
    NSMutableURLRequest* request = [[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:_url]]autorelease];
    //    NSString* header = [MunerisHelper toJsonWithObject:[[MunerisManager sharedManager] getHeader]];
    
    //    [request setValue:header forHTTPHeaderField:@"x-muneris-header"];
    
    //    MunerisApiRequest* api = [MunerisApiRequest requestWithMethod:@"getMoreapps" payload:nil];
    //      
    //    NSData* data = [api postBody];
    //      
    //    [request setHTTPMethod:@"POST"];
    //      
    //    [request setHTTPBody:data];
    //      
    [request setHTTPMethod:@"GET"];
    
    MLOG(@"Muneris MOREAPPS : requesting more apps from %@",_url);
    
    BOOL show = YES;
    if (delegate) {
      if (![delegate shouldShowTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:zone forKey:@"zone"] forKey:@"moreapps"]]) {
        show = NO;
      }

//      if ([[Reachability reachabilityWithHostName:_url] currentReachabilityStatus] == NotReachable) {
//        MLOG(@"%@", _url);
//        MLOG(@"%d", [[Reachability reachabilityWithHostName:_url] currentReachabilityStatus]);
//        MLOG(@"%d", NotReachable);
//        show = NO;
//        [delegate didFailToLoadTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:zone forKey:@"zone"] forKey:@"moreapps"]];
//      }
    }
    if (show) {
      MunerisWebView* webview = [[[MunerisWebView alloc] initWithViewController:viewController withDictionary:_info withRequest:request] autorelease];
      
      NSString *keyArray[2];
      keyArray[0] = @"zone";
      keyArray[1] = @"MunerisWebView";
      
      id valueArray[2];
      valueArray[0] = zone;
      valueArray[1] = webview;
      //todo please implement a delegate for uiwebview close button
      [delegate didFinishedLoadingTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObjects:valueArray forKeys:keyArray count:2] forKey:@"moreapps"]];
      
//      [webview loadRequest:request];
      
      _webview = [webview retain];
    }
  }
  
  
}

-(void) dealloc;
{
  MLOG(@"* DEALLOC MUNERIS: MOREAPPS PLUGIN");
  
  if (_url) {
    [_url release];
    _url = nil;
  }
  
  if (_info) {
    [_info release];
    _info = nil;
  }
  
  //  if (_viewController) {
  //    [_viewController release];
  //    _viewController = nil;
  //  }
  //  
  //  if (_hud) {
  //    [_hud release];
  //    _hud = nil;
  //  }
  //  if (_delegate) {
  //    [_delegate release];
  //    _delegate = nil;
  //  }
  //  
  if (_webview) {
    [_webview release];
    _webview = nil;
  }
  
  [super dealloc];
}

@end

