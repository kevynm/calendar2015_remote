//
//  MunerisAnalyticsPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 10/10/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisAnalyticsPlugin.h"
#import "MunerisApi.h"
#import "MunerisMacro.h"
#import "MunerisLogger.h"
#import "MunerisLogUploader.h"

@interface MunerisAnalyticsPlugin (private)


@end

@implementation MunerisAnalyticsPlugin


-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [super init])) {
    
    NSNumber* logSize = [dictionary objectForKey:@"logSize"];
    if (logSize) {
      _logger = [[MunerisLogger alloc] initWithSize:[logSize intValue]];
    }else{
      _logger = [[MunerisLogger alloc] init];
    }
    _uploader = [[MunerisLogUploader alloc] initWithLogger:_logger];
    
    
  }
  return self;
}

-(void) logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  [_logger logEvent:eventName withParameters:parameters];
}

-(void) logStartEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters
{
  [_logger logEvent:[NSString stringWithFormat:@"%@.%@",eventName,@"start"] withParameters:parameters];
}

-(void) logEndEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  [_logger logEvent:[NSString stringWithFormat:@"%@.%@",eventName,@"end"] withParameters:parameters];
}

-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  [self logStartEvent:@"sess" withParameters:nil];
  return YES;
}


-(void) applicationDidFinishLaunching:(UIApplication *)application;
{
  [self logStartEvent:@"sess" withParameters:nil];
}

-(void) applicationDidEnterBackground:(UIApplication *)application;
{
  [self logEvent:@"sess.suspend" withParameters:nil];
  [_uploader upload:NO];
}

-(void) applicationWillEnterForeground:(UIApplication *)application;
{
  [self logEvent:@"sess.resume" withParameters:nil];
}

-(void) applicationWillResignActive:(UIApplication *)application;
{
  [self logEvent:@"sess.suspend" withParameters:nil];
  [_uploader upload:NO];
}

-(void) applicationDidBecomeActive:(UIApplication *)application;
{
  [self logEvent:@"sess.resume" withParameters:nil];
}

-(void) applicationDidReceiveMemoryWarning:(UIApplication *)application;
{
  [self logEvent:@"ios.memorywarning" withParameters:nil];
}

-(void)applicationWillTerminate:(UIApplication *)application {
  [self logEndEvent:@"sess" withParameters:nil];
  [_uploader upload:NO];
}


-(void) didSuccessfulCallingApiWithDictionary:(NSDictionary *)data;
{
  MLOG(@"Muneris : ANALYTICS PLUGIN upload OK %@", data);
}


-(void) didFailCallingApiWithError:(NSError *)error;
{
  MLOG(@"Muneris : ANALYTICS PLUGIN upload FAILED %@", error);
}







-(void) dealloc;
{
  if (_uploader) {
    [_uploader release];
    _uploader = nil;
  }
  if (_logger) {
    [_logger release];
    _logger = nil;
  }
  [super dealloc];
}

@end
