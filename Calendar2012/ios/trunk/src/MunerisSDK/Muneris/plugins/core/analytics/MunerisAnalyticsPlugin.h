//
//  MunerisAnalyticsPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/10/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisAnalyticsPluginProtocol.h"
#import "MunerisPluginProtocol.h"
#import "AbstractMunerisPlugin.h"
#import "MunerisDelegates.h"
@class MunerisLogger;
@class MunerisLogUploader;

@interface MunerisAnalyticsPlugin : AbstractMunerisPlugin<MunerisAnalyticsPluginProtocol, MunerisPluginProtocol, MunerisApiCallDelegate> {
  
  MunerisLogger* _logger;
  
  MunerisLogUploader* _uploader;
  
}


@end
