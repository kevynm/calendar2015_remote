//
//  MunerisVerisonPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/27/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisVersionPlugin.h"
#import "MunerisApi.h"
#import "MunerisAlert.h"
#import "MunerisManager.h"
#import "MunerisMacro.h"


@implementation MunerisVersionPlugin


-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [super init])) {
    [self updateConfiguration:dictionary];
  }
  return self;
}


-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  NSNumber* shouldCheckVersion = [_info objectForKey:@"autoCheck"];
  if (shouldCheckVersion) {
    if ([shouldCheckVersion boolValue]) {
      [self checkAppVersion];
    }
  }
  
  return YES;
}

-(void) checkAppVersion;
{
  [MunerisApi apiMethod:@"checkAppVersion" payload:nil delegate:self];
}

-(void) didSuccessfulCallingApiWithDictionary:(NSDictionary *)data;
{
  
  NSDictionary* params = [data objectForKey:@"params"];
  if (params) {
    NSString* version = [params objectForKey:@"ver"];
    if (!version) {
      return;
    }
    
    NSString* appleId = [[[data objectForKey:@"header"] objectForKey:@"app"] objectForKey:@"appleId"];
    MLOG(@"Muneris : PLUGIN VERSION CHECK OK %@",params);
    
    NSNumber* criticalUpdate = [params objectForKey:@"critical"];
    
    MunerisAlert* v = nil;
    if(criticalUpdate){
      if ([criticalUpdate boolValue]) {
        NSString* message = [_info objectForKey:@"criticalMessage"];
        NSString* title = [_info objectForKey:@"criticalTitle"];
        MunerisVersionUpdateViewDelegate* del = [[[MunerisVersionUpdateViewDelegate alloc] initWithCriticalUpdates] autorelease];
        [del setAppleId:appleId];
        v = [[[MunerisAlert alloc] initWithTitle:title message:message delegate:del cancelButtonTitle:@"Exit" otherButtonTitles:@"Update", nil] autorelease];
      }
    }
    if (!v) {
      NSString* message = [_info objectForKey:@"message"];
      NSString* title = [_info objectForKey:@"title"];
      MunerisVersionUpdateViewDelegate* del = [[[MunerisVersionUpdateViewDelegate alloc] init] autorelease];
      [del setAppleId:appleId];
      v = [[[MunerisAlert alloc] initWithTitle:title message:message delegate:del cancelButtonTitle:@"Cancel" otherButtonTitles:@"Update", nil] autorelease];
    }
    [v show];
  }
}

-(void) didFailCallingApiWithError:(NSError *)error;
{

}


@end


@implementation MunerisVersionUpdateViewDelegate

@synthesize appleId = _appleId;

-(id) initWithCriticalUpdates;
{
  if ((self = [self init])) {
    _criticalUpdate = YES;
  }
  return self;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
  
  NSString* url = [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%@", _appleId];
  
  NSString* encoded = [url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
  
  MLOG(@"Muneris: VERSION UPDATE alert clicked redirecting to %@", encoded);
  
  switch (buttonIndex) {
    case 1:
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:encoded]];
      break;
    case 0:
      if (_criticalUpdate) {
        exit(0);
      }
      break;
    default:
      break;
  }
  
}

- (void)alertViewCancel:(UIAlertView *)alertView;{}

- (void)willPresentAlertView:(UIAlertView *)alertView;{}

- (void)didPresentAlertView:(UIAlertView *)alertView;{}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex;{}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;{}

-(void) dealloc;
{
  if (_appleId) {
    [_appleId release];
    _appleId = nil;
  }
  MLOG(@"* DEALLOC UIMuneris: Delegate Alert Box Verion Update");
  [super dealloc];
}

@end
