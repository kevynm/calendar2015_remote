//
//  MunerisUrlschemePlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/15/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisUrlschemePlugin.h"
#import "MunerisManager.h"
#import "MunerisMacro.h"
#import <UIKit/UIKit.h>

@implementation MunerisUrlschemePlugin

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{

  if ((self = [self init])) {
    
  }
  return self;
}


-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  
  MLOG(@"Muneris: URLSCHEME APPDELEGATE application did finish launching");
  UIPasteboard* paste = [UIPasteboard pasteboardWithName:@"muneris" create:YES];
//  [paste setValue:@"url" forKey:@"shouldGo"];
  [paste setPersistent:YES];
  NSLog(@"paste %@", [paste string]);
  [paste setString:@"asdas111"];
  
  return YES;
}

-(BOOL) application:(UIApplication *)application handleOpenURL:(NSURL *)url;
{
  MLOG(@"Muneris %@",url);
  return YES;
}




@end
