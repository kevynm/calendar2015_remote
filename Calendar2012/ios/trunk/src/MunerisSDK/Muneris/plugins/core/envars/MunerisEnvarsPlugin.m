//
//  MunerisEnvarsPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/5/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisEnvarsPlugin.h"
#import "MunerisManager.h"
#import "MunerisMacro.h"

@interface MunerisEnvarsPlugin(prvate);

-(id) initWithDictionary:(NSDictionary*) dictionary;

-(void) save;

@end

@implementation MunerisEnvarsPlugin

@synthesize delegate = _delegate;

const NSString* kMunerisStateEnvars = @"envars";


//+(id) pluginWithDictionary:(NSDictionary *)dictionary;
//{
//  id ret = [[self alloc] initWithDictionary:dictionary];
//  return [ret autorelease];
//}

-(id) initPluginWithDictionary:(NSDictionary*) dictionary;
{
  if ((self = [self init])) {
    //last update envars
  }
  return self;
}

-(NSDictionary*) getEnvars;
{
  if ([_store objectForKey:@"envars"]) {
    if ([[_store objectForKey:@"envars"] count] == 0) {
      return nil;
    }
  }
  return [_store objectForKey:@"envars"];
}

-(void) save;
{
  [_store setObject:_defaultValues forKey:@"envars"];
  [self storeSave];
}


-(void) didSuccessfulCallingApiWithDictionary:(NSDictionary *)data;
{
  NSDictionary* params = [data objectForKey:@"params"];
  if (params) {
    NSDictionary* envars = [params objectForKey:@"envars"];
    if (envars) {
      NSDictionary* muneris = [envars objectForKey:@"muneris"];
      
      id temp = _defaultValues;
//      if ([muneris objectForKey:@"reset"]) {
//        _defaultValues = [[MunerisManager sharedManager].config retain];
//      }else{
        _defaultValues = [envars retain];
//      }
      
      if (temp) {
        [temp release];
      }
      
      [self save];
      
      MLOG(@"Muneris: ENVARS PLUGIN envars success %@" , _defaultValues);
      
      if ([[MunerisManager sharedManager] respondsToSelector:@selector(reloadPlugin:)]) {
        if (muneris) {
          NSDictionary* dic = [muneris objectForKey:@"plugins"];
          if (dic) {
            for (NSString* mkey  in [dic allKeys]) {
              [[MunerisManager sharedManager] performSelector:@selector(reloadPlugin:) withObject:mkey];
            }
          }
        }
        for (NSString* key in [_defaultValues allKeys]) {
          [[MunerisManager sharedManager] performSelector:@selector(reloadPlugin:) withObject:key];
        }
      }
    }
  }
  [[NSNotificationCenter defaultCenter] postNotificationName:MUNERIS_ENVARS_UPDATE_SUCCESS_NOTIFICATION object:_defaultValues];
  
  [_delegate didSuccessfulCallingApiWithDictionary:data];
}


-(void) didFailCallingApiWithError:(NSError *)error;
{
  MLOG(@"Muneris: ENVARS PLUGIN failed calling envars");
  [_delegate didFailCallingApiWithError:error];
}

-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  MLOG(@"Muneris: ENVARS APPDELEGATE application did finish launching");
  //[[MunerisManager sharedManager] apiCall:@"getEnvars" payload:nil delegate:self];
  [self updateEnvars];
  return YES;
}

-(void) applicationDidFinishLaunching:(UIApplication *)application;
{
  [self updateEnvars];
}

-(void) updateEnvars;
{
  [[MunerisManager sharedManager] apiCall:@"getEnvars" payload:nil delegate:self];
}



-(void) dealloc;
{
  
  if (_delegate) {
    [_delegate release];
    _delegate = nil;
  }
  if (_defaultValues) {
    [_defaultValues release];
    _defaultValues = nil;
  }
  [super dealloc];
}


@end