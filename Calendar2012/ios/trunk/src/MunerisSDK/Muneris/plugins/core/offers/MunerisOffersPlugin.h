//
//  MunerisOffersPlugin.h
//  MunerisKit
//
//  Created by Casper Lee on 20/10/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MunerisPluginProtocol.h"
#import "AbstractMunerisPlugin.h"
#import "MunerisDelegates.h"
#import "MBProgressHUD.h"

@interface MunerisOffersPlugin : AbstractMunerisPlugin<MunerisPluginProtocol, UIAlertViewDelegate>
{
  NSArray* _offersArray;
  UIViewController* _viewController;
  id<MunerisOffersDelegate> _delegate;
}


-(void) showOffers:(UIViewController*) viewController delegate:(id<MunerisOffersDelegate>) delegate;
-(BOOL) hasOffers;

@end


@interface MunerisOffersView : UIViewController <UITableViewDelegate, UITableViewDataSource, MunerisTakeoverDelegate> {
  NSDictionary* _info;
  NSMutableArray* _offersArray;
  UIViewController* _viewController;
  MBProgressHUD* _hud;
  
  id<MunerisOffersDelegate> _delegate;
  IBOutlet UILabel *offersTitle;
  IBOutlet UIButton *offersCloseMsg;
  IBOutlet UITableView *offersTableView;
}

@property (nonatomic, assign) NSDictionary *info;
@property (nonatomic, assign) UIViewController *viewController;
@property (nonatomic, assign) id<MunerisOffersDelegate> delegate;

@property (nonatomic, retain) IBOutlet UILabel *offersTitle;
@property (nonatomic, retain) IBOutlet UIButton *offersCloseMsg;
@property (nonatomic, retain) IBOutlet UITableView *offersTableView;

-(id) initWithDictionary:(NSDictionary *)dictionary andViewController: (UIViewController*) viewController;
- (IBAction)closeOffers:(id)sender;

@end