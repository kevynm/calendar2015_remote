//
//  MunerisFlurryPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/8/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisFlurryPlugin.h"
#import "MunerisMacro.h"
#import "FlurryAnalytics.h"
#import "FlurryClips.h"
#import "FlurryVideoOffer.h"
#import "MunerisManager.h"


@interface MunerisFlurryPlugin(private)

-(NSMutableArray*) messageArray;

@end

@implementation MunerisFlurryPlugin


-(id) initPluginWithDictionary:(NSDictionary*) dictionary;
{
  if ((self = [self init])) {
    
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisAnalyticsPluginProtocol))];
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisInboxPluginProtocol))];
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisTakeoverPluginProtocol))];
    [self updateConfiguration:dictionary];
    //init flurry
    
    [FlurryAnalytics setEventLoggingEnabled:YES];
    
    [FlurryClips setVideoDelegate:self];
    
    [FlurryAnalytics setAppVersion:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
  }
  return self;
}

-(void) updateConfiguration:(NSDictionary *)dict;
{
  [super updateConfiguration:dict];
  
  
  _rewardPoints = [[NSNumber alloc] initWithInt:[[[[_info objectForKey:@"rewards"] objectForKey:@"default"] objectForKey:@"points"] intValue]];
  
  
  //please use muneris helper

  switch ([MunerisManager mode]) {
    case MunerisModeDebug:
    case MunerisModeTesting:
      [FlurryAnalytics setDebugLogEnabled:[[_info objectForKey:@"DEBUG"] boolValue]];
      break;
    default:
      [FlurryAnalytics setDebugLogEnabled:NO];
      break;
  }
  
  
  NSDictionary* userDict = [[[MunerisManager sharedManager] state] objectForKey:@"user"];
  if (userDict) {
    NSString* userId = [userDict objectForKey:@"userId"];
    
    int userAge = [[userDict objectForKey:@"userAge"] intValue];
    //only m / f read docs
    NSString* userGender = [userDict objectForKey:@"userGender"];
    
    if (userId) {
      [FlurryAnalytics setUserID:userId];
    }
    if (userAge > 0) {
      [FlurryAnalytics setAge:userAge];
    }
    if (userGender) {
      [FlurryAnalytics setGender:userGender];
    }
  }
}

-(NSMutableArray*) messageArray;
{
  if (![_store objectForKey:@"flurryPoints"]) {
    [_store setValue:[NSMutableArray array] forKey:@"flurryPoints"];
  }
  return [_store objectForKey:@"flurryPoints"];
}

-(void) readInbox:(NSArray *)types withDelegate:(id<MunerisInboxDelegate>)delegate;
{
  MunerisFlurryPoints *fyp = [[[MunerisFlurryPoints alloc] init] autorelease];
  [fyp readInbox:types withMessage:[self messageArray] withDelegate:delegate];
}

-(void) markUnread:(MunerisMessages *)msgs;
{
  for (NSDictionary* m in msgs.unreadMessage) {
    [[self messageArray] addObject:m];
  }
  [self storeSave];
}

-(BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  [FlurryClips setVideoAdsEnabled:YES];
  [FlurryAnalytics startSession:[_info objectForKey:@"apiKey"]];
  return YES;
}

-(void) logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  [FlurryAnalytics logEvent:eventName withParameters:parameters];
}

-(void) logStartEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  [FlurryAnalytics logEvent:eventName withParameters:parameters timed:YES];
}

-(void) logEndEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  [FlurryAnalytics endTimedEvent:eventName withParameters:parameters];
}

- (void) showVideoBox: (NSString*) ori rewardImage:(UIImage*) rewardImg rewardMessage:(NSString*) rewardMsg userCookies:(NSDictionary*) userCookies;
{
  if (_rewardPoints) {
    [FlurryClips openVideoTakeover:_zone orientation:ori rewardImage:rewardImg rewardMessage:rewardMsg userCookies:nil]; 
  } else {
    [_todelegate didFailToLoadTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"flurry"]];
  }
}

- (void) playFlurryVideo;
{
  NSString* rewardImageURL = [[[_info objectForKey:@"rewards"] objectForKey:@"default"] objectForKey:@"image"];
  if (rewardImageURL) {
    [[SDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:rewardImageURL] delegate:self]; 
  }
}

- (NSString*) checkOri {
  NSString* ori;
  switch ([[UIDevice currentDevice] orientation]) {
    case UIDeviceOrientationLandscapeLeft:
      ori = @"landscapeRight";
      break;
    case UIDeviceOrientationLandscapeRight:
      ori = @"landscapeLeft";
      break;
    case UIDeviceOrientationPortraitUpsideDown:
      ori = @"portraitUpsideDown";
      break;
    case UIDeviceOrientationPortrait:
    default:
      ori = @"portrait";
      break;
  }
  return ori;
}

- (NSString*) getRewardMsg {
  
  NSString* rewardMsg;
  NSString* rewardCurrency = [[[_info objectForKey:@"rewards"] objectForKey:@"default"] objectForKey:@"currency"];  
  
  if ([[rewardCurrency stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] != 0) {
    rewardMsg = [NSString stringWithFormat:@"%d %@", [_rewardPoints intValue], rewardCurrency];
    rewardMsg = [rewardMsg stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  } else {
    rewardMsg = nil;
  }
  return rewardMsg;
}

#pragma mark SDWebImageManager Delegate
- (void) webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image {
  
  NSString* ori = [self checkOri];
  NSString* rewardMsg = [self getRewardMsg];
  
  [self showVideoBox:ori rewardImage:image rewardMessage:rewardMsg userCookies:nil];
}

- (void) webImageManager:(SDWebImageManager *)imageManager didFailWithError:(NSError *)error {
  MLOG(@"Muneris: Flurry Video Plugin Image Loading Error");
  
  NSString* ori = [self checkOri];
  NSString* rewardMsg = [self getRewardMsg];
  
  [self showVideoBox:ori rewardImage:nil rewardMessage:rewardMsg userCookies:nil];
}

#pragma mark Flurry Ad Delegate
-(void) loadTakeover:(NSString*)zone withViewController:(UIViewController*) viewController delegate:(id<MunerisTakeoverDelegate>) delegate
{
  
  if (!_todelegate) {
    _todelegate = [delegate retain];
    _zone = [zone copy];
  }else{
    MLOG(@"Muneris : FLURRY Takeover - already running failing this request");
    [delegate didFailToLoadTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"flurry"]];
    [self cleanup];
    return;
  }

  MLOG(@"%@" , delegate);
  if(![FlurryClips videoAdIsAvailable:_zone]){
    [delegate didFailToLoadTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"flurry"]];
    [FlurryClips rotateVideoOffer:_zone];
    [self cleanup];
  }else{
    if ([delegate shouldShowTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"flurry"]]) {
      MLOG(@"Muneris Takeover : FLURRY TAKEOVER");
//      [self performSelector:@selector(playFlurryVideo) withObject:nil afterDelay:2.0f];
      [self playFlurryVideo];
    }else{
      [self cleanup];
    }
  }
}

- (void)takeoverWillClose {
  MLOG(@"MuneriFlurryPlugin: takevoerWillClose");
  if (_todelegate) {
    [_todelegate didFinishedLoadingTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"flurry"]];
  }

  //  ////////////////////////////////////////////////////////////////////////
  //  #warning muneris testing only
  //  
  //  MLOG(@"Muneris: FlurryPoint updated %d", [_rewardPoints intValue]);
  //  int points = [_rewardPoints intValue];
  //  
  //  if (points) {
  //    NSDictionary* message = [MunerisMessages createCreditMessageWithPlugin:@"flurry" credits:points reason:@"flurry points"];
  //    [[self messageArray] addObject:message];
  //  }
  //  
  //  [self storeSave];
  //  
  //  MLOG(@"MuneriFlurryPlugin: videoDidFinish");
  //  
  //  ////////////////////////////////////////////////////////////////////////

  [self cleanup];
}

- (void)videoDidNotFinish:(NSString *)hook {
  MLOG(@"MuneriFlurryPlugin: videoDidNotFinish");
  [_todelegate didFinishedLoadingTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"flurry"]];
  [self cleanup];  
}

- (void)videoDidFinish:(NSString*)hook withUserCookies:(NSDictionary*)userCookies {
  
  MLOG(@"Muneris: FlurryPoint updated %d", [_rewardPoints intValue]);
  int points = [_rewardPoints intValue];
  
  if (points) {
    NSDictionary* message = [MunerisMessages createCreditMessageWithPlugin:@"flurry" credits:points reason:@"flurry points"];
    [[self messageArray] addObject:message];
  }
  
  [self storeSave];
  
  MLOG(@"MuneriFlurryPlugin: videoDidFinish");
}

- (void) takeoverWillDisplay:(NSString *)hook {
}

- (void) cleanup;
{
  if (_zone) {
    [_zone release];
    _zone = nil;
  }
  
  if (_todelegate) {
    [_todelegate release];
    _todelegate = nil;
  }
}

-(void) dealloc;
{
  MLOG(@"* Muneris: FLURRY  DEALLOC");
  
  if (_rewardPoints) {
    [_rewardPoints release];
    _rewardPoints = nil;
  }
  
  [super dealloc];
}

@end



#pragma mark -
#pragma mark FlurryPoints

@interface MunerisFlurryPoints (private)

-(void) reply;

@end


@implementation MunerisFlurryPoints

-(void) readInbox:(NSArray *)types withMessage:(NSMutableArray*)msg withDelegate:(id<MunerisInboxDelegate>)delegate;
{
  NSArray* message = [msg copy]; 
  [msg removeAllObjects];
  if ([message count] > 0) {
    [delegate didRecievedMessage:[MunerisMessages messages:message]];
  } else {
    [delegate didRecievedMessage:nil];
  }
  
  [message release];
}


-(void) dealloc;
{
  MLOG(@"* DEALLOC: MUNERIS FlurryPoints");  
  [super dealloc];
}

@end