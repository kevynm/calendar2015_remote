//
//  MunerisFlurryPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/8/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractMunerisPlugin.h"
#import "MunerisPluginProtocol.h"
#import "MunerisAnalyticsPluginProtocol.h"
#import "MunerisTakeoverPluginProtocol.h"
#import "MunerisInboxPluginProtocol.h"
#import "FlurryAdDelegate.h"
#import "MunerisDelegates.h"
#import "SDWebImageManager.h"

@class MunerisFlurryPoints;

@interface MunerisFlurryPlugin : AbstractMunerisPlugin<MunerisPluginProtocol, MunerisAnalyticsPluginProtocol, MunerisTakeoverPluginProtocol, MunerisInboxPluginProtocol, FlurryAdDelegate, SDWebImageManagerDelegate> {
  id<MunerisTakeoverDelegate> _todelegate;
  NSString* _zone;
  NSNumber* _rewardPoints;
  NSMutableArray* _messages;
}

- (void) cleanup;

@end

@interface MunerisFlurryPoints : NSObject {
  
}

-(void) readInbox:(NSArray *)types withMessage:(NSArray*)msg withDelegate:(id<MunerisInboxDelegate>)delegate;

@end