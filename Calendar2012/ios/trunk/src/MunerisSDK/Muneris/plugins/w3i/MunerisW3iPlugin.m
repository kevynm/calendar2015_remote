//
//  MunerisW3iPlugin.m
//  MunerisKit
//
//  Created by Jacky Yuk on 11/8/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisManager.h"

#import "MunerisW3iPlugin.h"

#import "MunerisMacro.h"

#import "MunerisMessages.h"

@interface MunerisW3iPlugin(private)

-(void) closeAndRelease;

-(void) cleanUp;

@end

@implementation MunerisW3iPlugin

-(id)initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if (self = [self init]) {
    
    [self updateConfiguration:dictionary];
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisInboxPluginProtocol))];
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisTakeoverPluginProtocol))];
 
    
  }
  return self;
}

-(void) updateConfiguration:(NSDictionary *)dict;
{
  [super updateConfiguration:dict];
  
  NSMutableDictionary * w3iconf = [[[NSMutableDictionary alloc] initWithCapacity:0] autorelease];
  
  //Name (REQUIRED).  This is the app/game to be shown within the OfferWall screens.
  NSString* aowname = [_info objectForKey:@"offerwallName"];
  if (!aowname) {
    aowname = [[MunerisManager sharedManager] applicationName];
  }
  
  [w3iconf setObject:aowname forKey:@AOW_NAME];
  
  //ApplicationId (REQUIRED).  A unique identifier assigned to the app/game by the offer wall system. 
  //Note that this value is different from the AppId used for the Advertiser SDK.
  
  if ([_info objectForKey:@"advertiserId"]) {
      [w3iconf setObject:[_info objectForKey:@"advertiserId"] forKey:@AOW_ADVERTISER_APPID];
  }

  if ([_info objectForKey:@"appId"]) {
      [w3iconf setObject:[_info objectForKey:@"appId"] forKey:@AOW_APPID];
  }
  
//  NSArray *orientations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:UIDeviceOrientationPortrait],
//                           [NSNumber numberWithFloat:UIDeviceOrientationLandscapeLeft],
//                           [NSNumber numberWithFloat:UIDeviceOrientationLandscapeRight],
//                           [NSNumber numberWithFloat:UIDeviceOrientationPortraitUpsideDown],
//                           nil];
//  
//  [dictionary setObject:orientations forKey:@AOW_SUPPORTED_ORIENTATIONS];
  
  //offerWall = [[OfferWall alloc] initWithUserInfo:dictionary andDelegate:self];
  

  MLOG(@"Muneris W3i Config : %@" , w3iconf);
  if (!offerWall) {
      offerWall = [self replaceConfigurationWith:[[[OfferWall alloc] initWithUserInfo:w3iconf andDelegate:self] autorelease] oldConfigutration:offerWall];
  }else{
    MLOG(@"Muneris W3I : cannot reload configuration");
  }

  
}

-(void) loadTakeover:(NSString *)area withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate;
{
  NSDictionary* toDict = [_info objectForKey:@"takeovers"];
  if (toDict) {
    NSDictionary* zones = [toDict objectForKey:@"zones"];
    if (zones) {
      NSString* takeovers = [zones objectForKey:area];
      if (takeovers) {
        area = takeovers;
      }
    }
  }
  
  
//  offerwallInitialise = YES;
  NSDictionary* takeoverinfo = [NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:area forKey:@"zone"] forKey:@"w3i"];
  if ([area isEqualToString:@"offers"]) {
    if (!offerwallInitialise) {
      [delegate didFailToLoadTakeover:takeoverinfo];
    }else{
      if ([delegate shouldShowTakeover:takeoverinfo]) {
        if (!_offerWallTakeoverDelegate) {
          _zone = [area copy];
          _offerWallTakeoverDelegate = [delegate retain];
//          if (viewController) {
//            [offerWall showOfferWall:Offer_Wall_Type_Offers fromView:viewController.view];
//          }else{
            [offerWall showOfferWall:Offer_Wall_Type_Offers];
//          }
        } else {
          MLOG(@"Muneris W3i: OfferWall Already Running");
          [delegate didFailToLoadTakeover:takeoverinfo];
        }
      }
    }
  }else{
    //show featured app
    BOOL show = YES;
    if (featuredOfferInitialize) {
      if (delegate) {
        show = [delegate shouldShowTakeover:takeoverinfo];
      }
    }else{
      show = NO;
    }

    if (show) {
      [offerWall showFeaturedOffer];
      [delegate didFinishedLoadingTakeover:takeoverinfo];
    }else{
      [delegate didFailToLoadTakeover:takeoverinfo];
    }
  }
}

-(void) readInbox:(NSArray*)type withDelegate:(id<MunerisInboxDelegate>) delegate;
{
  
  if (_inboxDelegate || !offerwallInitialise) {
    [delegate didRecievedMessage:nil];
  }else{
    _inboxDelegate = [delegate retain];
    [offerWall redeemBalance];
  }
}

-(void) markUnread:(MunerisMessages*)msgs;
{
  
  for (NSDictionary* msg in msgs.readMessage) {
    [[MunerisManager sharedManager] logEvent:@"w3iPotints" withParameters:msg];
  }
  

}


// delegate methods which inform the developer about the OfferWall initialization status
- (void)offerWallDidFailToInitialize;{
  MLOG(@"Muneris W3i : failed");
  offerwallInitialise = false;
}
- (void)offerWallDidInitialize;{
    MLOG(@"Muneris W3i : success");
  offerwallInitialise = true;
}

// delegate methods which inform the developer about the OfferWall display status

- (void)offerWallDidDisplay;{}
- (void)offerWallDidFailToDisplay;{}

- (void)offerWallDidDismiss;{
  if (_offerWallTakeoverDelegate) {
    [_offerWallTakeoverDelegate didFinishedLoadingTakeover:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:_zone forKey:@"zone"] forKey:@"w3i"]];
  }
  [self cleanUp];

}

// delegate method which inform the developer when failed to link the appstore because of applicationID
- (void)redirectingUserToAppStoreDidFailWithError:(NSError*)error;{

}


// delegate method to inform the developer that user is about to be redirected to iTunes.
- (void)offerWallWillRedirectUserToAppStore;{}

// delegate method to fetch the offer list
-(void) offerWallDidReceiveOfferList:(NSMutableArray*)offerList;
{
}

// delegate methods which get called when offerwall failed to respond
- (void)offerWallDidFailToGetFeaturedOffer;
{
  featuredOfferInitialize = NO;
}

// delegate methods which get called when featured offer available
- (void)offerWallFeaturedOfferDidBecomeAvailable;
{
  featuredOfferInitialize = YES;
}

// delegate methods which get called when Game Currency Balance related functions are called
-(void)offerWallDidRedeemGameCurrencies:(NSArray*)currencies withReceipt:(NSString*)receiptID  withError:(NSError*)error;{
  
  if (!error) {
    NSMutableArray* messages = [NSMutableArray array];
    for (NSDictionary* dict in currencies) {
      if ([dict objectForKey:@"amount"] && receiptID)
      {
        int points = [[dict objectForKey:@"amount"] intValue];
        NSString* reason = [NSString stringWithFormat:@"w3i points with reciept : %@" , receiptID];
        NSDictionary* message = [MunerisMessages createCreditMessageWithPlugin:@"w3i" credits:points reason:reason];
        [messages addObject:message];
      }
    }
    [_inboxDelegate didRecievedMessage:[MunerisMessages messages:messages]];
    
  }else{
    [_inboxDelegate didRecievedMessage:nil];
  }
  
  [_inboxDelegate release];
  _inboxDelegate = nil;
  
  
  
  MLOG(@"w3i %@" , currencies );
  MLOG(@"w3i %@" , receiptID );
  MLOG(@"w3i %@" , error );
}

-(void) cleanUp;
{
  if (_offerWallTakeoverDelegate) {
    [_offerWallTakeoverDelegate release];
    _offerWallTakeoverDelegate = nil;
  }
  
  if (_zone) {
    [_zone release];
    _zone = nil;
  }
}

-(void) closeAndRelease;
{
  if(offerWall){
    [offerWall close];
    [offerWall release];
  }
}

-(void) dealloc;
{
  [super dealloc];
}

@end
