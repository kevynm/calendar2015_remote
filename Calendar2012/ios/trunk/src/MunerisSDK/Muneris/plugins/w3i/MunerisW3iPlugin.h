//
//  MunerisW3iPlugin.h
//  MunerisKit
//
//  Created by Jacky Yuk on 11/8/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import "AbstractMunerisPlugin.h"
#import "MunerisPluginProtocol.h"
#import "MunerisPlugins.h"
#import "OfferWall.h"

@interface MunerisW3iPlugin : AbstractMunerisPlugin<MunerisPluginProtocol,MunerisTakeoverPluginProtocol, MunerisInboxPluginProtocol,OfferWallDelegate>
{
  OfferWall *offerWall;
  
  BOOL offerwallInitialise;
  
  BOOL featuredOfferInitialize;
  
  id<MunerisTakeoverDelegate> _offerWallTakeoverDelegate;
  
  id<MunerisInboxDelegate> _inboxDelegate;
  
  NSString* _zone;
}


@end
