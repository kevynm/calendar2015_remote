//
//  MunerisMopubPlugin.h
//  MunerisKit
//
//  Created by Casper LEE on 08/09/2011.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractMunerisPlugin.h"
#import "MunerisBannerAdsPluginProtocol.h"
#import "MunerisInboxPlugin.h"
#import "MunerisPluginProtocol.h"
#import "MPAdView.h"

@interface MunerisMopubPlugin : AbstractMunerisPlugin<MunerisBannerAdsPluginProtocol> {
  //    CGSize _bannerSize;
}

@end

@interface MunerisMopub : NSObject<MPAdViewDelegate> {
  
  UIViewController* _viewController;
  MPAdView *_bannerView;
  id<MunerisBannerAdsDelegate> _bannerAdsdelegate;
  NSString* _appId;
  MunerisBannerAdsSizes _bannerSize;
  int _retryCount;
  int noOfRetries;
  
}

-(id) initWithDictionary:(NSDictionary*) dictionary withSize:(MunerisBannerAdsSizes) bannerSize withUIViewController:(UIViewController*) viewController;

-(void) loadAdsWithDelegate:(id<MunerisBannerAdsDelegate>)delagate;

-(void) reloadAdsWithViewController: (UIViewController*) viewController;

@end
