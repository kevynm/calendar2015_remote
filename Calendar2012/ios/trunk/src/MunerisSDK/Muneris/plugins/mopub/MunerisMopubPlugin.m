//
//  MunerisMopubPlugin.m
//  MunerisKit
//
//  Created by Casper LEE on 08/09/2011.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisMopubPlugin.h"
#import "MunerisMacro.h"
#import "MunerisConstant.h"
#import "MunerisAdsView.h"

@implementation MunerisMopubPlugin

-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
  if ((self = [self init])) {
    [self addNamespace:NSStringFromProtocol(@protocol(MunerisBannerAdsPluginProtocol))];
    [self updateConfiguration:dictionary];
  }
  return self;
}





-(void) loadAdsWithSize:(MunerisBannerAdsSizes)size withZone:(NSString*)zone WithDelegate:(id<MunerisBannerAdsDelegate>)delagate withUIViewController:(UIViewController*) viewController;
{
  
  //#TODO FIX THIS MEMORY LEAK
  MunerisMopub* mpa = [[[MunerisMopub alloc] initWithDictionary:_info withSize:size withUIViewController:(UIViewController*) viewController] autorelease];
  
  [mpa loadAdsWithDelegate:delagate];
  
}

-(void) dealloc;
{
  if (_info) {
    [_info release];
    _info = nil;
  }
  
  [super dealloc];
}

@end



@interface MunerisMopub(private)

-(void) cleanUp;

@end

@implementation MunerisMopub

- (id) initWithDictionary:(NSDictionary *)dictionary withSize:(MunerisBannerAdsSizes)bannerSize withUIViewController:(UIViewController*) viewController{
  //  [self retain];
  if ((self = [self init])) {
    _viewController = viewController;
    _appId = [[dictionary objectForKey:@"appId"] copyWithZone:[self zone]];
    if ([[dictionary objectForKey:@"retryCount"] copyWithZone:[self zone]] != nil) {
      _retryCount = [[[dictionary objectForKey:@"retryCount"] copyWithZone:[self zone]] intValue];   
    }
    _bannerSize = bannerSize;
  }
  return self;
  
}

-(void) loadAdsWithDelegate:(id<MunerisBannerAdsDelegate>)delagate;
{
  MunerisAdsView * obmAdsView = [[[MunerisAdsView alloc] initWithSize:_bannerSize] autorelease];
  obmAdsView.delegate = self;
  
  _bannerView = [[MPAdView alloc] initWithAdUnitId:_appId size:obmAdsView.frame.size];
  
  _bannerView.delegate = self;
  _bannerView.frame = obmAdsView.frame;
  
  _bannerAdsdelegate = [delagate retain];
  
  [_bannerView loadAd];
  
  [obmAdsView addSubview:_bannerView];
  
  [delagate didReceiveAds:obmAdsView];
  [obmAdsView.superview bringSubviewToFront:obmAdsView];
}

-(UIViewController *) viewControllerForPresentingModalView
{
  return _viewController;
}

- (void) adViewDidLoadAd:(MPAdView *)bannerView;
{
  MLOG(@"Muneris : PLUGIN MOPUB Banner View Action Did Loaded");
}

- (void) didDismissModalViewForAd:(MPAdView *)bannerView;
{
  MLOG(@"Muneris : PLUGIN MOPUB Banner View Action Did Finish");
}

- (void) adViewDidFailToLoadAd:(MPAdView *)bannerView;
{   
  MLOG(@"Muneris : PLUGIN MOPUB Banner View Action Did Fail");
  
  noOfRetries++;
  if (noOfRetries >= _retryCount) {  
    NSError* error = [NSError errorWithDomain:@"muneris" code:38 userInfo:[NSDictionary dictionaryWithObject:[bannerView superview] forKey:@"MunerisAdsView"]];
    [_bannerAdsdelegate didFailToReceiveAds:error];
    return;
  }
  
}

- (void) refreshAds;
{
  [_bannerView refreshAd];
  MLOG(@"Muneris: PLUGIN MOPUB Did Refresh");
}

-(void) reloadAdsWithViewController: (UIViewController*) viewController;
{
  _viewController = viewController;
  [_bannerView loadAd];
  MLOG(@"Muneris: PLUGIN MOPUB Did Reload with ViewController %@", viewController);
}

-(void) dealloc;
{
  MLOG(@"* DEALLOC Muneris : PLUGIN MOPUB");
  if (_bannerAdsdelegate) {
    [_bannerAdsdelegate release];
    _bannerAdsdelegate = nil;
  }
  
  if (_appId) {
    [_appId release];
    _appId = nil;
  }
  
  if (_bannerView) {
    [_bannerView release];
    _bannerView = nil;
  }
  [super dealloc];
}

@end
