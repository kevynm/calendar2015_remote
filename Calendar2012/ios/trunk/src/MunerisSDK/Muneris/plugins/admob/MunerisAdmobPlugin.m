//
//  MunerisAdmobPlugin.m
//  MunerisKit
//
//  Created by Casper LEE on 08/09/2011.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisAdmobPlugin.h"
#import "MunerisMacro.h"
#import "GADBannerView.h"

@implementation MunerisAdmobPlugin
-(id) initPluginWithDictionary:(NSDictionary *)dictionary;
{
    if ((self = [self init])) {
      [self addNamespace:NSStringFromProtocol(@protocol(MunerisBannerAdsPluginProtocol))];
      [self updateConfiguration:dictionary];
    }
    return self;
}


-(void) loadAdsWithSize:(MunerisBannerAdsSizes)size withZone:(NSString *)area WithDelegate:(id<MunerisBannerAdsDelegate>)delagate withUIViewController:(UIViewController *)viewController;
{

    //#TODO FIX THIS MEMORY LEAK
    MunerisAdmob* ama = [[[MunerisAdmob alloc] initWithDictionary:_info withSize:size] autorelease];
    
    [ama loadAdsWithDelegate:delagate withUIViewController:viewController];
    
}

-(void) dealloc;
{
    if (_info) {
        [_info release];
        _info = nil;
    }
    
    [super dealloc];
}

@end

@interface MunerisAdmob (private)
- (void) cleanup;
@end

@implementation MunerisAdmob

- (id) initWithDictionary:(NSDictionary *)dictionary withSize:(MunerisBannerAdsSizes)bannerSize;
{
    //[self retain];
    if ((self = [self init])) {
        _appId = [[dictionary objectForKey:@"appId"] copyWithZone:[self zone]];
        _bannerSize = bannerSize;
        if ([[dictionary objectForKey:@"retryCount"] copyWithZone:[self zone]] != nil) {
            _retryCount = [[[dictionary objectForKey:@"retryCount"] copyWithZone:[self zone]] intValue];   
        }
    }
    return self;
    
}

- (void) loadAdsWithDelegate:(id<MunerisBannerAdsDelegate>)delagate withUIViewController:(UIViewController*) viewcontroller;
{
    MunerisAdsView* obmAdsView = [[[MunerisAdsView alloc] initWithSize:_bannerSize] autorelease];
    obmAdsView.delegate = self;
    
    _bannerView = [[GADBannerView alloc] initWithFrame:obmAdsView.frame];
    
    _bannerView.delegate = self;
    _bannerView.adUnitID = _appId;
    _bannerView.rootViewController = viewcontroller;

    _bannerAdsdelegate = [delagate retain];
    
    [_bannerView loadRequest:[GADRequest request]];
    
    [obmAdsView addSubview:_bannerView];
    
    [delagate didReceiveAds:obmAdsView];
    [obmAdsView.superview bringSubviewToFront:obmAdsView];
}

- (void) adViewDidReceiveAd:(GADBannerView *)bannerView {
    MLOG(@"Muneris : PLUGIN ADMOB Banner View Action Did Loaded");
    
}

- (void) adViewDidDismissScreen:(GADBannerView *)bannerView {
    MLOG(@"Muneris : PLUGIN ADMOB Banner View Action Did Finish");    
}

- (void) adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    MLOG(@"Muneris : PLUGIN ADMOB Banner View Action Did Fail");
    noOfRetries++;
    if (noOfRetries >= _retryCount) {
        NSError* error = [NSError errorWithDomain:@"muneris" code:38 userInfo:[NSDictionary dictionaryWithObject:[bannerView superview] forKey:@"MunerisAdsView"]];
        [_bannerAdsdelegate didFailToReceiveAds:error];
        return;
    }
}

- (void) refreshAds;
{
    [_bannerView loadRequest:[GADRequest request]];
    MLOG(@"Muneris: PLUGIN ADMOB Did Refresh");
}

-(void) reloadAdsWithViewController: (UIViewController*) viewController;
{
  _bannerView.rootViewController = viewController;
  MLOG(@"Muneris: PLUGIN MOPUB Did Reload with ViewController %@", viewController);
}

- (void) dealloc;
{
    MLOG(@"* DEALLOC MUNERIS: PLUGIN ADMOB");
    if (_bannerAdsdelegate) {
        [_bannerAdsdelegate release];
        _bannerAdsdelegate = nil;
    }
    
    if (_appId){
        [_appId release];
        _appId = nil;
    }
    
    if (_bannerView){
        [_bannerView release];
        _bannerView = nil;
    }
    
    [super dealloc];
}

@end