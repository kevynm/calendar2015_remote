//
//  MunerisAdmobPlugin.h
//  MunerisKit
//
//  Created by Casper LEE on 08/09/2011.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AbstractMunerisPlugin.h"
#import "MunerisBannerAdsPluginProtocol.h"
#import "MunerisInboxPlugin.h"
#import "MunerisPluginProtocol.h"
#import "MunerisAdsView.h"
#import "GADBannerView.h"

@interface MunerisAdmobPlugin : AbstractMunerisPlugin<MunerisBannerAdsPluginProtocol> {
  //    CGRect _bannerSize;
}
@end

@interface MunerisAdmob : NSObject <GADBannerViewDelegate> {
  
  GADBannerView * _bannerView;
  id<MunerisBannerAdsDelegate> _bannerAdsdelegate;
  NSString* _appId;
  MunerisBannerAdsSizes _bannerSize;
  UIViewController* _viewController;
  int _retryCount;
  int noOfRetries;
  
}

- (id) initWithDictionary: (NSDictionary *) dictionary withSize:(MunerisBannerAdsSizes) bannerSize;

-(void) loadAdsWithDelegate:(id<MunerisBannerAdsDelegate>)delegate withUIViewController:(UIViewController*) viewcontroller;

-(void) reloadAdsWithViewController: (UIViewController*) viewController;

@end