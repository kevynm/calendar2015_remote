//
//  MunerisApiRequestProtocol.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/29/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>


@class MunerisBaseApiRequest;

@protocol MunerisApiRequestDelegate <NSObject>


//-(void) didSuccessRecievingInstallId: (NSString*) installId;
//
//-(void) didFailRecievingInstallId: (NSError*) error;

-(void) requestDidSuccess:(MunerisBaseApiRequest *)request dictionary:(NSDictionary*) data;

-(void) requestDidFail:(MunerisBaseApiRequest *)request error:(NSError*) error;



@end
