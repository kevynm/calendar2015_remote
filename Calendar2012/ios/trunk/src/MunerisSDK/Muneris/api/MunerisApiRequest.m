//
//  MunerisIntallIdApiRequest.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/29/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisApiRequest.h"
#import "MunerisMacro.h"
#import "MunerisApiResponseDelegatesBroadcaster.h"

@interface MunerisApiRequest(private)

-(void) retryInInterval:(int) interval;

-(void) startRequest;

@end

@implementation MunerisApiRequest

@synthesize retry = _retry, interval = _interval, munerisDelegate = _munerisDelegate;

-(void) startAsynchronous;
{
  MLOG(@"Muneris: Reqest StartAsync %@", self.method);
  [self setNumberOfTimesToRetryOnTimeout:3];
  [super startAsynchronous];
  // retry// capture current callbacks here
}


-(void) retryInInterval:(int) interval;
{
  retryCount++;
  MLOG(@"Muneris: Reqest Retry %@", self.method);

  [self performSelector:@selector(startAsynchronous) withObject:self afterDelay:_interval];
}


-(id) init;
{
  
  if ((self = [super init])) {
    MunerisApiResponseDelegatesBroadcaster* del = [MunerisApiResponseDelegatesBroadcaster delegateWithZone:[self zone]];

    [del addDelegates:self];
    
    self.delegate = del;
    [self.delegate retain];
  }
  return self;
}


-(void) requestDidFail:(MunerisBaseApiRequest *)req error:(NSError *)err;
{
  
  if (self.retryCount >= 0) {
    MLOG(@"Muneris: REQUEST FAILED %@", _method);
    [_munerisDelegate requestDidFail:req error:err];
    [self.delegate release];
  }else{
    [self retryInInterval:1];
  }
  
}

-(void) requestDidSuccess:(MunerisBaseApiRequest *)req dictionary:(NSDictionary *)data;
{
  MLOG(@"Muneris: REQUEST OK %@", _method);
  [_munerisDelegate requestDidSuccess:req dictionary:data];
  [self.delegate release];
}


-(void) dealloc;
{
  
  MLOG(@"* DEALLOC Muneris: REQUEST %@", _method);
  if (_munerisDelegate) {
    [_munerisDelegate release];
    _munerisDelegate = nil;
  }
  
  [super dealloc];
}

@end