//
//  MunerisIntallIdApiRequest.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/29/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "MunerisApiRequestDelegate.h"
#import "MunerisBaseApiRequest.h"



@class MunerisApiResponseDelegatesBroadcaster;

@interface MunerisApiRequest : MunerisBaseApiRequest<MunerisApiRequestDelegate> {
  
  BOOL _hasStartAsync;
  
  int _retry;
  
  int _interval;
  
  MunerisApiResponseDelegatesBroadcaster* _munerisDelegate;
  
}

@property (assign,nonatomic) int retry;

@property (assign,nonatomic) int interval;

@property (retain) MunerisApiResponseDelegatesBroadcaster* munerisDelegate;




@end
