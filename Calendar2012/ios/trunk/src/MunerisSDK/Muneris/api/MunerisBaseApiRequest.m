//
//  MunerisApiRequest.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/25/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisApiRequest.h"
#import "MunerisManager.h"
#import "MunerisConstant.h"
#import "SBJson.h"
#import "MunerisMacro.h"


@interface MunerisBaseApiRequest(private) 

-(NSString*) jsonStringWithMethod:(NSString*) method payload:(NSDictionary*) payload;

@end

@implementation MunerisBaseApiRequest

@synthesize method = _method;

+(id) requestWithMethod:(NSString*) method payload:(NSDictionary*) payload zone:(NSZone*) zone;
{
  id ret  = [[[self allocWithZone:zone] initWithMethod:method payload:payload] autorelease];
  return ret;
}


+(id) requestWithMethod:(NSString*) method payload:(NSDictionary*) payload;
{
  //check user install ID;
  
  
  id ret  = [[[self alloc] initWithMethod:method payload:payload] autorelease];
  
  return ret;
}


-(id) initWithMethod:(NSString*) method payload:(NSDictionary*) payload;
{
  MunerisManager* man = [MunerisManager sharedManager];
  
  [man retain];
  
  _method = [method copyWithZone:[self zone]];
  
//  NSString* munerisUrl = [[[man.config objectForKey:@"muneris"] objectForKey:@"apiUrl"] copyWithZone:[self zone]];
  
  NSString* munerisUrl = [[MunerisManager apiURL] copyWithZone:[self zone]];
  
  NSAssert(munerisUrl != nil, @"apiUrl must exists");
  
  NSURL* apiUrl = [[NSURL URLWithString:munerisUrl] retain];
  
  
  //this is bundleId
  NSString* user = [[[[man getHeader] objectForKey:@"app"] objectForKey:@"bundleId"] copyWithZone:[self zone]];
  
  //create password here
  //NSString* pass = [[[man.config objectForKey:@"muneris"] objectForKey:@"apiKey"] copyWithZone:[self zone]];
  NSString* pass = [[MunerisManager apiKey] copyWithZone:[self zone]];
  
  NSString* body = [[MunerisApiRequest jsonStringWithMethod:_method payload:payload] copyWithZone:[self zone]];
  
  NSString* ua = [NSString stringWithFormat:@"%@ %@, %@",kMunerisSDKAgent,kMunerisSDKPlaform,kMunerisSDKVersion];
  
  NSAssert((user != nil && pass != nil), @"both apiUsername and apiPassword should exists");
  
//  [self setUsername:user];
//  
//  [self setPassword:pass];
  
  [self addBasicAuthenticationHeaderWithUsername:user andPassword:pass];
  
  
  [self setRequestMethod:@"POST"];
  
  [self setUserAgent:ua];
  
  //[self addRequestHeader:@"User-Agent" value:userAgent]; 
  
  [self addRequestHeader:@"Content-Type" value:@"application/json"];
  
  [self appendPostData:[body dataUsingEncoding:kMunerisApiEncoding]];
  
  [man release];
  
  [munerisUrl release];
  
  [apiUrl release];
  
  [user release];
  
  [pass release];
  
  [body release];
  
  if ((self = [self initWithURL: apiUrl])) {
    
  }
  return self;
}


-(id) init;
{
  if ((self = [super init])) {
    
    //[self setShouldAttemptPersistentConnection:YES];
    
    //[self setShouldCompressRequestBody:YES];
    
    //[self setShouldContinueWhenAppEntersBackground:YES];
    
    //[self setShouldRedirect:YES];
    
    [self setShouldPresentAuthenticationDialog:NO];
    
    //[self setTimeOutSeconds:10];
    
    //[self setNumberOfTimesToRetryOnTimeout:1];
    
    [self setValidatesSecureCertificate:false];

    
  }
  return self;
}

+(NSString*) jsonStringWithMethod:(NSString*) method payload:(NSDictionary*) payload;
{
  NSMutableDictionary* dict = [[NSMutableDictionary dictionary] retain];
  
  [dict setValue:method forKey:@"method"];
  
  [dict setValue:kMunerisSDKVersion forKey:@"api"];
  
  [dict setValue:[[MunerisManager sharedManager] getHeader] forKey:@"header"];
  
  
  if (payload) {
      [dict setValue:payload forKey:@"params"];
  }

  
  SBJsonWriter* writer = [[SBJsonWriter alloc] init];
  
  NSString* jsonString = [writer stringWithObject:dict];
  
  [writer release];
  
  [dict release];
  
  return jsonString;
}

+(NSDictionary*) dictionaryFromJsonPayload:(NSString*) payload;
{
  SBJsonParser* parser = [[SBJsonParser alloc] init];
  
  NSDictionary* dict  = [[parser objectWithString:payload] retain];
  
  [parser release];
  
  [dict release];
  
  return dict;
}





-(void) dealloc;
{
  if (_method) {
    [_method release];
    _method = nil;
  }
  [super dealloc];
}

@end
