//
//  MunerisApi.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/30/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisDelegates.h"
#import "MunerisApiRequestDelegate.h"

@class MunerisApiRequest;
@class MunerisApiResponseDelegatesBroadcaster;

//A wrapper around Muneris Api
@interface MunerisApi : NSObject<MunerisApiRequestDelegate>{
  
  
  MunerisApiRequest* _request;
  
  id<MunerisApiCallDelegate> _delegate;
  
  
  NSString* _method;
  
  NSDictionary* _payload;
  
}

+(id) apiMethod:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate;

+(id) apiMethod:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate options:(NSDictionary*) options;

+(id) apiMethod:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate zone:(NSZone*) zone;


@end
