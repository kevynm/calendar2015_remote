//
//  MunerisApi.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/30/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisApi.h"
#import "MunerisApiRequest.h"
#import "MunerisApiResponseDelegatesBroadcaster.h"
#import "MunerisManager.h"
#import "MunerisState.h"
#import "MunerisStateManager.h"
#import "MunerisMacro.h"

@interface MunerisApi(private)

-(id) initWithApi:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate;

-(id) initWithApi:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate options:(NSDictionary*) options;

-(void) didSuccessRecievingInstallId:(NSString *)installId;

//-(void) didFailRecievingInstallId:(NSError *)error;

-(void) checkInstallId;

-(void) closeLoop:(MunerisBaseApiRequest*) req;

@end

@implementation MunerisApi


+(id) apiMethod:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate;
{
  id ret = [[[self alloc] initWithApi:method payload:payload delegate:delegate] autorelease];
  return ret;
}

+(id) apiMethod:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate options:(NSDictionary*) options;
{
  id ret = [[[self alloc] initWithApi:method payload:payload delegate:delegate options:options] autorelease];
  return ret;
}

+(id) apiMethod:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate zone:(NSZone*) zone;
{
  
  id ret = [[[self allocWithZone:zone] initWithApi:method payload:payload delegate:delegate] autorelease];
  return ret;
}

-(id) initWithApi:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate;
{
  if ((self = [self initWithApi:method payload:payload delegate:delegate options:nil])) {
    
  }
  return self;
}


-(id) initWithApi:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate options:(NSDictionary*) options;
{
  NSAssert(delegate,@"all api calls needs a callback");
  
  MLOG(@"Muneris: API init %@",method);
  
  _delegate = [delegate retain];
  
  _method = [method copyWithZone:[self zone]];
  
  _payload = [_payload retain];
  
  MunerisApiResponseDelegatesBroadcaster* multidelegate = [MunerisApiResponseDelegatesBroadcaster delegateWithZone:[self zone]];
  
  [multidelegate addDelegates:self];
  
  
  if ((self = [self init])) {
    
    _request = [[MunerisApiRequest requestWithMethod:method payload:payload zone:[self zone]] retain];
    
    [_request setValidatesSecureCertificate:false];
    
    [_request setMunerisDelegate:multidelegate];
    if (options) { 
      BOOL sync = [[options objectForKey:@"sync"] boolValue];
      if (sync) {
        [_request startSynchronous]; 
      }else{
        [_request startAsynchronous];
      }
    }else{
      [_request startAsynchronous];
    }
  }
  return self;
}


#pragma mark installId Check


-(void) requestDidSuccess:(MunerisBaseApiRequest *)request dictionary:(NSDictionary*) data;
{
  MLOG(@"Muneris: API Success %@ status %d \n url : %@ \n response : %@ \n requestHeaders : %@ \n requestBody : %@", 
       request.method,
       request.responseStatusCode,
       request.url,
       data,
       request.requestHeaders,
       [[[NSString alloc] initWithData:request.postBody encoding:NSUTF8StringEncoding] autorelease]
       );
  NSDictionary* header = [data objectForKey:@"header"];
  if (header) {
    //update api info
    [[MunerisManager sharedManager].apiInfo removeAllObjects];
    [[MunerisManager sharedManager].apiInfo setDictionary:header];
    MLOG(@"Muneris: header update %@", request.method);
  }
  
  [_delegate didSuccessfulCallingApiWithDictionary:data];
    
  [self closeLoop:request];
  
}


-(void) requestDidFail:(MunerisBaseApiRequest *)request error:(NSError*) error;
{
  
  
  
  
  MLOG(@"Muneris: API Failed %@ status %d \n url : %@ \n requestBody : %@ \n****\n error: %@  \n errorResponse : %@", 
       request.method,
       request.responseStatusCode,
       request.url,
       [[[NSString alloc] initWithData:request.postBody encoding:NSUTF8StringEncoding] autorelease],
       error,
       [request responseString]
       );
  
  [_delegate didFailCallingApiWithError:error];
  
  [self closeLoop:request];

}

-(void) closeLoop:(MunerisBaseApiRequest*) req;
{
  [_request release];
  _request = nil;
}


-(void) dealloc;
{
  
  MLOG(@"* DEALLOC Muneris API: %@", _method );

  if (_method) {
    [_method release];
    _method = nil;
  }
  
  if (_payload) {
    [_payload release];
    _payload = nil;
  }
  
  if (_delegate) {
    [_delegate release];
    _delegate = nil;
  }
  
  if (_request) {
    [_request release];
    _request = nil;
  }
  

  [super dealloc];
}

@end
