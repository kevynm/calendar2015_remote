//
//  MunerisApiChainedDelegates.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/30/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequestDelegate.h"
#import "MunerisApiRequestDelegate.h"
#import "MunerisMultiDelegate.h"


@interface MunerisApiResponseDelegatesBroadcaster : MunerisMultiDelegate<ASIHTTPRequestDelegate,MunerisApiRequestDelegate> {
  
  
}


-(void) addDelegates:(id<MunerisApiRequestDelegate>) delegate;

@end
