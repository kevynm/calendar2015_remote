//
//  MunerisApiRequest.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/25/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"

#import "Reachability.h"

@interface MunerisBaseApiRequest : ASIHTTPRequest {
  
  NSString* _method;
  
  
}

@property (readonly) NSString* method;


+(id) requestWithMethod:(NSString*) method payload:(NSDictionary*) payload;

+(id) requestWithMethod:(NSString*) method payload:(NSDictionary*) payload zone:(NSZone*) zone;

-(id) initWithMethod:(NSString*) method payload:(NSDictionary*) payload;

+(NSString*) jsonStringWithMethod:(NSString*) method payload:(NSDictionary*) payload;

+(NSDictionary*) dictionaryFromJsonPayload:(NSString*) payload;

@end
