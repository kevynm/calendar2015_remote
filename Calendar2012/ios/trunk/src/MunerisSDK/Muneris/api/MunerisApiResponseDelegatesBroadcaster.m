//
//  MunerisApiChainedDelegates.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/30/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisApiResponseDelegatesBroadcaster.h"
#import "MunerisConstant.h"
#import "MunerisBaseApiRequest.h"
#import "MunerisApiRequest.h"
#import "MunerisDelegates.h"
#import "MunerisMacro.h"

@implementation MunerisApiResponseDelegatesBroadcaster


-(void) addDelegates:(id<MunerisApiRequestDelegate>) delegate;
{
  [super addDelegates:delegate];
}


- (void)requestFinished:(ASIHTTPRequest *) req;
{
  [self retain];
  MunerisBaseApiRequest* theReq = (MunerisBaseApiRequest*)req;
  if ([_delegates count] > 0) {
    
    NSString* responseBody = [[req responseString] copyWithZone:[self zone]];
    
    NSDictionary* body = [[MunerisBaseApiRequest dictionaryFromJsonPayload:responseBody] retain];
    
    for (id<MunerisApiRequestDelegate> del in  _delegates) {
      [del retain];
      MLOG(@"Muneris: BOARDCAST %@ to %@",theReq.method,  del);
      
      NSError* mError;
      
      switch ([req responseStatusCode]) {
        case 200:
          [del requestDidSuccess:theReq dictionary:body];
          break;      
        default:
          if (!body) {
            [body release];
            body = [[NSDictionary dictionaryWithObject:@"noContent" forKey:@"error"] retain];
          }
          mError = [NSError errorWithDomain:@"muneris" code:MunerisApiServerError userInfo:body];
          [del requestDidFail:(MunerisBaseApiRequest*)req error: mError];
          //[del requestDidSuccess:theReq dictionary:body];
          break;
      }
      [del release];
    }
    
    [responseBody release];
    
    [body release];
    
  }
  [self release];
}


-(void) requestFailed:(ASIHTTPRequest *)req;
{
  [self retain];
  if ([_delegates count] > 0) {
  
    NSError* mError;
    
    NSString* responseBody = [[req responseString] copyWithZone:[self zone]];
    
    if (responseBody) {
      
      NSDictionary* body = [[MunerisBaseApiRequest dictionaryFromJsonPayload:responseBody] retain];
      
      mError = [NSError errorWithDomain:@"muneris" code:MunerisApiServerError userInfo:body];
      
      [body release];
    }else{
      mError = [req error];
    }
    
    for (id<MunerisApiRequestDelegate> del in  _delegates) {
      [del retain];
      [del requestDidFail:(MunerisBaseApiRequest*)req error:mError];
      [del release];
    }
    
    [responseBody release];
    
  }
  [self release];
}

-(void) requestDidSuccess:(MunerisBaseApiRequest *)request dictionary:(NSDictionary*) data;
{
  [self retain];
  if ([_delegates count] > 0) {
    for (id<MunerisApiRequestDelegate> del in  _delegates) {
      [del retain];
      [del requestDidSuccess:request dictionary:data];
      [del release];
    }
  }
  [self release];
}

-(void) requestDidFail:(MunerisBaseApiRequest *)request error:(NSError*) error;
{
  [self retain];
  if ([_delegates count] > 0) {
    for (id<MunerisApiRequestDelegate> del in  _delegates) {
      [del retain];
      [del requestDidFail:request error:error];
      [del release];
    }
  }
  [self release];
}



-(void) dealloc;
{
  MLOG(@"* DEALLOC Muneris: BOARDCASTER with delegates %d", [_delegates count]);
  //MLOG(@"* DEALLOC Muneris: Boardcaster delegate with delegates %@" , _delegates);
  if (_delegates) {
    [_delegates release];
    _delegates = nil;
  }
  [super dealloc];
}


@end
