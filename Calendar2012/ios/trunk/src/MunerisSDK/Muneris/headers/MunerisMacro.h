//
//  MunnerisMacro.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/30/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//


//debug on off
//comment below to turn off

// 0 = MunerisModeDebug,
// 1 = MunerisModeProduction,
// 2 = MunerisModeTesting


//to override following configuration
#import "MunerisConfig.h"

#ifndef MUNERIS_API_KEY
#error please define the muneris api key in prefix
#endif

#ifndef MUNERIS_MODE
#define MUNERIS_MODE 1
#endif

#ifndef MUNERIS_DEBUG
#define MUNERIS_DEBUG 0
#endif

#if !defined(DEBUG) || DEBUG == 0 || !defined (MUNERIS_DEBUG) || MUNERIS_DEBUG == 0
#define MLOG(...) do {} while (0)
#elif DEBUG >= 1
#define MLOG(...) NSLog(__VA_ARGS__)
#endif

#if (MUNERIS_MODE == 1) && (defined(DEBUG) && DEBUG == 1 || MUNERIS_DEBUG > 0)
#error please switch off DEBUG / MUNERIS_DEBUG flag if MUNERIS_MODE is production
#endif


#ifndef MUNERIS_API_BASE
#define MUNERIS_API_BASE @"https://muneris.animoca.com/api/%@/commands/"
#endif

#define RANDOM() ((random() / (float)0x7fffffff ))

#ifndef MUNERIS_PLIST_PREFIX
#define MUNERIS_PLIST_PREFIX @""
#endif

