//
//  MunerisBaseAppDelegate.h
//  MunerisKit
//
//  Created by Jacky Yuk on 10/24/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MunerisApplicationDelegate.h"


@interface MunerisBaseAppDelegate : NSObject<UIApplicationDelegate>
{
  MunerisApplicationDelegate* _mAppDel;

}

@end
