//
//  MunerisDelegates.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MunerisAdsView.h"
#import "MunerisMessages.h"


@protocol MunerisApiCallDelegate <NSObject>

@required
-(void) didSuccessfulCallingApiWithDictionary:(NSDictionary*) data;

-(void) didFailCallingApiWithError:(NSError*) error;

@end

@protocol MunerisInboxDelegate <NSObject>

@required
-(void) didRecievedMessage:(MunerisMessages*) msgs;

@end


@protocol MunerisBannerAdsDelegate <NSObject>

@required
-(void) didReceiveAds:(MunerisAdsView*) view;

-(void) didFailToReceiveAds:(NSError *)view;

@end

@protocol MunerisTakeoverDelegate <NSObject>

@required
-(BOOL) shouldShowTakeover:(NSDictionary*) takeoverInfo;

//@optional
-(void) didFailToLoadTakeover:(NSDictionary*) takeoverInfo;

-(void) didFinishedLoadingTakeover:(NSDictionary*) takeoverInfo;

@end

@protocol MunerisOffersDelegate <NSObject,MunerisInboxDelegate,MunerisTakeoverDelegate>

@required
-(void) didClosedOffersView;

@end

