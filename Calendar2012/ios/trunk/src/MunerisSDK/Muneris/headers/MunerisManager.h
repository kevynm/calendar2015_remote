//
//  MunerisManager.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/25/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MunerisConstant.h"
#import "MunerisDelegates.h"

@class AbstractMunerisPlugin;
@class MunerisStateManager;
@class MunerisApi;

@interface MunerisManager : NSObject<UIApplicationDelegate> {
  
  
  MunerisStateManager* _sm;
  
  NSDictionary* _config;
  
  NSMutableDictionary* _apiInfo;
  
  NSDictionary* _plugins;
  
  NSOperationQueue* _taskQueue;
  
  UIViewController* _takeoverViewController;
  
}
@property (retain) UIViewController* takeoverViewController;

@property (readonly) NSOperationQueue* operationQueue;

@property (readonly) MunerisStateManager* stateManager;

@property (readonly) NSDictionary* config;

@property (readonly) NSMutableDictionary* apiInfo;

@property (readonly) NSString* udid;

@property (readonly) NSMutableDictionary* state;

+(void) setMode:(MunerisMode) mode;

+(MunerisMode) mode;

+(MunerisManager*) sharedManager;

+(NSString*) apiURL;

+(NSString*) apiKey;

+(void) purgeSharedManager;

-(NSDictionary*) getHeader;

-(NSString*) applicationName;

-(void) saveState;



//methods;
-(void) apiCall:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate;

-(void) checkMessages:(NSArray*)type delegate:(id<MunerisInboxDelegate>) delegate;

-(void) loadAdswithBannerSize:(MunerisBannerAdsSizes)size withZone:(NSString*) zone withDelegate:(id<MunerisBannerAdsDelegate>) delegate withUIViewController:(UIViewController*) viewController;

-(void) loadTakeover:(NSString*)zone withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate;

-(void) showTakeover:(NSString*)zone withViewController:(UIViewController*)viewController;

-(BOOL) hasTakeover:(NSString*)zone;

-(void) showOffers:(UIViewController*)viewController delegate:(id<MunerisOffersDelegate>) delegate;

-(BOOL) hasOffers;

-(BOOL) hasBannerAds:(NSString*)zone;

-(void) logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;

-(void) logStartEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;

-(void) logEndEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;


//ENVARS
-(NSDictionary*) getEnvars;

-(void) updateEnvars;

-(void) checkAppVersion;

-(NSArray*) pluginForwardInvocation:(NSInvocation*) invocation;

-(NSArray*) getPluginsByNamespace:(NSString*) ns;

-(NSInvocation*) getInvocationWith:(Class) cls selector:(SEL) selector arguments:(id) arguments,...;

@end
