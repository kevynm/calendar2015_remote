//
//  MunerisConstant.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/24/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

extern NSString* const kMuenrisFolder;

extern NSString* const kMunerisSDKPlaform;

extern NSString* const kMunerisSDKVersion;

extern NSString* const kMunerisSDKAgent;

extern NSString* const kMuenrisStateFile;

extern NSString* const kMunerisPlistFileFormat;

extern NSString* const kMunerisInstallIdApiMethod;

extern NSString* const kMunerisPluginClassFormat;

extern NSStringEncoding const kMunerisApiEncoding;

extern NSString* const MUNERIS_ENVARS_UPDATE_SUCCESS_NOTIFICATION;

typedef enum {
  MunerisUnKnownMessage,
  MunerisTextMessage,
  MunerisCreditsMessage
//  ,
//  MunerisVersionUpdateMessage
} MunerisMessageType;


typedef enum {
  MunerisModeDebug,
  MunerisModeProduction,
  MunerisModeTesting
} MunerisMode;


typedef enum {
  MunerisApiServerError
} MunerisApiError;

typedef enum {
  MunerisBannerAdsSizes320x50,
    MunerisBannerAdsSizes728x90
} MunerisBannerAdsSizes;



