//
//  MunerisApplication.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MunerisApplicationDelegate.h"

@interface MunerisApplication : UIApplication {
  
  MunerisApplicationDelegate* _del;
}

@end
