//
//  MunerisWebView.m
//  MunerisKit
//
//  Created by Casper Lee on 24/11/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisWebView.h"
#import "Muneris.h"

@interface MunerisWebView (private)
- (void) positionView;
@end

@implementation MunerisWebView
@synthesize webView = webView, webViewCloseMsg = webViewCloseMsg, webViewTitle = webViewTitle;
@synthesize viewController = _viewController;

- (id) initWithViewController: (UIViewController*) viewController withDictionary: (NSDictionary*) dictionary withRequest: (NSMutableURLRequest*) request;
{
  if ((self = [self initWithNibName:@"MunerisWebView" bundle:[NSBundle mainBundle]])) {
    if (viewController) {
      _viewController = [viewController retain];
      
      [self positionView];
    }
    
    if (request) {
      _request = [request retain];
    }
    [self retain];
    
    if ([dictionary objectForKey:@"title"]) {
      NSString *webViewTitleText = [NSString stringWithFormat:@"  %@", [dictionary objectForKey:@"title"]];
      [webViewTitle setText:webViewTitleText];
    }
    if ([dictionary objectForKey:@"closeButton"]) {
      [webViewCloseMsg setTitle:[dictionary objectForKey:@"closeButton"] forState:UIControlStateNormal];
    }

  }
  return self;
}

-(void) positionView;
{
  CGRect rect = [[UIScreen mainScreen] bounds];
  
  if (_viewController) {
    rect = _viewController.view.bounds;
  }
  [self.view setFrame:rect];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)didReceiveMemoryWarning
{
  // Releases the view if it doesn't have a superview.
  [super didReceiveMemoryWarning];
  
  // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  CGRect screenBound = [[UIScreen mainScreen] bounds];
  [webView setFrame:CGRectMake(webView.frame.origin.x, webView.frame.origin.y, screenBound.size.width ,webView.frame.size.height)];
  [self.view setBackgroundColor:[UIColor clearColor]];
  
  [_viewController.view addSubview:self.view];

  self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  
  
  CGRect targetpos = self.view.frame;
  
  [UIView beginAnimations:@"displayWebView" context:nil];
  
//  [self.view setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
  [self.view setFrame:CGRectOffset(targetpos, 0, targetpos.size.height)];
  [UIView setAnimationDuration:0.4];
  [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
  [UIView setAnimationDelegate:self];
  [UIView setAnimationDidStopSelector:@selector(createWebView)];
  [self.view setFrame:targetpos];
  
  MLOG(@"%@" , NSStringFromCGRect(CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)));

  [UIView commitAnimations];
  
  [super viewDidLoad];
  

  // Do any additional setup after loading the view from its nib.
}

- (void) createWebView;
{
  [webView loadRequest:_request];
  [UIView beginAnimations:@"dimBackground" context:nil];
  [UIView setAnimationDuration:0.2];
  [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.7]];
  [UIView commitAnimations];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
  // Release any retained subviews of the main view.
  // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  // Return YES for supported orientations
  //  return (interfaceOrientation == UIInterfaceOrientationPortrait);
  return YES;
}


- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
  [self positionView];
}

- (IBAction)closeWebView:(id)sender;
{
  [self.view setUserInteractionEnabled:NO];
  [UIView beginAnimations:@"lightenBackground" context:nil];
  [UIView setAnimationDuration:0.2];
  [UIView setAnimationDelegate:self];
  [UIView setAnimationDidStopSelector:@selector(dismissWebView)];
  [self.view setBackgroundColor:[UIColor clearColor]];
  [UIView commitAnimations];
}

- (void) dismissWebView;
{
  [UIView beginAnimations:@"closeWebView" context:nil];
  [UIView setAnimationDuration:0.4];
  [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
  [self.view setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
  
  [UIView setAnimationDelegate:self];
  [UIView setAnimationDidStopSelector:@selector(removeWebView)];
  [UIView commitAnimations];
}

- (void) removeWebView;
{
  [self.view removeFromSuperview];
  [self release];
}

//- (void) loadRequest: (NSMutableURLRequest*)request;
//{
//  [webView loadRequest:request];
//}

- (void)webViewDidStartLoad:(UIWebView *)webView;
{
  if (!_hud) {
    _hud = [[MBProgressHUD alloc] initWithView:self.view];
  }
  
  [self.view addSubview:_hud];
  [_hud setLabelText:@"Loading..."];
//  [_hud setDimBackground:NO];
  [_hud show:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
  [_hud hide:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;{
  [_hud hide:YES];  
}

- (void) dealloc
{
  if (_viewController) {
    [_viewController release];
    _viewController = nil;
  }
  
  if (_request) {
    [_request release];
    _request = nil;
  }
  
  if (webView) {
    [webView release];
    webView = nil;
  }
  
  if (webViewTitle) {
    [webViewTitle release];
    webViewTitle = nil;
  }
  
  if (webViewCloseMsg) {
    [webViewCloseMsg release];
    webViewCloseMsg = nil;
  }
  
  [super dealloc];
  
}

@end