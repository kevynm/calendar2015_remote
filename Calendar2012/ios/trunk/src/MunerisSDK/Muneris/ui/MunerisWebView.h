//
//  MunerisWebView.h
//  MunerisKit
//
//  Created by Casper Lee on 24/11/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface MunerisWebView : UIViewController <UIWebViewDelegate> {

  MBProgressHUD* _hud;
  IBOutlet UILabel *webViewTitle;
  IBOutlet UIButton *webViewCloseMsg;
  IBOutlet UIWebView *webView;
  
  UIViewController* _viewController;
  NSMutableURLRequest* _request;
}

@property (nonatomic, retain) IBOutlet UILabel *webViewTitle;
@property (nonatomic, retain) IBOutlet UIButton *webViewCloseMsg;
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, assign) UIViewController *viewController;

- (id) initWithViewController: (UIViewController*) viewController withDictionary: (NSDictionary*) dictionary withRequest: (NSMutableURLRequest*) request;
- (IBAction)closeWebView:(id)sender;
//- (void) loadRequest: (NSMutableURLRequest*)request;

@end
