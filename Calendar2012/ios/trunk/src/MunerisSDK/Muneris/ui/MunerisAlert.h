//
//  MunerisAlert.h
//  MunerisKit
//
//  Created by Jacky Yuk on 9/14/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MunerisAlert : UIAlertView {
  
  NSMutableDictionary* _dict;
 
  id<UIAlertViewDelegate> _del;
  
}

@end
