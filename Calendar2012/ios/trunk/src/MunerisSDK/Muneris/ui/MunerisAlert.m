//
//  MunerisAlert.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/14/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisAlert.h"
#import "MunerisMacro.h"

@implementation MunerisAlert

-(id) init;
{
  if ((self = [super init])) {
    _dict = [[NSMutableDictionary alloc] init];
  }
  return self;
}

-(void) setDelegate:(id)delegate;
{
  _del = [delegate retain];
  [super setDelegate:_del];
}




-(void) dealloc;
{
  MLOG(@"* DEALLOC UIMuneris: Alert Box");
  
  if (_dict) {
    [_dict release];
    _dict = nil;
  }
  if (_del) {
    [_del release];
    _del = nil;
  }
  [super dealloc];
}


@end
