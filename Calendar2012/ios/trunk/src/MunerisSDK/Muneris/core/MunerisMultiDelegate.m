//
//  MunerisMultiDelegate.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/31/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisMultiDelegate.h"


@implementation MunerisMultiDelegate

+(id) delegate;
{
  id ret = [[[self alloc] init] autorelease];
  return ret;
}

+(id) delegateWithZone:(NSZone*) zone;
{
  id ret = [[[self allocWithZone:zone] init] autorelease];
  return ret;
}

-(id) init;
{
  _delegates = [[NSMutableArray alloc] init];
  if (( self = [super init])) {
    
  }
  return self;
}

-(void) addDelegates:(id) delegate;
{
  if ([_delegates indexOfObject:delegate] == NSNotFound) {
    [_delegates addObject:delegate];
  }
}




-(void) dealloc;
{
  if (_delegates) {
    [_delegates release];
    _delegates = nil;
  }
  [super dealloc];
}


@end
