//
//  MunerisHelper.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/25/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisHelper.h"
#import "SBJson.h"


@interface UIDevice(machine)
- (NSString *)machine;
@end

#include <sys/types.h>
#include <sys/sysctl.h>

@implementation UIDevice(machine)

- (NSString *)machine
{
  size_t size;
  
  // Set 'oldp' parameter to NULL to get the size of the data
  // returned so we can allocate appropriate amount of space
  sysctlbyname("hw.machine", NULL, &size, NULL, 0); 
  
  // Allocate the space to store name
  char *name = malloc(size);
  
  // Get the platform name
  sysctlbyname("hw.machine", name, &size, NULL, 0);
  
  // Place name into a string
  NSString *machine = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
  
  // Done with this
  free(name);
  
  return machine;
}

@end



@implementation MunerisHelper

+(NSString*) fullPathFromRelativePath:(NSString*) relPath;
{
	NSAssert(relPath != nil, @"CCFileUtils: Invalid path");
  
	NSString *fullpath = nil;
	
	// only if it is not an absolute path
	if( ! [relPath isAbsolutePath] )
	{
		NSString *file = [relPath lastPathComponent];
		NSString *imageDirectory = [relPath stringByDeletingLastPathComponent];
		
		fullpath = [[NSBundle mainBundle] pathForResource:file
                                               ofType:nil
                                          inDirectory:imageDirectory];
	}
	
	if (fullpath == nil)
		fullpath = relPath;
  
  return fullpath;
}


+(NSDictionary*) getAgent;
{
  NSMutableDictionary* info = [NSMutableDictionary dictionary];
  [info setValue:kMunerisSDKAgent forKey:@"name"];
  [info setValue:kMunerisSDKPlaform forKey:@"platform"];
  [info setValue:kMunerisSDKVersion forKey:@"ver"];
  return info;
}


+(NSDictionary*) getDevice;
{
  UIScreen *MainScreen = [UIScreen mainScreen];
  CGSize size = [MainScreen bounds].size;
  
  if ([MunerisHelper isIOSVersionGreaterThan:@"3.2"]) {
    UIScreenMode *ScreenMode = [MainScreen currentMode];
    size = [ScreenMode size]; 
  }
  
  int width = size.width;
  int height = size.height;
//  NSString* orientation = @"unknown";
//  switch ([UIDevice currentDevice].orientation) {
//    case UIDeviceOrientationPortrait:
//    case UIDeviceOrientationPortraitUpsideDown:
//      orientation = @"potrait";
//      break;
//    case UIDeviceOrientationLandscapeLeft:
//    case UIDeviceOrientationLandscapeRight:
//      orientation = @"landscape";
//      break;
//    default:
//      break;
//  };
  
  NSMutableDictionary* info = [NSMutableDictionary dictionary];
  [info setValue:[[UIDevice currentDevice] machine] forKey:@"model"];
  [info setValue:[UIDevice currentDevice].systemName forKey:@"osName"];
  [info setValue:[UIDevice currentDevice].systemVersion forKey:@"osVer"];
  [info setValue:@"Apple" forKey:@"manuf"];
  //[info setValue:orientation forKey:@"orientation"];
  [info setValue:[NSString stringWithFormat:@"%dx%d",width,height] forKey:@"screenSize"];
  return info;
}

+(NSDictionary*) getLocale;
{
  NSMutableDictionary* info = [NSMutableDictionary dictionary];
  [info setValue:[[NSLocale preferredLanguages] objectAtIndex:0] forKey:@"lang"];
  [info setValue:[[NSLocale currentLocale] objectForKey:NSLocaleCountryCode] forKey:@"country"];
  return info;

}

+(NSDictionary*) getApp;
{
  NSMutableDictionary* info = [NSMutableDictionary dictionary];
  [info setValue:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundleId"];
  [info setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:@"ver"];
  return info;
}

+(NSDictionary*) getAllInfo;
{
  NSMutableDictionary* info = [NSMutableDictionary dictionary];
  [info setValue:[self getAgent] forKey:@"agent"];
  [info setValue:[self getApp] forKey:@"app"];
  [info setValue:[self getDevice] forKey:@"device"];
  [info setValue:[self getLocale] forKey:@"locale"];
  return info;
}

+(NSString*) toJsonWithObject:(id) obj; 
{
  SBJsonWriter* writer = [[SBJsonWriter alloc] init];
  
  NSString* str =[writer stringWithObject:obj];
  
  [writer release];
 
  return str;
}

+(BOOL) isInternetReachable;
{
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus internetStatus = [reachability currentReachabilityStatus];
  
  switch (internetStatus) {
    case NotReachable:
      return NO;
      break;
      
    case ReachableViaWiFi:
      return YES;
      break;
      
    case ReachableViaWWAN:
      return YES;
      break;
      
    default:
      return NO;
      break;
  }
}

+(BOOL) isIOSVersionGreaterThan:(NSString*) version;
{
  return ([[[UIDevice currentDevice] systemVersion] compare:version options:NSNumericSearch] != NSOrderedAscending);
}

//"user":{"installId":"3243-432432-4324-432424-234", "deviceId":"54354353554343"},

@end
