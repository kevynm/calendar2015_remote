//
//  MunerisBaseAppDelegate.m
//  MunerisKit
//
//  Created by Jacky Yuk on 10/24/11.
//  Copyright (c) 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisBaseAppDelegate.h"
#import "MunerisApplication.h"
@implementation MunerisBaseAppDelegate



-(id) init;
{
  if (self = [super init]) {
    _mAppDel = [[MunerisApplicationDelegate alloc] init];
  }
  return self;
}

- (void)applicationDidFinishLaunching:(UIApplication *)application;{}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0);{
  return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application;{}
- (void)applicationWillResignActive:(UIApplication *)application;{}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;{
  return [_mAppDel application:application handleOpenURL:url];
}  // Will be deprecated at some point, please replace with application:openURL:sourceApplication:annotation:
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
{
  return [_mAppDel application:application openURL:url sourceApplication:sourceApplication annotation:annotation];

} // no equiv. notification. return NO if the application can't open for some reason

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application;{}      // try to clean up as much memory as possible. next step is to terminate app
- (void)applicationWillTerminate:(UIApplication *)application;{}
- (void)applicationSignificantTimeChange:(UIApplication *)application;{}        // midnight, carrier time update, daylight savings time change

- (void)application:(UIApplication *)application willChangeStatusBarOrientation:(UIInterfaceOrientation)newStatusBarOrientation duration:(NSTimeInterval)duration;{}
- (void)application:(UIApplication *)application didChangeStatusBarOrientation:(UIInterfaceOrientation)oldStatusBarOrientation;{}

- (void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame;{}   // in screen coordinates
- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame;{}

// one of these will be called after calling -registerForRemoteNotifications
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0);{}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0);{}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0);{}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification;{}

- (void)applicationDidEnterBackground:(UIApplication *)application;{}
- (void)applicationWillEnterForeground:(UIApplication *)application;{}

- (void)applicationProtectedDataWillBecomeUnavailable:(UIApplication *)application;{}
- (void)applicationProtectedDataDidBecomeAvailable:(UIApplication *)application;{}

-(void) dealloc;
{
  if (_mAppDel) {
    [_mAppDel release];
    _mAppDel = nil;
  }
  [super dealloc];
}

@end
