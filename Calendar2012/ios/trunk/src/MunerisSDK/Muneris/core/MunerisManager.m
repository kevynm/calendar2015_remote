//
//  MunerisManager.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/25/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisManager.h"
#import "MunerisStateManager.h"
#import "MunerisState.h"
#import "MunerisConstant.h"
#import "MunerisHelper.h"
#import "MunerisApiRequest.h"
#import "MunerisApi.h"
#import "MunerisApiResponseDelegatesBroadcaster.h"
#import "ASIHTTPRequest.h"
#import "MunerisMacro.h"
#import "MunerisPlugins.h"
#import "UIDevice+IdentifierAddition.h"
#import "MunerisConfig.h"
#if MUNERIS_DEBUG > 0
#import "SBJson.h"
#endif

@interface MunerisManager(private) 

-(NSDictionary*) getUser;

//-(void) moduleLoader:(NSDictionary*) dict;

-(void) loadPlugins;

-(void) loadPlugin:(NSString*)name;

-(void) reloadPlugin:(NSString*) name;

-(NSDictionary*) getPluginConfiguration:(NSString*) name;

-(NSArray*) getPluginsByProtocol:(Protocol*)protocol;

@end

@implementation MunerisManager


@synthesize stateManager = _sm, config = _config, apiInfo = _apiInfo, operationQueue = _taskQueue;

@dynamic udid, takeoverViewController;

static MunerisManager* _sharedManager = nil;

static MunerisMode runningMode = MunerisModeProduction;

static NSString* _apiURL = nil;

static NSString* _apiKey = nil;


+(void) setMode:(MunerisMode) mode;
{
  runningMode = mode;
}

+(MunerisMode) mode;
{
  return runningMode;
}

+(MunerisManager*) sharedManager;
{
	if (!_sharedManager) {
    _sharedManager = [[self alloc] init];
    //fill the headers cache
    [_sharedManager getHeader];
    //load plugins
    [_sharedManager loadPlugins];
    
	}
	return _sharedManager;
}

+(NSString*) apiURL;
{
  if(_apiURL){
    return _apiURL;
  }
  
  NSString* baseApi = MUNERIS_API_BASE;
  
  NSString* api;
  switch (runningMode) {
    case MunerisModeDebug:
    case MunerisModeTesting:
      api = [NSString stringWithFormat:baseApi,@"test"];
      break;
    case MunerisModeProduction:
      api = [NSString stringWithFormat:baseApi,@"animoca"];
      break;
    default:
      NSAssert(false, @"please give MunerisMode before starting the manager");
      break;
  }
  _apiURL = [api retain];
  return api;
}

+(NSString*) apiKey;
{
  if (_apiKey) {
    return _apiKey;
  }
  _apiKey = [MUNERIS_API_KEY retain];
  return _apiKey;
}

+(void) purgeSharedManager;
{
  [_sharedManager release];
  _sharedManager = nil;
}


-(id) init;
{
  
  NSString* mode = nil;
  switch (runningMode) {
    case MunerisModeDebug:
      mode = @"debug";
      break;
      
    case MunerisModeTesting:
      mode = @"testing";
      break;
      
    case MunerisModeProduction:
      mode = @"production";
      break;
    default:
      NSAssert(false, @"please give MunerisMode before starting the manager");
      break;
  }
  
  
  
  
  _taskQueue = [[NSOperationQueue alloc] init];
  
  [_taskQueue setMaxConcurrentOperationCount:2];
  
  _plugins = [[NSMutableDictionary dictionary] retain];
  
  _apiInfo = [[NSMutableDictionary dictionaryWithDictionary:[MunerisHelper getAllInfo]] retain];
  
  if ((self = [super init])) {
    
    _sm  = [[MunerisStateManager managerWithZone:[self zone]] retain];
    
    //ASIConfigs
    [ASIHTTPRequest setShouldUpdateNetworkActivityIndicator:NO];
    _config = nil;
    
    Class cls = NSClassFromString(@"MunerisConfig");
    if (cls) {
      id munerisConfig = [[[cls alloc] init] autorelease];
      if ([munerisConfig respondsToSelector:@selector(config)]) {
        NSDictionary* config = [munerisConfig performSelector:@selector(config)];
        if ([[config allKeys] count] >= 1) {
          _config = [config objectForKey:mode];
        }
      }
    }
    if (!_config) {
      NSString* path = [NSString stringWithFormat:kMunerisPlistFileFormat,MUNERIS_PLIST_PREFIX,mode];
      _config = [NSDictionary dictionaryWithContentsOfFile:[MunerisHelper fullPathFromRelativePath:path]];
    }
    
    if (!_config) {
#if MUNERIS_DEBUG > 0
      MLOG(@"Muneris Manager cannot load config file env %@", mode);
      
      NSAssert(false, @"Muneris Manager *** config not found *** " );
#endif
    }
    
#if MUNERIS_DEBUG > 0
    
    MLOG(@"Muneris DEBUG: ******* JSON *****: \n  %@ \n ***** end *****", _config);
    
#endif
    
    
    [_config retain];
    
  }
  return self;
}

#pragma mark CORE FEATURE
-(NSDictionary*) getUser;
{
  NSMutableDictionary* info = [NSMutableDictionary dictionary];
  
  if ([self udid]) {
    [info setValue:[self udid] forKey:@"deviceId"];
  }
  
  [info setValue:[[UIDevice currentDevice] macaddress] forKey:@"mac"];
  return info;
}

-(NSDictionary*) getHeader;
{
  if (![_apiInfo objectForKey:@"user"]) {
    [_apiInfo setValue:[self getUser] forKey:@"user"];
  }

  return _apiInfo;
}


-(NSString*) applicationName;
{
  return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
}

-(NSString*) udid;
{
  NSArray* plugins = [self getPluginsByNamespace:@"udid"];
  if (!plugins) {
    [self loadPlugin:@"udid"];
    plugins = [self getPluginsByNamespace:@"udid"];
  }
  MunerisUdidPlugin* plugin = (MunerisUdidPlugin*)[plugins lastObject];
  return [plugin getUDID];
}


-(NSMutableDictionary*) state;
{
  return _sm.state.info;
}

-(void) saveState;
{
  [_sm saveState];
}

-(void) setTakeoverViewController:(UIViewController *)takeoverViewController;
{
  if (_takeoverViewController == takeoverViewController) {
    return;
  }
  if (_takeoverViewController) {
    [_takeoverViewController release];
    _takeoverViewController = nil;
  }
  if (takeoverViewController) {
    _takeoverViewController = takeoverViewController;
  }
}



-(UIViewController*) takeoverViewController;
{
  return _takeoverViewController;
}



#pragma mark PLUGIN LOADER

-(void) loadPlugins;
{
  //Custom plugins
  NSDictionary* config;
  
  config = [self getEnvars];
  MLOG(@"Muneris: PLUGIN LODAER - Start \n %@" , config);
  if (!config) {
    config = _config;
  }
  
  if ([config objectForKey:@"muneris"]) {
    if ([[config objectForKey:@"muneris"] objectForKey:@"plugins"]) {
      for (NSString* pluginName in [[config objectForKey:@"muneris"] objectForKey:@"plugins"]) {
        [self loadPlugin:pluginName];
      }
      
      for (NSString* pluginName in [config allKeys]) {
        [self loadPlugin:pluginName];
      }
    }
  }
  MLOG(@"Muneris: PLUGIN LOADER finished, \n %@ ", _plugins);
  
}

-(void) reloadPlugin:(NSString*) name;
{
  if ([name isEqualToString:@"muneris"]) {
    return;
  }
  NSDictionary* dict = [self getPluginConfiguration:name];
  NSArray* pluginArray = [self getPluginsByNamespace:name];
  if ([pluginArray count] < 1) {
    [self loadPlugin:name];
  }
  for (id<MunerisPluginProtocol> plugin in pluginArray) {
    [plugin updateConfiguration:dict];
    MLOG(@"Muneris: PLUGIN RELOAD finished,  %@ \n %@ %@", name , plugin , dict);
  }
}

-(NSDictionary*) getPluginConfiguration:(NSString*) name;
{
  NSDictionary* config = nil;
  if (![name isEqualToString:@"envars"]) {
    NSDictionary* envars = [self getEnvars];
    config = [envars objectForKey:name];
    if (!config) {
      config = [[[envars objectForKey:@"muneris"] objectForKey:@"plugins"] objectForKey:name];
    }
  }
  
  if (!config) {
    config = [_config objectForKey:name];
    if (!config) {
      config = [[[_config objectForKey:@"muneris"] objectForKey:@"plugins"] objectForKey:name];
    }
  }
  return config;
}

-(void) loadPlugin:(NSString*)name;
{
  if ([name isEqualToString:@"muneris"]) {
    return;
  }
  if ([_plugins objectForKey:name]) {
    return;
  }
  
#if MUNERIS_DEBUG > 0  
  NSMutableArray* pluginNames = [_plugins objectForKey:@"pluginNames"];
  if (!pluginNames) {
    pluginNames = [NSMutableArray array];
    [_plugins setValue:pluginNames forKey:@"pluginNames"];
  }
#endif
  
  
//  NSDictionary* pluginInfo = [_config objectForKey:name];
//  
//  if (!pluginInfo) {
//    pluginInfo = [[[_config objectForKey:@"muneris"] objectForKey:@"plugins"] objectForKey:name];
//  }

  NSString* ucFirstName = [name stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[name substringToIndex:1] uppercaseString]];
  
  NSString* pluginClsName = [NSString stringWithFormat:kMunerisPluginClassFormat,ucFirstName];
  
  
  //ucfirst Char
  
  Class cls = NSClassFromString(pluginClsName);
  
  MLOG(@"Muneris: *** PLUGIN LOADER *** Load plugin - %@ with name %@", NSStringFromClass(cls), pluginClsName);
  
  NSDictionary* pluginInfo = [self getPluginConfiguration:name];
#if MUNERIS_DEBUG
  NSAssert([cls conformsToProtocol:@protocol(MunerisPluginProtocol)], @"Class should conform to protocol MunerusPluginProtocol.");
#endif
  
  
  //fuse for misconfiguration
  if (![cls conformsToProtocol:@protocol(MunerisPluginProtocol)]) {
    return;
  }
  
  NSString* protocolNS = NSStringFromProtocol(@protocol(MunerisPluginProtocol));
  
  if (![_plugins objectForKey:protocolNS]) {
    [_plugins setValue:[NSMutableArray array] forKey:protocolNS];
  }
  
  NSMutableArray* arr = [_plugins objectForKey:protocolNS];
  
  //getplugin instance
  
  
  id<MunerisPluginProtocol> plugin = [[[cls alloc] performSelector:@selector(initPluginWithDictionary:) withObject:pluginInfo] autorelease];
  
  [plugin addNamespace:name];
#if MUNERIS_DEBUG > 0    
  [pluginNames addObject:name];
#endif  
  [arr addObject:plugin];
  
  for (NSString* pluginNs in [plugin namespaces]) {
    if (![_plugins objectForKey:pluginNs]) {
      [_plugins setValue:[NSMutableArray array] forKey:pluginNs];
    }
    NSMutableArray* arr = [_plugins objectForKey:pluginNs];
    [arr addObject:plugin];
  }
  
  MLOG(@"Muneris: PLUGIN LODAER Registering plugin %@ ", plugin);
  
}


-(NSArray*) getPluginsByProtocol:(Protocol*)protocol;
{
  return [self getPluginsByNamespace:NSStringFromProtocol(protocol)];
}

-(NSArray*) getPluginsByNamespace:(NSString*)namespace;
{
  return [_plugins objectForKey:namespace];
}


#pragma mark API IMPL
-(void) apiCall:(NSString*)method payload:(NSDictionary*) payload delegate:(id<MunerisApiCallDelegate>) delegate;
{
  [MunerisApi apiMethod:method payload:payload delegate:delegate zone:[self zone]];
}


-(void) checkMessages:(NSArray*)type delegate:(id<MunerisInboxDelegate>) delegate;
{
  NSArray* plugins =  [self getPluginsByNamespace:@"inbox"];
  for (id<MunerisPluginProtocol> plugin in plugins) {
    if ([plugin conformsToProtocol:@protocol(MunerisInboxPluginProtocol)]) {
      id<MunerisInboxPluginProtocol> inboxPlugin = (id<MunerisInboxPluginProtocol>) plugin;
      [inboxPlugin readInbox:type withDelegate:delegate];
      return ;
    }
  }
}


-(void) markMessages:(MunerisMessages*) msgs;
{
  NSArray* plugins =  [self getPluginsByNamespace:@"inbox"];
  for (id<MunerisPluginProtocol> plugin in plugins) {
    if ([plugin conformsToProtocol:@protocol(MunerisInboxPluginProtocol)]) {
      id<MunerisInboxPluginProtocol> inboxPlugin = (id<MunerisInboxPluginProtocol>) plugin;
      [inboxPlugin markUnread:msgs];
      return ;
    }
  }
}


-(void) loadAdswithBannerSize:(MunerisBannerAdsSizes)size withZone:(NSString*) zone withDelegate:(id<MunerisBannerAdsDelegate>) delegate withUIViewController:(UIViewController*) viewController;
{
  for (MunerisMediationPlugin* plugin in [self getPluginsByNamespace:@"mediation"]) {
      [plugin loadAdswithBannerSize:size withZone:zone withDelegate:delegate withUIViewController:viewController];
  }
  
}

-(void) loadTakeover:(NSString*)zone withViewController:(UIViewController *)viewController delegate:(id<MunerisTakeoverDelegate>)delegate;
{
  if (!viewController) {
    viewController = self.takeoverViewController;
  }
  for (MunerisMediationPlugin* plugin in [self getPluginsByNamespace:@"mediation"]) {
    [plugin loadTakeover:zone withViewController:viewController delegate:delegate];
  }
}

-(BOOL) hasTakeover:(NSString*)zone;
{
  for (MunerisMediationPlugin* plugin in [self getPluginsByNamespace:@"mediation"]) {
   return [plugin canMediates:@"takeovers" withZone:zone];
  }
  return NO;
}

-(void) showTakeover:(NSString*)zone withViewController:(UIViewController*)viewController;
{
  [self loadTakeover:zone withViewController:viewController delegate:nil];
}

-(void) showOffers:(UIViewController*)viewController delegate:(id<MunerisOffersDelegate>) delegate;
{
  for (MunerisOffersPlugin* plugin in [self getPluginsByNamespace:@"offers"]) {
    [plugin showOffers:viewController delegate:delegate];
  }
}

-(BOOL) hasOffers;
{
  BOOL offersAvailable = NO;
  for (MunerisOffersPlugin* plugin in [self getPluginsByNamespace:@"offers"]) {
    offersAvailable = [plugin hasOffers];
  }
  return offersAvailable;
}

-(BOOL) hasBannerAds:(NSString *)zone;
{
  for (MunerisMediationPlugin* plugin in [self getPluginsByNamespace:@"mediation"]) {
    return [plugin canMediates:@"ads" withZone:zone];
  }
  return NO;
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  for (id<MunerisPluginProtocol> plugin in [self getPluginsByProtocol:@protocol(MunerisAnalyticsPluginProtocol)]) {
    if ([plugin conformsToProtocol:@protocol(MunerisAnalyticsPluginProtocol)]) {
      id<MunerisAnalyticsPluginProtocol>  anaPlugin = (id<MunerisAnalyticsPluginProtocol>) plugin;
      [anaPlugin logEvent:eventName withParameters:parameters];
    }
  }
}

- (void)logStartEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  for (id<MunerisPluginProtocol> plugin in [self getPluginsByProtocol:@protocol(MunerisAnalyticsPluginProtocol)]) {
    if ([plugin conformsToProtocol:@protocol(MunerisAnalyticsPluginProtocol)]) {
      id<MunerisAnalyticsPluginProtocol>  anaPlugin = (id<MunerisAnalyticsPluginProtocol>) plugin;
      [anaPlugin logStartEvent:eventName withParameters:parameters];
    }
  }
}

- (void)logEndEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
{
  for (id<MunerisPluginProtocol> plugin in [self getPluginsByProtocol:@protocol(MunerisAnalyticsPluginProtocol)]) {
    if ([plugin conformsToProtocol:@protocol(MunerisAnalyticsPluginProtocol)]) {
      id<MunerisAnalyticsPluginProtocol>  anaPlugin = (id<MunerisAnalyticsPluginProtocol>) plugin;
      [anaPlugin logEndEvent:eventName withParameters:parameters];
    }
  }
}



#pragma mark Envars

-(NSDictionary*) getEnvars;
{
  NSArray* plugins = [self getPluginsByNamespace:@"envars"];
  if ([plugins count] < 1) {
    [self loadPlugin:@"envars"];
    plugins = [self getPluginsByNamespace:@"envars"];
  }
  for (id<MunerisPluginProtocol> plugin in plugins) {
    if ([plugin isKindOfClass:[MunerisEnvarsPlugin class]]) {
      MunerisEnvarsPlugin* envarsPlugin = (MunerisEnvarsPlugin*) plugin;
      return [envarsPlugin getEnvars];
    }
  }
  return nil;
}

-(void) updateEnvars;
{
  for (id<MunerisPluginProtocol> plugin in [self getPluginsByNamespace:@"envars"]) {
    if ([plugin isKindOfClass:[MunerisEnvarsPlugin class]]) {
      MunerisEnvarsPlugin* envarsPlugin = (MunerisEnvarsPlugin*) plugin;
      [envarsPlugin updateEnvars];
    }
  }
}

-(void) checkAppVersion;
{
  for (id<MunerisPluginProtocol> plugin in [self getPluginsByNamespace:@"version"]) {
    if ([plugin isKindOfClass:[MunerisVersionPlugin class]]) {
      MunerisVersionPlugin* envarsPlugin = (MunerisVersionPlugin*) plugin;
      [envarsPlugin checkAppVersion];
    }
  }
}

#pragma mark UIApplicationDelegate
-(NSArray*) pluginForwardInvocation:(NSInvocation*) invocation;
{
  BOOL retVal;
  NSUInteger length = [[invocation methodSignature] methodReturnLength];

  NSMutableArray* arr = [NSMutableArray array];
  for (id<MunerisPluginProtocol> plugin in [self getPluginsByProtocol:@protocol(MunerisPluginProtocol)]) {
    if([plugin respondsToSelector:[invocation selector]])
    { 
      [invocation setTarget:plugin];
      [invocation invoke];
      if (length > 0) {
        [invocation getReturnValue:&retVal];
        [arr addObject:[NSNumber numberWithBool:retVal]];
      }
      
    }
  }
  return arr;
}

-(NSInvocation*) getInvocationWith:(Class) class selector:(SEL) selector arguments:(id) arguments,...;
{
  
  NSMethodSignature * sig = [class instanceMethodSignatureForSelector:selector];
  NSInvocation * invocation = [NSInvocation invocationWithMethodSignature:sig];
  
  int i = 2;
  if (arguments != nil) {
    [invocation setArgument:arguments atIndex:i];
    va_list args;
    va_start(args, arguments);
    id argument = nil;
    while((argument = va_arg(args,id))) {
      i++;
      [invocation setArgument:&argument atIndex:i];
    }
    va_end(args);
  }
  
  [invocation setSelector:selector];
  return invocation;
}



-(void) dealloc;
{
  
  if (_takeoverViewController) {
    [_takeoverViewController release];
    _takeoverViewController = nil;
  }
  if (_taskQueue) {
    [_taskQueue release];
    _taskQueue = nil;
  }
  if (_plugins) {
    [_plugins release];
    _plugins = nil;
  }
  if (_apiInfo) {
    [_apiInfo release];
    _apiInfo = nil;
  }
  
  if (_apiKey) {
    [_apiKey release];
    _apiKey = nil;
  }
  
  if (_apiURL) {
    [_apiURL release];
    _apiKey = nil;
  }
  
  if (_config) {
    [_config release];
    _config = nil;
  }
  
  if (_sm) {
    [_sm release];
    _sm = nil;
  }
  [super dealloc];
}




@end
