//
//  MunerisStateManager.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/25/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MunerisState;

@interface MunerisStateManager : NSObject {
  
  MunerisState* _state;
  
  NSString* _statePath;
  
  NSString* _appendPath;
 
}



@property(readonly) MunerisState* state;

+(id) managerWithZone: (NSZone*) zone;

+(id) manager;

-(NSString*) getBasePath;

-(void) saveState;

-(void) loadStateFromFile:(NSString*) filename;

-(void) saveStateToFile:(NSString*) filename;



@end
