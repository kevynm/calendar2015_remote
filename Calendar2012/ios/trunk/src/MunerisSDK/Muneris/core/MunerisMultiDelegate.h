//
//  MunerisMultiDelegate.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/31/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MunerisMultiDelegate : NSObject {
  
  NSMutableArray* _delegates;
  
}

+(id) delegate;

+(id) delegateWithZone:(NSZone*) zone;

-(void) addDelegates:(id) delegate;

@end
