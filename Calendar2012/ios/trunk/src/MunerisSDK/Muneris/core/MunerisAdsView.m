//
//  MunerisAdsView.m
//  MunerisKit
//
//  Created by Casper on 14/09/2011.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisAdsView.h"
#import "MunerisBannerAdsPluginProtocol.h"
#import "Muneris.h"

@implementation MunerisAdsView
@synthesize delegate = _delegate;

-(id) initWithSize:(MunerisBannerAdsSizes) size;
{
  if ((self = [self init])) {
    switch (size) {
      case MunerisBannerAdsSizes320x50:
        [self setFrame:CGRectMake(0, 0, 320, 50)];
        break;
        case MunerisBannerAdsSizes728x90:
            [self setFrame:CGRectMake(0, 0, 728, 90)];
            break;
      default:
        break;
    }
  }
  return self;
}

-(id) init;
{
    if ((self = [super init])) {
        // Initialization code
    }
    return self;
}

- (void) refreshAds;
{
    if (_delegate) {
        if ([_delegate respondsToSelector:@selector(refreshAds) ]) {
            [_delegate refreshAds];
        }
    }
}

-(void) reloadAdsWithViewController: (UIViewController*) viewController;
{
  if (_delegate) {
    if ([_delegate respondsToSelector:@selector(reloadAdsWithViewController:)]) {
      [_delegate reloadAdsWithViewController:viewController];
    }
  }
}


- (void)dealloc
{
  MLOG(@"* DEALLOC: Muneris Ads View");
    if (_delegate) {
        [_delegate release];
        _delegate = nil;
    }

    [super dealloc];
}

@end
