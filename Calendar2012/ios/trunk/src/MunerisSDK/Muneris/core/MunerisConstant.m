//
//  MunerisConstant.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/24/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisConstant.h"


NSString* const kMuenrisFolder = @".muneris";

NSString* const kMuenrisStateFile = @"muneris.dat";

NSString* const kMunerisPlistFileFormat = @"%@muneris-%@.plist";

NSString* const kMunerisSDKVersion = @"2.0.2";

NSString* const kMunerisSDKAgent = @"muneris";

NSString* const kMunerisSDKPlaform = @"ios";

NSString* const kMunerisInstallIdApiMethod = @"generateId";

NSString* const kMunerisPluginClassFormat = @"Muneris%@Plugin";

NSString* const MUNERIS_ENVARS_UPDATE_SUCCESS_NOTIFICATION = @"MUNERIS_ENVARS_UPDATE_SUCCESS_NOTIFICATION";

NSStringEncoding const kMunerisApiEncoding = NSUTF8StringEncoding;





