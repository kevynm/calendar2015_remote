//
//  MunerisApplication.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisApplication.h"
#import "MunerisMacro.h"
#import "MunerisManager.h"

@implementation MunerisApplication

-(id) init;
{
  if ((self = [super init])) {
    MLOG(@"Muneris: APPLICATION STARTED");
    [MunerisManager setMode:MUNERIS_MODE];
    [MunerisManager sharedManager];
    _del = [[MunerisApplicationDelegate alloc] init];
    [super setDelegate:_del];
  }
  return self;
}

-(void) setDelegate:(id<UIApplicationDelegate>)delegate;
{
  if (_del.delegate) {
    [_del.delegate release];
    _del.delegate = nil;
  }
  [_del setDelegate:delegate];
}

-(id<UIApplicationDelegate>) delegate;
{
  return _del.delegate;
}

-(void) dealloc;
{
  if (_del) {
    [_del release];
    _del = nil;
  }
  [super dealloc];
}

@end