//
//  MunerisStateManager.m
//  MunerisKit
//
//  Created by Jacky Yuk on 8/25/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisStateManager.h"
#import "MunerisConstant.h"
#import "MunerisState.h"

@interface MunerisStateManager(private)


-(NSString*) getFilePath:(NSString*) file;

-(BOOL) saveStateToFile:(NSString*) filename;

-(BOOL) getFileExists:(NSString*) file;



@end



@implementation MunerisStateManager



@synthesize state = _state;



+(id) manager;
{
  id ret = [[self alloc] init];
  return [ret autorelease];
}

+(id) managerWithZone:(NSZone *)zone;
{
  id ret = [[self allocWithZone:zone] init];
  return [ret autorelease];
}

-(id) init;
{
  [self loadStateFromFile:kMuenrisStateFile];
  if ((self = [super init])) {
    
  }
  return self;
}


-(void) loadStateFromFile:(NSString*) filename;
{
  if (_state) {
    [_state release];
    _state = nil;
  }
  
  if (_statePath) {
    [_statePath release];
    _statePath = nil;
  }
  
  NSString* path = [self getFilePath:filename];
  if ([self getFileExists:path]) {
    _state = [[MunerisState stateWithFile:path] retain];
  }else{
    _state = [[MunerisState state] retain];
  
  }
}

-(void) saveStateToFile:(NSString*) filename;
{
  @synchronized(self){
    NSString* path = [self getFilePath:filename];
    [_state saveToFile:path];
  }
  
}

-(void) saveState;
{
  [self saveStateToFile:kMuenrisStateFile];
}


-(NSString*) getFilePath:(NSString*) file;
{
  if (_statePath) {
    return _statePath;
  }

  NSString* documentsDirectory = [self getBasePath];

  NSString* filepath = [documentsDirectory stringByAppendingPathComponent:file];

  NSFileManager* fm = [NSFileManager defaultManager];
  BOOL isDir = YES;
  if(![fm fileExistsAtPath:documentsDirectory isDirectory:&isDir])
    if(![fm createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil])
      NSLog(@"Error: Create folder failed");
  
  _statePath = [filepath copy];
  return filepath;
}

-(NSString*) getBasePath;
{
  NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
  NSString* documentsDirectory;
  if (_appendPath) {
    documentsDirectory = [[[paths objectAtIndex:0] stringByAppendingPathComponent:kMuenrisFolder] stringByAppendingPathComponent:_appendPath];
  }else{
    documentsDirectory = [[paths objectAtIndex:0] stringByAppendingPathComponent:kMuenrisFolder];
  }
  return documentsDirectory;
}


-(BOOL) getFileExists:(NSString*) file;
{
  return [[NSFileManager defaultManager] fileExistsAtPath:file];
}


-(void) dealloc;
{
  if (_appendPath) {
    [_appendPath release];
    _appendPath = nil;
  }
  if (_statePath) {
    [_statePath release];
    _statePath = nil;
  }
  if (_state) {
    [_state release];
    _state = nil;
  }
  [super dealloc];
}

@end
