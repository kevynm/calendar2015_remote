//
//  MunerisHelper.h
//  MunerisKit
//
//  Created by Jacky Yuk on 8/25/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MunerisConstant.h"
#import "Reachability.h"

@interface MunerisHelper : NSObject {
    
}

+(NSString*) fullPathFromRelativePath:(NSString*) relPath;

+(NSDictionary*) getAgent;

+(NSDictionary*) getDevice;

+(NSDictionary*) getLocale;

+(NSDictionary*) getApp;

+(NSDictionary*) getAllInfo;

+(NSString*) toJsonWithObject:(id) obj; 

+(BOOL) isIOSVersionGreaterThan:(NSString*) version;

+(BOOL) isInternetReachable;

@end
