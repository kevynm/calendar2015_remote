//
//  GameState.m
//  jump
//
//  Created by Jacky Yuk on 3/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MunerisState.h"
#import "MunerisMacro.h"

@interface MunerisState(private)

-(id) initWithDefault;

@end


@implementation MunerisState


@synthesize udid = _udid, info = _info, dirty = _dirty ;


-(id) init;
{
	if((self = [super init])) {
	}
	return self;
}


-(id) initWithDefault;
{
  if ((self = [self init])) {
    _udid = nil;
  }
  return self;
}


-(id) initWithCoder:(NSCoder*) coder;
{
  self = [super init];
  if ( ! self) return nil;
  _udid = [[coder decodeObjectForKey:@"udid"] retain];
  if (![coder decodeObjectForKey:@"info"]) {
    _info = [[NSMutableDictionary dictionary] retain];
  }else{
    _info = [[coder decodeObjectForKey:@"info"] retain];
  }
	return self;
}



+(id) stateWithFile:(NSString*) file;
{
  id ret = [[NSKeyedUnarchiver unarchiveObjectWithFile:file] retain];
  return [ret autorelease];
}

+(id) state;
{
  id ret = [[self alloc] initWithDefault];
  
  return [ret autorelease];
}


#pragma mark -
#pragma mark NSCoding Protocol Methods



-(void) saveToFile:(NSString*) file;
{
  [NSKeyedArchiver archiveRootObject:self toFile:file];
}

-(void) encodeWithCoder:(NSCoder*) coder;
{
  [coder encodeObject:_udid forKey:@"udid"];
  [coder encodeObject:_info forKey:@"info"];
}



-(void) dealloc;
{
  if (_info) {
    [_info release];
    _info = nil;
  }
  if (_udid) {
    [_udid release];
    _udid = nil;
  }

  [super dealloc];
 
}

@end