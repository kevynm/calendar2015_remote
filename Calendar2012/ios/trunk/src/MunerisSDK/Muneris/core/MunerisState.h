//
//  GameState.h
//  jump
//
//  Created by Jacky Yuk on 3/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MunerisState : NSObject <NSCoding>
{
	NSMutableDictionary* _info;

  NSString* _udid;
  
  BOOL _dirty;
}

@property (assign,nonatomic) NSMutableDictionary* info;

@property (copy, nonatomic) NSString* udid;

@property (assign) BOOL dirty;


+(id) stateWithFile:(NSString*) file;

+(id) state;

-(void) saveToFile:(NSString*) file;


@end