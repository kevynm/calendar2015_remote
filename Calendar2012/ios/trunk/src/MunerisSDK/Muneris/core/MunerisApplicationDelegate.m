//
//  MunerisApplicationDelegate.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/7/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisApplicationDelegate.h"
#import "MunerisManager.h"
#import "MunerisMacro.h"
@interface MunerisApplicationDelegate(private)

-(BOOL) invoke:(NSInvocation*) invocation;

@end

@implementation MunerisApplicationDelegate

@synthesize delegate = _delegate;

-(Class) getClass;
{
  if ([_delegate isMemberOfClass:[self class]]) {
    return [_delegate class];
  }
  return [self class];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,launchOptions,nil];
  BOOL retVal = [self invoke:inv];
  if (![_delegate respondsToSelector:@selector(application:didFinishLaunchingWithOptions:)]){
    if ([_delegate respondsToSelector:@selector(applicationDidFinishLaunching:)]) {
      [_delegate applicationDidFinishLaunching:application];
    }
  }
  return retVal;
}

- (void) applicationWillResignActive:(UIApplication *)application
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}

- (void) applicationDidEnterBackground:(UIApplication *)application
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}

- (void) applicationWillEnterForeground:(UIApplication *)application
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}

- (void) applicationDidBecomeActive:(UIApplication *)application
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}

- (void) applicationWillTerminate:(UIApplication *)application
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}

- (void)applicationDidFinishLaunching:(UIApplication *)application;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}


-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,url,nil];
  return [self invoke:inv];
//  return YES;
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,url,sourceApplication,annotation, nil];
  return [self invoke:inv];
//  return YES;
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}

- (void)applicationSignificantTimeChange:(UIApplication *)application;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}

- (void)application:(UIApplication *)application willChangeStatusBarOrientation:(UIInterfaceOrientation)newStatusBarOrientation duration:(NSTimeInterval)duration;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,newStatusBarOrientation,duration,nil];
  [self invoke:inv];
}

- (void)application:(UIApplication *)application didChangeStatusBarOrientation:(UIInterfaceOrientation)oldStatusBarOrientation;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,oldStatusBarOrientation,nil];
  [self invoke:inv];
}

- (void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,newStatusBarFrame,nil];
  [self invoke:inv];
}


- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,oldStatusBarFrame,nil];
  [self invoke:inv];
}

//// one of these will be called after calling -registerForRemoteNotifications
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,deviceToken,nil];
  [self invoke:inv];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,error,nil];
  [self invoke:inv];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,userInfo,nil];
  [self invoke:inv];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,notification,nil];
  [self invoke:inv];
}

- (void)applicationProtectedDataWillBecomeUnavailable:(UIApplication *)application;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}
- (void)applicationProtectedDataDidBecomeAvailable:(UIApplication *)application;
{
  NSInvocation* inv = [[MunerisManager sharedManager] getInvocationWith:[self getClass] selector:_cmd arguments:application,nil];
  [self invoke:inv];
}


-(BOOL) invoke:(NSInvocation*) invocation;
{
  MLOG(@"Muneris APPDELEGATE : invoking %@",NSStringFromSelector([invocation selector]));
  BOOL value = NO;
  NSArray* pluginRetVal = [[MunerisManager sharedManager] pluginForwardInvocation:invocation];
  NSMutableArray* arr = [NSMutableArray arrayWithArray:pluginRetVal];
  
  if (_delegate) {
    if ([_delegate respondsToSelector:[invocation selector]]) {
      [invocation invokeWithTarget:_delegate];
      NSUInteger length = [[invocation methodSignature] methodReturnLength];
      if (length > 0) {
        [invocation getReturnValue:&value];
        [arr addObject:[NSNumber numberWithBool:value]];
      }
    }
  }
  
  //  NSLog(@"%@",arr);
  for (NSNumber* num in arr ){
    if (![num boolValue]) {
      return NO;
    }
  }
  return YES;
}

-(void) dealloc;
{
  if (_delegate) {
    [_delegate release];
    _delegate = nil;
  }
  [super dealloc];
}

@end
