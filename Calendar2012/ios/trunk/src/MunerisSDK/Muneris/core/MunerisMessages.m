//
//  MunerisMessages.m
//  MunerisKit
//
//  Created by Jacky Yuk on 9/19/11.
//  Copyright 2011 Outblaze Limited. All rights reserved.
//

#import "MunerisMessages.h"
#import "MunerisMacro.h"
@interface MunerisMessages(private)

-(id) initWithArray:(NSArray*) array;

@end


@implementation MunerisMessages


@dynamic readMessage;

@synthesize unreadMessage = _unread;

@synthesize messages = _messages;


+(id) messages:(NSArray*) array;
{
  id ret = [[[MunerisMessages alloc] initWithArray:array] autorelease];
  return ret;
}

-(id) initWithArray:(NSArray*) array;
{
  if ((self = [self init])) {
    _messages = [array retain];
  }
  return self;
}

-(id) init;
{
  if ((self = [super init])) {
    _unread = [[NSMutableArray alloc] init];
  }
  return self;
}

-(void) markUnread:(NSDictionary*) dictionary;
{
  if (dictionary) {
    if ([_messages containsObject:dictionary] && ![_unread containsObject:dictionary]) {
      [_unread addObject:dictionary];
    }
  }
}

-(void) markUnreadWithIndex:(int) index;
{
  if (index >= 0 && index < [_messages count]) {
    [self markUnread:[_messages objectAtIndex:index]];
  }
}

-(NSArray*) readMessage;
{
  
  if ([_unread count] <= 0) {
    return _messages;
  }
  NSMutableArray* read = [[NSMutableArray allocWithZone:[self zone]] init];
  NSMutableArray* unreadMsg = [NSMutableArray arrayWithArray:_unread];
  
  for (NSDictionary* m in _messages) {
    //check if it exits in unread container;
    if (![unreadMsg containsObject:m]) {
      [read addObject:m];
    }
  }
  return read;
}

+(NSDictionary*) createCreditMessageWithPlugin:(NSString*) pluginName credits:(int) credits reason:(NSString*)reason;
{
  NSString *keyArray[2];
  keyArray[0] = @"credits";
  keyArray[1] = @"reason";
  
  id valueArray[2];
  valueArray[0] = [NSNumber numberWithInt:credits];
  valueArray[1] = reason;
  
  NSMutableDictionary* msg = [NSMutableDictionary dictionary];
  NSDictionary* plugin = [NSDictionary dictionaryWithObject:pluginName forKey:@"pluginName"];
  NSDictionary* data = [NSDictionary dictionaryWithObjects:valueArray forKeys:keyArray count:2];
  [msg setValue:plugin forKey:@"plugin"];
  [msg setValue:pluginName forKey:@"subj"];
  [msg setValue:@"c" forKey:@"ty"];
  [msg setValue:data forKey:@"body"];
  
  return msg;
}


-(void) dealloc;
{
  if (_messages) {
    [_messages release];
    _messages = nil;
  }
  
  if (_unread) {
    [_unread release];
    _unread = nil;
  }
  
  [super dealloc];
}

@end
