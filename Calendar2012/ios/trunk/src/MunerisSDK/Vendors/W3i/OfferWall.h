//
//  OfferWall.h
//  OfferWall
//
//  Copyright 2010 W3i. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

typedef enum Offer_Wall_Type
{
		Offer_Wall_Type_Offers = 0,
		Offer_Wall_Type_History
}
Offer_Wall_Type;

//Error Codes
typedef enum Offer_Wall_Error
{
	OfferWallErrorUnknown=0,
	OfferWallErrorNotInitialized,
	OfferWallErrorInvalidAppId,
	OfferWallErrorRateTheAppNotNow,
	OfferWallErrorNoBalance,
	OfferWallErrorNoNetWork,
}
Offer_Wall_Error;


#pragma mark -- Keys used in the dictionary to pass UserInfo


/// App/Game Name and necessary IDs for integration
#define AOW_NAME					"Name"
#define AOW_APPSTORE_APPID          "AppStoreApplicationId"
#define AOW_APPID					"ApplicationId"
#define AOW_ADVERTISER_APPID		"AdvertiserApplicationId"

#define AOW_USERID					"UserId"

/// Paths for background images used in the OfferWall UI
#define AOW_BGIMAGE_PORTRAIT		"BackgroundImage_Portrait"
#define AOW_BGIMAGE_LANDSCAPE		"BackgroundImage_Landscape"

/// Colors for various visual items used in OfferWall UI
#define AOW_COLOR_TITLE				"TitleColor"
#define AOW_COLOR_OFFERNAME			"OfferNameColor"
#define AOW_BGIMAGE_OFFERBG			"OfferBackgroundImage"

#define AOW_COLOR_ACTIVE_BTNTEXT	"ActiveButtonTextColor"
#define AOW_COLOR_INACTIVE_BTNTEXT	"InActiveButtonTextColor"

/// Orientations supported by the app/game
#define AOW_SUPPORTED_ORIENTATIONS	"SupportedOrientations"

//Domain Name
#define AOW_ERROR_DOMAIN					"OfferWall"

// the delegate protocol to be implemented the game/app developer
@protocol OfferWallDelegate<NSObject>

@required

// delegate methods which inform the developer about the OfferWall initialization status
- (void)offerWallDidFailToInitialize;
- (void)offerWallDidInitialize;

// delegate methods which inform the developer about the OfferWall display status

- (void)offerWallDidDisplay;
- (void)offerWallDidFailToDisplay;

- (void)offerWallDidDismiss;

// delegate method which inform the developer when failed to link the appstore because of applicationID
- (void)redirectingUserToAppStoreDidFailWithError:(NSError*)error;


// delegate method to inform the developer that user is about to be redirected to iTunes.
- (void)offerWallWillRedirectUserToAppStore;

// delegate method to fetch the offer list
-(void) offerWallDidReceiveOfferList:(NSMutableArray*)offerList;

// delegate methods which get called when offerwall failed to respond
- (void)offerWallDidFailToGetFeaturedOffer;

// delegate methods which get called when featured offer available
- (void)offerWallFeaturedOfferDidBecomeAvailable;

// delegate methods which get called when Game Currency Balance related functions are called
-(void)offerWallDidRedeemGameCurrencies:(NSArray*)currencies withReceipt:(NSString*)receiptID  withError:(NSError*)error;

@end


// the OfferWall class whose object should be created
// and used by the developer to interact with  OfferWall system
@interface OfferWall : NSObject
{
	NSDictionary*			_userInfo;
	id<OfferWallDelegate>	_delegate;
	BOOL isFeaturedOfferAvailable;
}

@property(nonatomic,assign)	BOOL isFeaturedOfferAvailable;
@property(nonatomic,retain)	id<OfferWallDelegate> _delegate;

// init and un-init methods
- (id)initWithUserInfo:(NSDictionary*)userInfo andDelegate:(id<OfferWallDelegate>)delegate;
- (void)close;

// methods to show OfferWall and featured offers
- (void)showFeaturedOffer;

- (void)showOfferWall:(Offer_Wall_Type)type;

//method to show OfferWall for iPad 
- (void)showOfferWall:(Offer_Wall_Type)type fromView:(UIView*)view;

//method to download the QualifiedOfferList
-(void) fetchOfferListWithStartIndex:(int)startIndex;

// method to dismiss OfferWall
- (void)dismissOfferWall;

// method to get all supported currency details
- (NSArray*) getCurrencyList;

// set currency Name
-(void) setCurrency:(NSString*) currency;

// Method to save the offer download on server.
-(void) downloadOffer:(NSDictionary*) offerDetails;

// methods to retrieve and redeem game currency balances
- (void)redeemBalance;

- (void)setDelegate:(id<OfferWallDelegate>)delegate;

//To rate the application
- (void)rateTheAppWithMessage:(NSString*)message onlyLatestVersion:(BOOL)onlyLatestVersion;

@end
