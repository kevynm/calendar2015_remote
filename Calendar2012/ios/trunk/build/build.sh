#!/bin/sh
function failed()
{
    echo "Failed: $@" >&2
    exit 1
}

set -ex

export OUTPUT=$WORKSPACE/output
rm -rf $OUTPUT
mkdir -p $OUTPUT
PROFILE_HOME=~/Library/MobileDevice/Provisioning\ Profiles/
KEYCHAIN=~/Library/Keychains/login.keychain

. "$WORKSPACE/build/build.config"

[ -d "$PROFILE_HOME" ] || mkdir -p "$PROFILE_HOME"
security list-keychains -s $KEYCHAIN
security unlock-keychain -p $PASSWORD $KEYCHAIN
security find-cert -a -c "iPhone Distribution: Dream Cortex" -Z | grep ^SHA-1 | cut -d: -f2 | xargs -n1 security delete-cert -Z
security import "$WORKSPACE/build/distribution_identity.cer" -k $KEYCHAIN -P $PASSWORD -A
security find-cert -a -c "$IdentityRelease" -Z | grep ^SHA-1 | cut -d: -f2 | xargs -n1 security delete-cert -Z
security import "$WORKSPACE/build/Animoca_Outblaze.p12" -k $KEYCHAIN -P ii1237 -A

cd $WORKSPACE/src
agvtool new-version -all $BUILD_NUMBER

for sdk in $SDKS; do
    for config in $CONFIGURATIONS; do
        provision=$(eval echo \$`echo Provision$config`)
        identity="$(eval echo \$`echo Identity$config`)"
        cert="$WORKSPACE/build/$provision.mobileprovision"
        archive="$OUTPUT/$JOB_NAME-$BUILD_NUMBER-$config.zip"
        manifest="$OUTPUT/manifest.plist"
        [ -f "$cert" ] && cp "$cert" "$PROFILE_HOME"
        cd $WORKSPACE/src

        xcodebuild -configuration $config -sdk $sdk CODE_SIGN_IDENTITY="$identity" PROVISIONING_PROFILE=$provision clean
        xcodebuild -configuration $config -sdk $sdk CODE_SIGN_IDENTITY="$identity" PROVISIONING_PROFILE=$provision || failed build
        cd build/$config-iphoneos
        for app in *.app; do
            xcrun -sdk $sdk PackageApplication $app -o "$OUTPUT/$JOB_NAME-$BUILD_NUMBER-$config-`basename $app .app`.ipa" --sign "$identity" --embed $cert
        done

        bundle_id=`plutil -convert xml1 -o - *.app/*Info.plist | grep -A1 CFBundleIdentifier | tail -1 | sed -e 's|</.*||; s|.*>||'`
        bundle_version=`plutil -convert xml1 -o -  *.app/*Info.plist | grep -A1 CFBundleVersion | tail -1 | sed -e 's|</.*||; s|.*>||'`
        marketing_version=`plutil -convert xml1 -o -  *.app/*Info.plist | grep -A1 CFBundleShortVersionString | tail -1 | sed -e 's|</.*||; s|.*>||'`
        if [ $config == "Release" ]; then
            zip -r -T -y "$archive" *.app *.dSYM || failed zip
            #zip -j -T "$archive" "$WORKSPACE/src/icon.png" || failed icon

            cd $OUTPUT
            cat > "$manifest" <<__END_MANIFEST_HEAD
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
   <key>items</key>
   <array>
       <dict>
           <key>assets</key>
           <array>
               <dict>
__END_MANIFEST_HEAD

            for ipa in *.ipa; do
                cat >> "$manifest" <<__END_MANIFEST_ARRAY
                   <key>kind</key>
                   <string>software-package</string>
                   <key>url</key>
                   <string>http://hudson.outblaze.net/ipa/job/$JOB_NAME/builds/$BUILD_NUMBER/archive/output/$ipa</string>
__END_MANIFEST_ARRAY
           done

           cat >> "$manifest" <<__END_MANIFEST_FOOTER
               </dict>
           </array>
           <key>metadata</key>
           <dict>
               <key>bundle-identifier</key>
               <string>$bundle_id</string>
               <key>bundle-version</key>
               <string>$bundle_version</string>
               <key>kind</key>
               <string>software</string>
               <key>title</key>
               <string>$JOB_NAME $marketing_version (build $BUILD_NUMBER)</string>
           </dict>
       </dict>
   </array>
</dict>
</plist>
__END_MANIFEST_FOOTER

        else
            zip -r -T -y "$archive" *.app || failed zip
            zip -j -T "$archive" "$cert" || failed cert
        fi
    done
done
